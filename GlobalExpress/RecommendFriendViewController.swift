//
//  RecommendFriendViewController.swift
//  GlobalExpress
//
//  Created by Ann on 2/17/16.
//  Copyright © 2016 webcash. All rights reserved.
//

import UIKit
import Social
import MessageUI


class RecommendFriendViewController: UIViewController,MFMessageComposeViewControllerDelegate {
    
    
    @IBOutlet var referFriendTitleLabel: UILabel!
    @IBOutlet var referFriendSubtitleLabel: UILabel!
    @IBOutlet var describeLabel: UILabel!
    
    
    @IBOutlet var shareFacebookButton: UIButton!
    @IBOutlet var smsButton: UIButton!
    @IBOutlet var copyURLButton: UIButton!
    
    var accessToken: FBSDKAccessToken!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.localize()
        
        
        let leftNavigationImage = UIImage(named: "back_btn.png")
        let leftNavigationButton = UIButton(frame: CGRectMake(0,0,(leftNavigationImage?.size.width)! / 2 , (leftNavigationImage?.size.height)! / 2))
        leftNavigationButton.setBackgroundImage(leftNavigationImage, forState: .Normal)
        leftNavigationButton.addTarget(self, action: "leftNavigationClicked", forControlEvents: .TouchUpInside)
        
        let leftBarButtonItem = UIBarButtonItem(customView: leftNavigationButton)
        self.navigationItem.leftBarButtonItem = leftBarButtonItem
        
        
        
        
        if (FBSDKAccessToken.currentAccessToken() != nil) { //available
            print("view did load\(FBSDKAccessToken.currentAccessToken())")
            accessToken = FBSDKAccessToken.currentAccessToken()
        }
        
        
    }
    
    override func localize() {
        self.navigationItem.title = Language.localizedStr("S25_01")
        self.referFriendTitleLabel.text = Language.localizedStr("S25_02")
        self.referFriendSubtitleLabel.text = Language.localizedStr("S25_03")
        self.describeLabel.text = Language.localizedStr("S25_07")
        self.shareFacebookButton.setTitle(Language.localizedStr("S25_04"), forState: .Normal)
        self.smsButton.setTitle(Language.localizedStr("S25_05"), forState: .Normal)
        self.copyURLButton.setTitle(Language.localizedStr("S25_06"), forState: .Normal)
    }
    
    
    
    @IBAction func sharedButtonClicked(sender: UIButton) {
        let transcationClass = TransactionClass()
        sender.enabled = false
        let group = "0\((sender).tag)"
        
        print("group \(group)")
        var responseData: [String:String]?
        
        print("ttttt \(responseData)")
        
        
        transcationClass.sendAPI(APICode.RECOMMEND_C001, argument: ["RCMD_GB" : group], success: { (success) -> Void in
            
            if let data = success["RSLT_CD"] where data as! String == "0000" {
                guard let rawData = success["RESP_DATA"] as? [String:String] else{
                    print("pasre data error")
                    return
                }
                
                print("share button")
                responseData = rawData
                print("--------------> \(responseData)")
                
                if let data = responseData {
                    
                    switch sender.tag {
                    case 1: //facebook
                        self.sharedFacebook(datatoShare: data)
                    case 2: //sms
                        self.sendSMS(dataToShare: data)
                    case 3: //copy URL
                        self.copyURL(dataToShare: data)
                    default:
                        print("N/A")
                    }
                }
                sender.enabled = true
            }else{
                self.showEventAlert(success["RSLT_MSG"] as! String, hasCancelButton: false, delegate: self, tag: 7655, confirmBlock: nil, cancelBlock: nil)
            }
            
            
            
        })
    }
    
    //MARK: - private methods
    
    private func sharedFacebook(datatoShare data:[String:String]) {
//        let fbloginManager = FBSDKLoginManager()
//        print("facebok share")
//        let message = self.formatMessage(data)
//                print("facebok share \(message) \(FBSDKAccessToken.currentAccessToken())")
//        if let token = accessToken where token.hasGranted("publish_actions") {
//            print("grant permission")
//            self.publishMessage(message)
//        }else{
//            print("no grant ")
//            let fbLogInManger = FBSDKLoginManager()
//            fbLogInManger.logInWithPublishPermissions(["publish_actions"], fromViewController: self, handler: { (result, error) -> Void in
//                if error != nil {
//                    print("error \(error)")
//                }else if result.isCancelled {
//                    print("cancelled")
//                }else{
//                    print("result loggin")
//                    self.publishMessage(message)
//                }
//            })
//        }
        
            let fbLogInManger = FBSDKLoginManager()
            fbLogInManger.logInWithReadPermissions(["public_profile"], fromViewController: self, handler: { (result,error) -> Void in
                if error != nil {
                    print("error")
                }else if result.isCancelled {
                    print("cancelled")
                }else{
                    print("i'm logined")
//                    let messsage = self.formatMessage(data)
//                    fbLogInManger.logInWithPublishPermissions(["publish_actions"], fromViewController: self, handler: { (result, error) -> Void in
//                        if result.isCancelled {
//                            print("canacle")
//                        }else if error != nil {
//                            print("error")
//                        }else{
//                            self.publishMessage(messsage)
//                        }
//                    })

                }
            })
//        }
        
        
//        fbloginManager.logInWithReadPermissions(["public_profile", "publish_actions"], fromViewController: self, handler: { (result,error) -> Void in
//            if error != nil {
//                print("error")
//            }else if result.isCancelled {
//                print("cancelled")
//            }else{
//                print("logged in......")
//////                let fbSharedContent = FBSDKShareLinkContent()
//////                fbSharedContent.contentURL = NSURL(string: data["ESTB_URL"]!)
//////                fbSharedContent.imageURL = NSURL(string: data["RCMD_IMG"]!)
//////                fbSharedContent.contentDescription = data["RCMD_TXT"]
//////                
////////                FBSDKShareDialog.showFromViewController(self, withContent: fbSharedContent, delegate: nil)
////////                FBSDKMessageDialog.showWithContent(fbSharedContent, delegate: nil)
//////                
//////                let dialog = FBSDKShareDialog()
//////                dialog.mode = FBSDKShareDialogMode.FeedBrowser
//////                dialog.shareContent = fbSharedContent
//////                dialog.fromViewController = self
////////                if !dialog.canShow() {
////////                    dialog.mode = .FeedBrowser
////////                }
//////                dialog.show()
////                
//////                let fbAppInviteContent = FBSDKAppInviteContent()
//////                fbAppInviteContent.appLinkURL = NSURL(string: data["ESTB_URL"]!)
//////                
//////                
////////              FBSDKAppInviteDialog.showWithContent(fbAppInviteContent, delegate: nil)
//////                FBSDKAppInviteDialog.showFromViewController(self, withContent: fbAppInviteContent, delegate: nil)
////                
//////                let message = self.formatMessage(data)
//////                print("ccccc \(message)")
//////                let paramaters = ["message" :  message]
//////                let request = FBSDKGraphRequest(graphPath: "/me/feed", parameters: paramaters, HTTPMethod:"POST")
//////                request
//////                request.startWithCompletionHandler({ (connect,result, error) -> Void in
//////                    print("result \(result)")
//////                    print("error \(error)")
//////                    print("connect \(connect)")
//////                })
////                
////                
////                
//            }
////
//        })
//        
        
        
//        let messsage = self.formatMessage(data)
//
//        let fbAccessToken = FBSDKAccessToken.currentAccessToken()
//        
//        if fbAccessToken == nil {
//            let fbloginButton = FBSDKLoginManager()
//            fbloginButton.logInWithReadPermissions(["public_profile", "publish_actions"], fromViewController: self, handler: { (result, error) -> Void in
//                if (error != nil) {
//                    print("process error")
//                }else if result.isCancelled {
//                    print("cancelled")
//                }else{
//                    print("logged in")
//                }
//            })
//        }else{
//
//            if fbAccessToken.hasGranted("publish_actions") {
//                print("permisssion granted")
//            }
//        }
        
//        print("vvvvvv \(FBSDKAccessToken.currentAccessToken())")
//        if FBSDKAccessToken.currentAccessToken().hasGranted("publish_actions") {    //check permission
//            print("permission granted")
////            publishMessage(messsage)
//        }
//        else{
//            print("permission declined")
////            let fbloginButton = FBSDKLoginManager()
////            fbloginButton.logInWithPublishPermissions(["publish_actions"], fromViewController: self, handler: { (result, error) -> Void in
////                print("declined & granted")
////                self.publishMessage(messsage)
////            })
//        }
        
    }
    
    
    private func publishMessage(message: String) {
        let paramaters = ["message" : message]
        let request = FBSDKGraphRequest(graphPath: "me/feed", parameters: paramaters, HTTPMethod: "POST")
        request.startWithCompletionHandler({ (connect, result, error) -> Void in
//            print("result \(result.status)")
            print("error \(error)")
            print("connect \(connect)")
                        print("resutl \(result)")
        })
    }
    
    private func sendSMS(dataToShare data:[String:String]) {
        let messageVC = MFMessageComposeViewController()
        messageVC.body = formatMessage(data)
        messageVC.messageComposeDelegate = self
        presentViewController(messageVC, animated: true, completion: nil)
    }
    
    private func copyURL(dataToShare data:[String:String]) {
        let pasteBoard = UIPasteboard.generalPasteboard()
        pasteBoard.string = formatMessage(data)
    }
    
    private func formatMessage(data: [String:String]) -> String{
        
        return ("\(data["RCMD_TXT"]!)\n\(data["ESTB_URL"]!)\n\(Language.localizedStr("M03_2_09")):\(data["RCMD_ID"]!)")
    }
    
    //MARK: - Navigation
    
    func leftNavigationClicked(){
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    //MARK: - MFMessageComposeControllerDelegate
    func messageComposeViewController(controller: MFMessageComposeViewController, didFinishWithResult result: MessageComposeResult) {
        switch result.rawValue {
            case MessageComposeResultCancelled.rawValue: // cancel message view controller
                controller.dismissViewControllerAnimated(true, completion: nil)
            case MessageComposeResultFailed.rawValue: // failed to send
                break
            case MessageComposeResultSent.rawValue: // sent
                controller.dismissViewControllerAnimated(true, completion: nil)
            default:
                break
        }
    }
    
}
