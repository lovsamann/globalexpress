//
//  RLib.swift
//  GlobalExpress
//
//  Created by Ralex on 3/1/16.
//  Copyright © 2016 webcash. All rights reserved.
//

import UIKit

struct CurrencyCode{
    static let USD = "USD"
    static let KRW = "KRW"
}
enum UIUserInterfaceIdiom : Int
{
    case Unspecified
    case Phone
}

struct ScreenSize
{
    static let SCREEN_WIDTH         = UIScreen.mainScreen().bounds.size.width
    static let SCREEN_HEIGHT        = UIScreen.mainScreen().bounds.size.height
    static let SCREEN_MAX_LENGTH    = max(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
    static let SCREEN_MIN_LENGTH    = min(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
}

struct DeviceType
{
    static let IS_IPHONE_4_OR_LESS  = UIDevice.currentDevice().userInterfaceIdiom == .Phone && ScreenSize.SCREEN_MAX_LENGTH < 568.0
    static let IS_IPHONE_5          = UIDevice.currentDevice().userInterfaceIdiom == .Phone && ScreenSize.SCREEN_MAX_LENGTH == 568.0
    static let IS_IPHONE_6          = UIDevice.currentDevice().userInterfaceIdiom == .Phone && ScreenSize.SCREEN_MAX_LENGTH == 667.0
    static let IS_IPHONE_6P         = UIDevice.currentDevice().userInterfaceIdiom == .Phone && ScreenSize.SCREEN_MAX_LENGTH == 736.0
}

public class RLib: NSObject {
    /* 
        + TODO: Set Navigation Title
        - param target: Your target
        - param title : Your navigation title
    */
    public class func setNavigationTitle(target:AnyObject, title:String)->Void{
        guard target.isKindOfClass(UIViewController) == true else {return}
        let viewController:UIViewController = target as! UIViewController
        viewController.title = title
    }
    
    /*
        + TODO: Set Left Navigation Item
        - param target: Your target
        - param image : Your image name
        - param action: Selector handle selection when click on left navigation
    */
    public class func setLeftNavigationItem(target:AnyObject,image:String, action:Selector)->Void{
        guard target.isKindOfClass(UIViewController) == true else{return}
        let viewController:UIViewController = target as! UIViewController
        let image_nav = UIImage(named: image)
        let imageView = UIImageView(frame: CGRectMake(0.0, 0.0, 46.0, 46.0))
        let leftNavBtn = UIButton(frame: CGRectMake(-5.0,0,imageView.frame.size.width,imageView.frame.size.height))
        leftNavBtn.setBackgroundImage(image_nav, forState: UIControlState.Normal)
        
        leftNavBtn.addTarget(viewController, action: action, forControlEvents: UIControlEvents.TouchUpInside)
        imageView.addSubview(leftNavBtn)
        
        let btnNewBarLeft = UIBarButtonItem(customView: imageView)
        viewController.navigationItem.leftBarButtonItem = btnNewBarLeft
    }
    
    public class func setRightNavigationItem(target:AnyObject, image:String, action:Selector) -> Void{
        guard target.isKindOfClass(UIViewController) == true else {return}
        let viewController:UIViewController = target as! UIViewController
        let right_image = UIImage(named: image)
        let imageView = UIImageView(frame: CGRectMake(0.0, 0.0, 55.0 , 46.0))
        let rightNavBtn = UIButton(frame: CGRectMake(0.0,0.0,imageView.frame.size.width,imageView.frame.size.height))
        rightNavBtn.setBackgroundImage(right_image, forState: .Normal)
        
        rightNavBtn.addTarget(viewController, action: action, forControlEvents: .TouchUpInside)
        imageView.addSubview(rightNavBtn)
        
        let btnNewBarRight = UIBarButtonItem(customView: imageView)
        viewController.navigationItem.rightBarButtonItem = btnNewBarRight
    }
    
    public class func setNavigationBackground(target:AnyObject,red:CGFloat,green:CGFloat,blue:CGFloat,alpha:CGFloat){
        guard target.isKindOfClass(UIViewController) == true else{return}
        let viewController:UIViewController = target as! UIViewController
        viewController.navigationController?.navigationBar.barTintColor = UIColor(red: red/255, green: green/255, blue: blue/255, alpha: alpha)
        viewController.navigationController?.navigationBar.tintColor = UIColor.whiteColor();
        viewController.navigationController?.navigationBar.translucent = false
        viewController.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor()]
    }
    
    /*
        TODO:Convert String Date to format(yyy.mm.dd) print(self.convDate("19930317"))
    */
    public class func formatDate(date : String) ->String {
        let myNSString = date as NSString
        let year = myNSString.substringWithRange(NSRange(location: 0, length: 4))
        let month = myNSString.substringWithRange(NSRange(location: 4, length: 2))
        let day = myNSString.substringWithRange(NSRange(location: 6, length: 2))
        return "\(year).\(month).\(day)"
    }
}