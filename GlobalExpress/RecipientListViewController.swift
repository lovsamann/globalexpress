//
//  RecipientListViewController.swift
//  GlobalExpress
//
//  Created by Ann on 2/2/16.
//  Copyright © 2016 webcash. All rights reserved.
//

import UIKit

class RecipientListViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    //MARK: - Outlet
    @IBOutlet var tableView: UITableView!
    @IBOutlet var recvierInfoLabel: UILabel!
    var nationlityList: [Nationality]?
    
    var recipients: [Recipient]?
    
    var selectedIndexPath: NSIndexPath!
    
    //MARK: - Override
    
    override func localize() {
        self.navigationItem.title = Language.localizedStr("S09_01")
        self.recvierInfoLabel.text = Language.localizedStr("S09_02")
        
    }
    
    
    //MARK: - UITableViewDataSource
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("cell") as? ListRecipientTableViewCell
        
        
        let recipient = recipients![indexPath.row]
        cell?.recipientNameLabel.text = recipient.name
        cell?.nationalityLabel.text = recipient.nationality?.name
        cell?.recevingType.text = recipient.bankName
        cell?.acountNoLabel.text = recipient.accountNumber
        
        
        return  cell!
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return recipients == nil ? 0 : recipients!.count
    }
    
    //MARK: - UITableViewDelegate
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if recipients == nil || recipients!.count == 0 {
            return nil
        }else{
            let headerImage = UIImage(named: "con_rad_top01.png")
            let headerImageView = UIImageView(frame: CGRectMake(0, 0, CGRectGetWidth(tableView.frame), 9))
            
            headerImageView.image = headerImage
            
            return headerImageView
        }
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return  recipients == nil || recipients!.count == 0 ? 0.00000000000000000000000000001 : 9.0
    }
    
    func tableView(tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let frame =  CGRectMake(0,0,CGRectGetWidth(tableView.frame),62)
        let footerView = UIView(frame: frame)
        footerView.backgroundColor = UIColor.clearColor()
        
        let addButton = addReceiverButton(CGRectMake(0,18,CGRectGetWidth(tableView.frame),45))
        addButton.addTarget(self, action: "navigationToAddRecipeint", forControlEvents: .TouchUpInside)
        if recipients?.count > 0 { //add both footerView & add recipeint view
            let footerImageView = addFooterImageView(CGRectMake(0, 0, CGRectGetWidth(tableView.frame), 9))
            footerView.addSubview(footerImageView)
        }
        
        footerView.addSubview(addButton)
        
        return footerView
    }
    
    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 62
    }

    //MARK: - private methods
    
    func addReceiverButton(frame: CGRect) -> UIButton {
        let addButton = UIButton(frame: frame)
        addButton.setBackgroundImage(UIImage(named: "w_btn.png"), forState: .Normal)
        addButton.setTitle(Language.localizedStr("R05_03"), forState: .Normal)
        
        addButton.setTitleColor(UIColor(red: 48 / 255, green: 50 / 255, blue: 51 / 255, alpha: 1.0), forState: .Normal)
        
        let addIcon = UIImage(named: "add_icon.png")
        let newAddIcon = UIImage.customResizeImage(addIcon!, newSize: CGSizeMake(18,18))
        addButton.setImage(newAddIcon, forState: .Normal)
        
        addButton.imageEdgeInsets = UIEdgeInsetsMake(0, -8, -1, 0)
        
        return addButton
    }

    func addFooterImageView(frame: CGRect) -> UIImageView {
        //footerImage
        let footerImage = UIImage(named: "con_rad_bottom01.png")
        let footerImageView = UIImageView(frame: frame)
        footerImageView.image = footerImage
        
        let clearSeparatorView = UIView(frame: CGRectMake(8, -1 , CGRectGetWidth(footerImageView.frame) - 12 , 1))
        clearSeparatorView.backgroundColor = UIColor.whiteColor()
        footerImageView.addSubview(clearSeparatorView)
        
        return footerImageView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.localize()
        
        let leftNavigationImage = UIImage(named: "back_btn.png")
        let leftNavigationButton = UIButton(frame: CGRectMake(0,0,(leftNavigationImage?.size.width)! / 2 , (leftNavigationImage?.size.height)! / 2))
        leftNavigationButton.setBackgroundImage(leftNavigationImage, forState: .Normal)
        leftNavigationButton.addTarget(self, action: "leftNavigationClicked", forControlEvents: .TouchUpInside)
        
        let leftBarButtonItem = UIBarButtonItem(customView: leftNavigationButton)
        self.navigationItem.leftBarButtonItem = leftBarButtonItem
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        self.tableView.reloadData()
    }
    
    //MARK: - Action
    func navigationToAddRecipeint() {
        self.performSegueWithIdentifier("addRecipientSeuge", sender: nil)
    }
    
    func leftNavigationClicked(){
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if segue.identifier == "selectedCellSegue" {
            
            selectedIndexPath = self.tableView.indexPathForCell(sender as! UITableViewCell)
            
            let recipient = self.recipients![(selectedIndexPath?.row)!]
            
            TransactionClass().sendAPI(APICode.RECIPIENT.RECIPIENT_ROO2, argument: ["RCPT_CD" : recipient.recipientCode!], success: { (success) -> Void in
                
                let recipientInfoDetailVC = segue.destinationViewController as? RecipientInfoDetailViewController
                
                guard let rawData = success["RESP_DATA"] as? [String: String] else {
                    print("rawData parse nil....")
                    return
                }
                
                recipientInfoDetailVC?.detailDic = rawData
                recipientInfoDetailVC?.recipeint = recipient
            })
            
        }else{
            let addRecipientVC = segue.destinationViewController as? AddRecipientViewController
            
            addRecipientVC?.nationalityList = self.nationlityList
        }
        
    }
    
    @IBAction func unwindFromRecipientInfoDetailViewController(segue: UIStoryboardSegue) {
        guard let indexPath = selectedIndexPath else{
            print("selected IndexPath nil")
            return
        }
        
        self.recipients?.removeAtIndex(indexPath.row)
    }
    
    
    @IBAction func unwindFromRecipeintInfoViewController(segue: UIStoryboardSegue) {
        if ((self.navigationController?.navigationBarHidden) != nil) {
            self.navigationController?.navigationBarHidden = false
        }
        
        let sourceViewController = segue.sourceViewController as? RecipientInfoViewController
        let dic = sourceViewController?.recipientDic
        
        guard let newRecipeintDic: [String: AnyObject] = dic else{
            print("recipeint nil")
            return
        }
        
        let newRecipeint = Recipient(data: newRecipeintDic)
        
        guard let recipeint = newRecipeint else{
            print("new recipeint nil")
            return
        }
        
        recipients?.append(recipeint)
        self.tableView.reloadData()
        print("source vc \(newRecipeint)")
    }
}
