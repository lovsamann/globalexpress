//
//  TelecomCell.swift
//  GlobalExpress
//
//  Created by UDAM on 3/22/16.
//  Copyright © 2016 webcash. All rights reserved.
//

import UIKit

class TelecomCell: UITableViewCell {

    @IBOutlet weak var radioButton: UIButton!
    @IBOutlet weak var telecomTitle: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var centerView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
        
        radioButton.selected = selected
        
    }
    
}
