//
//  TelecomPopup.swift
//  GlobalExpress
//
//  Created by UDAM on 3/16/16.
//  Copyright © 2016 webcash. All rights reserved.
//

import UIKit

@objc protocol TelecomPopupDelegate {
    func getTelecomData(dict: NSDictionary!)

}

class TelecomPopup: MJPopupViewController, UITableViewDelegate, UITableViewDataSource {

    // MARK: - IBOutlet
    // -------------------------------------------------------------------------------
    //	IBOutlet tableView, View, Label
    // -------------------------------------------------------------------------------
    @IBOutlet weak var tableView    : UITableView!
    @IBOutlet weak var titleLabel   : UILabel!
    
    // MARK: - Property
    // -------------------------------------------------------------------------------
    // Store country as NSArray
    // -------------------------------------------------------------------------------
    var telecomArray    : NSArray!
    var delegate        : TelecomPopupDelegate?
    
    // MARK: - IBAction
    // -------------------------------------------------------------------------------
    // Click for closing Popup CountryViewController
    // -------------------------------------------------------------------------------
    @IBAction func closeButton(sender: UIButton) {
        
        self.dismissPopupViewController(self, animationType: MJPopupViewAnimationFade)
        
    }
    
    // MARK: - Private function
    // -------------------------------------------------------------------------------
    // Localizing language
    // -------------------------------------------------------------------------------
    override func localize() {
        
        titleLabel.text = Language.localizedStr("C09P_03")
        
    }
    
    // MARK: - View Life Cycle
    // -------------------------------------------------------------------------------
    // viewDidLoad customize view and table view
    // -------------------------------------------------------------------------------
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.layer.cornerRadius = 8.0
        
        //Localize
        localize()
    }
    
    // -------------------------------------------------------------------------------
    // viewDidDisappear lear delegate
    // -------------------------------------------------------------------------------
    override func viewDidDisappear(animated: Bool) {
        delegate = nil
    }

    // -------------------------------------------------------------------------------
    // viewDidLayoutSubviews
    // -------------------------------------------------------------------------------
    override func viewDidLayoutSubviews(){
            
        let tableMaskLayer = CAShapeLayer()
        tableMaskLayer.path = UIBezierPath(roundedRect: self.tableView.bounds, byRoundingCorners: UIRectCorner.BottomLeft.union(.BottomRight), cornerRadii: CGSizeMake(8, 8)).CGPath
        self.tableView.layer.mask = tableMaskLayer
        
    }
    
    // MARK: - UITableViewDelegate
    // -------------------------------------------------------------------------------
    // didSelectRowAtIndexPath: Get Country Data After select for each cell
    // -------------------------------------------------------------------------------
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        self.delegate?.getTelecomData(telecomArray[indexPath.row] as! NSDictionary)

    }
    
    // MARK: - UITableViewDataSource
    // -------------------------------------------------------------------------------
    // Customize the number of rows in table view
    // -------------------------------------------------------------------------------
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return telecomArray.count - 3
    }
    
    // -------------------------------------------------------------------------------
    // UITableView cellForRowAtIndexPath
    // -------------------------------------------------------------------------------
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        tableView.registerNib(UINib.init(nibName: "TelecomCell", bundle: NSBundle.mainBundle()), forCellReuseIdentifier: "telecomCell")
        
        let cell = tableView.dequeueReusableCellWithIdentifier("telecomCell") as! TelecomCell
        cell.selectionStyle = UITableViewCellSelectionStyle.None
        cell.centerView.layer.cornerRadius = 8.0
        
        cell.telecomTitle.text = "\(telecomArray[indexPath.row]["TELCOM_NM"])"
        
        if indexPath.row == 0 {
            cell.descriptionLabel.text = Language.localizedStr("C09P_06")
        }
        if indexPath.row == 1 {
            cell.descriptionLabel.text = Language.localizedStr("C09P_08")
        }
        if indexPath.row == 2 {
            cell.descriptionLabel.text = Language.localizedStr("C09P_10")
        }
        
        return cell
    }
    
}
