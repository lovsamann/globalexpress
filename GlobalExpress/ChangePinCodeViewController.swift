//
//  ChangePinCodeViewController.swift
//  GlobalExpress
//
//  Created by Ralex on 3/18/16.
//  Copyright © 2016 webcash. All rights reserved.
//

import UIKit

class ChangePinCodeViewController: UIViewController,UITextFieldDelegate{
    @IBOutlet weak var pincodeTextField:UITextField!
    @IBOutlet weak var viewInset : UIView!
    @IBOutlet weak var lblCurrentPIN: UILabel!
    
    @IBOutlet weak var btnConfirm: UIButton!
   
    let numOfImageInset = 5
    var startx:CGFloat = 0.0
    var isDelete:Bool = false
    var navigationTitle = "Current PIN No."
    
    var tradeStatus:Int?
    var numOfNumberInput:Int = 0
    
    override func localize() {
        navigationTitle = Language.localizedStr("T10_06")
        self.lblCurrentPIN.text = Language.localizedStr("T10_07")
        self.btnConfirm.setTitle(Language.localizedStr("T10_03"), forState: .Normal)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        localize()
        
        RLib.setNavigationTitle(self, title: navigationTitle)
        RLib.setLeftNavigationItem(self, image: "back_btn.png", action: Selector("handleLeftNavigationItemClick:"))
        
        pincodeTextField.becomeFirstResponder()
        pincodeTextField.tintColor = UIColor.clearColor()
    }
    
    func handleLeftNavigationItemClick(sender:UIBarButtonItem){
        navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func handleViewPress(sender: AnyObject) {
        pincodeTextField.resignFirstResponder()
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func initPinCharLayout(numOfNumberInput:Int){
        var count = 0
        if isDelete == true{
            for subview in viewInset.subviews{
                subview.removeFromSuperview()
            }
        }
        for index in 0...numOfImageInset{
            startx = CGFloat(index * 29)
            let imgPasscode = UIImageView()
            if numOfNumberInput == 0{
                imgPasscode.frame = CGRectMake(startx + 8, 19, 13 , 3)
                imgPasscode.image = UIImage(named: "pin_emp_char.png")
            }else{
                //Add it depend on index of inputted
                imgPasscode.frame = CGRectMake(startx + 8, 13, 13 , 13)
                imgPasscode.image = UIImage(named: "pin_char.png")
                count++
                if count > numOfNumberInput {
                    imgPasscode.frame = CGRectMake(startx + 8, 19, 13 , 3)
                    imgPasscode.image = UIImage(named: "pin_emp_char.png")
                }
            }
            viewInset.addSubview(imgPasscode)
        }
        
    }
    
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
        //LOOP IMAGE IN VIEW]
        initPinCharLayout(numOfNumberInput)
        return true
        
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        numOfNumberInput = (textField.text?.utf16.count)! + string.utf16.count - range.length
        //IF BACK KEY PRESS
        if range.length == 1{
            isDelete = true
            initPinCharLayout(numOfNumberInput)
        }else{
            isDelete = false
            initPinCharLayout(numOfNumberInput)
        }
        return numOfNumberInput > 6 ? false : true
    }
    
    @IBAction func ActionConfirmCurrentPassCode(sender: AnyObject) {
            guard pincodeTextField.text != "" && numOfNumberInput > 5 else{
                //Should be alert here
                self.showSimpleAlert("Please enter PIN No.", delegate: self)
                return

            }
            TransactionClass().sendAPI(APICode.PSWDCHK_R001, argument: ["SETL_PSWD":pincodeTextField.text!], success: { (success) -> Void in
                print("Data = \(success)")
                guard let code = success["RSLT_CD"] where code as! String == "0000" else{
                    //Should be alert here
                    self.showSimpleAlert(success["RSLT_MSG"] as! String, delegate: self)
                    return
                    
                }
                let storyboard = UIStoryboard(name: "Transaction", bundle: nil)
                let viewcontroller = storyboard.instantiateViewControllerWithIdentifier("PinCodeViewController") as? PinCodeViewController
                viewcontroller!.tradeStatus = self.tradeStatus
                viewcontroller!.beforePassword = self.pincodeTextField.text
                self.navigationController?.pushViewController(viewcontroller!, animated: true)
            })
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
