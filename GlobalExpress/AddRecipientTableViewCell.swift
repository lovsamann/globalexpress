//
//  AddRecipientTableViewCell.swift
//  GlobalExpress
//
//  Created by Ann on 3/1/16.
//  Copyright © 2016 webcash. All rights reserved.
//

import UIKit

class AddRecipientTableViewCell: UITableViewCell {

    @IBOutlet var customTitleLabel: UILabel!
    @IBOutlet var customTextField: UITextField!
    @IBOutlet var requireImageView: UIImageView!

}
