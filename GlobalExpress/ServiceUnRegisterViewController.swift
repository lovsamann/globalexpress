//
//  ServiceUnRegisterViewController.swift
//  GlobalExpress
//
//  Created by UDAM on 3/9/16.
//  Copyright © 2016 webcash. All rights reserved.
//

import UIKit

class ServiceUnRegisterViewController: UIViewController, UIAlertViewDelegate {

    //MARK: - IBOutlet
    // -------------------------------------------------------------------------------
    //	IBOutlet UILabel
    // -------------------------------------------------------------------------------
    @IBOutlet weak var titleLabel           : UILabel!
    @IBOutlet weak var text1                : UILabel!
    @IBOutlet weak var text2                : UILabel!
    
    // -------------------------------------------------------------------------------
    // IBOutlet UIButton
    // -------------------------------------------------------------------------------
    @IBOutlet weak var terminateButton      : UIButton!
    
    // -------------------------------------------------------------------------------
    //IBOutlet NSLayoutConstraint
    // -------------------------------------------------------------------------------
    @IBOutlet weak var topImageConstraint           : NSLayoutConstraint!
    @IBOutlet weak var bottomCenterViewConstraint   : NSLayoutConstraint!
    
    //MARK: - Private function
    // -------------------------------------------------------------------------------
    // Localize
    // -------------------------------------------------------------------------------
    override func localize() {
        self.navigationItem.title   = Language.localizedStr("S22_01")
        self.titleLabel.text        = Language.localizedStr("S22_02")
        self.text1.text             = Language.localizedStr("S22_03")
        self.text2.text             = Language.localizedStr("S22_04")
        self.terminateButton.setTitle(Language.localizedStr("S22_05"), forState: .Normal)
        
    }
    
    // -------------------------------------------------------------------------------
    // Customize View Layout
    // -------------------------------------------------------------------------------
    func customizeViewLayout() {
        
        self.navigationController?.navigationBarHidden=false
        //Customize navigation item ,barButton
        let backImage = UIImage(imageLiteral: "back_btn")
        let leftNavButton = UIButton(frame: CGRectMake(269.0, 20.0, backImage.size.width/2, backImage.size.height/2))
        leftNavButton.setBackgroundImage(backImage, forState: .Normal)
        leftNavButton.addTarget(self, action: "popUpViewController", forControlEvents:UIControlEvents.TouchUpInside)
        let barButton = UIBarButtonItem(customView: leftNavButton)
        self.navigationItem.leftBarButtonItem = barButton
        navigationItem.setHidesBackButton(true, animated: false)
        
        if DeviceType.IS_IPHONE_4_OR_LESS {
            topImageConstraint.constant         = 54 - 40
            bottomCenterViewConstraint.constant = 148 - 74
        }
    }
    
    // -------------------------------------------------------------------------------
    // PopupViewController
    // -------------------------------------------------------------------------------
    func popUpViewController() {
        self.navigationController?.popToRootViewControllerAnimated(true)
    }
    
    // MARK: - View Life Cycle
    // -------------------------------------------------------------------------------
    // ViewDidLoad
    // -------------------------------------------------------------------------------
    override func viewDidLoad() {
        super.viewDidLoad()

        //Localize
        localize()
        
        //Customize
        customizeViewLayout()
        
    }
    
    // MARK: - Handle Button
    // -------------------------------------------------------------------------------
    //	Click next will go to CompletedUnRegisterViewController
    // -------------------------------------------------------------------------------
    @IBAction func terminateButtonClicked(sender: UIButton) {
        
        //## Swift2.0 수정부분 - 테스트 필요
        if #available(iOS 8.0, *) {
            let alertController : UIAlertController = UIAlertController(title: "", message: "\(Language.localizedStr("S22_02"))", preferredStyle: .Alert)
            let cancelAction    : UIAlertAction     = UIAlertAction(title: "취소", style: .Cancel, handler: nil)
            let confirmAction   : UIAlertAction     = UIAlertAction(title: "확인", style: .Default){ UIAlertAction in
                
                let transaction = TransactionClass()
                transaction.sendAPI(APICode.SVC_D001, argument: ["" : ""], success: { (success) -> Void in
                    
                    if let rslt_cd = success["RSLT_CD"] as? String {
                        if rslt_cd != "0000" { //오류
                            if let rsltMsg = success["RSLT_MSG"] as? String {
                                self.showEventAlert("Error Message :\(rsltMsg)", hasCancelButton: false, delegate: self, tag: nil, confirmBlock: nil, cancelBlock: nil)
                                print("Error Code : \(rslt_cd), Error Message : \(rsltMsg)")
                            }
                        } else { //정상
                            
                            //Service Unregister N
                            NSUserDefaults.standardUserDefaults().setObject(nil, forKey: "SIGNUP_YN")
                            NSUserDefaults.standardUserDefaults().synchronize()
                            self.performSegueWithIdentifier("ShowUnregistered", sender: nil)
                            
                        }
                    }
                    
                })
//                self.performSegueWithIdentifier("ShowUnregistered", sender: nil)
            }
            
            alertController.addAction(cancelAction)
            alertController.addAction(confirmAction)
            presentViewController(alertController, animated: true, completion: nil)
        } else {
            // Fallback on earlier versions
            let alertView       : UIAlertView = UIAlertView()
            alertView.title     = ""
            alertView.message   = "\(Language.localizedStr("S22_02"))"
            alertView.addButtonWithTitle("취소")
            alertView.addButtonWithTitle("확인")
            alertView.delegate  = self
            alertView.show()
        }
        
    }
    
    // MARK: - UIAlertView Delegate
    // -------------------------------------------------------------------------------
    //	alertView
    // -------------------------------------------------------------------------------
    func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int) {
        if (buttonIndex == 0) {

            let transaction = TransactionClass()
            transaction.sendAPI(APICode.SVC_D001, argument: ["" : ""], success: { (success) -> Void in
                        
                    if let rslt_cd = success["RSLT_CD"] as? String {
                        if rslt_cd != "0000" { //오류
                            if let rsltMsg = success["RSLT_MSG"] as? String {
                                self.showEventAlert("Error Message :\(rsltMsg)", hasCancelButton: false, delegate: self, tag: nil, confirmBlock: nil, cancelBlock: nil)
                                print("Error Code : \(rslt_cd), Error Message : \(rsltMsg)")
                            }
                        } else { //정상
                                
                            //Service Unregister N
                            NSUserDefaults.standardUserDefaults().setObject(nil, forKey: "SIGNUP_YN")
                            NSUserDefaults.standardUserDefaults().synchronize()
                            self.performSegueWithIdentifier("ShowUnregistered", sender: nil)
                                
                        }

                    }
                        
            })
            
        }
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
