//
//  Constants.h
//  GlobalExpress
//
//  Created by JangWooil on 2015. 12. 29..
//  Copyright © 2015년 webcash. All rights reserved.
//

////////////////////////////////////////////////////////////////////////////////////////////////////
// WCPush, Server constants
////////////////////////////////////////////////////////////////////////////////////////////////////
#if _DEBUG_
    #define kPushServerAddress      @"http://112.187.199.29/push/standard_gateway/gateway.jsp?"
    #define _MG_GATE_URL            @"http://211.217.152.190:28080/MgGate?"
//    #define _SM_GATEWAY_URL			@"http://211.217.152.190:28080/"		//Development Server URL Sample
//    #define _SM_GATEWAY_URL	@"http:fxremit.wecambodia.com/gateway/gauus_api.jsp?JSONData=" //development
    #define _SM_GATEWAY_URL			@"http://192.168.178.83:20000/gateway/gauus_api.jsp?JSONData="		//Development Server URL Sample
    #define kAppIronAuthServerURL   @"http://183.111.160.141:8480/authCheck.call"                   // 위변조 서버 URL
#else
    #define kPushServerAddreppass      @"https://sws.webcash.co.kr/wcp/gateway/gateway.jsp?" //@"https://sws.webcash.co.kr/wcp/gateway/gateway.jsp?"
    #define _SM_GATEWAY_URL			@"https://www.bizplay.co.kr/"		//Production Server URL Sample
    #define _MG_GATE_URL            @"https://www.bizplay.co.kr/MgGate?"
    #define kAppIronAuthServerURL   @"http://app.coocon.co.kr/authCheck.call"                   // 위변조 서버 URL
#endif
#define kPushStartNotification               @"PushStartNotification"
#define kShowAlert                           @"showAlert"
#define kShowAlertMain                       @"showAlertMain"
#define kIntroEnded                          @"introEnded"
#define _SM_GATEWAY_PATH            @"MgGate" //GATEWAY PATH
#define kKoreaCode                  @"ko"
#define kCambodiaCode               @"km-KH"
#define kEnglishCode                @"Base"
#define progressViewTag             5001
#define kUserSavedLanguage          @"UserSavedLanguage"

///////////////////////////////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////////////////////////////////
#define IS_OS_5_BEFORE ([[[UIDevice currentDevice] systemVersion] floatValue] < 5.0)
#define IS_OS_6_BEFORE ([[[UIDevice currentDevice] systemVersion] floatValue] < 6.0)
#define IS_OS_7_BEFORE ([[[UIDevice currentDevice] systemVersion] floatValue] < 7.0)
#define IS_OS_8_BEFORE ([[[UIDevice currentDevice] systemVersion] floatValue] < 8.0)
#define IS_OS_9_BEFORE ([[[UIDevice currentDevice] systemVersion] floatValue] < 9.0)
#define kDeviceToken               @"_deviceToken" //디바이스 토큰값
#define kNotiAPNS                  @"NotiAPNS"
#define kShowWaitingViewNotification @"ShowWaitingViewNotification"
#define kCloseWaitingViewNotification @"CloseWaitingViewNotification"

@interface Constants : NSObject {
    
}
+(NSString *)PushServerAddress;
+(NSString *)SM_GATEWAY_URL;
+(NSString *)PushStartNotification;
+(NSString *)KoreaCode;
+(NSString *)CambodiaCode;
+(NSString *)EnglishCode;

@end