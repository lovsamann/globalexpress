//
//  SimpleTableViewCell.swift
//  GlobalExpress
//
//  Created by Ann on 3/16/16.
//  Copyright © 2016 webcash. All rights reserved.
//

import UIKit

class SimpleTableViewCell: UITableViewCell {

    @IBOutlet var rightLabel: UILabel!
    @IBOutlet var leftLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
