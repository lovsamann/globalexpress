//
//  GenderViewController.swift
//  GlobalExpress
//
//  Created by Ralex on 2/26/16.
//  Copyright © 2016 webcash. All rights reserved.
//

import UIKit

protocol GenderViewDelegate{
    func selectedGender(gender:String)
}

class GenderPopupViewController: MJPopupViewController,UITableViewDataSource,UITableViewDelegate {

    @IBOutlet weak var genderTableView: UITableView!
    @IBOutlet weak var selectGender:UILabel!
    
    
    @IBOutlet weak var viewHeader: UIView!
    
    var delegate :GenderViewDelegate?
    
    var arrGendar:Array<String>!
    var male = "Male"
    var female = "Female"
    
    override func localize() {
        selectGender.text = Language.localizedStr("C11P_01")
        male = Language.localizedStr("C11P_02")
        female = Language.localizedStr("C11P_03")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        localize()
        self.genderTableView.registerNib(UINib(nibName: "GenderTableViewCell", bundle: nil), forCellReuseIdentifier: "GenderCell")
        arrGendar = [male,female]
        
        let headerMaskLayer = CAShapeLayer()
        headerMaskLayer.path = UIBezierPath(roundedRect: self.viewHeader.bounds, byRoundingCorners: UIRectCorner.TopLeft.union(.TopRight), cornerRadii: CGSizeMake(5, 5)).CGPath
        self.viewHeader.layer.mask = headerMaskLayer
        
        let maskLayer = CAShapeLayer()
        maskLayer.path = UIBezierPath(roundedRect: self.genderTableView.bounds, byRoundingCorners: UIRectCorner.BottomLeft.union(.BottomRight), cornerRadii: CGSizeMake(5, 5)).CGPath
        self.genderTableView.layer.mask = maskLayer
        
        self.view.layer.cornerRadius = 5
       
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrGendar.count
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let gendercell = genderTableView.dequeueReusableCellWithIdentifier("GenderCell", forIndexPath: indexPath) as! GenderTableViewCell
        gendercell.lblString.text = arrGendar[indexPath.row]
        
        //Configure color Cell selection
        let customColorView = UIView()
        customColorView.backgroundColor = UIColor(colorLiteralRed: 235.0/255.0, green: 240.0/255.0, blue: 242.0/255.0, alpha: 1.0)
        gendercell.selectedBackgroundView = customColorView
        
        return gendercell
    }
    
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        guard delegate != nil else{
            print("")
            return
        }
        self.delegate?.selectedGender("\(indexPath.row + 1)")
        self.dismissPopupViewController(self, animationType: MJPopupViewAnimationFade)
    }
    
    
    @IBAction func ActionDismiss(sender: AnyObject) {
        self.dismissPopupViewController(self, animationType: MJPopupViewAnimationFade)
    }

    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
