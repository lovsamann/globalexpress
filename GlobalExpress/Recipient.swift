//
//  Recipient.swift
//  GlobalExpress
//
//  Created by Ann on 1/25/16.
//  Copyright © 2016 webcash. All rights reserved.
//

import Foundation

struct RecipientJSONProperty {
    static let ACCOUNT_NO = "ACCT_NO"
    static let BANK_NAME = "BANK_NM"
    static let CUSTOMER_NAME = "CUST_NM"
    static let NATIONALITY_NAME = "NATION_NM"
    static let RECIPEINT_CODE = "RCPT_CD"
    static let TYPE = "RCPT_GB"
    static let TELPHONE = "TEL_NO"
}


class Recipient: Person {
    
    //conforms protocol
    var name: String?
    var gender: String?
    var address: String?
    
    //members
    var accountNumber: String?
    var recipientCode: String?
    var telephone: String?
    var bankName: String?
    var nationality: Nationality?
    var orgainzation: Organize?
    var recipientType: String? // 1: bank 2: cash pick up
    var email: String?
    
    init?(data: [String: AnyObject?]?) {
        guard let rawData = data else{
            name = nil
            gender = nil
            address = nil
            print("data is nil.")
            return
        }
        
        name = rawData[RecipientJSONProperty.CUSTOMER_NAME] as? String == nil ? rawData["RCPT_NM"] as? String : rawData[RecipientJSONProperty.CUSTOMER_NAME] as? String

        gender = nil
        address = nil
        accountNumber = rawData[RecipientJSONProperty.ACCOUNT_NO] as? String
        recipientCode = rawData[RecipientJSONProperty.RECIPEINT_CODE] as? String
        telephone = rawData[RecipientJSONProperty.TELPHONE] as? String
        print("telephone \(telephone)")
        bankName = rawData[RecipientJSONProperty.BANK_NAME] as? String
//        nationalName = rawData[RecipientJSONProperty.NATIONALITY_NAME] as? String
        nationality = Nationality(nameNM: (rawData[RecipientJSONProperty.NATIONALITY_NAME] as? String)!)
        recipientType = rawData[RecipientJSONProperty.TYPE] as? String
    }
    
}