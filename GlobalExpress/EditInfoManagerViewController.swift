//
//  EditInfoManagerViewController.swift
//  GlobalExpress
//
//  Created by UDAM on 2/18/16.
//  Copyright © 2016 webcash. All rights reserved.
//

import UIKit

class EditInfoManagerViewController: UIViewController, UITextFieldDelegate, TelecomPopupDelegate {
    
    // MARK: - IBOutlet
    // -------------------------------------------------------------------------------
    // IBOutlet UIScrollView
    // -------------------------------------------------------------------------------
    @IBOutlet weak var Scroller         : UIScrollView!
    
    // -------------------------------------------------------------------------------
    // IBOutlet UILabel
    // -------------------------------------------------------------------------------
    @IBOutlet weak var topTitleLabel    : UILabel!
    @IBOutlet weak var nameKOLabel      : UILabel!
    @IBOutlet weak var nameENLabel      : UILabel!
    @IBOutlet weak var dobLabel         : UILabel!
    @IBOutlet weak var countryLabel     : UILabel!
    @IBOutlet weak var phoneNoLabel     : UILabel!
    @IBOutlet weak var passNoLabel      : UILabel!
    @IBOutlet weak var expireDateLabel  : UILabel!
    @IBOutlet weak var promoCodeLabel   : UILabel!
    @IBOutlet weak var requiredLabel    : UILabel!
    
    // -------------------------------------------------------------------------------
    // IBOutlet UITextField
    // -------------------------------------------------------------------------------
    @IBOutlet weak var nameKOResult         : UITextField!
    @IBOutlet weak var nameENResult         : UITextField!
    @IBOutlet weak var dateOfBirth          : UITextField!
    @IBOutlet weak var country              : UITextField!

    @IBOutlet weak var intl_Telecom         : UITextField!
    @IBOutlet weak var phoneNoTextField     : UITextField!
    @IBOutlet weak var passNoTextField      : UITextField!
    @IBOutlet weak var expireDateTextField  : UITextField!
    @IBOutlet weak var proCodeTextField     : UITextField!

    // -------------------------------------------------------------------------------
    // IBOutlet UIButton
    // -------------------------------------------------------------------------------
    @IBOutlet weak var editButton       : UIButton!
    
    // -------------------------------------------------------------------------------
    // IBOutlet ADVSegmentController, UIView
    // -------------------------------------------------------------------------------
    @IBOutlet weak var telecomView              : ADVSegmentedControl!
    
    // -------------------------------------------------------------------------------
    // IBOutlet UIImageView
    // -------------------------------------------------------------------------------
    @IBOutlet weak var img6: UIImageView!
    @IBOutlet weak var img7: UIImageView!
    
    // -------------------------------------------------------------------------------
    // IBOutlet NSLayoutConstraint
    // -------------------------------------------------------------------------------
    @IBOutlet weak var bottomEditButtonConstraint           : NSLayoutConstraint!
    @IBOutlet weak var centerViewHeightConstraint           : NSLayoutConstraint!
    @IBOutlet weak var heightLabelNameEngConstraint         : NSLayoutConstraint!
    @IBOutlet weak var heightTextFieldNameEngConstraint     : NSLayoutConstraint!
    @IBOutlet weak var heightLabelPromotionCodeConstraint   : NSLayoutConstraint!
    @IBOutlet weak var heightTextFieldPromoCodeConstraint   : NSLayoutConstraint!
    @IBOutlet weak var heightLine4Constraint                : NSLayoutConstraint!
    @IBOutlet weak var widthViewPwdConstraint               : NSLayoutConstraint!
    
    //MARK: - Properties
    // -------------------------------------------------------------------------------
    // Property: NSDicionary
    // -------------------------------------------------------------------------------
    var passData            : NSDictionary?
    var arrayTelecom        : Array<String> = []
    var telecomCode         : String?
    var responseCode_R001   : NSArray?
    var phoneEdited         : NSString?
    private var isKorean: Bool = false
    
    //MARK: - IBAction
    // -------------------------------------------------------------------------------
    //	IBOutlet Click to Update MyInfo Manager
    // -------------------------------------------------------------------------------
    @IBAction func editButtonClicked(sender: UIButton) {
        
        /////////// USE THIS FORMAT
        let transaction = TransactionClass()
        transaction.sendAPI(APICode.PROFILE_U001, argument: [
            "HP_NO" : "\(self.phoneNoTextField.text!)",
            "TELCOM_CD" : "\(telecomCode!)"
            ], success: { (success) -> Void in
                
                if let rslt_cd = success["RSLT_CD"] as? String {
                    
                    if rslt_cd != "0000" { //오류
                        if let rsltMsg = success["RSLT_MSG"] as? String {
                            
                            print("Error Message : \(rsltMsg)")
                            self.showEventAlert("Error Message :\(rsltMsg)", hasCancelButton: false, delegate: self, tag: nil, confirmBlock: nil, cancelBlock: nil)
                        }
                    } else { //정상
                        if let respData = success["RESP_DATA"] as? NSDictionary {
                            self.phoneEdited = self.phoneNoTextField.text!
                            
                            print("Sucess Code : \(rslt_cd), Message Reponse Empty: \(respData)")
                            
                            self.performSegueWithIdentifier("ShowEdited", sender:self.passData)
                        }
                        
                    }
                }
        })
        
//        performSegueWithIdentifier("ShowEdited", sender: nil)
    }
    
    // -------------------------------------------------------------------------------
    // Click to view TelecomPopup
    // -------------------------------------------------------------------------------
    func selectTelecomClicked() {
        
        let telecom : TelecomPopup = TelecomPopup(nibName: "TelecomPopup", bundle: NSBundle.mainBundle())
        telecom.delegate = self
        telecom.telecomArray = responseCode_R001
        self.presentPopupViewController(telecom, animationType: MJPopupViewAnimationFade, contentInteraction: MJPopupViewContentInteractionNone)
        
    }
    
    // -------------------------------------------------------------------------------
    // Click to view TelecomPopup
    // -------------------------------------------------------------------------------
    @IBAction func clickedIndexButton(sender: UIButton) {
        
        if sender.tag == 0 {
            telecomView.selectedIndex = 0
            telecomCode = self.responseCode_R001![sender.tag+3]["TELCOM_CD"] as? String
        }
        else if sender.tag == 1 {
            telecomView.selectedIndex = 1
            telecomCode = self.responseCode_R001![sender.tag+3]["TELCOM_CD"] as? String
        }
        else if sender.tag == 2 {
            telecomView.selectedIndex = 2
            telecomCode = self.responseCode_R001![sender.tag+3]["TELCOM_CD"] as? String
        }
        else if sender.tag == 3 {
            selectTelecomClicked()
        }
        
        print(telecomCode!)
    }
    
    // -------------------------------------------------------------------------------
    // TelecomPopupDelegate
    // -------------------------------------------------------------------------------
    func getTelecomData(dict: NSDictionary!) {
        telecomCode = dict["TELCOM_CD"] as? String
        telecomView.selectedIndex = 3
        print("getTelecom Sucess:\(dict)")
    }
    
    // MARK: - Private function
    // -------------------------------------------------------------------------------
    // Localizing language
    // -------------------------------------------------------------------------------
    override func localize() {
        
        self.navigationItem.title   = Language.localizedStr("S13_1_01")
        self.topTitleLabel.text     = Language.localizedStr("S13_1_02")
        self.nameKOLabel.text       = Language.localizedStr("S13_1_03")
        self.nameENLabel.text       = Language.localizedStr("S13_1_03")
        self.dobLabel.text          = Language.localizedStr("S13_1_04")
        self.countryLabel.text      = Language.localizedStr("S13_1_05")
        self.phoneNoLabel.text      = Language.localizedStr("S13_1_06")
        self.promoCodeLabel.text    = Language.localizedStr("S13_1_11")
        self.requiredLabel.text     = Language.localizedStr("S13_1_12")
        
        self.editButton.setTitle(Language.localizedStr("S13_2_13"), forState: .Normal)
        
        self.passNoLabel.text       = Language.localizedStr("S13_1_08")
        
    }
    
    // -------------------------------------------------------------------------------
    // Customize View Layout
    // -------------------------------------------------------------------------------
    func customizeViewLayout(){
        
        //Customize navigation item ,barButton
        let backImage = UIImage(imageLiteral: "back_btn")
        let leftNavButton = UIButton(frame: CGRectMake(269.0, 20.0, backImage.size.width/2, backImage.size.height/2))
        leftNavButton.setBackgroundImage(backImage, forState: .Normal)
        leftNavButton.addTarget(self, action: "popUpViewController", forControlEvents:UIControlEvents.TouchUpInside)
        let barButton = UIBarButtonItem(customView: leftNavButton)
        self.navigationItem.leftBarButtonItem = barButton
        
        centerViewHeightConstraint.constant         = 394 - 45
//        if UIDevice().modelName == "iPhone 6" {
//            bottomEditButtonConstraint.constant = 17 - 36 - 47
//        }
//        
//        if UIDevice().modelName == "iPhone 6 Plus" {
//            bottomEditButtonConstraint.constant = 17 - 105 - 47
//        }
//        
//        if UIDevice().modelName == "iPhone 5" {
//            bottomEditButtonConstraint.constant = 17 + 17
//            Scroller.scrollEnabled = false
//        }
        
        if DeviceType.IS_IPHONE_5 {
            bottomEditButtonConstraint.constant = 17 + 17
            Scroller.scrollEnabled = false
        }
        
        if DeviceType.IS_IPHONE_6 {
            bottomEditButtonConstraint.constant = 17 - 36 - 47
        }
        
        if DeviceType.IS_IPHONE_6P {
            bottomEditButtonConstraint.constant = 17 - 105 - 47
        }
//        
        //Foreigner
        //passData["NATION_GB"] == "1"
//        if passData!["NATION_GB"] as! String == "1"{
        
//        let user = 0
//        if user == 1 {
        if passData!["NATION_GB"] as! String == "1"{
            heightLabelNameEngConstraint.constant       = 18 - 44
            heightTextFieldNameEngConstraint.constant   = 18 - 44
            nameKOLabel.hidden      = true
            nameKOResult.hidden     = true
            heightLine4Constraint.constant  = 44 - 46
            
        }
        else {
            
            heightLabelPromotionCodeConstraint.constant = 18 - 44
            heightTextFieldPromoCodeConstraint.constant = 18 - 44
            expireDateLabel.hidden      = true
            expireDateTextField.hidden  = true
            
        }
        
        //DismissKeyboard
        let recognizer              = UITapGestureRecognizer(target: self, action:Selector("dismissKeyboard:"))
        view.addGestureRecognizer(recognizer)
        
    }
    
    // -------------------------------------------------------------------------------
    // setResultToView
    // -------------------------------------------------------------------------------
    func setResultToView() {
        
        
        if isKorean {
            //Korean
            self.passNoLabel.text       = Language.localizedStr("S13_1_08")
            
            //Format PassportNo and Social SecurityNumber
            let securityNo  = self.passData!["RESIDENT_NO"] as! NSString
            
            if securityNo.length < 6 {
                self.passNoTextField.text  = "\(securityNo)-"
            }else{
                let result      = securityNo.substringWithRange(NSRange(location: 0, length: 6))
                img6.hidden = false
                img7.hidden = false
                widthViewPwdConstraint.constant    = 88
                self.passNoTextField.text  = "\(result)-"
            }
            
        }
        else {
            //Foreigner
            self.passNoLabel.text       = Language.localizedStr("M03_2_07")
            

            let passportNo  = self.passData!["PASSPORT_NO"] as! NSString
            
            if passportNo.length < 5 {
                self.passNoTextField.text  = "\(passportNo)-"
                
            }else{
                let result1     = passportNo.substringWithRange(NSRange(location: 0, length: 5))
                img6.hidden = true
                img7.hidden = true
                widthViewPwdConstraint.constant    = 88 - 23
                self.passNoTextField.text  = "\(result1)-"
                
            }
            
        }
        
        //Fill data BirthDate into Textfield
        let stringDate = self.passData!["BIRTH_YMD"] as! NSString
        
        if stringDate.length < 8 {
            self.dateOfBirth.text = "\(self.passData!["BIRTH_YMD"])"
        }
        
        let year    = stringDate.substringWithRange(NSRange(location: 0, length: 4))
        let month   = stringDate.substringWithRange(NSRange(location: 4, length: 2))
        let day     = stringDate.substringWithRange(NSRange(location: 6, length: 2))
        
        //Fill data PassportExpireDate into Textfield
        let stringDate1 = self.passData!["PASSPORT_EXPRDT"] as! NSString
        
        if stringDate1.length < 8 {
            self.expireDateTextField.text = "\(self.passData!["PASSPORT_EXPRDT"])"
        }
        
        let year1    = stringDate1.substringWithRange(NSRange(location: 0, length: 4))
        let month1   = stringDate1.substringWithRange(NSRange(location: 4, length: 2))
        let day1     = stringDate1.substringWithRange(NSRange(location: 6, length: 2))
        
        self.nameKOResult.text          = "\(self.passData!["KOR_NM"]!)"
        self.nameENResult.text          = "\(self.passData!["CUST_NM"]!)"
        self.dateOfBirth.text           = "\(year).\(month).\(day)"
        self.country.text               = "\(self.passData!["NATION_NM"]!)"
        self.intl_Telecom.text          = "\(self.passData!["INTL_TELCD"]!)-"
//        self.phoneNoTextField.text      = "\(self.passData!["HP_NO"]!)"
        self.proCodeTextField.text      = "\(self.passData!["RCMD_ID"]!)"
        self.expireDateTextField.text   = "\(year1).\(month1).\(day1)"
        
        //Promotion code
        if self.passData!["RCMD_ID"]! as! String == "" {
            self.proCodeTextField.text   = "N\\A"
        }else{
            self.proCodeTextField.text   = "\(self.passData!["RCMD_ID"]!)"
        }
        
        //Set telecomCode
        if self.passData!["TELCOM_CD"] as! String == "001" {
            
            telecomView.selectedIndex = 0
            
        }else if self.passData!["TELCOM_CD"] as! String == "002" {
            
            telecomView.selectedIndex = 1
            
        }else if self.passData!["TELCOM_CD"] as! String == "003" {

            telecomView.selectedIndex = 2
            
        }else {

            telecomView.selectedIndex = 3
        }

        //First set up, set Keyboard alert
        self.telecomCode =  self.passData!["TELCOM_CD"] as? String
//        phoneNoTextField.becomeFirstResponder()
//        TextFieldMoveUp(true, txtfield: phoneNoTextField)
        
    }
    
    // -------------------------------------------------------------------------------
    // PopupViewController
    // -------------------------------------------------------------------------------
    func popUpViewController() {
        navigationController?.popViewControllerAnimated(true)
    }
    
    //MARK: - View Life Cycle
    // -------------------------------------------------------------------------------
    // ViewDidLoad
    // -------------------------------------------------------------------------------
    override func viewDidLoad() {
        super.viewDidLoad()
        
        /////////// USE THIS FORMAT
        let transaction = TransactionClass()
        transaction.sendAPI(APICode.CODE_R001, argument: ["" : ""], success: { (success) -> Void in
            
            if let rslt_cd = success["RSLT_CD"] as? String {
                
                if rslt_cd != "0000" { //오류
                    if let rsltMsg = success["RSLT_MSG"] as? String {
                        
                        print("Error Code : \(rslt_cd), Error Message : \(rsltMsg)")
                    }
                } else { //정상
                    if let respData = success["RESP_DATA"] as? NSDictionary {
                        
                        self.responseCode_R001  = respData["TELCOM_REC"] as? NSArray
                        
                        //Telecom
                        for index in 4...self.responseCode_R001!.count {
                            
                            self.arrayTelecom.append((self.responseCode_R001![index - 1]["TELCOM_NM"] as? String)!)
                            
                        }
                        
                        self.arrayTelecom.append("\(Language.localizedStr("C09P_02"))")
                        self.telecomView.items                   = self.arrayTelecom
                        self.telecomView.font                    = UIFont.systemFontOfSize(12.0)
                        
                    }
                    
                }
            }

        })
        
        //Localize
        localize()
        
        //Customizeview
        customizeViewLayout()
        
        //setResultToView
        setResultToView()
        
    }
    
    // -------------------------------------------------------------------------------
    // viewWillAppear
    // -------------------------------------------------------------------------------
    override func viewWillAppear(animated: Bool) {
        
        //Check responseData nil
        if passData == nil {
            return
        }
        
        //Set isKorean true/false
        (passData!["NATION_GB"]! as! String != "0") ? (isKorean = false) : (isKorean = true)
        
        //SetResultToView
        setResultToView()

    }
    
    //MARK: - UITextFieldDelegate
    // -------------------------------------------------------------------------------
    // textFieldShouldReturn
    // -------------------------------------------------------------------------------
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        phoneNoTextField.resignFirstResponder()
        return true
    }
    
    // -------------------------------------------------------------------------------
    // textFieldDidBeginEditing
    // -------------------------------------------------------------------------------
    func textFieldDidBeginEditing(textField: UITextField) {
        if textField == phoneNoTextField {
            TextFieldMoveUp(true, txtfield: phoneNoTextField)
        }
    }
    
    // -------------------------------------------------------------------------------
    // textFieldDidEndEditing
    // -------------------------------------------------------------------------------
    func textFieldDidEndEditing(textField: UITextField) {
        if textField == phoneNoTextField {
            TextFieldMoveUp(false, txtfield: phoneNoTextField)
        }
    }
    
    
    // -------------------------------------------------------------------------------
    // shouldChangeCharactersInRange: For max length in textfield
    // -------------------------------------------------------------------------------
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        
        //Max Length
        guard let text = textField.text else { return true }
        
        let newLength = text.characters.count + string.characters.count - range.length
        
        if textField == phoneNoTextField {
            
            return newLength <= 13
        }

        return true
    }
    
    // -------------------------------------------------------------------------------
    // dismissKeyboard
    // -------------------------------------------------------------------------------
    func dismissKeyboard(recognizer: UITapGestureRecognizer) {
        phoneNoTextField.resignFirstResponder()
    }
    
    // MARK: - Text Feild Animation Up
    // -------------------------------------------------------------------------------
    // TextFieldMoveUp
    // -------------------------------------------------------------------------------
    func TextFieldMoveUp(move:Bool , txtfield:UITextField) {
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDelegate(self)
        UIView.setAnimationDuration(0.2)
        UIView.setAnimationBeginsFromCurrentState(true)
        var rect = self.view.frame
        if(move){
            
            if DeviceType.IS_IPHONE_4_OR_LESS {
                rect.origin.y -= 70.0
            }
            
            if DeviceType.IS_IPHONE_5{
                rect.origin.y -= 40.0
            }
        }else{
            
            if DeviceType.IS_IPHONE_4_OR_LESS {
                rect.origin.y += 70.0
            }
            
            if DeviceType.IS_IPHONE_5{
                rect.origin.y += 40.0
            }
        }
        
        self.view.frame=rect
        UIView.commitAnimations()
    }
    
    
    // MARK: - Navigation
    // -------------------------------------------------------------------------------
    // In a storyboard-based application, you will often want to do a little
    // preparation before navigation
    // -------------------------------------------------------------------------------
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if segue.identifier == "ShowEdited" {
            
            if sender == nil {
                return
            }
            
            let editedInfo              = segue.destinationViewController as! EditedInfoManagerViewController
            editedInfo.passData         = sender as? NSDictionary
            editedInfo.phoneString      = phoneEdited
        }
        
    }

}
