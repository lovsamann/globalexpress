//
//  Person.swift
//  GlobalExpress
//
//  Created by Ann on 1/25/16.
//  Copyright © 2016 webcash. All rights reserved.
//

import Foundation


protocol Person {
    var name: String? { get }
    var gender: String? { get }
    var address: String? { get }
}