//
//  LeftPopupView.swift
//  GlobalExpress
//
//  Created by JangWooil on 2016. 3. 21..
//  Copyright © 2016년 webcash. All rights reserved.
//

import UIKit

class LeftPopupView: UIView {

    @IBOutlet var popupTitle: UILabel!
    @IBOutlet var popupCloseBtn: UIButton!
    @IBOutlet var popupLabel1: UILabel!
    @IBOutlet var popupNoBtn: UIButton!
    @IBOutlet var popupYesBtn: UIButton!
    
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */

}
