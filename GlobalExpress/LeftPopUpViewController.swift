//
//  LeftPopUpViewController.swift
//  GlobalExpress
//
//  Created by Ralex on 3/19/16.
//  Copyright © 2016 webcash. All rights reserved.
//

import UIKit

class LeftPopUpViewController: MJPopupViewController {
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.layer.cornerRadius = 6
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func ActionDismiss(sender: AnyObject) {
        self.dismissPopupViewController(self, animationType: MJPopupViewAnimationFade)
    }
    
    
    @IBAction func ACtionConfirm(sender: AnyObject) {
        self.dismissPopupViewController(self, animationType: MJPopupViewAnimationFade)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
