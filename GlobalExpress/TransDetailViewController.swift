
//
//  TransDetailViewController.swift
//  GlobalExpress
//
//  Created by Ralex on 2/9/16.
//  Copyright © 2016 webcash. All rights reserved.
//

import UIKit

class TransDetailViewController: UIViewController,UITableViewDataSource,UITableViewDelegate{

    var tranDetail = Dictionary<String,String>()
    var navigationTitle: String?
    let numOfSection:Int = 3
    
    @IBOutlet weak var tableView: UITableView!
    
    var contents: [[[String:String!]]]!
    
    func initLoad() -> Void{
        //Initialize cell in section
        navigationTitle = Language.localizedStr("S08_01")
        RLib.setNavigationTitle(self, title: navigationTitle!)
        
        
        let rightNavigationImage = UIImage(named: "title_close_icon.png")
        let rightNavigationButton = UIButton(frame: CGRectMake(0,0, (rightNavigationImage?.size.width)! / 2, (rightNavigationImage?.size.height)! / 2 ))
        rightNavigationButton.setBackgroundImage(rightNavigationImage, forState: .Normal)
        rightNavigationButton.addTarget(self, action: "rigthNavigationClicked", forControlEvents: .TouchUpInside)
        
        let rightBarButtonItem = UIBarButtonItem(customView: rightNavigationButton)
        self.navigationItem.rightBarButtonItem = rightBarButtonItem
        
        self.navigationItem.hidesBackButton = true
        
        print("transc \(tranDetail)")
        
        self.contents = loadContent(tranDetail)
        print("vvvvv \(self.contents)")
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initLoad()
        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController!.navigationBar.translucent = false
     
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController!.navigationBar.translucent = true
    }
    
    func handleLeftNavigationItemClick(sender:UIBarButtonItem){
        navigationController?.popViewControllerAnimated(true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK: - UITableViewDataSource
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return self.contents.count
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //content[section] 
        //content[0] -> numberOfRowofSection 1
        //content[1] -> numberOfRowofSetion 5
        print("row row \(self.contents[section].count)")
        print("row ---- row \(self.contents[section])")
        return self.contents[section].count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cellIdentifier = self.contents[indexPath.section][indexPath.row]["CellIdentifier"]!
        
        print("ttttt \(cellIdentifier)")
        
        let cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier)
//
        let dic = self.contents[indexPath.section][indexPath.row]
        
        if cell is ProfileTableViewCell {
            let profileCell = cell as! ProfileTableViewCell
            profileCell.nameLabel.text = dic["REC_NM"]!
            profileCell.nationalLabel.text = dic["RCP_NATION"]!
            profileCell.telLabel.text = dic["RCP_TEL"]!
            profileCell.emailLabel.text = dic["E_MAIL"]!
            switch Int(dic["Sat"]!)! {
                case 0,1,3:
                    profileCell.statusImageView?.image = UIImage(named: "st_waiting_bg.png")
                    profileCell.statusLabel.text = "Waiting"
                case 2:
                    profileCell.statusImageView?.image = UIImage(named: "st_depo_bg.png")
                    profileCell.statusLabel.textColor = UIColor(red: 229 / 255.0 , green: 116 / 255.0, blue: 91 / 255.0, alpha: 1.0)
                    profileCell.statusLabel.text = "Deposited"
                default:
                    profileCell.statusImageView.image = UIImage(named: "st_cancel_bg.png")
                    profileCell.statusLabel.textColor = UIColor(red: 88 / 255.0 , green: 106 / 255.0, blue: 116 / 255.0, alpha: 1.0)
                    profileCell.statusLabel.text = "Cancelled"
            }
        } else if cell is SimpleTableViewCell {
            let simpleCell = cell as! SimpleTableViewCell
            simpleCell.leftLabel.text = dic["localText"]!
            simpleCell.rightLabel.text = dic["localValue"]!
        }else if cell is AccountTableViewCell {
            let accountCell = cell as! AccountTableViewCell
            accountCell.fxTextLabel.text = dic["localText"]
            accountCell.fxValueLabel.text = dic["localValue"]
            accountCell.infoCellLabel.text = dic["info"]
            
        }else if cell is MoneyTableViewCell {
            let moneyCell = cell as! MoneyTableViewCell
            moneyCell.fxTextLabel.text = dic["sendingText"]
            moneyCell.fxValueLabel.text = dic["sendingValue"]
            moneyCell.fxRecevingTextLabel.text = dic["receivingText"]
            moneyCell.fxRecevingValueLabel.text = dic["receivingValue"]
        }else if cell is ServiceFeeTableViewCell {
            let serviceCell = cell as! ServiceFeeTableViewCell
            serviceCell.fxTextLabel.text = dic["exchangeRateText"]
            serviceCell.fxValueLabel.text = dic["exchangeRateValue"]
            serviceCell.fxSendingFeeTextLabel.text = dic["sendingFeeText"]
            serviceCell.fxSendingFeeValueLabel.text = dic["sendingFeeValue"]
            serviceCell.info.text = dic["info"]
            if serviceCell.fxActualPayTextLabel != nil {
                serviceCell.fxDiscountTextLabel.text = dic["DiscountText"] == nil ? "" : dic["DiscountText"]
                serviceCell.fxDiscountValueLabel.text = dic["DiscountValue"] == nil ? "" : dic["DiscountValue"]
                serviceCell.fxActualPayTextLabel.text = dic["actalSendingFeeText"] == nil ? "" : dic["actalSendingFeeText"]
                serviceCell.fxAcutalPayValueLabel.text = dic["actualSendingFeeValue"] == nil ? "" : dic["actualSendingFeeValue"]
            }
            

        }else if cell is PaymentPeriodTableViewCell {
            let paymentCell = cell as! PaymentPeriodTableViewCell
            paymentCell.periodInfoLabel.text = dic["paymentPeriod"]
            paymentCell.cancelButton.setTitle(dic["cancel"], forState: .Normal)
            paymentCell.infolabel.text = dic["period"]

            let type = tranDetail["TX_STS"]
            
            if type == "0" || type == "1" || type == "3" {
                paymentCell.cancelButton.hidden = false
            }else {
                paymentCell.cancelButton.hidden = true
            }
            
            paymentCell.cancelButton.addTarget(self, action: "cancelClicked:", forControlEvents: .TouchUpInside)
            
        }
        
        return cell!
    }
    
    
    //MARK: - UITableViewDelegate

    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        
        let height = Float(self.contents[indexPath.section][indexPath.row]["HeaderViewHieght"]!)
        
        if let height = height {
            return CGFloat(height)
        }else{
            return 0.0
        }
        
//        return CGFloat(height!)
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {        
        
        let headerView = UIView(frame: CGRectZero)
        headerView.backgroundColor = UIColor.clearColor()
        
        let leftLabel = makeLabel(CGRectMake(17,14,CGRectGetWidth(tableView.frame) / 2, 11), alignment: .Left)
        let leftText = self.contents[section][0]["LeftHeaderViewText"]!
        
        leftLabel.text = leftText
        leftLabel.backgroundColor = UIColor.clearColor()

        
        headerView.addSubview(leftLabel)

        let rigthLabel = makeLabel(CGRectMake(CGRectGetWidth(leftLabel.frame),14,(CGRectGetWidth(tableView.frame) / 2) - 17 , 11), alignment: .Right)
        
        let rightText = self.contents[section][0]["RigthHeaderViewText"]!
        rigthLabel.text = rightText == nil ? "" : rightText
        rigthLabel.backgroundColor = UIColor.clearColor()
        
        headerView.addSubview(rigthLabel)
        
        
        
        let headerImage = UIImage(named: self.contents[section][0]["CellIdentifier"] == CELLIDENTIFIER.PROFILE_CELL ? "con_rad_top03.png" : "con_rad_top01.png")
        let headerImageView = UIImageView(image: headerImage)
        headerImageView.frame = CGRectMake(17, CGRectGetHeight(leftLabel.frame) + CGRectGetMinY(leftLabel.frame) + 9, CGRectGetWidth(tableView.frame) - 17 - 17, 9)
        headerView.addSubview(headerImageView)
        
        return headerView
        
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footerView = UIView(frame: CGRectZero)
        footerView.backgroundColor = UIColor.clearColor()
        let footerImageView = UIImageView(image: UIImage(named:"con_rad_bottom01.png"))
        footerImageView.frame = CGRectMake(17, 0, CGRectGetWidth(tableView.frame) - 17 - 17, 9)
        
        let clearSeperatorView = UIView(frame: CGRectMake(18, -2 , CGRectGetWidth(tableView.frame) - 18 - 18, 1))
        clearSeperatorView.backgroundColor = UIColor.whiteColor()
        
        footerView.addSubview(footerImageView)
        footerView.addSubview(clearSeperatorView)
        return footerView
    }
    
    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 9.0
    }
    
    //MARK: - Private metods
    
    func makeLabel(frame: CGRect, alignment: NSTextAlignment) -> UILabel{
        let label = UILabel(frame: frame)
        label.font = UIFont.systemFontOfSize(12)
        label.textAlignment = alignment
        label.textColor = UIColor(red: 48 / 255.0, green: 50 / 255.0, blue: 51 / 255.0, alpha: 1.0)
        
        return label
    }
    
    func loadProfile(dic: [String:String]) -> [[String:String!]]{
        let formatter = NSDateFormatter()
        formatter.dateFormat = "yyyyMMddHHmmss"
        let date = formatter.dateFromString(dic["TX_DT"]! + dic["TX_TM"]!)
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let dateToString = formatter.stringFromDate(date!)
        
        
        return [["REC_NM" : dic["RCP_NAME"],
                "RCP_NATION": dic["RCP_NATION"],
                "RCP_TEL" : dic["RCP_TEL"],
                "E_MAIL" : dic["E_MAIL"],
                "Sat": dic["TX_STS"],
                "HeaderViewHieght": "84",
                "CellIdentifier": CELLIDENTIFIER.PROFILE_CELL,
                "LeftHeaderViewText" : Language.localizedStr("S08_07"),
                "RigthHeaderViewText" : dateToString,
                "TX_STS_R001" : dic["TX_STS_R001"]]]
    }
    
    func loadPayments(dic: [String:String]) -> [[String:String!]] {
        let type = dic["TX_STS"]!
        let sendingCurrency = dic["REMI_CURRCD"]!
        let recevingCurrency = dic["RCP_CURRCD"]
        let fxRate = dic["FX_RATE"]?.formatKRWCurrency()
        let remiAmount = dic["REMI_AMT"]?.formatKRWCurrency()
        let remiFee = dic["REMI_FEE"]?.formatKRWCurrency()
        let dis = dic["FEE_DISAMT"]?.formatKRWCurrency()
        let payFee = dic["PAY_FEE"]?.formatKRWCurrency()
        
        let payment = [ ["LeftHeaderViewText" : type == "0" || type == "1" || type == "3" ? Language.localizedStr("S08_02") : "Payment Detail",
                            "RigthHeaderViewText" : "",
                            "localText" : Language.localizedStr("S08_03"),
                            "localValue" : type == "0" || type == "1" || type == "3" ? dic["REC_TXTM"] : Language.localizedStr("S07_03"),
                            "CellIdentifier" : CELLIDENTIFIER.SIMPLE_CELL,
                            "HeaderViewHieght" : "44"],
                            ["localText" : Language.localizedStr("S08_04"),
                                "localValue" : dic["REC_BANK"],
                                "CellIdentifier" : CELLIDENTIFIER.SIMPLE_CELL,
                                "HeaderViewHieght" : "44"],
                            ["localText" : Language.localizedStr("S08_05"),
                                "localValue" : dic["REC_ACCT"],
                                "CellIdentifier" : CELLIDENTIFIER.SIMPLE_CELL,
                                "HeaderViewHieght" : "44"],
            
                            ["localText" : Language.localizedStr("S08_06"),
                                "localValue" : remiAmount! + sendingCurrency,
                                "CellIdentifier" : CELLIDENTIFIER.SIMPLE_CELL,
                                "HeaderViewHieght" : "44"],
                        ]
        
        
        
        
        
        
        let dic1 = ["LeftHeaderViewText" : Language.localizedStr("S08_37"),
            "RigthHeaderViewText" : "",
            "localText" : Language.localizedStr("S08_38"),
            "localValue" : "1 USD="  + fxRate! +  sendingCurrency,
            "CellIdentifier" : CELLIDENTIFIER.SIMPLE_CELL,
            "HeaderViewHieght" : "44"]
        
        let dic2 = ["localText" : Language.localizedStr("S08_39"),
            "localValue" : remiAmount! + sendingCurrency,
            "CellIdentifier" : CELLIDENTIFIER.SIMPLE_CELL,
            "HeaderViewHieght" : "44"]
        
        let dic3 = ["localText" : Language.localizedStr("S08_40"),
            "localValue" : remiFee! + sendingCurrency,
            "CellIdentifier" : CELLIDENTIFIER.SIMPLE_CELL,
            "HeaderViewHieght" : "44"]
       
        let dic4 = ["localText" : Language.localizedStr("S08_41"),
            "localValue" : dis! + sendingCurrency,
            "CellIdentifier" : CELLIDENTIFIER.SIMPLE_CELL,
            "HeaderViewHieght" : "44"]
        
        let dic5 = ["localText" : Language.localizedStr("S08_42"),
            "localValue" : payFee! + sendingCurrency,
            "CellIdentifier" : CELLIDENTIFIER.SIMPLE_CELL,
            "HeaderViewHieght" : "44"]
        
        let dic6 = ["localText" : Language.localizedStr("S08_43"),
            "localValue" : (dic["RCP_AMT"]?.formatUSDCurrency())! + recevingCurrency! ,
            "CellIdentifier" : CELLIDENTIFIER.SIMPLE_CELL,
            "HeaderViewHieght" : "44"]
        
        let resultPayment = [ dic1, dic2 , dic3 , dic4, dic5, dic6]
       
        
        return dic["TX_STS"]! == "2" ? resultPayment : payment
//        return payment
    }
    
    func loadCashPickup(data: [String:String]) -> [[String:String!]] {
        return [ ["LeftHeaderViewText" : Language.localizedStr("S08_11"),
                    "RigthHeaderViewText" : "",
                    "localText" : Language.localizedStr("S08_12"),
                    "localValue" : data["RCP_CPORG"],
                    "CellIdentifier" : CELLIDENTIFIER.SIMPLE_CELL,
                    "HeaderViewHieght" : "44"],
        
                 ["localText" : Language.localizedStr("S08_13"),
                    "localValue" : Language.localizedStr("S08_14"),
                     "info": Language.localizedStr("S08_15"),
                    "CellIdentifier" : CELLIDENTIFIER.SIMPLE_WITH_INFO_CELL,
                    "HeaderViewHieght" : "88"]
                ]
    }
    
    func loadBank(data: [String:String]) -> [[String:String!]] {
        
        let type = data["TX_STS"]
        
        var bank = [ ["LeftHeaderViewText" : Language.localizedStr("T03_05"),
            "RigthHeaderViewText" : "",
            "localText" : Language.localizedStr("S08_25"),
            "localValue" : data["RCP_BANK"],
            "CellIdentifier" : CELLIDENTIFIER.SIMPLE_CELL,
            "HeaderViewHieght" : "44"],
            
            ["localText" : Language.localizedStr("S08_26"),
                "localValue" : data["RCP_NAME"],
                "CellIdentifier" : CELLIDENTIFIER.SIMPLE_CELL,
                "HeaderViewHieght" : "44"],
            
            ["localText" : Language.localizedStr("S08_27"),
                "localValue" : data["RCP_ACCT"],
                "CellIdentifier" : CELLIDENTIFIER.SIMPLE_CELL,
                "HeaderViewHieght" : "44"]
        ]

        if type == "4" || type == "8"  || type == "9" {
            let cancelBankInfo: [String:String!] = [ "localText" : Language.localizedStr("S08_27"),
                "localValue" : data["RCP_ACCT"],
                "info": Language.localizedStr("T03_09"),
                "CellIdentifier" : CELLIDENTIFIER.SIMPLE_WITH_INFO_CELL,
                "HeaderViewHieght" : "88"]
            
            bank[2] = cancelBankInfo
        }
        
        return bank
    }
    
    func loadInfo(data: [String:String]) -> [[String:String!]] {

        let remitCurrency = data["REMI_CURRCD"]
        let receCurrency = data["RCP_CURRCD"]
        let remiAmount = data["REMI_AMT"]?.formatKRWCurrency()
        let recAmount = data["RCP_AMT"]?.formatUSDCurrency()
        let exchangeRate = data["FX_RATE"]?.formatKRWCurrency()
        let remiFee = data["REMI_FEE"]?.formatKRWCurrency()
        let dis = data["FEE_DISAMT"]?.formatKRWCurrency()
        let payFee = data["PAY_FEE"]?.formatUSDCurrency()
        
        var info: [[String:String!]] = [ [ "sendingText" : Language.localizedStr("S08_16") + " " + remitCurrency!,
                        "sendingValue" : remiAmount,
                        "receivingText" : Language.localizedStr("S08_17") + " " + receCurrency!,
                        "receivingValue" : recAmount,
                        "CellIdentifier" : CELLIDENTIFIER.MONEY_INFO_CELL,
                            "HeaderViewHieght" : "80"],
            
                    [ "exchangeRateText" : Language.localizedStr("S08_18"),
                        "exchangeRateValue" : "1.00 USD = " + exchangeRate! + remitCurrency!,
                        "sendingFeeText" : Language.localizedStr("S08_19"),
                        "sendingFeeValue" : remiFee! + remitCurrency!,
                        "info" : Language.localizedStr("S08_34"),
                        "CellIdentifier" : CELLIDENTIFIER.SERVICE_FEE_CELL,
                        "HeaderViewHieght" : "119"],
                    
                    [ "paymentPeriod" : Language.localizedStr("S08_36"),
                        "period": Language.localizedStr("S08_22") ,
                        "cancel" : Language.localizedStr("S08_21"),
                        "CellIdentifier" : CELLIDENTIFIER.PAYMENT_PERIOD_CELL,
                        "HeaderViewHieght" : "82"]
                ]
        
        
        if data["TX_STS"]! == "2" {
            let feeInfo: [String:String!] = [ "exchangeRateText" : Language.localizedStr("S08_18"),
                "exchangeRateValue" : exchangeRate! + remitCurrency! ,
                "sendingFeeText" : Language.localizedStr("S08_19"),
                "sendingFeeValue" : remiFee! + remitCurrency!,
                "DiscountText" : Language.localizedStr("S08_32"),
                "DiscountValue" : dis! + remitCurrency!,
                "actalSendingFeeText" : Language.localizedStr("S08_33"),
                "actualSendingFeeValue" : payFee! + remitCurrency!,
                "info" : Language.localizedStr("S08_34"),
                "CellIdentifier" : CELLIDENTIFIER.SERVICE_FEE_DISCOUNT_CELL,
                "HeaderViewHieght" : "180"]
            
            info[1] = feeInfo
        }
        
        return info
    }
    
    private func loadContent(data: [String:String]) -> [[[String: String!]]]!{
        
        var section = [[[String:String!]]]()
        
        
        //section 0
        let payments = loadPayments(data)
        
        //section 1
        let profile = loadProfile(data)
        
        //section 2
        let cashPickup:[[String:String!]] = loadCashPickup(data)
        let bank = loadBank(data)
        
        let info:[[String:String!]] = loadInfo(data)
        
        var merge:[[String:String!]]
        
        if let type:String = data["RCPT_GB"] where type == "1" { //1: bank 2: cash pick up
            merge = bank + info
        }else {
            merge = cashPickup + info
        }
        
        if let type = Int(data["TX_STS"]!) {
            switch type {
            case 0,1,3: //waiting
                print("waiting")
                section = [payments,profile,merge]
            default: //cancelled
                section = [ profile, merge, payments]
                print("defautl")
            }

        }

        return section

    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    //MARK: - Actions
    
    @IBAction func comfirmClicked(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    func rigthNavigationClicked() {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    func cancelClicked(sender: UIButton) {
        
        guard let orderno = self.tranDetail["ORDER_NO"] else{
            self.showEventAlert("Order no nil", hasCancelButton: false, delegate: self, tag: 9000000, confirmBlock: nil, cancelBlock: nil)
            return
        }
        
        self.showEventAlert(Language.localizedStr("S08_44"), hasCancelButton: true, delegate: self, tag: 29000, confirmBlock: { (string) -> Void in
            
            TransactionClass().sendAPI(APICode.HISTORY_D001 , argument: ["ORDER_NO" : orderno], success: { (success) -> Void in
                
                if let data = success["RSLT_CD"] where data as! String == "0000" {
                    
                    self.navigationController?.popViewControllerAnimated(true)
                }else{
                    self.showEventAlert(success["RSLT_MSG"] as! String, hasCancelButton: false, delegate: self, tag: 8000000, confirmBlock: nil, cancelBlock: nil)
                }
            })
            
            }, cancelBlock: { (string) -> Void in
        })
    }
    
    private struct CELLIDENTIFIER {
        static let PROFILE_CELL = "profileCell"
        static let SIMPLE_CELL = "simpleCell"
        static let SIMPLE_WITH_INFO_CELL = "simpleInfoCell"
        static let MONEY_INFO_CELL = "moneyCell"
        static let SERVICE_FEE_CELL = "serviceFeeCell"
        static let PAYMENT_PERIOD_CELL = "paymentPeriodCell"
        static let SERVICE_FEE_DISCOUNT_CELL = "serviceCashPickupCell"
    }
}

extension Dictionary {
    mutating func merge<K,V>(dic: [K:V]) {
        for (k, v) in dic {
            self.updateValue(v as! Value, forKey: k as! Key)
        }
    }
}
