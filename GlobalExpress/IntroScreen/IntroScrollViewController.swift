//
//  IntroScrollViewController.swift
//  GlobalExpress
//
//  Created by Ann on 1/6/16.
//  Copyright © 2016 Unemployment. All rights reserved.
//

import UIKit

class IntroScrollViewController: UIViewController,UIScrollViewDelegate {

    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet var pageControl: UIPageControl!
    @IBOutlet var pageControlButtomConstraint: NSLayoutConstraint!
    
    var viewControllers: [IntroContentViewController?]?
    var contentList: [[String:String!]]?
    var clientLangCd = "" //클라이언트 언어코드
    
    //Button
    @IBAction func popupClose(sender: AnyObject) {
        
        //그냥 팝업 종료할 경우 영어로 기본세팅 한다
        //사용자 언어 확인
        NSUserDefaults.standardUserDefaults().setObject(LanguageCode().English, forKey: "UserSavedLanguage")
        NSUserDefaults.standardUserDefaults().synchronize()
    }
    
    //MARK: view lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //load content
        self.contentList = self.loadContent()
        
        var controllers = [IntroContentViewController?]()
        
        for _ in self.contentList! {
            controllers.append(nil)
        }
        
        self.viewControllers = controllers
        self.scrollView.scrollsToTop = false
        
        //set page controll
        pageControl.numberOfPages = (self.contentList?.count)!
        pageControl.currentPage = 0
        
        localize()
        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        //adjust button constraint
        let deviceType = UIDevice().modelName
        
        if deviceType == "iPhone 4" || deviceType == "iPhone 4s" {
//            pageControlButtomConstraint.constant = 60
        }else{
//            pageControlButtomConstraint.constant = 176
        }
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        self.scrollView.contentSize = CGSizeMake(CGRectGetWidth(scrollView.frame) * CGFloat((self.contentList?.count)!) , CGRectGetHeight(scrollView.frame))
 
        //load page
        loadScrollViewWithPage(0)
        loadScrollViewWithPage(1)
        self.view.layoutIfNeeded()
    }
    
    //MARK: private methods
    func loadContent() -> [[String:String!]] {
        let firstIntro = ["image" : "intro_sf_icon.png",
                           "mainTitle": Language.localizedStr("S02_01") ,
                            "subTitle" : Language.localizedStr("S02_03")]
        let secondIntro = ["image" : "intro_lc_icon.png",
            "mainTitle": Language.localizedStr("S03_01"),
            "subTitle" : Language.localizedStr("S03_02")]
        let thridIntro = ["image" : "intro_es_icon.png",
            "mainTitle": Language.localizedStr("S04_01"),
            "subTitle" : Language.localizedStr("S04_02")]
        
        return [firstIntro,secondIntro,thridIntro]
    }
    
    func loadScrollViewWithPage(page: Int){
        if page >= self.contentList?.count || page < 0{
            return
        }
        
        
        var introScreenVC = self.viewControllers![page]
        if introScreenVC == nil {
            introScreenVC = self.storyboard?.instantiateViewControllerWithIdentifier("IntroContentViewController") as? IntroContentViewController
            self.viewControllers![page] = introScreenVC
        }
        
        if introScreenVC?.view.superview == nil {
            var scrollviewRect = scrollView.frame
            scrollviewRect.origin.x = CGRectGetWidth(scrollviewRect) * CGFloat(page)
            scrollviewRect.origin.y = 0;
            introScreenVC?.view.frame = scrollviewRect
            
            self.addChildViewController(introScreenVC!)
            self.scrollView.addSubview(introScreenVC!.view)
            introScreenVC?.didMoveToParentViewController(self)
        }
        
        
        //set value 
        let dic = self.contentList![page]
        introScreenVC?.mainImageView.image = UIImage(named: dic["image"]!)
        introScreenVC?.mainTitleLabel.text = dic["mainTitle"]
        introScreenVC?.subTitleLabel.text = dic["subTitle"]
    }
    
    //MARK: - UIScrollViewDelegate
    func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
        let width = CGRectGetWidth(self.scrollView.frame)
        let page:Int = Int(floor((self.scrollView.contentOffset.x - width / 2) / width)) + 1
        
        pageControl.currentPage = page
        
        loadScrollViewWithPage(page - 1)
        loadScrollViewWithPage(page)
        loadScrollViewWithPage(page + 1)
        
    }
    
}
