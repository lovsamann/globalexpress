//
//  IntroContentViewController.swift
//  GlobalExpress
//
//  Created by Ann on 1/6/16.
//  Copyright © 2016 Unemployment. All rights reserved.
//

import UIKit


class IntroContentViewController: UIViewController {

    
    @IBOutlet var mainImageView: UIImageView!
    @IBOutlet var mainTitleLabel: UILabel!
    @IBOutlet var subTitleLabel: UILabel!
    @IBOutlet var topContrants: NSLayoutConstraint!
    private var page: Int?
    
    
    //MARK: - init()
    
//    init(pageNumber: NSInteger) {
//        page = pageNumber
//        
//        var introScreen = UIStoryboard(name: "IntroScreen", bundle: nil)
////        let
//        
//        
////        super.init(nibName: nil, bundle: nil)
//    }
//
//    required init?(coder aDecoder: NSCoder) {
//        fatalError("init(coder:) has not been implemented")
//    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
    }
    
    @IBAction func kickOffClicked(sender: UIButton) {
        print("self.presenting \(self.presentingViewController)")
        if (self.presentingViewController != nil) {
            NSNotificationCenter.defaultCenter().postNotificationName("introEnded", object: nil, userInfo: nil)
            dismissViewControllerAnimated(true, completion: nil)
        }

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
