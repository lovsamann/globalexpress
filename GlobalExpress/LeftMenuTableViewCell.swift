//
//  LeftMenuTableViewCell.swift
//  GlobalExpress
//
//  Created by JangWooil on 2016. 1. 13..
//  Copyright © 2016년 webcash. All rights reserved.
//

import Foundation
import UIKit

class LeftMenuTableViewCell : UITableViewCell {
    
    @IBOutlet var menuName: UILabel!
    @IBOutlet var menuIcon: UIImageView!
    @IBOutlet var flagImage: UIImageView!
    
}