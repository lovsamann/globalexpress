//
//  SignupViewController.swift
//  GlobalExpress
//
//  Created by UDAM on 1/25/16.
//  Copyright © 2016 webcash. All rights reserved.
//

import UIKit

class SignupViewController: UIViewController {
    
    //MARK: - IBOutlet
    // -------------------------------------------------------------------------------
    //	IBOutlet label and button , Constraint
    // -------------------------------------------------------------------------------
    @IBOutlet weak var stepView: UIView!
    @IBOutlet weak var stepTitleLabel: UILabel!
    @IBOutlet weak var topTitle: UILabel!
    @IBOutlet weak var signupLabel: UILabel!
    @IBOutlet weak var setupLabel: UILabel!
    @IBOutlet weak var confirmLabel: UILabel!
    @IBOutlet weak var fundLabel: UILabel!
    @IBOutlet weak var nextButton: UIButton!
    
    // -------------------------------------------------------------------------------
    // NSLayoutConstraint
    // -------------------------------------------------------------------------------
    @IBOutlet weak var splitRightConstraint         : NSLayoutConstraint!
    @IBOutlet weak var splitLeftConstraint          : NSLayoutConstraint!
    @IBOutlet weak var centerViewHeightConstraint   : NSLayoutConstraint!
    
    // MARK: - Private function
    // -------------------------------------------------------------------------------
    //	localize project
    // -------------------------------------------------------------------------------
    override func localize() {
        
        self.navigationItem.title   = Language.localizedStr("M01_01")//Sign Up
        self.stepTitleLabel.text    = "1. \(Language.localizedStr("M01_01"))"
        self.topTitle.text          = Language.localizedStr("M01_02")//Send money in 4 easy steps
        self.signupLabel.text       = Language.localizedStr("M01_03")//Sign up
        self.setupLabel.text        = Language.localizedStr("M01_04")//set up recipient
        self.confirmLabel.text      = Language.localizedStr("M01_05")//confirm
        self.fundLabel.text         = Language.localizedStr("M01_06")//fund transfer
        self.nextButton.setTitle(Language.localizedStr("M01_07"), forState: .Normal)//Next
        
    }
    
    // -------------------------------------------------------------------------------
    // Customize View Layout
    // -------------------------------------------------------------------------------
    func customizeViewLayout(){
        
        stepView.layer.cornerRadius = 5.0
        
//        if UIDevice().modelName == "iPhone 4" || UIDevice().modelName == "iPhone 4s" {
//            centerViewHeightConstraint.constant = 82 - 29
//            splitRightConstraint.constant       = 58 + 7
//            splitLeftConstraint.constant        = 167 + 8
//        }
//        
//        if UIDevice().modelName == "iPhone 6" {
//            splitRightConstraint.constant       = 58 + 18
//            splitLeftConstraint.constant        = 167 + 16
//        }
//        
//        if UIDevice().modelName == "iPhone 6 Plus" {
//            splitRightConstraint.constant       = 58 + 29
//            splitLeftConstraint.constant        = 167 + 28
//        }
        
        if DeviceType.IS_IPHONE_4_OR_LESS {
            centerViewHeightConstraint.constant = 82 - 66
            splitRightConstraint.constant       = 58
            splitLeftConstraint.constant        = 167
        }
        
        if DeviceType.IS_IPHONE_6 {
            splitRightConstraint.constant       = 58 + 18
            splitLeftConstraint.constant        = 167 + 16
        }
        
        if DeviceType.IS_IPHONE_6P {
            splitRightConstraint.constant       = 58 + 29
            splitLeftConstraint.constant        = 167 + 28
        }
        
        //Customize navigation item ,barButton
        let backImage = UIImage(imageLiteral: "back_btn")
        let leftNavButton = UIButton(frame: CGRectMake(269.0, 20.0, backImage.size.width/2, backImage.size.height/2))
        leftNavButton.setBackgroundImage(backImage, forState: .Normal)
        leftNavButton.addTarget(self, action: "popUpViewController", forControlEvents:UIControlEvents.TouchUpInside)
        let barButton = UIBarButtonItem(customView: leftNavButton)
        self.navigationItem.leftBarButtonItem = barButton
        
    }
    
    // -------------------------------------------------------------------------------
    // PopupViewController
    // -------------------------------------------------------------------------------
    func popUpViewController() {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    // MARK: - View Life Cycle
    // -------------------------------------------------------------------------------
    //	viewDidLoad
    // -------------------------------------------------------------------------------
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //localizing
        localize()
        
        //Customize View Layout
        customizeViewLayout()
    }
    
    // MARK: - Navigation
    // -------------------------------------------------------------------------------
    //	prepareForSegue
    // -------------------------------------------------------------------------------
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if sender == nil {
            return
        }

        guard let totBankRecord: [AnyObject]    = sender!["TOTBANK_REC"] as? [AnyObject] else{
            #if _DEBUG_
                print("totBank nil.----->")
            #endif
            return
        }
        
        guard let telcomRecord: [AnyObject]     = sender!["TELCOM_REC"] as? [AnyObject] else{
            #if _DEBUG_
                print("telcom nil.----->")
            #endif
            return
        }
        
        guard let countryRecord: [AnyObject]    = sender!["NATION_REC"] as? [AnyObject] else{
            #if _DEBUG_
                print("country nil.---->")
            #endif
            return
        }
        
        let ivvc = segue.destinationViewController as! IdentityVerificationViewController
//        ivvc.fxBankRecord   = sender!["FXBANK_REC"] as? [AnyObject]
        ivvc.totBankRecord  = totBankRecord
        ivvc.telcomRecord   = telcomRecord
        ivvc.countryRecord  = countryRecord
    }
    
    // MARK: - Handle Button
    // -------------------------------------------------------------------------------
    //	Click next will go to IdentityVericationViewController
    // -------------------------------------------------------------------------------
    @IBAction func nextClicked(sender: UIButton) {
        
        view.userInteractionEnabled = false
        
        ///////// USE THIS FORMAT
        let transaction = TransactionClass()
        transaction.sendAPI(APICode.CODE_R001, argument: ["" : ""],
            success: { (success) -> Void in
                
                if let rslt_cd = success["RSLT_CD"] as? String {
                    
                    if rslt_cd != "0000" { //오류
                        if let rsltMsg = success["RSLT_MSG"] as? String {
                            
                            print("Error Code : \(rslt_cd), Error Message : \(rsltMsg)")
                        }
                    } else { //정상
                        if let respData = success["RESP_DATA"] as? NSDictionary {
                            print("Success Code : \(rslt_cd), Data  : \(respData)")
                            
                            self.performSegueWithIdentifier("ShowIdentityVerification", sender: respData)
                        }
                        
                    }
                }
                
                self.view.userInteractionEnabled = true
                
        })
        
//        self.performSegueWithIdentifier("ShowIdentityVerification", sender: nil)
    }
    
}
