//
//  KeypadView.m
//  GlobalExpress
//
//  Created by JangWooil on 2016. 1. 18..
//  Copyright © 2016년 webcash. All rights reserved.
//

#import "KeypadView.h"
#import "TranskeyParam.h"
#import "TranskeyResult.h"
#import "Constants.h"

@interface KeypadView () {
    NSMutableArray *paramArray;
    
    BOOL isTranskeyShowing;
    BOOL isShowQwerty;
    BOOL isShowNumber;
    
    UIViewController *ownerViewController;
}

@property (nonatomic, retain) IBOutlet TransKeyView *mQwertyTransKeyView;
@property (nonatomic, retain) IBOutlet TransKeyView *mNumberTransKeyView;
@property (retain) NSData *secureKey;
@property (retain) NSString *qwertyChiperString;
@property (retain) NSString *numberChiperString;

@end

@implementation KeypadView

@synthesize mQwertyTransKeyView;
@synthesize mNumberTransKeyView;
@synthesize secureKey;
@synthesize qwertyChiperString;
@synthesize numberChiperString;

NSInteger selectedTag = 0;
static KeypadView *keyPadView = nil;


- (instancetype)init{
    isTranskeyShowing = NO;
    isShowQwerty = NO;
    isShowNumber = NO;

    unsigned char iv[16] = { 'M', 'o', 'b', 'i', 'l', 'e', 'T', 'r' , 'a', 'n', 's', 'K', 'e', 'y', '1', '0' };
    self.secureKey = [[NSData alloc] initWithBytes:iv length:16];

    [self setQwertyTrasnkey];
    [self setNumberTrasnkey];
    
    return self;
}

- (void)setQwertyTrasnkey{
    mQwertyTransKeyView = [[TransKeyView alloc] mTK_Init:self];
    mQwertyTransKeyView.delegate = self;
    
    [mQwertyTransKeyView mTK_SetSecureKey:self.secureKey];
}

- (void)setNumberTrasnkey{
    self.mNumberTransKeyView = [[TransKeyView alloc] mTK_Init:self];
    self.mNumberTransKeyView.delegate = self;
    
    [self.mNumberTransKeyView mTK_SetSecureKey:self.secureKey];
}

- (void)viewDidDisappear:(BOOL)animated{
//    NSLog(@"KeypadView viewDidDisappear");
////    [super viewDidDisappear:animated];
//    
//    if (self.mQwertyTransKeyView!=nil)
//        [self.mQwertyTransKeyView mTK_ClearDelegateSubviews];
//    
//    if (self.mNumberTransKeyView!=nil)
//        [self.mNumberTransKeyView mTK_ClearDelegateSubviews];
//    
//    if(isTranskeyShowing == NO){
//        self.mQwertyTransKeyView.delegate = nil;
//        self.mNumberTransKeyView.delegate = nil;
//    }
}

+ (KeypadView *)sharedKeypadView
{
    if (keyPadView == nil) {
        keyPadView = [[KeypadView alloc] init];
        
        if (IS_OS_8_BEFORE == NO) {
            [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
            
        }
    }
    
    return keyPadView;
}

-(void)showNumberTranskey: (UIViewController *)ownerView minLength:(int)minLength maxLength:(int)maxLength tagNumber:(NSInteger)tagNumber title:(NSString *)title {
    //별도의 식별번호
    selectedTag = tagNumber;
    ownerViewController = ownerView;
    keyPadView.delegate = ownerView;
    
    [mNumberTransKeyView mTK_SetSecureKey:self.secureKey];
    [mNumberTransKeyView mTK_SetHint:[NSString stringWithFormat:@"%d자리 이상 입력해주세요", minLength] font:[UIFont fontWithName:@"Arial" size:15]];
    [mNumberTransKeyView mTK_ShowMessageIfMinLength:[NSString stringWithFormat:@"%d자리 이상 입력해주세요.", minLength]];
    [mNumberTransKeyView mTK_ShowMessageIfMaxLength:[NSString stringWithFormat:@"%d자리 입력이 초과되었습니다.", maxLength]];
    [mNumberTransKeyView mTK_UseVoiceOver:NO];
    [mNumberTransKeyView mTK_setIgnoreStatusbar:NO];
    [mNumberTransKeyView mTK_SetControlCenter:NO];
    [mNumberTransKeyView mTK_EnableSamekeyInputDataEncrypt:NO];
    [mNumberTransKeyView SetKeyboardType:self
                              keypadType:1
                           mTK_inputType:2
                          mTK_inputTitle:title
                           mTK_cryptType:0
                           mTK_maxLength:maxLength
                           mTK_minLength:minLength
                         mTK_keypadUpper:NO
                        mTK_languageType:mTK_Language_Korean];
    
    //Modal로 실행
    [ownerView presentViewController:mNumberTransKeyView animated:YES completion:nil];
    
    
    isTranskeyShowing = YES;
    isShowNumber = YES;
}

-(void)showQwertyTranskey: (UIViewController *)ownerView minLength:(int)minLength maxLength:(int)maxLength tagNumber:(NSInteger)tagNumber title:(NSString *)title
{
    //별도의 식별번호
    selectedTag = tagNumber;
    ownerViewController = ownerView;
    keyPadView.delegate = ownerView;
    
    NSString* hintStr = [NSString stringWithFormat:@"%d자리 이상 입력해주세요.",minLength];
    NSString* overStr = [NSString stringWithFormat:@"%d자리 입력이 초과되었습니다.", maxLength];
    [mQwertyTransKeyView mTK_SetHint:hintStr font:[UIFont fontWithName:@"Arial" size:15] textAlignment:UITextAlignmentCenter];
    [mQwertyTransKeyView mTK_ShowMessageIfMinLength:hintStr];
    [mQwertyTransKeyView mTK_ShowMessageIfMaxLength:overStr];
    [mQwertyTransKeyView mTK_UseCursor:YES];
    //[mQwertyTransKeyView mTK_SetInputEditboxImage:YES];
    [mQwertyTransKeyView mTK_UseAllDeleteButton:YES];
    [mQwertyTransKeyView mTK_UseNavigationBar:YES];
    [mQwertyTransKeyView mTK_UseVoiceOver:YES];
    [mQwertyTransKeyView mTK_ShowNaviBar:NO];
    [mQwertyTransKeyView mTK_setHideInputPasswordDelay:3];
    //    [mQwertyTransKeyView mTK_setVerticalKeypadPosition:3];
    //    [mQwertyTransKeyView mTK_SetButtonMargin:-3];
    [mQwertyTransKeyView mTK_SetControlCenter:NO];
    [mQwertyTransKeyView mTK_setIgnoreStatusbar:NO];
    //        [mQwertyTransKeyView mTK_SetAlertTitle:@"타이틀테스트"];
    //    [mQwertyTransKeyView mTK_SupportedByDeviceOrientation:SupportedByDeviceLandscape];
    [mQwertyTransKeyView SetKeyboardType:self
                              keypadType:0
                           mTK_inputType:1
                          mTK_inputTitle:title
                           mTK_cryptType:0
                           mTK_maxLength:maxLength
                           mTK_minLength:minLength
                         mTK_keypadUpper:NO
                        mTK_languageType:mTK_Language_Korean];
    //animation으로 실행
    [ownerView presentViewController:mQwertyTransKeyView animated:YES completion:nil];
    
    //그냥 실행
    //[self.view addSubview:mQwertyTransKeyView.view];
    //[mQwertyTransKeyView actToIos4];
    
    isTranskeyShowing = YES;
    isShowQwerty = YES;
}

//Delegate 방식으로 쓰게 될 경우 삭제될 method
/*-(void)showNumberTranskeyAction:(UIButton *)sender
{
    [mNumberTransKeyView mTK_SetSecureKey:self.secureKey];
    [mNumberTransKeyView mTK_SetHint:@"4자리 이상 입력해주세요" font:[UIFont fontWithName:@"Arial" size:15]];
    [mNumberTransKeyView mTK_ShowMessageIfMinLength:@"4자리 이상 입력해주세요."];
    [mNumberTransKeyView mTK_ShowMessageIfMaxLength:@"16자리 입력이 초과되었습니다."];
    [mNumberTransKeyView mTK_UseVoiceOver:NO];
    [mNumberTransKeyView mTK_setIgnoreStatusbar:NO];
    [mNumberTransKeyView mTK_SetControlCenter:NO];
    [mNumberTransKeyView mTK_EnableSamekeyInputDataEncrypt:NO];
    [mNumberTransKeyView SetKeyboardType:self
                              keypadType:1
                           mTK_inputType:2
                          mTK_inputTitle:@"숫자입력"
                           mTK_cryptType:0
                           mTK_maxLength:16
                           mTK_minLength:4
                         mTK_keypadUpper:NO
                        mTK_languageType:mTK_Language_Korean];
    
    
    //Modal로 실행
    [self presentViewController:mNumberTransKeyView animated:YES completion:nil];
    
    
    isTranskeyShowing = YES;
    isShowNumber = YES;
}
*/

//키보드 띄우기
/*
-(void)showQwertyTranskeyAction:(UIButton *)sender
{
    NSString* hintStr = [NSString stringWithFormat:@"%d자리 이상 입력해주세요.",4];
    [mQwertyTransKeyView mTK_SetHint:hintStr font:[UIFont fontWithName:@"Arial" size:15] textAlignment:UITextAlignmentCenter];
    //    [mQwertyTransKeyView mTK_SetHint:@"4자리 이상 입력해주세요." font:[UIFont fontWithName:@"Arial" size:15] textAlignment:UITextAlignmentCenter];
    //    [mQwertyTransKeyView mTK_ShowMessageIfMinLength:@"4자리 이상 입력해주세요."];
    [mQwertyTransKeyView mTK_ShowMessageIfMinLength:hintStr];
    //    [mQwertyTransKeyView mTK_ShowMessageIfMaxLength:@"16자리 입력이 초과되었습니다."];
    [mQwertyTransKeyView mTK_ShowMessageIfMaxLength:hintStr];
    [mQwertyTransKeyView mTK_UseCursor:YES];
    //[mQwertyTransKeyView mTK_SetInputEditboxImage:YES];
    [mQwertyTransKeyView mTK_UseAllDeleteButton:YES];
    [mQwertyTransKeyView mTK_UseNavigationBar:YES];
    [mQwertyTransKeyView mTK_UseVoiceOver:YES];
    [mQwertyTransKeyView mTK_ShowNaviBar:NO];
    [mQwertyTransKeyView mTK_setHideInputPasswordDelay:3];
    //    [mQwertyTransKeyView mTK_setVerticalKeypadPosition:3];
    //    [mQwertyTransKeyView mTK_SetButtonMargin:-3];
    [mQwertyTransKeyView mTK_SetControlCenter:NO];
    [mQwertyTransKeyView mTK_setIgnoreStatusbar:NO];
    //        [mQwertyTransKeyView mTK_SetAlertTitle:@"타이틀테스트"];
    //    [mQwertyTransKeyView mTK_SupportedByDeviceOrientation:SupportedByDeviceLandscape];
    [mQwertyTransKeyView SetKeyboardType:self
                              keypadType:0
                           mTK_inputType:1
                          mTK_inputTitle:@"입력"
                           mTK_cryptType:0
                           mTK_maxLength:16
                           mTK_minLength:4
                         mTK_keypadUpper:NO
                        mTK_languageType:mTK_Language_Korean];
    //animation으로 실행
    
    [self presentViewController:mQwertyTransKeyView animated:YES completion:nil];
    
    //그냥 실행
    //[self.view addSubview:mQwertyTransKeyView.view];
    //[mQwertyTransKeyView actToIos4];
    
    isTranskeyShowing = YES;
    isShowQwerty = YES;
}
 */

-(void)secureInputFinish:(NSInteger)type
{
    NSLog(@"secureInputFinish Btn Type: %ld", (long)type);
    char qwertyPlainData[256];
    char numberPlainData[256];
    int plainLength;
    BOOL isCancel = YES;
    
    if (type == 1) {
        isCancel = NO;
    }
    
    if(isShowQwerty){
        NSData *qkey = [mQwertyTransKeyView mTK_GetSecureKey];
        qwertyChiperString = [mQwertyTransKeyView mTK_GetCipherDataExWithPadding];
        
        //        NSString* test = [mQwertyTransKeyView mTK_GetPBKDF2DataEncryptCipherData];
        
        [mQwertyTransKeyView mTK_GetPlainDataExWithPaddingWithKey:qkey cipherString:qwertyChiperString plainString:qwertyPlainData length:256];
        NSString *qwertyPlanString = [[NSString alloc] initWithCString:qwertyPlainData encoding:NSUTF8StringEncoding];
        plainLength = (int)[mQwertyTransKeyView mTK_GetDataLength];
        
        NSLog(@"Qwerty Cipher Text: %@", qwertyChiperString);
        NSLog(@"Qwerty Plain Text: %@", qwertyPlanString);
        if (keyPadView.delegate != nil && [keyPadView.delegate respondsToSelector:@selector(returnKeyboard:inputlength:plainText:encText:isCancel:)]) {
            [keyPadView.delegate returnKeyboard:selectedTag inputlength:plainLength plainText:qwertyPlanString encText:qwertyChiperString isCancel:isCancel];
        }
        
        if (ownerViewController != nil) {
            [ownerViewController dismissViewControllerAnimated:YES completion:nil];
        }
    }
    
    if(isShowNumber){
        NSData *nkey = [mNumberTransKeyView mTK_GetSecureKey];
        numberChiperString = [mNumberTransKeyView mTK_GetCipherDataExWithPadding];
        
        [mNumberTransKeyView mTK_GetPlainDataExWithPaddingWithKey:nkey cipherString:numberChiperString plainString:numberPlainData length:256];
        NSString *numberPlanString = [[NSString alloc] initWithCString:numberPlainData encoding:NSUTF8StringEncoding];
        
        NSLog(@"Number Cipher Text: %@", numberChiperString);
        NSLog(@"Number Cipher Text: %@", numberPlanString);
        if (keyPadView.delegate != nil && [keyPadView.delegate respondsToSelector:@selector(returnKeyboard:inputlength:plainText:encText:isCancel:)]) {
            [keyPadView.delegate returnKeyboard:selectedTag inputlength:numberPlanString.length plainText:numberPlanString encText:numberChiperString isCancel:isCancel];
        }
        
        if (ownerViewController != nil) {
            [ownerViewController dismissViewControllerAnimated:YES completion:nil];
        }
    }
    
    isTranskeyShowing = NO;
    
    isShowQwerty = NO;
    isShowNumber = NO;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
