//
//  TranCompletedViewController.swift
//  GlobalExpress
//
//  Created by Ralex on 2/5/16.
//  Copyright © 2016 webcash. All rights reserved.
//

import UIKit

class TransCompletedViewController: UIViewController,UITableViewDelegate,UITableViewDataSource{

    @IBOutlet weak var viewNocontent: UIView!
    @IBOutlet weak var tranCompletedTableView: UITableView!
    
    @IBOutlet weak var lblNoTrans: UILabel!
    //variable
    var completedArr = Array<AnyObject>()
    var transDetail = Dictionary<String,String>()
    
    override func localize() {
        lblNoTrans.text = Language.localizedStr("S07_11")
    }
    
    func initLoad()->Void{
        let args = [
            "INQ_GB":"1",
            "MORE_GB":"0",
            "LAST_ORDNO":"10"
        ]
        TransactionClass().sendAPI(APICode.HIST_R001, argument: args, success: { (success)->Void in
            guard let code = success["RSLT_CD"] where code as! String == "0000" else{
                //Should be alert here
                print("RESULT MESSAGE = \(success["RSLT_MSG"])")
                return
            }
            if let data = success["RESP_DATA"]!["INQ_REC"] as? Array<AnyObject>{
                self.completedArr = data
                print("ArrayData = \(success["RESP_DATA"])")
                guard self.completedArr.count > 0 else{
                    //TODO: Display no content
                    self.viewNocontent.hidden = false
                    self.tranCompletedTableView.hidden = true
                    return
                }
                //TODO: Display data in table view
                self.viewNocontent.hidden = true
                self.tranCompletedTableView.hidden = false
                
                self.tranCompletedTableView.reloadData()
                
            }
        })
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        localize()
        self.initLoad()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tranCompletedTableView.registerNib(UINib(nibName: "TransTableVeiwCell", bundle: nil), forCellReuseIdentifier: "TransCell")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.completedArr.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tranCompletedTableView.dequeueReusableCellWithIdentifier("TransCell", forIndexPath: indexPath) as! TransTableVeiwCell
        if self.completedArr.count > 0{
            var imgStrBackgroud:String = ""
            var imgStatus:String = ""
            let status = completedArr[indexPath.row]["TX_STS"] as! String
            if status == "2"{
                imgStrBackgroud = "l_transf_bg"
                imgStatus = "transf_icon"
            }
            if imgStrBackgroud != ""{
                cell.imgBackground.image = UIImage(named:imgStrBackgroud)
                cell.btnStatus.setImage(UIImage(named: imgStatus), forState: UIControlState.Normal)
            }
            if completedArr[indexPath.row]["RCPT_GB"] as? String == "1"{
                cell.lblBank.text = "Bank account"
            }else if completedArr[indexPath.row]["RCPT_GB"] as? String == "2"{
                cell.lblBank.text = "Cash Pick-up"
            }
            cell.lblName.text = self.completedArr[indexPath.row]["RCP_NM"] as? String
            cell.lblTransdate.text = RLib.formatDate(self.completedArr[indexPath.row]["TX_DT"] as! String)
            cell.lblKRWMoney.text = (self.completedArr[indexPath.row]["TX_WONAMT"] as! String).formatKRWCurrency()
            cell.lblUSDMoney.text = (self.completedArr[indexPath.row]["TX_AMT"] as! String).formatUSDCurrency()
            
            cell.btnStatus.tag = indexPath.row
            cell.btnStatus.addTarget(self, action: "ActionClick:", forControlEvents: .TouchUpInside)
            
        }
        return cell
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 80.0
    }
    
    func ActionClick(sender:AnyObject){
        print("Tag = \(sender.tag)")
        if let orderno = self.completedArr[sender.tag]["ORDER_NO"] as? String {
            print("ORDER_NO = \(orderno)")
            TransactionClass().sendAPI(APICode.HIST_R002, argument: ["ORDER_NO": orderno], success: { (success) -> Void in
                guard let code = success["RSLT_CD"] where code as! String != "0000" else{
                    if let data = success["RESP_DATA"] {
                        self.transDetail = data as! Dictionary<String, String>
                        //transDetailSegueprint("Detail Trans = \(self.transDetail)")
                        let storyboard = UIStoryboard(name: "Transaction", bundle: nil)
                        let viewcontroller = storyboard.instantiateViewControllerWithIdentifier("TransDetailViewController") as? TransDetailViewController
                        self.transDetail["ORDER_NO"] = orderno
                        viewcontroller?.tranDetail = self.transDetail
                        self.navigationController?.pushViewController(viewcontroller!, animated: true)
                    }
                    return
                }
                print("RESULT MESSAGE = \(success["RSLT_MSG"]!)")
            })
        }
    }

    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
