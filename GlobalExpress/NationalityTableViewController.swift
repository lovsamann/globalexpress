//
//  NationalityTableViewController.swift
//  GlobalExpress
//
//  Created by Ann on 1/28/16.
//  Copyright © 2016 webcash. All rights reserved.
//

import UIKit

class NationalityTableViewController: UITableViewController {

    
    var nationalityList: [Nationality]?
    var currencies: [Currency]?
    
    //MARK: - UITableViewDataSource
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return nationalityList!.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("nationalityCell")
    
        let nationality = nationalityList![indexPath.row]
        
        cell?.textLabel?.text = nationality.name
        
        return cell!
    }
    
    //MARK: - UITableViewDelegate
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        guard let nationality: Nationality = nationalityList![indexPath.row] else{
            print("nationalisty Row nil")
            return
        }
        
        TransactionClass().sendAPI(APICode.CURRENCY_R001, argument: ["NATION_CD" : nationality.code!], success: { (success) -> Void in

            if let list = success["RESP_DATA"], currencies = list["CURRENCY_REC"] as? [AnyObject] {

                self.currencies = [Currency]()
                for item in currencies {
                    let currency = Currency(data: item as? NSDictionary)
                    self.currencies?.append(currency!)
                }
            }
        })
        
    }
    
}
