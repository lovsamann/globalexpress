
//
//  CommonSwift.swift
//  GlobalExpress
//
//  Created by JangWooil on 2015. 12. 30..
//  Copyright © 2015년 webcash. All rights reserved.
//

import Foundation

extension UIViewController {
    
    // 확인 버튼만 보이는 AlertView 표시
    func showSimpleAlert(message: String, delegate: UIViewController) {
        if #available(iOS 8.0, *) {
            let alertController : UIAlertController = SysUtil.getAlertControllerWithmessage(message)
            delegate.presentViewController(alertController, animated: true, completion: nil)
        } else {
            // Fallback on earlier versions
            let alertView : UIAlertView = SysUtil.getAlertViewWithmessage(message, delegate: delegate, hasCancelButton: false)
            
            alertView.show()
        }
    }
    
    // 이벤트 있는 Alert 표시
    func showEventAlert(message : String, hasCancelButton : Bool, delegate: UIViewController?, tag : NSInteger?, confirmBlock : ((String!)-> Void)?, cancelBlock : (String! -> Void)?) {
        if #available(iOS 8.0, *) {
            let alertController : UIAlertController = self.getAlertController(message, confirmBlock: confirmBlock, cancelBlock: cancelBlock, hasCancelButton: hasCancelButton)
            if delegate != nil {
                delegate?.presentViewController(alertController, animated: true, completion: nil)
            }
        } else {
            let alertView : UIAlertView = self.getAlertView(message, tag: tag, hasCancelButton: hasCancelButton, delegate: delegate as? UIAlertViewDelegate)
            alertView.show()
        }
    }
    
    @available(iOS 8.0, *)
    func getAlertController(message : String, confirmBlock : ((String!)-> Void)?, cancelBlock : (String! -> Void)?, hasCancelButton : Bool) -> UIAlertController {
        let alertController : UIAlertController
        
        alertController = SysUtil.getAlertControllerWithmessage(message, hasCancelButton: hasCancelButton, confirmSelectBlock: confirmBlock, cancelSelectBlock: cancelBlock)
        
        
        return alertController
    }
    
    func getAlertView(message : String, tag : NSInteger?, hasCancelButton : Bool, delegate : UIAlertViewDelegate?) -> UIAlertView{
        let confirmText = Language.localizedStr("S24_06") //확인
        let cancelText  = Language.localizedStr("cancel")  //취소
        let alertText   = Language.localizedStr("MSG_01")   //안내
        let alertView : UIAlertView
        
        if(hasCancelButton) { //취소 버튼이 있을 때
            alertView = UIAlertView.init(title: alertText, message: message, delegate: delegate, cancelButtonTitle: confirmText, otherButtonTitles: cancelText)
        } else { //취소 버튼이 없을 때
            alertView = UIAlertView.init(title: alertText, message: message, delegate: delegate, cancelButtonTitle: confirmText)
        }
        
        if tag != nil {
            alertView.tag = tag!
        }
        
        return alertView
    }
    
    //다국어처리
    func localize() {
        
    }
    
    //뒤로가기 버튼 세팅
    func cf_setBackButton(){
        let leftNavigationImage = UIImage(named: "back_btn.png")
        let leftNavigationButton = UIButton(frame: CGRectMake(0,0,(leftNavigationImage?.size.width)! / 2 , (leftNavigationImage?.size.height)! / 2))
        leftNavigationButton.setBackgroundImage(leftNavigationImage, forState: .Normal)
        leftNavigationButton.addTarget(self, action: "cf_leftNavigationClicked", forControlEvents: .TouchUpInside)
        
        let leftBarButtonItem = UIBarButtonItem(customView: leftNavigationButton)
        self.navigationItem.leftBarButtonItem = leftBarButtonItem
    }
    
    func cf_leftNavigationClicked(){
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    
}

extension String {
    var length: Int {
        return characters.count
    }
    
    func indexOf(str: String) -> Int {
        let index : Int
        if let range: Range<String.Index> = self.rangeOfString(str) {
            index = self.startIndex.distanceTo(range.startIndex)
        } else {
            index = -1
        }
        
        return index
    }
}

public extension UIDevice {
    var modelName: String {
        var systemInfo = utsname()
        uname(&systemInfo)
        let machineMirror = Mirror(reflecting: systemInfo.machine)
        let identifier = machineMirror.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8 where value != 0 else { return identifier }
            return identifier + String(UnicodeScalar(UInt8(value)))
        }
        
        switch identifier {
        case "iPod5,1":                                 return "iPod Touch 5"
        case "iPod7,1":                                 return "iPod Touch 6"
        case "iPhone3,1", "iPhone3,2", "iPhone3,3":     return "iPhone 4"
        case "iPhone4,1":                               return "iPhone 4s"
        case "iPhone5,1", "iPhone5,2":                  return "iPhone 5"
        case "iPhone5,3", "iPhone5,4":                  return "iPhone 5c"
        case "iPhone6,1", "iPhone6,2":                  return "iPhone 5s"
        case "iPhone7,2":                               return "iPhone 6"
        case "iPhone7,1":                               return "iPhone 6 Plus"
        case "iPhone8,1":                               return "iPhone 6s"
        case "iPhone8,2":                               return "iPhone 6s Plus"
        case "iPad2,1", "iPad2,2", "iPad2,3", "iPad2,4":return "iPad 2"
        case "iPad3,1", "iPad3,2", "iPad3,3":           return "iPad 3"
        case "iPad3,4", "iPad3,5", "iPad3,6":           return "iPad 4"
        case "iPad4,1", "iPad4,2", "iPad4,3":           return "iPad Air"
        case "iPad5,3", "iPad5,4":                      return "iPad Air 2"
        case "iPad2,5", "iPad2,6", "iPad2,7":           return "iPad Mini"
        case "iPad4,4", "iPad4,5", "iPad4,6":           return "iPad Mini 2"
        case "iPad4,7", "iPad4,8", "iPad4,9":           return "iPad Mini 3"
        case "iPad5,1", "iPad5,2":                      return "iPad Mini 4"
        case "iPad6,7", "iPad6,8":                      return "iPad Pro"
        case "AppleTV5,3":                              return "Apple TV"
        case "i386", "x86_64":                          return "Simulator"
        default:                                        return identifier
        }
    }
}
