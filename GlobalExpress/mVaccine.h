/*=============================================================================================================
 - mVaccine.h
 =============================================================================================================*/
@class mVaccine;

@interface mVaccine : NSObject

// 싱글톤 객체 리턴
+ (mVaccine*)sharedInstance;

// jailBreak 체크 후 앱 종료
- (void)selfJailBreakChecking;

// jailBreak 체크
- (BOOL)isJailBreak;

@property (retain) NSString *selfJailBreakMsg;
@property (retain) NSString *selfJailBreakTitle;

@end
 