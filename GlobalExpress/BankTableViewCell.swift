//
//  BankTableViewCell.swift
//  GlobalExpress
//
//  Created by Ralex on 2/3/16.
//  Copyright © 2016 webcash. All rights reserved.
//

import UIKit

class BankTableViewCell: UITableViewCell {

    @IBOutlet weak var lblBank: UILabel!
    @IBOutlet weak var imgBankPic: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        if selected{
            self.accessoryType = UITableViewCellAccessoryType.Checkmark
        }
        else{
            self.accessoryType = UITableViewCellAccessoryType.None
        }
    }
}
