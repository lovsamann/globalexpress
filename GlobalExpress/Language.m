//
//  Language.m
//  GlobalExpress
//
//  Created by JangWooil on 2016. 1. 14..
//  Copyright © 2016년 webcash. All rights reserved.
//

#import "Language.h"

@implementation Language

+(NSString *)localizedStr:(NSString *)key
{
    NSString *lang = [SessionManager sharedSessionManager].language;
    if (lang == nil || [lang isEqualToString:@""]) {
        lang = @"ko";
    }
    
    NSString *bundlePath = [[NSBundle mainBundle] pathForResource:@"Localizable" ofType:@"strings" inDirectory:nil forLocalization:lang]; //ko, Base, km-KH
    NSBundle *localizableBundle = [[NSBundle alloc] initWithPath:[bundlePath stringByDeletingLastPathComponent]];

    NSString *localStr = NSLocalizedStringFromTableInBundle(key, nil, localizableBundle, nil);
    
    return localStr;
}

@end
