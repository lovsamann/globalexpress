//
//  RecipientInfoDetailViewController.swift
//  GlobalExpress
//
//  Created by Ann on 2/2/16.
//  Copyright © 2016 webcash. All rights reserved.
//

import UIKit

class RecipientInfoDetailViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {

    
    var detailDic: [String: String]!{
        didSet{
            tableView.reloadData()
        }
    }
    var recipeint: Recipient!
    var titles: [String]! {
        didSet{
            if let _ = detailDic where detailDic["RCPT_GB"] == "1" {
                titles.removeAtIndex(6)
            }
        }
    }
    
    var keys: [String]! {
        didSet {
            if let _ = detailDic where detailDic["RCPT_GB"] == "1" {
                keys.removeAtIndex(6)
            }
        }
    }
    
    @IBOutlet var tableView: UITableView!
    @IBOutlet var infoLabel: UILabel!
    @IBOutlet var deleteButton: UIButton!
    @IBOutlet var descriptionLabel: UILabel!
    @IBOutlet var confirmButton: UIButton!
    
    override func localize() {
        infoLabel.text = Language.localizedStr("S10_1_01")
        deleteButton.setTitle(Language.localizedStr("S10_1_03"), forState: .Normal)
        deleteButton.setTitle(Language.localizedStr("S10_1_03"), forState: .Selected)
//        descriptionLabel.text = Language.localizedStr("")
        titles = loadTitle()
        keys = loadKeys()
        self.confirmButton.setTitle(Language.localizedStr("S10_1_10"), forState: .Normal)
        self.navigationItem.title = Language.localizedStr("S10_1_01")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.localize()
        
        
        let leftNavigationImage = UIImage(named: "back_btn.png")
        let leftNavigationButton = UIButton(frame: CGRectMake(0,0,(leftNavigationImage?.size.width)! / 2 , (leftNavigationImage?.size.height)! / 2))
        leftNavigationButton.setBackgroundImage(leftNavigationImage, forState: .Normal)
        leftNavigationButton.addTarget(self, action: "leftNavigationClicked", forControlEvents: .TouchUpInside)
        
        let leftBarButtonItem = UIBarButtonItem(customView: leftNavigationButton)
        self.navigationItem.leftBarButtonItem = leftBarButtonItem

    }
    
    //MARK: - UITableViewDataSource
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("cell") as? RecipientInfoTableViewCell
        
        cell?.titleLabel.text = titles[indexPath.row]
        
        let key = keys[indexPath.row]
        
        var value: String
        
        if key == "RCPT_GB" {
            value = detailDic[key] == "1" ? "Receiving account" : "Cash pick up"
        }else{
            value = detailDic[key]!
        }
        
        cell?.valueLabel.text = value
        
        return cell!
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return detailDic == nil ? 0 : detailDic["RCPT_GB"] == "1" ? detailDic.count - 1 : detailDic.count - 2
    }
    
    //MARK: - UITableViewDelegate
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerImage = UIImage(named: "con_rad_top01.png")
        let headerImageView = UIImageView(frame: CGRectMake(0, 0, CGRectGetWidth(tableView.frame), 9))
        
        headerImageView.image = headerImage
        
        return headerImageView
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 9.0
    }
    
    func tableView(tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        
        let footerImage = UIImage(named: "con_rad_bottom01.png")
        let footerImageView = UIImageView(frame: CGRectMake(0, 0, CGRectGetWidth(tableView.frame), 9))
        footerImageView.image = footerImage
        
        let clearSeparatorView = UIView(frame: CGRectMake(8, -2, CGRectGetWidth(footerImageView.frame) - 12, 2))
        clearSeparatorView.backgroundColor = UIColor.whiteColor()
        footerImageView.addSubview(clearSeparatorView)
        
        return footerImageView
    }
    
    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 9.0
    }
    
    
    //MARK: Private Methods
    func loadTitle() -> [String] {
        return [Language.localizedStr("S10_1_04") //country
                ,Language.localizedStr("S10_1_05") //receiving method
                ,Language.localizedStr("S10_1_06") // receiving bank
                ,Language.localizedStr("S10_1_07") //bank acc no
                ,Language.localizedStr("S10_1_08") //receiver
                ,Language.localizedStr("S10_1_09") //mobile phone
                ,Language.localizedStr("S10_2_10") //e-mail
        ]
    }
    
    func loadKeys() -> [String] {
        return ["NATION_NM", "RCPT_GB", "BANK_NM", "ACCT_NO", "RCPT_NM", "TEL_NO", "E_MAIL" ]
    }
    
    
    //MARK: - Action
    @IBAction func deletedClicked(sender: AnyObject) {
        
        showEventAlert("Do you really want to delete?", hasCancelButton: true, delegate: self, tag: 1973, confirmBlock: { (value) -> Void in
            
                TransactionClass().sendAPI(APICode.RECIPIENT.RECIPIENT_D001, argument: ["RCPT_CD" : "\(self.recipeint.recipientCode!)"], success: { (success) -> Void in
                    
                    print("yyyyy \(success)")
                    
                    if let ok = success["RSLT_CD"] where ok as! String == "0000" {
                        self.navigationController?.popViewControllerAnimated(true)
                    }else{
                        self.showEventAlert(success["RSLT_MSG"] as! String, hasCancelButton: false, delegate: self, tag: 1980, confirmBlock: nil, cancelBlock: nil)
                    }
                })
            }, cancelBlock: { (value) -> Void in })
    }
    
    @IBAction func confirmClicked(sender: UIButton) {
        self.navigationController?.popToRootViewControllerAnimated(true)
    }
  
    func leftNavigationClicked(){
        self.navigationController?.popViewControllerAnimated(true)
    }
}
