//
//  PaymentPeriodTableViewCell.swift
//  GlobalExpress
//
//  Created by Ann on 3/16/16.
//  Copyright © 2016 webcash. All rights reserved.
//

import UIKit

class PaymentPeriodTableViewCell: UITableViewCell {

    @IBOutlet var cancelButton: UIButton!
    @IBOutlet var periodInfoLabel: UILabel!
    @IBOutlet var infolabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
