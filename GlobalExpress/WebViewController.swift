//
//  WebViewController.swift
//  GlobalExpress
//
//  Created by JangWooil on 2016. 1. 14..
//  Copyright © 2016년 webcash. All rights reserved.
//

import UIKit

class WebViewController: UIViewController, UIWebViewDelegate {

    @IBOutlet var webView: UIWebView!
    var destURL : String = ""
    var webViewTitle : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        self.navigationItem.hidesBackButton = true
        
        let leftNavigationImage = UIImage(named: "back_btn.png")
        let leftNavigationButton = UIButton(frame: CGRectMake(0,0,(leftNavigationImage?.size.width)! / 2 , (leftNavigationImage?.size.height)! / 2))
        leftNavigationButton.setBackgroundImage(leftNavigationImage, forState: .Normal)
        leftNavigationButton.addTarget(self, action: "back:", forControlEvents: .TouchUpInside)
        
        let leftBarButtonItem = UIBarButtonItem(customView: leftNavigationButton)
        self.navigationItem.leftBarButtonItem = leftBarButtonItem
        
        if(self.webViewTitle != ""){
            self.title = self.webViewTitle
        }

        if let viewURL = NSURL(string: destURL) {
            let request = NSURLRequest(URL: viewURL)
            webView.loadRequest(request)
        }
        
        
    }
    
    func back(sender : UIBarButtonItem) {
        if let url = webView.request?.URL?.absoluteString {
            if url == destURL {
                self.navigationController?.popViewControllerAnimated(true)
            } else {
                webView.goBack()
            }
        } else {
            self.navigationController?.popViewControllerAnimated(true)
        }
    }
    
    //웹 -> 앱 이벤트 처리하기
    func webView(webView: UIWebView, shouldStartLoadWithRequest request: NSURLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        if let requestURL = request.URL {
            let sURLScheme = requestURL.scheme
            let sURL       = requestURL.absoluteString
            let sDecodedURL : String
            
            if let escapedURL : String = sURL.stringByRemovingPercentEncoding{
                if sURLScheme == "iWebAction" { //웹 액션
                    sDecodedURL = escapedURL.stringByReplacingOccurrencesOfString("iWebAction:", withString: "")
                    if(sDecodedURL == "") {
                        return false
                    }
                    self.executeWebAction(sDecodedURL)
                }
            } else {
                return false
            }
        } else {
            return false
        }
        
        return true
    }
    
    func showLoadingBar() {
        
    }
    
    func hideLoadingBar() {
        
    }
    
    func goLoginView() {
        
    }
    
    func showTransKey() {
        
    }
    
    func changeTitle(menuTitle: String?) {
        if menuTitle != nil {
            self.title = menuTitle!
        }
    }
    
    func convertStringToDictionary(text: String) -> [String:AnyObject]? {
        if let data = text.dataUsingEncoding(NSUTF8StringEncoding) {
            do {
                return try NSJSONSerialization.JSONObjectWithData(data, options: []) as? [String:AnyObject]
            } catch let error as NSError{
                print(error)
            }
        }
        return nil
    }
    
    func showURLBySafari(url:String) {
        //사파리로 Url open
        if let url : NSURL = NSURL.init(string: url)! {
            UIApplication.sharedApplication().openURL(url)
        }
    }
    
    func gobackToMenu() {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    //웹 액션 처리하기 (웹 -> 앱)
    func executeWebAction(aSource : NSString) {
        #if _DEBUG_
            print("aSource : \(aSource)");
        #endif
        
        if let ActionDic : NSDictionary = aSource.JSONValue() as? NSDictionary {
            if ActionDic.count <= 0 {
                return;
            }
            if let actionCode : String = ActionDic.objectForKey("_action_code") as? String{
                if actionCode == "" {
                    return;
                }
//                let action_data : Dictionary? = ActionDic.objectForKey("_action_data") as? Dictionary?
                let actionData : [String:AnyObject]?
                var sTitle : String? = nil
                var url : String? = nil
                if let strActionData = ActionDic.objectForKey("_action_data") as? String {
                    actionData = self.convertStringToDictionary(strActionData)
                    if actionData != nil {
                        sTitle = actionData!["_title"] as? String
                        url    = actionData!["_url"] as? String
                    }
                }
                
                switch actionCode {
                case "1000" :
                    //로딩바 시작
                    self.showLoadingBar()
                case "1001" :
                    //로딩바 종료
                    self.hideLoadingBar()
                case "2000" :
                    //로그인 화면 이동
                    self.goLoginView()
                case "2002" :
                    //보안키패드 화면 호출
                    self.showTransKey()
                case "4000" :
                    //상단 Title 변경
                    self.changeTitle(sTitle)
                case "5000" :
                    //외부 브라우저 호출
                    if(url != nil) {
                        self.showURLBySafari(url!)
                    }
                case "6000" :
                    //webview에서 메뉴화면으로 이동
                    self.gobackToMenu()
                default :
                    return;
                    
                }
            } else {
                return;
            }
        } else {
            return;
        }
    }
    
    func webViewDidStartLoad(webView: UIWebView) {
        
    }
    
    func webViewDidFinishLoad(aWebView: UIWebView) {
        self.title = aWebView.stringByEvaluatingJavaScriptFromString("document.title")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
