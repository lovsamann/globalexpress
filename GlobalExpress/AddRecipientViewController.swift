//
//  AddRecipientViewController.swift
//  GlobalExpress
//
//  Created by Ann on 2/29/16.
//  Copyright © 2016 webcash. All rights reserved.
//

import UIKit

class AddRecipientViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate,PopupControllerDelegate,CountryViewControllerDelegate,BankSelectViewControllerDelegate {

    //outlet
    @IBOutlet var bankAccount: UIButton!
    @IBOutlet var cashPickup: UIButton!
    @IBOutlet var nextStep: UIButton!
    
    @IBOutlet var scrollView: UIScrollView!
    
    @IBOutlet var headerView: ViewCustom!
    @IBOutlet var addYourReciverImageView: UIImageView!
    @IBOutlet var tableView: UITableView!
    
    //label
    @IBOutlet var addReciverLabel: UILabel!
    @IBOutlet var addReceiverMethodLabel: UILabel!
    @IBOutlet var bankAcountIntroLabel: UILabel!
    @IBOutlet var bankAccountSubDescriptionLabel: UILabel!
    @IBOutlet var delivertyTimeLabel: UILabel!
    @IBOutlet var delivertyDescriptionLabel: UILabel!
    @IBOutlet var enterReceiverBankLabel: UILabel!
    @IBOutlet var requiredFieldLabel: UILabel!
    @IBOutlet var stepBarLabel: UILabel!
    @IBOutlet var bankAccountOptionLabel: UILabel!
    @IBOutlet var cashpickupOptionLabel: UILabel!
    
    //constraint
    @IBOutlet var topCustomViewConstraint: NSLayoutConstraint!
    
    
    private var selectedTextFeild: UITextField!
    
    
    
//    var contentDic: [String: [String]]! {
//        didSet {
//            self.tableView.reloadData()
//        }
//    }
    
    var contentDic: [String: [String]]!
    
    var nationalityList: [Nationality]?
    var banks: [Bank]?
    var organizes: [Organize]?
    var selectedBank: Bank!
    var selectedNationality: Nationality!
    var selectedOrganization: Organize!
    var transcation: Transaction!
    
    
    static let SCROLLVIEW_HEIGHT = 665
    static let SCROLLVIEW_WIDTH = 400
    
    static let SCROLLVIEW_CONTENT_HEIGHT = 770
    static let SCROLLVIEW_CONTENT_WIDTH = 400
    
    var keyboardVisible = false
    var orignailScrollViewOffset: CGFloat!
    
    
    //MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        bankAccount.selected = true
        contentDic = fillCell(bankAccount)
        
        
        let leftNavigationImage = UIImage(named: "back_btn.png")
        let leftNavigationButton = UIButton(frame: CGRectMake(0,0,(leftNavigationImage?.size.width)! / 2 , (leftNavigationImage?.size.height)! / 2))
        leftNavigationButton.setBackgroundImage(leftNavigationImage, forState: .Normal)
        leftNavigationButton.addTarget(self, action: "leftNavigationClicked", forControlEvents: .TouchUpInside)
        
        let leftBarButtonItem = UIBarButtonItem(customView: leftNavigationButton)
        self.navigationItem.leftBarButtonItem = leftBarButtonItem
        
        self.scrollView.scrollsToTop = false
        
        registerKeyboard()
        self.localize()
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        
        let controllers = self.navigationController?.viewControllers
        guard let viewControllers = controllers else{
            print("nothing on stack navigation")
            return
        }
        

        if let index: Int = viewControllers.count - 2 where index >= 0 {
            let previousViewController = viewControllers[index]
            
            if previousViewController is RecipientListViewController { // come from RecipientListViewController
                print("well well \(self.topCustomViewConstraint.constant)")
                topCustomViewConstraint.constant = -35
                self.headerView.hidden = true
                
            }else{ //somewhere else
                topCustomViewConstraint.constant = 15
                self.headerView.hidden = false
            }
            
            self.updateViewConstraints()
        }
    }
    
    override func localize() {
        self.navigationItem.title = Language.localizedStr("R03_01")
        self.addReciverLabel.text = Language.localizedStr("R03_02")
        self.addReceiverMethodLabel.text = Language.localizedStr("R03_03")
        self.bankAcountIntroLabel.text = Language.localizedStr("R03_04")
        self.bankAccountSubDescriptionLabel.text = Language.localizedStr("R03_05")
        self.delivertyTimeLabel.text = Language.localizedStr("R03_06")
        self.delivertyDescriptionLabel.text = Language.localizedStr("R03_07")
        self.enterReceiverBankLabel.text = Language.localizedStr("R03_08")
        self.stepBarLabel.text = "2." + Language.localizedStr("R07_05")
        self.nextStep.setTitle(Language.localizedStr("R03_24"), forState: .Normal)
        self.bankAccountOptionLabel.text = Language.localizedStr("T14_10")
        self.cashpickupOptionLabel.text = Language.localizedStr("R06_04")
    }
    
    //MARK: - UITableViewDataSource
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("AddRecipientTableViewCell") as? AddRecipientTableViewCell
        
        switch indexPath.row {
            case 0..<5:
                cell?.requireImageView.hidden = false
            default:
                cell?.requireImageView.hidden = true
        }
        

        switch indexPath.row {
            case 0..<2:
                cell?.customTextField.rightView = doPaddingView()
                cell?.customTextField.rightViewMode = .Always
            default:
                cell?.customTextField.rightViewMode = .Never
            
        }
        
        let title = contentDic["title"]
        let textFields = contentDic["textField"]
        cell?.customTextField.placeholder = textFields![indexPath.row]
        cell?.customTitleLabel.text = title![indexPath.row]
        cell?.customTextField.tag = 1000 + indexPath.row
        cell?.customTextField.keyboardType = cell?.customTextField.tag == 1002 ? .NumberPad : cell?.customTextField.tag == 1004 ? .PhonePad : .Default
        cell?.customTextField.delegate = self
        return cell!
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (contentDic == nil) ? 0 : contentDic["title"]!.count
    }
    
    //MARK: -  UITableViewDelegate
    func tableView(tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        
        let view = UIView(frame: CGRectZero)
        
        let footerImage = UIImage(named: "con_rad_bottom01.png")
        let footerImageView = UIImageView(frame: CGRectMake(0, 0, CGRectGetWidth(tableView.frame), 9))
        footerImageView.image = footerImage
        
        let clearSeparatorView = UIView(frame: CGRectMake(8, -1, CGRectGetWidth(footerImageView.frame) - 12, 1))
        clearSeparatorView.backgroundColor = UIColor.whiteColor()
        footerImageView.addSubview(clearSeparatorView)
        
        view.addSubview(footerImageView)
        
        let requireView = ViewCircle(frame: CGRectMake(0, CGRectGetHeight(footerImageView.frame) + 12, 5 , 5))
        requireView.backgroundColor = UIColor(red: 1.0, green: 80 / 255.0, blue: 80 / 255.0, alpha: 1.0)
        requireView.cornerRadius = 2.5
        
        let requireInfoLabel = UILabel(frame: CGRectMake( CGRectGetWidth(requireView.frame) + 5, CGRectGetHeight(footerImageView.frame) + 10, CGRectGetWidth(tableView.frame), 12))
        requireInfoLabel.font = UIFont.systemFontOfSize(12.0)
        requireInfoLabel.textColor = UIColor(red: 49 / 255.0, green: 50 / 255.0, blue: 51 / 255.0, alpha: 1.0)
        requireInfoLabel.text = Language.localizedStr("R03_23")
        
        view.addSubview(requireView)
        view.addSubview(requireInfoLabel)
        
        return view
    }
    
    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 50
    }

    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerImage = UIImage(named: "con_rad_top01.png")
        let headerImageView = UIImageView(frame: CGRectMake(0, 0, CGRectGetWidth(tableView.frame), 9))
        
        headerImageView.image = headerImage
        
        return headerImageView
    }
    
    
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 9.0
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
                print("selected \(tableView)")
    }
    
    func getPreviousViewController() -> UIViewController! {
        
        let viewControllers = self.navigationController?.viewControllers
        
        guard let controllers = viewControllers else{
            print("all controllers on stack = nil")
            return nil
        }
        
        let numberOfControllers = controllers.count
        let previousController = controllers[numberOfControllers - 2]
        
        return previousController
    }
    
    //MARK:- PopupController
    
    @available(iOS 8.0, *)
    func popupController<T : PrintableCell>(popupController: PopupController<T>, didSelectRowAtIndexPath indexPath: NSIndexPath) {

        if let textField = selectedTextFeild where selectedTextFeild.tag == 1000 {
            let nationality = self.nationalityList![indexPath.row]
            textField.text = nationality.name
            
            TransactionClass().sendAPI(APICode.RCPBANK_R001, argument: ["NATION_CD" : nationality.code!], success: { (success) -> Void in
                if let data = success["RSLT_CD"] where data as! String == "0000" {
                    print("gg \(success)")
                    var banks = [Bank]()

                    if let bankList = success["RESP_DATA"]!["RECBANK_REC"] as? [AnyObject]{

                        for item in bankList {
                            let bank = Bank(data: item as? NSDictionary)
                            banks.append(bank!)
                        }
                        self.banks = banks
                    }
                    
                    var organizes = [Organize]()
                    if let organizeList = success["RESP_DATA"]!["RECBANK_REC"] as? [AnyObject] {
                        for item in organizeList {
                            let organization = Organize(data: item as? NSDictionary)
                            organizes.append(organization!)
                        }
                        self.organizes = organizes
                    }
                }
            })
            
        }else{
            
        }
        selectedTextFeild = nil
    }
    
    //MARK: - UITextFeildDelegate
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
        switch textField.tag {
            case 1000:
//                if #available(iOS 8.0, *) {
//                    self.selectedTextFeild = textField
//                    let popOver = PopupController<Nationality>(title: Language.localizedStr("C02P_01"), inView: self.view, preferredStyle: UIPopupControllerStyle.TableView)
//                    popOver.delegate = self
//                    popOver.data = self.nationalityList
//                    presentViewController(popOver.navigationController!, animated: true, completion: nil)
//                }
                
                guard let nationalityList = self.nationalityList else{
                    print("nationaltiy nil in tag 1000")
                    return false
                }
                
                let country: CountryViewController = CountryViewController(nibName: "CountryViewController", bundle: NSBundle.mainBundle())
                country.delegate = self

                country.countryArray = nationalityList as [AnyObject]
                presentPopupViewController(country, animationType: MJPopupViewAnimationFade, contentInteraction: MJPopupViewContentInteractionDismissBackgroundOnly)
                
                return false
            case 1001:
//                if #available(iOS 8.0, *) {
//                    self.selectedTextFeild = textField
//                    let popOver = PopupController<Bank>(title: Language.localizedStr("M02_17"), inView: self.view, preferredStyle: UIPopupControllerStyle.CollectionView)
//                    popOver.delegate = self
//                    popOver.data = self.banks
//                    print("do we have s.th to show \(self.banks)")
//                    presentViewController(popOver.navigationController!, animated: true, completion: nil)
//                }
                
                let textFieldCountry = self.view.viewWithTag(1000) as? UITextField
                

                
                guard let fieldCountry = textFieldCountry else{
                    print("filed is nil")
                    return false
                }
                
                if let text = fieldCountry.text where text != "" {
                    
                    if self.organizes == nil && cashPickup.selected {
                        guard selectedNationality != nil else{
                            print("selectedNationality")
                            return false
                        }
                        
                        TransactionClass().sendAPI(APICode.CPORG_R001, argument: [ "NATION_CD" : selectedNationality.code!], success: { (success) -> Void in
                            
                            if let data = success["RSLT_CD"] where data as! String == "0000" {
                                var organizes = [Organize]()
                                if let organizeList = success["RESP_DATA"]!["ORG_REC"] as? [AnyObject] {
                                    for item in organizeList {
                                        let organization = Organize(data: item as? NSDictionary)
                                        organizes.append(organization!)
                                    }
                                    self.organizes = organizes
                                    
                                    let bank : BankSelectViewController = BankSelectViewController(nibName: "BankSelectViewController", bundle: NSBundle.mainBundle())
                                    bank.delegate = self
                                    bank.bankArray = self.organizes
                                    self.presentPopupViewController(bank, animationType: MJPopupViewAnimationFade,contentInteraction: MJPopupViewContentInteractionNone)
                                }
                            }
                        })
                    }else if self.banks == nil && bankAccount.selected{
                        
                            TransactionClass().sendAPI(APICode.RCPBANK_R001, argument: ["NATION_CD" : selectedNationality.code!], success:  { (success) -> Void in
                                
                                if let data = success["RSLT_CD"] where data as! String == "0000" {
                                    var banks = [Bank]()
                                    
                                    if let bankList = success["RESP_DATA"]!["RECBANK_REC"] as? [AnyObject]{
                                        for item in bankList {
                                            let bank = Bank(data: item as? NSDictionary)
                                            banks.append(bank!)
                                        }
                                        self.banks = banks
                                        
                                        let bank : BankSelectViewController = BankSelectViewController(nibName: "BankSelectViewController", bundle: NSBundle.mainBundle())
                                        bank.delegate = self
                                        bank.bankArray =  self.banks
                                        self.presentPopupViewController(bank, animationType: MJPopupViewAnimationFade,contentInteraction: MJPopupViewContentInteractionNone)
                                    }
                                }
                            })
                    }else{
                        let bank : BankSelectViewController = BankSelectViewController(nibName: "BankSelectViewController", bundle: NSBundle.mainBundle())
                        bank.delegate = self
                        bank.bankArray =  bankAccount.selected ? self.banks : self.organizes
                        self.presentPopupViewController(bank, animationType: MJPopupViewAnimationFade,contentInteraction: MJPopupViewContentInteractionNone)
                    }
                        
//                            TransactionClass().sendAPI(APICode.CPORG_R001, argument: [ "NATION_CD" : selectedNationality.code!], success: { (success) -> Void in
//                                
//                                if let data = success["RSLT_CD"] where data as! String == "0000" {
//                                    var organizes = [Organize]()
//                                    if let organizeList = success["RESP_DATA"]!["ORG_REC"] as? [AnyObject] {
//                                        for item in organizeList {
//                                            let organization = Organize(data: item as? NSDictionary)
//                                            organizes.append(organization!)
//                                        }
//                                        self.organizes = organizes
//                                        
//                                        let bank : BankSelectViewController = BankSelectViewController(nibName: "BankSelectViewController", bundle: NSBundle.mainBundle())
//                                        bank.delegate = self
//                                        bank.bankArray = self.organizes
//                                        self.presentPopupViewController(bank, animationType: MJPopupViewAnimationFade,contentInteraction: MJPopupViewContentInteractionNone)
//                                    }
//                                }
//                            })

                            
                            
//                        }

//                    }
                }else{
                    print("text is none")
                }
                
                return false
            default:
                    return true
        }
    }
    
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        
        let textLength = textField.text?.characters.count ?? 0
        
        if range.length + range.location > textLength {
            return false
        }
    
        let newLegth = textLength + string.characters.count - range.length

        var isMaxLength: Bool
        
        if bankAccount.selected {
            isMaxLength = textField.tag == 1002 || textField.tag == 1004 ? newLegth <= 20 : newLegth <= 50
        }else{
            isMaxLength = textField.tag == 1004 ? newLegth <= 20 : newLegth <= 50
        }
        
        
//        if isMaxLength {
//            var upToDateText: String
//            guard let text = textField.text else{
//                print("textFiled.text error")
//                return false
//            }
//            
////            upToDateText = (text as NSString).stringByReplacingCharactersInRange(NSMakeRange(text.characters.count, 0), withString: string)
////
////            
////            guard selectedNationality != nil else {
////                return true
////            }
////            
////            let getCallingCode = self.getCallingCode(selectedNationality)
////            
////
////
////            if bankAccount.selected {
////                if textField.tag == 1004 {
////                    textField.attributedText = getCallingCode.TextFieldTextAttributeString(upToDateText)
////
////                    return false
////                }else{
////                    return true
////                }
////            }else{
////                if textField.tag == 1003 {
////                    textField.attributedText = getCallingCode.TextFieldTextAttributeString(upToDateText)
////                    return false
////                }else {
////                    return true
////                }
////            }
        
//        }

        return isMaxLength
    }

    
    @IBAction func textFieldEditingChange(sender: AnyObject) {
        if sender is UITextField {
            let textField = sender as! UITextField
            
            
            guard selectedNationality != nil else {
                return
            }
            
            let callingCode = self.getCallingCode(selectedNationality)

            guard let textString = textField.text else {
                print("textFeild nil")
                return
            }

//            let text = textString.stringByReplacingOccurrencesOfString(callingCodeWithNoPlusSign, withString: callingCodeWithNoPlusSign)
//            print("text code \(text)")
//
//            
//            if bankAccount.selected {
//                if textField.tag == 1004 {
//                    textField.text = text + textString
//                }else{
//                                print("text tag 1004\(textField.text)")
//                }
//            }else{
//                if textField.tag == 1003 {
//                    textField.text = text + textString
//                }else{
//                    print("text tag 1003 \(textField.text)")
//                }
//            }
            
        }
    }
    
    
    //MARK: - Private Methods
    
    func getCallingCode(nationality: Nationality) -> String {
        
        let callingCode = NSDictionary(contentsOfFile: NSBundle.mainBundle().pathForResource("callingcode", ofType: "plist")!) as! [String: String]
        
        guard let countryName = selectedNationality.name else{
            print("countryName nil")
            return ""
        }
        
        let grabCountryName = countryName == "Korea" ? "South Korea" : countryName
        print("coutry name == \(countryName)")
        let callingCodeString = callingCode[grabCountryName]! as String
        
        return callingCodeString
    }
    
    func registerKeyboard (){
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardWasShown:", name: UIKeyboardDidShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardWillBeHidden:", name: UIKeyboardDidHideNotification, object: nil)
    }
    
    
    func leftNavigationClicked(){
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    
    func loadListBox() -> UIImageView {
        let image = UIImage(named: "listbox_arr_icon.png")
        let imageView = UIImageView(frame: CGRectMake(0, 0, 9, 6))
        imageView.image = image
        
        return imageView
    }
    
    func fillCell(type: UIButton)  -> [String: [String]] {

        var titles = self.loadTitle()
        var textFields = self.loadPlaceholder()
        
        if type === bankAccount {
            return  ["title" : titles,
                    "textField" : textFields]
        }else{
            titles.removeAtIndex(2)
            textFields.removeAtIndex(2)
            return ["title" : titles,
                "textField" : textFields]
        }
        

    }
    
    func doPaddingView() -> UIView {
        let imageView = loadListBox()
        let view = UIView(frame: CGRectMake(0, 0, 27, 9))
        var imageViewRect = imageView.frame
        imageViewRect.origin.x = view.center.x
        imageView.frame = imageViewRect
        view.addSubview(imageView)
        return view
    }
    
    func loadTitle() -> [String] {
        return [Language.localizedStr("R03_09") //country
                , Language.localizedStr("R03_11") //bank
                ,Language.localizedStr("R03_13") //bank Acc No.
                ,Language.localizedStr("R03_15") //receiver
                ,Language.localizedStr("R03_17") //Mobile Phone
                ,Language.localizedStr("R03_21") //E-mail
        ]
    }
    
    func loadPlaceholder() -> [String] {
        return [Language.localizedStr("R03_10") //select country
                ,Language.localizedStr("R03_12") //select bank
                ,Language.localizedStr("R03_14") //acc No.
                ,Language.localizedStr("R03_16") //receiver
                ,Language.localizedStr("R03_18") //mobile phone
                ,Language.localizedStr("R03_22") //e-mail
        ]
    }
    
    
    
    //MARK: - PopupDelegate
    func getCountryData(dict: NSDictionary!){
        // do nothing
    }
    
    func getBankData(dict: NSDictionary) {
        // do nothing
    }
    
    func getCountryObject(indexPath: NSIndexPath) {
        
        selectedNationality = self.nationalityList![indexPath.row]
        
        
        if bankAccount.selected && self.banks == nil {
            TransactionClass().sendAPI(APICode.RCPBANK_R001, argument: ["NATION_CD" : selectedNationality.code!], success:  { (success) -> Void in
                
                if let data = success["RSLT_CD"] where data as! String == "0000" {
                    var banks = [Bank]()
                    
                    if let bankList = success["RESP_DATA"]!["RECBANK_REC"] as? [AnyObject]{
                        for item in bankList {
                            let bank = Bank(data: item as? NSDictionary)
                            banks.append(bank!)
                        }
                        self.banks = banks
                    }
                }
            })
        }else{
            
            TransactionClass().sendAPI(APICode.CPORG_R001, argument: ["NATION_CD" : selectedNationality.code!], success:  { (success) -> Void in
                
                if let data = success["RSLT_CD"] where data as! String == "0000" {
                    var org = [Organize]()
                    
                    if let organizeList = success["RESP_DATA"]!["ORG_REC"] as? [AnyObject]{
                        for item in organizeList {
                            let organization = Organize(data: item as? NSDictionary)
                            org.append(organization!)
                        }
                        self.organizes = org
                    }
                }
            })

            
        }
        
        let cell = self.tableView.cellForRowAtIndexPath(NSIndexPath(forRow: 0, inSection: 0)) as! AddRecipientTableViewCell
        cell.customTextField.text = nil
        cell.customTextField.text = self.selectedNationality.name
        
        
        var customCell: AddRecipientTableViewCell!
        
        let grabCallingCode = self.getCallingCode(self.selectedNationality == nil ? nil : self.selectedNationality)
        
        if self.bankAccount.selected {
            customCell = self.tableView.cellForRowAtIndexPath( NSIndexPath(forRow: 4, inSection: 0)) as! AddRecipientTableViewCell
        }else{
            customCell = self.tableView.cellForRowAtIndexPath( NSIndexPath(forRow: 3, inSection: 0)) as! AddRecipientTableViewCell
        }
        
        customCell.customTextField.attributedPlaceholder = grabCallingCode.textFieldPlaceHolderAttributeString()
        
    }
    
    func getBankObject(indexPath: NSIndexPath) {

        let cell = self.tableView.cellForRowAtIndexPath(NSIndexPath(forRow: 1, inSection: 0)) as! AddRecipientTableViewCell
        cell.customTextField.text = nil
        selectedBank = self.cashPickup.selected ? nil : self.banks![indexPath.row]
        selectedOrganization = self.bankAccount.selected ? nil : self.organizes![indexPath.row]
        cell.customTextField.text = bankAccount.selected ? selectedBank.name : selectedOrganization.name
    }
    
    
    //MARK: - Action
    @IBAction func receiverType(sender: UIButton) {
        
        if sender == bankAccount {
            bankAccount.selected = true
            cashPickup.selected = false

            let titles = contentDic["title"]
            if titles?.count == 5 { //insert new row
                contentDic = fillCell(sender)
                self.tableView.insertRowsAtIndexPaths([ NSIndexPath(forRow: 2, inSection: 0) ], withRowAnimation: .Automatic)
            }else{
                //nothing to do here
                print("bank account insert")
            }
            
            
            self.bankAcountIntroLabel.text = Language.localizedStr("R03_04")
            self.bankAccountSubDescriptionLabel.text = Language.localizedStr("R03_05")
            
            if selectedBank == nil {
                //clear bank text
                let cell = self.tableView.cellForRowAtIndexPath(NSIndexPath(forRow: 1, inSection: 0)) as! AddRecipientTableViewCell
                cell.customTextField.text = nil
            }
                        print("123123123 bankAccount\(selectedBank)")
        }else{
            bankAccount.selected = false
            cashPickup.selected = true

            self.bankAcountIntroLabel.text = Language.localizedStr("R06_04")
            self.bankAccountSubDescriptionLabel.text = Language.localizedStr("R06_05")
            
            contentDic = fillCell(sender)
            self.tableView.deleteRowsAtIndexPaths( [ NSIndexPath(forRow: 2, inSection: 0) ], withRowAnimation: .Automatic)
            
            print("123123123 \(selectedOrganization)")
            if selectedOrganization == nil {
                //clear bank text
                let cell = self.tableView.cellForRowAtIndexPath(NSIndexPath(forRow: 1, inSection: 0)) as! AddRecipientTableViewCell
                cell.customTextField.text = nil
            }
        }
    }
    
    
    func keyboardWasShown(notification: NSNotification) {
        let info = notification.userInfo
        let kbSize = info![UIKeyboardFrameBeginUserInfoKey]?.CGRectValue.size
        
        
        let modelName = UIDevice().modelName
        let heightOffSet: CGFloat
        
        switch modelName {
            case "iPhone 4", "iPhone 4s":
                heightOffSet = -180
            case "iPhone 5", "iPhone 5c", "iPhone 5s":
                heightOffSet = -30
            case "iPhone 6", "iPhone 6s":
                heightOffSet = 130
            case "iPhone 6 Plus", "iPhone 6s Plus" :
                heightOffSet = 250.0
            default:
//                heightOffSet = 80.0
                    heightOffSet = -40
        }
        
        
        let viewHieght = CGRectGetHeight(self.view.frame) - heightOffSet
        let kbHieght = kbSize?.height
        orignailScrollViewOffset = self.scrollView.contentOffset.y
        
        self.scrollView.setContentOffset(CGPointMake(0, viewHieght - kbHieght!), animated: true)
    }
    
    func keyboardWillBeHidden(notification: NSNotification) {
        
        guard let offSet = orignailScrollViewOffset else{
            return
        }
        
        self.scrollView.setContentOffset(CGPointMake(0.0, offSet), animated: true)
    }
    
    //MARK: - Navigation
    @IBAction func nextStep(sender: AnyObject) {

        var value = [String]()
        for index in bankAccount.selected ?  2...5 : 2...4 {

            let cell = self.tableView.cellForRowAtIndexPath(NSIndexPath(forRow: index, inSection: 0)) as! AddRecipientTableViewCell
            let title = contentDic["title"]
            
            if let text = cell.customTextField.text where text != "" {
                value.append(text)
            }else{
                //alert required fields
                
                if title![index] == Language.localizedStr("R03_21") {
                    break
                }else{
                    self.showEventAlert(title![index] + "  " + Language.localizedStr("R03_23"), hasCancelButton: false, delegate:self, tag: 700000, confirmBlock: nil, cancelBlock: nil )
                    return
                }
            }
        }
        
        let grabCallingCode = getCallingCode(selectedNationality)
        
        if bankAccount.selected {

            if selectedBank == nil {
                  self.showEventAlert("Please Choose Banks", hasCancelButton: false, delegate: self, tag: 20000, confirmBlock: nil, cancelBlock: nil)
                return
            }
        }else{
            if selectedOrganization == nil {
                
                self.showEventAlert("Please Choose Cash Pick up", hasCancelButton: false, delegate: self, tag: 20000, confirmBlock: nil, cancelBlock: nil)
                return
            }
        }
        
        print("value \(value)")
        
        var req:[String:String]
        
        if bankAccount.selected {
            req = ["NATION_CD" : selectedNationality!.code!,
                "BANK_CD" : cashPickup.selected ? "" : selectedBank.code!,
                "ACCT_NO" : value[0],
                "RCPT_NM" : value[1],
                "TEL_NO"  : value[2],
                "EMAIL"   :  value.count <= 3 ? "" : value[3] ,
                "INTL_TELCD" : grabCallingCode
            ]
        }else{
            
            req = ["NATION_CD" : selectedNationality.code!,
                "RCPT_NM" : value[0],
                "TEL_NO" : value[1],
                "EMAIL" : value.count <= 2 ? "" : value[2],
                "CPORG_CD": bankAccount.selected ? "" : selectedOrganization.code!,
                "INTL_TELCD": grabCallingCode
            ]
        }
        
        print("req--------> \(req)")
        
        TransactionClass().sendAPI(bankAccount.selected ? APICode.RCPACCT_C001 : APICode.RCPINFO_COO1, argument: req , success: { (success) -> Void in
            print("success \(success)")
            if let noError = success["RSLT_CD"] where noError as! String == "0000" {
                
                let previousController = self.getPreviousViewController()
                
                if let controller = previousController where controller is RecipientViewController {
                    
                    var recipientDictionary = success["RESP_DATA"] as! [String: AnyObject]
                    recipientDictionary["RCPT_GB"] = self.bankAccount.selected ? "1" : "2"
                    
                    let recipient = Recipient(data: NSDictionary(dictionary: recipientDictionary) as? [String: AnyObject])
                    print("tranttt \(self.transcation)")
                    
                    guard let _ = self.transcation else {
                        print("transction nil")
                        self.showEventAlert("No transcation", hasCancelButton: false, delegate: self, tag: 1888, confirmBlock: nil, cancelBlock: nil)
                        return
                    }
                    

                    
                    self.transcation.recipient = recipient
                }
                
                //to confirm view controller
                self.performSegueWithIdentifier("confirmAddedRecevierSegue", sender: success["RESP_DATA"])
            }else{
                self.showEventAlert(success["RSLT_MSG"] as! String, hasCancelButton: false, delegate: self, tag: 1922, confirmBlock: nil, cancelBlock: nil)
            }
        })
    }
    
    //MARK: - Navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "confirmAddedRecevierSegue" { //create recipeint
            let recipientInfoVC = segue.destinationViewController as? RecipientInfoViewController
            recipientInfoVC?.recipientDic = sender as? [String: String]
            recipientInfoVC?.transcation = self.transcation
            if cashPickup.selected {
                guard let organization = selectedOrganization else{
                    print("organzaiont nil")
                    return
                }
                recipientInfoVC?.transcation?.recipient?.orgainzation = organization
            }
            
        }else{
            print("unknow thing is coming")
        }
    }
}

extension String {

    func textFieldPlaceHolderAttributeString() -> NSMutableAttributedString{
        let placeHolderString = self.stringByReplacingOccurrencesOfString("+", withString: "") + " - " + Language.localizedStr("R03_18")
        let customColor = UIColor(red: 49 / 255.0, green: 50 / 255.0, blue: 51 / 255.0, alpha: 1.0)
        let attriString = NSMutableAttributedString(string: placeHolderString)
        attriString.addAttribute(NSForegroundColorAttributeName, value: customColor, range: NSMakeRange(0, 3))
        
        return attriString
    }
    
    func TextFieldTextAttributeString(string: String) -> NSMutableAttributedString {
        let placeHolderString = self.stringByReplacingOccurrencesOfString("+", withString: "") + " - " + string
        
        let customColor = UIColor(red: 49 / 255.0, green: 50 / 255.0, blue: 51 / 255.0, alpha: 1.0)
        let attriString = NSMutableAttributedString(string: placeHolderString)
        attriString.addAttribute(NSForegroundColorAttributeName, value: customColor, range: NSMakeRange(0, 3))
        
        return attriString
    }
    

}

