//
//  BankSelectViewController.swift
//  GlobalExpress
//
//  Created by UDAM on 2/24/16.
//  Copyright © 2016 webcash. All rights reserved.
//

import UIKit

@objc protocol BankSelectViewControllerDelegate {
    func getBankData(dict: NSDictionary!)
    optional func getBankObject(indexPath: NSIndexPath)
}

class BankSelectViewController: MJPopupViewController, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {
    
    // MARK: - IBOutlet
    // -------------------------------------------------------------------------------
    //	IBOutlet ColllectionView, View, Label, Button
    // -------------------------------------------------------------------------------
    @IBOutlet weak var collectionView   : UICollectionView!
    @IBOutlet weak var labelTitle       : UILabel!
    @IBOutlet weak var popRadiusImage   : UIImageView!
    
    // MARK: - Property
    // -------------------------------------------------------------------------------
    // Bank array
    // -------------------------------------------------------------------------------
    var bankArray           : NSArray!
    var delegate            : BankSelectViewControllerDelegate?
    
    let fixedTotalCollection: Int       = 18
    let cellVertical        : Int       = 3
    let cellHeight          : Int       = 44
    
    // MARK: - IBAction
    // -------------------------------------------------------------------------------
    // Close popup when clicked
    // -------------------------------------------------------------------------------
    @IBAction func closePopupButton(sender: UIButton) {
        dismissPopupViewController(self, animationType: MJPopupViewAnimationFade)
    }
    
    // MARK: - Private function
    // -------------------------------------------------------------------------------
    // Localize language
    // -------------------------------------------------------------------------------
    override func localize() {
        labelTitle.text = Language.localizedStr("M02_17")
    }
    
    // MARK: - View Life Cycle
    // -------------------------------------------------------------------------------
    // viewDidLoad
    // -------------------------------------------------------------------------------
    override func viewDidLoad() {
        super.viewDidLoad()

        if bankArray.count <= fixedTotalCollection {
            if ((bankArray.count) % cellVertical) == 0 {
                let rowCell = ((bankArray.count) / cellVertical)
                self.view.frame = CGRectMake(view.frame.origin.x, view.frame.origin.y, self.view.frame.width, CGFloat(rowCell * cellHeight) + 47 )
            }
            else {
                self.view.frame = CGRectMake(view.frame.origin.x, view.frame.origin.y, self.view.frame.width, (CGFloat(((bankArray.count / cellVertical) + 1) * cellHeight)) + 47)
            }
            popRadiusImage.hidden = true
        }
        
        self.view.layer.cornerRadius = 8.0
        
        //Register BankCell Xib file
        collectionView.registerNib(UINib(nibName: "BankCell", bundle: NSBundle.mainBundle()), forCellWithReuseIdentifier: "Cell")
        
        //Localize
        localize()
    }
    
    override func viewDidLayoutSubviews() {
        if bankArray.count <= fixedTotalCollection {
            let collectionViewMaskLayer = CAShapeLayer()
            collectionViewMaskLayer.path = UIBezierPath(roundedRect: self.collectionView.bounds, byRoundingCorners: UIRectCorner.BottomLeft.union(.BottomRight), cornerRadii: CGSizeMake(5, 5)).CGPath
            self.collectionView.layer.mask = collectionViewMaskLayer
            
            collectionView.reloadData()
        }
    }
    
    // MARK: - UICollectionViewDataSource
    // -------------------------------------------------------------------------------
    //	numberOfItemInSection: Customize the number of Cell in UICollectionView
    // -------------------------------------------------------------------------------
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return bankArray.count
    }

    // -------------------------------------------------------------------------------
    //	cellForItemAtIndexPath: Customize the data of cell in UICollectionView
    // -------------------------------------------------------------------------------
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("Cell", forIndexPath: indexPath) as! BankCell
        cell.backgroundColor = UIColor.whiteColor()
        
        if bankArray[indexPath.row] is NSDictionary {
            cell.bankNameLabel.text = "\(bankArray[indexPath.row]["BANK_NM"])"
        }else if bankArray[indexPath.row] is Bank{
            
            let bank = bankArray[indexPath.row] as! Bank
            cell.bankNameLabel.text = bank.name
        }else if bankArray[indexPath.row] is Organize {
            let orgainze = bankArray[indexPath.row] as! Organize
            cell.bankNameLabel.text = orgainze.name
        }
        
        
        
        //Configure UICollectionView cell
        let customColorView = UIView()
        customColorView.backgroundColor = UIColor(colorLiteralRed: 235.0/255.0, green: 240.0/255.0, blue: 242.0/255.0, alpha: 1.0)
        cell.selectedBackgroundView = customColorView
        
        return cell
    }
    
    // MARK: - UICollectionViewDelegate
    // -------------------------------------------------------------------------------
    //	didSelectAtIndexPath: Get Bank Data After select for each cell
    // -------------------------------------------------------------------------------
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        if bankArray[indexPath.row] is NSDictionary {
            self.delegate?.getBankData(bankArray[indexPath.row] as! NSDictionary)
        }else{
            self.delegate?.getBankObject!(indexPath)
        }
        
        self.dismissPopupViewController(self, animationType: MJPopupViewAnimationFade)
        print("\(bankArray[indexPath.row])")
    }
    
}
