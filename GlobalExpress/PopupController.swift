//
//  PopupController.swift
//  PopupCommon
//
//  Created by Ann on 2/10/16.
//  Copyright © 2015년 webcash. All rights reserved.
//

import UIKit

protocol PrintableCell{
    var name:String! { get set }
    var image: String! { get set }
}

enum UIPopupControllerStyle : Int {
    case SimpleView = 0
    case TableView
    case CollectionView
}

@available(iOS 8.0, *)
protocol PopupControllerDelegate{
    func popupController<T: PrintableCell>(popupController: PopupController<T>, didSelectRowAtIndexPath indexPath: NSIndexPath)
}

@available(iOS 8.0, *)
class PopupController<T: PrintableCell>: UITableViewController,UIPopoverPresentationControllerDelegate {
    
    //MARK: - members
    var style: UIPopupControllerStyle!

    var data: [T]! {
        didSet{
//            //default size
            if preferredSize == nil {
                if style.rawValue == 1 {
                    preferredSize = CGSizeMake(280, CGFloat(data.count) * 44.0)
                }else{
                    preferredSize = CGSizeMake(280, CGFloat(data.count / 3) * 44.0)
                }
                
            }
            
            //register cell
            switch(style.rawValue) {
                case 0: break;
                case 2:
                    print("vvv")
                    let nib = UINib(nibName: "CollectionViewTableViewCell", bundle: nil)
                    self.tableView.registerNib(nib, forCellReuseIdentifier: "cell")
                    break;
                default:
                    self.tableView.registerClass(UITableViewCell.self, forCellReuseIdentifier: "cell")
            }
            
            self.tableView.reloadData()
        }
    }
    
    var preferredSize: CGSize! {
        didSet{
            super.preferredContentSize = preferredSize
        }
    }
    
    var delegate: PopupControllerDelegate?
    
//    MARK: - UITableViewDelegate
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if let delegate = self.delegate {
            delegate.popupController(self, didSelectRowAtIndexPath: indexPath)
            dismissViewControllerAnimated(true, completion: nil)
        }
    }
    
    //MARK: - UITableViewDataSource
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        var cell: UITableViewCell!
        
        if style.rawValue == 1 {
            cell = tableView.dequeueReusableCellWithIdentifier("cell")
        }else{
            cell = tableView.dequeueReusableCellWithIdentifier("cell") as! CollectionViewTableViewCell
        }
        
        
        let value = data[indexPath.row]
        
        
        let separatorLine = UIView(frame: CGRectMake(0,CGRectGetHeight((cell?.frame)!), CGRectGetWidth(tableView.frame),2))
        separatorLine.backgroundColor = UIColor(red: 213 / 255, green: 219 / 255, blue: 224 / 255, alpha: 1.0)
        
        print("cell type \(cell)")
        if cell is CollectionViewTableViewCell {
            print("i'm a new type")

            
        }else{
            cell!.showData(value)
            
        }
        
        cell?.contentView.addSubview(separatorLine)
        
        return cell!
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }

    //MARK: - init()
    init(){
        super.init(style: .Plain)
    }
    
    convenience init(title: String,inView view: UIView!, preferredStyle: UIPopupControllerStyle) {
        self.init()
        self.style = preferredStyle
        super.title = title
        
        
        tableView.separatorStyle = .None
        
        
        
        
        let rightNavigationImage = UIImage(named: "pop_close_icon.png")
        let rightNavigationButton = UIButton(frame: CGRectMake(20,0, (rightNavigationImage?.size.width)! / 2, (rightNavigationImage?.size.height)! / 2))
        rightNavigationButton.setBackgroundImage(rightNavigationImage, forState: .Normal)
        rightNavigationButton.addTarget(self, action: "dismissPopupController", forControlEvents: .TouchUpInside)
        
        let navigationController = UINavigationController(rootViewController: self)

        let barButtomItem = UIBarButtonItem(customView: rightNavigationButton)
        self.navigationItem.rightBarButtonItem = barButtomItem
        
        navigationController.modalPresentationStyle = .Popover
        let popover = navigationController.popoverPresentationController
        popover?.delegate = self
        popover?.sourceView = view
        let centerView = view.center
        popover?.sourceRect = CGRectMake(centerView.x, centerView.y, 0, 0)
        popover?.permittedArrowDirections = UIPopoverArrowDirection(rawValue: 0)
        
    }

    //MARK: - UIPopoverPresentaionControllerDelegate
    func adaptivePresentationStyleForPresentationController(controller: UIPresentationController) -> UIModalPresentationStyle {
        return .None
    }
 
    func presentationController(presentationController: UIPresentationController, willPresentWithAdaptiveStyle style: UIModalPresentationStyle, transitionCoordinator: UIViewControllerTransitionCoordinator?) {
        
        //do background opacity 80%
        transitionCoordinator?.animateAlongsideTransition({ context in
            let dimmingView = presentationController.containerView
            dimmingView?.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.8)
            
        }, completion: nil)
    }
    
    func popoverPresentationControllerShouldDismissPopover(popoverPresentationController: UIPopoverPresentationController) -> Bool {
        //DO NOT dismiss till user clicked on an actual button or cell
        return false
    }
    
    //MARK: - View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        

    }
    
    //MARK: - Action
    func dismissPopupController(){
        dismissViewControllerAnimated(true, completion: nil)
    }
    
}

extension UITableViewCell {
    func showData<T: PrintableCell>(item: T) {
        self.textLabel?.text = item.name
    }
}


