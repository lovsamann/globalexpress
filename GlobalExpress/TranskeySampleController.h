//
//  TranskeySampleController.h
//  GlobalExpress
//
//  Created by JangWooil on 2016. 1. 20..
//  Copyright © 2016년 webcash. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KeypadView.h"

@interface TranskeySampleController : UIViewController<KeypadViewDelegate>

@property (strong, nonatomic) IBOutlet UITextField *textPassword;
@property (strong, nonatomic) IBOutlet UITextField *numberPassword;

@end
