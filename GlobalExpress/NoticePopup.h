//
//  NoticePopup.h
//  GlobalExpress
//
//  Created by UDAM on 2/16/16.
//  Copyright © 2016 webcash. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MJPopupViewController.h"
#import "Language.h"

@interface NoticePopup : MJPopupViewController

@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIButton *confirmButton;

- (IBAction)shareButtonClicked:(UIButton *)sender;

@end
