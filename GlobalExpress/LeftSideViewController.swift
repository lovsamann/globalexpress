//
//  LeftSideViewController.swift
//  GlobalExpress
//
//  Created by JangWooil on 2015. 12. 29..
//  Copyright © 2015년 webcash. All rights reserved.
//
import UIKit

class LeftSideViewController: UIViewController, UITableViewDelegate, UITableViewDataSource{
    var arrSideMenu : Array<Dictionary<String, AnyObject>> = []
    
    @IBOutlet var tearmsBtn: UIButton!
    @IBOutlet var PrivacyBtn: UIButton!
    @IBOutlet var descriptionLabel: UILabel!
    @IBOutlet var menuTable: UITableView!
    var terms_web_url1 = "" //이용약관 URL
    var terms_web_url2 = "" //개인정보취급방침 URL
    var termsBtnTitle : String = ""
    var privacyBtnTitle : String = ""
    
    override func localize() {
        self.termsBtnTitle = Language.localizedStr("S06_13")
        self.privacyBtnTitle = Language.localizedStr("C08_02")
        
        self.tearmsBtn.setTitle(termsBtnTitle, forState: .Normal) //이용약관
        self.PrivacyBtn.setTitle(privacyBtnTitle, forState: .Normal) //개인정보취급방침
        self.descriptionLabel.text = Language.localizedStr("S06_01") //회원가입이 필요합니다.
    }
    
    override func viewDidLoad() {
        if let arrTableData = SessionManager.sharedSessionManager().leftMenuArr as? Array<Dictionary<String, AnyObject>> {
            arrSideMenu = arrTableData
        }
        
        localize();
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "refreshLang", name: "refreshSideMenu", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "refreshData", name: "refreshSideMenuData", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "updateSideUserNm", name: "updateSideUserNm", object: nil)
    }
    
    func updateSideUserNm() {
        self.descriptionLabel.text = Language.localizedStr("S06_02").stringByReplacingOccurrencesOfString("%%", withString: SessionManager.sharedSessionManager().user_nm)
    }
    
    func refreshData(){
        if let arrTableData = SessionManager.sharedSessionManager().leftMenuArr as? Array<Dictionary<String, AnyObject>> {
            arrSideMenu = arrTableData
        }
        localize()
        
        //이용약관을 조회한다.
        let inputDic = Dictionary(dictionaryLiteral: ("TERMS_GB", "01"))
        TransactionClass().sendAPI("frt_terms_r001", argument: inputDic, success: {
            (resultDic : NSDictionary) in
            if let rslt_cd = resultDic["RSLT_CD"] as? String {
                if rslt_cd != "0000" { //오류
                    if let rsltMsg = resultDic["RSLT_MSG"] as? String {
                        print("Error Code : \(rslt_cd), Error Message : \(rsltMsg)")
                    }
                } else { //정상
                    if let respData = resultDic["RESP_DATA"] as? NSDictionary {
                        if let terms_url = respData["TERMS_URL"] as? String {
                            self.terms_web_url1 = terms_url
                        }
                    }
                }
                
                self.setTerms2()
            }
        })
        
        
        menuTable.reloadData()
    }
    
    //개인정보 취급방침 보기
    func setTerms2(){
        let inputDic = Dictionary(dictionaryLiteral: ("TERMS_GB", "02"))
        TransactionClass().sendAPI("frt_terms_r001", argument: inputDic, success: {
            (resultDic : NSDictionary) in
            if let rslt_cd = resultDic["RSLT_CD"] as? String {
                if rslt_cd != "0000" { //오류
                    if let rsltMsg = resultDic["RSLT_MSG"] as? String {
                        print("Error Code : \(rslt_cd), Error Message : \(rsltMsg)")
                    }
                } else { //정상
                    if let respData = resultDic["RESP_DATA"] as? NSDictionary {
                        if let terms_url = respData["TERMS_URL"] as? String {
                            self.terms_web_url2 = terms_url
                        }
                    }
                }
            }
        })
    }
    
    func refreshLang(){
        localize()
        menuTable.reloadData()
    }
    
    //이용약관 보기
    @IBAction func showTerms1(sender: AnyObject) {
        let viewInput : Dictionary<String, AnyObject> = Dictionary(dictionaryLiteral: ("viewName", self.terms_web_url1), ("webviewYN", "Y"), ("webviewTitle", self.termsBtnTitle))
        
        NSNotificationCenter.defaultCenter().postNotificationName("changeMainView", object: nil, userInfo: viewInput) //메뉴 이동
        NSNotificationCenter.defaultCenter().postNotificationName("closeLeftMenu", object: nil, userInfo: nil) //사이드 메뉴 닫기
    }

    //개인정보취급방침 보기
    @IBAction func showTerms2(sender: AnyObject) {
        let viewInput : Dictionary<String, AnyObject> = Dictionary(dictionaryLiteral: ("viewName", self.terms_web_url2), ("webviewYN", "Y"), ("webviewTitle", self.privacyBtnTitle))
        
        NSNotificationCenter.defaultCenter().postNotificationName("changeMainView", object: nil, userInfo: viewInput) //메뉴 이동
        NSNotificationCenter.defaultCenter().postNotificationName("closeLeftMenu", object: nil, userInfo: nil) //사이드 메뉴 닫기
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let sectionNum : Int
        
        if(section == 0){
            sectionNum = 4
        } else if(section == 1){
            sectionNum = 2
        } else {
            sectionNum = 2
        }
        return sectionNum
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
//        if(section != 0){
            let headerView : UIView  = UIView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.width, height: 25))
            let label : UILabel      = UILabel(frame: CGRect(x: 18, y: 5, width: tableView.bounds.width - 20, height: 15))
            
            if(section == 1){
                label.text = Language.localizedStr("C07P_01")
            } else if(section == 2){
                label.text = Language.localizedStr("S06_10")
            }
            label.textColor = UIColor.whiteColor()
            label.font      = label.font.fontWithSize(14)
            headerView.backgroundColor = UIColor(red: 31/255, green: 65/255, blue: 87/255, alpha: 1.0)
            
            headerView.addSubview(label)
            
            return headerView
//        } else { //첫번째 섹션은 없는 걸로 간주한다.
//            
//        }
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell : UITableViewCell
        let arrayIndex : Int
        
        if(indexPath.section == 0){
            arrayIndex = indexPath.row
        } else if(indexPath.section == 1){
            arrayIndex = indexPath.row + 4
        } else { //2
            arrayIndex = indexPath.row + 6
        }
        
        let leftTableViewCell : LeftMenuTableViewCell = (tableView.dequeueReusableCellWithIdentifier("Cell") as? LeftMenuTableViewCell!)!;
//        leftTableViewCell.menuName.text = arrSideMenu.
        if(arrSideMenu.count > 0){
            if let tableDic : Dictionary<String, AnyObject> = arrSideMenu[arrayIndex] {
                if let menuTitleCd = tableDic["c_menu_title_cd"] as? String {
                    leftTableViewCell.menuName.text = Language.localizedStr(menuTitleCd)
                    leftTableViewCell.menuName.font = leftTableViewCell.menuName.font.fontWithSize(16)
                }
                
                if let imgName = tableDic["c_menu_img"] as? String {
                    if let iconImg = UIImage(named: imgName) {
                        leftTableViewCell.menuIcon.image = iconImg
                    }
                }
            }
        }
        
        
        cell = leftTableViewCell
        
        return cell
    }
    
    //숫자 String 으로 된 값을 YN 으로 변환하여 반환한다
    func numberToYn(number: String) -> String {
        if(number == "0") {
            return "N"
        } else {
            return "Y"
        }
    }
    
    //Sample
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        let arrLeftMenu = SessionManager.sharedSessionManager().leftMenuArr
        let arrayIndex : Int
        if(indexPath.section == 0){
            arrayIndex = indexPath.row
        } else if(indexPath.section == 1){
            arrayIndex = indexPath.row + 4
        } else { //2
            arrayIndex = indexPath.row + 6
        }
//
            if let dicMenuInfo = arrLeftMenu[arrayIndex] as? Dictionary<String, AnyObject> {
                let menu_url = dicMenuInfo["c_menu_url"] as? String
                let menu_name = dicMenuInfo["c_menu_title_cd"] as? String
                
                if menu_url != nil && menu_url != "" {
                    let viewInput : Dictionary<String, AnyObject>
                    
                    switch indexPath.section{
                    case 0,1:
                        switch(indexPath.row){
                        case 0,1:
                            let c_login_yn = self.numberToYn("\(dicMenuInfo["c_login_yn"])")
                            
                            //로그인 필수이고 사용자가 로그인을 안한 상태이면
                            if(c_login_yn == "Y" && SessionManager.sharedSessionManager().signupYn() == "N"){
//                                viewInput = Dictionary(dictionaryLiteral: ("message", Language.localizedStr("S06_01")))
//                                NSNotificationCenter.defaultCenter().postNotificationName("showHomeViewAlert", object: nil, userInfo: viewInput)
                                let title_label = Language.localizedStr("S06_15")
                                let label1 = Language.localizedStr("S06_16")
                                let buttonL = Language.localizedStr("S06_17")
                                let buttonR = Language.localizedStr("S06_18")
                                
                                let userInfo = ["title_label":title_label, "label1":label1, "buttonL":buttonL, "buttonR":buttonR]
                                NSNotificationCenter.defaultCenter().postNotificationName("openLeftPopup", object: nil, userInfo: userInfo)
                                NSNotificationCenter.defaultCenter().postNotificationName("closeLeftMenu", object: nil, userInfo: nil) //사이드 메뉴 닫기
                                return;
                            }
                            break
                        default:
                            break
                        }

                        break
                    default:
                        break
                    }
                    if menu_url!.indexOf("http") != 0 {
                        //네이티브 뷰
                        viewInput = Dictionary(dictionaryLiteral: ("viewName", menu_url!), ("webviewYN", "N"))
                    } else {
                        //웹뷰
                        let lastTitle : String
                        
                        if(menu_name == nil){
                            lastTitle = ""
                        } else {
                            lastTitle = Language.localizedStr("\(menu_name!)")
                        }
                        
                        viewInput = Dictionary(dictionaryLiteral: ("viewName", menu_url!), ("webviewYN", "Y"), ("webviewTitle", lastTitle))
//                        viewInput = Dictionary(dictionaryLiteral: ("viewName", "http://m.naver.com/"), ("webviewYN", "Y"))
                    }
                    NSNotificationCenter.defaultCenter().postNotificationName("changeMainView", object: nil, userInfo: viewInput) //메뉴 이동
                    NSNotificationCenter.defaultCenter().postNotificationName("closeLeftMenu", object: nil, userInfo: nil) //사이드 메뉴 닫기
                }
            }
        
    }
}
