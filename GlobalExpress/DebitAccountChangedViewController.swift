//
//  DebitAccountChangedViewController.swift
//  GlobalExpress
//
//  Created by UDAM on 3/7/16.
//  Copyright © 2016 webcash. All rights reserved.
//

import UIKit

class DebitAccountChangedViewController: UIViewController {

    // MARK: - IBOutlet
    // -------------------------------------------------------------------------------
    // UILabel, UIButton
    // -------------------------------------------------------------------------------
    @IBOutlet weak var titleLabel       : UILabel!
    @IBOutlet weak var bankLabel        : UILabel!
    @IBOutlet weak var descriptionLabel : UILabel!
    @IBOutlet weak var bankAccNo        : UILabel!
    @IBOutlet weak var bankResult       : UILabel!
    @IBOutlet weak var bankAccNoResult  : UILabel!
    
    @IBOutlet weak var confirmButton    : UIButton!
    
    // MARK: - Private function
    // -------------------------------------------------------------------------------
    // Localizing language
    // -------------------------------------------------------------------------------
    override func localize() {
        
        titleLabel.text         = Language.localizedStr("S18_01")
        descriptionLabel.text   = Language.localizedStr("S18_02")
        bankLabel.text          = Language.localizedStr("S18_03")
        bankAccNo.text          = Language.localizedStr("S18_04")
        confirmButton.setTitle(Language.localizedStr("S18_05"), forState: .Normal)
        
    }
    
    // -------------------------------------------------------------------------------
    // Customize View Layout
    // -------------------------------------------------------------------------------
    func customizeViewLayout(){
        
        self.navigationController?.navigationBarHidden=true
        
    }
    
    //MARK: - View Life Cycle
    // -------------------------------------------------------------------------------
    // ViewDidLoad
    // -------------------------------------------------------------------------------
    override func viewDidLoad() {
        super.viewDidLoad()

        //Localize
        localize()
        
        //Customize
        customizeViewLayout()
        
    }
    
    //MARK: - IBAction
    // -------------------------------------------------------------------------------
    //	IBOutlet Click to view MyInfoManagerViewController
    // -------------------------------------------------------------------------------
    @IBAction func confirmButtonClicked(sender: UIButton) {
        
//        let myInfoManagerStroyboard     = UIStoryboard(name: "MyInfoManager", bundle: NSBundle.mainBundle())
//        let myInfoManagerController = myInfoManagerStroyboard.instantiateViewControllerWithIdentifier("PaymentPinDebitAccount") as? PaymentPinDebitAccountViewController
//        
//        self.navigationController?.pushViewController(myInfoManagerController!, animated: true)
//        
//        self.navigationController?.navigationBarHidden=false
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
