//
//  SessionManager.h
//  GlobalExpress
//
//  Created by JangWooil on 2015. 12. 29..
//  Copyright © 2015년 webcash. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constants.h"

@interface SessionManager : NSObject {
 
}

+ (SessionManager *)sharedSessionManager;

@property (nonatomic, retain) NSString* strNumberAlam;
@property (nonatomic, copy)   NSString* appStoreURL;
@property (nonatomic, copy)   NSString* updateCloseYN;    //업데이트 메시지 표시 후 앱 종료 여부
@property (nonatomic, copy)   NSString* contentServerURL; //API GATE 주소
@property (nonatomic, copy)   NSString* chnl_cd; //채널코드
@property (nonatomic, copy)   NSString* user_nm; //사용자이름
@property (nonatomic, copy)   NSString* ver_cd;
@property (nonatomic, copy)   NSArray*  leftMenuArr; //좌측 메뉴 데이터
@property (nonatomic, copy)   NSString* language; //사용자 언어(ko : 한국어, Base : 영어, km-KH : 캄보디아어)
@property (nonatomic, copy)  NSString* sessionId; //위변조 세션아이디
@property (nonatomic, copy)  NSString* token; //위변조 토큰
@property (nonatomic, copy)  NSDictionary* progressBarDic; //ProgressBar를 담을 Dictionary
@property (nonatomic, copy)  NSString* mainYn; //메인화면인지 여부를 구분하기 위한 변수(메인화면일 때만 사이드바 열리게 하기 위함)

@property (nonatomic)   CGFloat  screenHeight;
@property (nonatomic)   CGFloat  screenWidth;
- (void)showProgressBar : (UIViewController *)viewC;
- (void)closeProgressBar : (UIViewController *)viewC;
- (NSString *)serverLanguageCd; //서버 언어코드
- (NSString *)user_no; //사용자 번호
//- (NSString *)user_nm; //사용자 이름
- (NSString *)signupYn; //회원가입여부를 반환한다.

@end