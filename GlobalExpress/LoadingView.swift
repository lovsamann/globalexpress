//
//  LoadingView.swift
//  LoadingView
//
//  Created by Ann on 3/23/16.
//  Copyright © 2016 webcash. All rights reserved.
//

import UIKit

public class LoadingView: UIView {

    
    var spinner: UIActivityIndicatorView!
    
    var coverView: UIView?
    override public var frame : CGRect {
        didSet {
            self.update()
        }
    }
    
    class var sharedInstance: LoadingView {
        struct Singleton {
            static let instance = LoadingView(frame: CGRectMake(0,0, 80.0 ,80.0))
        }
        return Singleton.instance
    }
    
    //MARK: - override super
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.alpha = 0.0
        self.backgroundColor = UIColor.clearColor()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    //MARK: - static method
    public class func show() {
        let currentWindow: UIWindow = UIApplication.sharedApplication().keyWindow!
        
        let view = LoadingView.sharedInstance
//        view.backgroundColor = UIColor.redColor()
        let height : CGFloat = UIScreen.mainScreen().bounds.size.height
        let width : CGFloat = UIScreen.mainScreen().bounds.size.width
        let center : CGPoint = CGPointMake(width / 2.0, height / 2.0 )
        view.center = center
        
        if view.superview == nil {
            view.coverView = UIView(frame: currentWindow.bounds)
            view.coverView?.backgroundColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.5)
            

            currentWindow.addSubview(view.coverView!)
            currentWindow.addSubview(view)
            view.start()
        }
        
    }
    
    public class func hide(){
        let loadingView = LoadingView.sharedInstance
        loadingView.stop()
    }
    
    private func start(){
        self.spinner.startAnimating()
        self.alpha = 1.0

    }
    
    private func stop(){
        if ((coverView?.superview) != nil) {
            coverView?.removeFromSuperview()
        }
        
        if self.superview != nil {
            self.removeFromSuperview()
        }
        
        self.spinner.stopAnimating()
        self.alpha = 0.0
    }
    
    private func update() {
        
        if self.spinner == nil {
            self.spinner = UIActivityIndicatorView(activityIndicatorStyle: .WhiteLarge)
            self.spinner.center = CGPointMake(80 / 2, 80 / 2)
            self.addSubview(self.spinner)
        }else{

        }
        


    }
    
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */

}
