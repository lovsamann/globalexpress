//
//  CompletedSignupViewController.swift
//  GlobalExpress
//
//  Created by UDAM on 3/4/16.
//  Copyright © 2016 webcash. All rights reserved.
//

import UIKit

class CompletedSignupViewController: UIViewController {

    //MARK: - IBOutlet
    // -------------------------------------------------------------------------------
    //	IBOutlet label and button
    // -------------------------------------------------------------------------------
    @IBOutlet weak var topTitleLabel    : UILabel!
    @IBOutlet weak var descriptionLabel : UILabel!
    @IBOutlet weak var confirmButton    : UIButton!

    //MARK: - Private function
    // -------------------------------------------------------------------------------
    // Localize
    // -------------------------------------------------------------------------------
    override func localize() {
        
        self.topTitleLabel.text     = Language.localizedStr("M04_01")
        self.descriptionLabel.text  = Language.localizedStr("M04_02")
        self.confirmButton.setTitle(Language.localizedStr("M04_03"), forState: .Normal)
        
    }
    
    // MARK: - View Life Cycle
    // -------------------------------------------------------------------------------
    // ViewDidLoad
    // -------------------------------------------------------------------------------
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Localize
        localize()
        
    }
    
    // -------------------------------------------------------------------------------
    // viewWillDisappear
    // -------------------------------------------------------------------------------
    override func viewWillDisappear(animated: Bool) {
        //Push to refresh HomeViewController
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "refreshHomeVC:", name: "refreshHomeVC", object: nil)
    }

    // MARK: - IBAction
    // -------------------------------------------------------------------------------
    // Click to view HomeViewController
    // -------------------------------------------------------------------------------
    @IBAction func confirmClicked(sender: UIButton) {
        
        view.userInteractionEnabled = false
        
        /////////// USE THIS FORMAT
        let transaction = TransactionClass()
        transaction.sendAPI(APICode.INIT_R001, argument: ["" : ""],
            success: { (success) -> Void in
                
                if let rslt_cd = success["RSLT_CD"] as? String {
                    
                    if rslt_cd != "0000" { //오류
                        if let rsltMsg = success["RSLT_MSG"] as? String {
                            
                            self.showSimpleAlert("Error Message \(rsltMsg) ", delegate: self)
                            print("Error Code : \(rslt_cd), Error Message : \(rsltMsg)")
                        }
                    } else { //정상
                        if let respData = success["RESP_DATA"] as? NSDictionary {
                            let mainStoryboard      : UIStoryboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
                            let homeViewController  : UIViewController = mainStoryboard.instantiateViewControllerWithIdentifier("KYDrawerController")
                            
                            self.navigationController?.pushViewController(homeViewController, animated: true)
                            
                        }
                     
                        self.view.userInteractionEnabled = true
                    }
                }
        })
        
//        let mainStoryboard      : UIStoryboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
//        let homeViewController  : UIViewController = mainStoryboard.instantiateViewControllerWithIdentifier("KYDrawerController")
//        
//        self.navigationController?.pushViewController(homeViewController, animated: true)
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
