//
//  ConfirmTransactionViewController.swift
//  GlobalExpress
//
//  Created by Ann on 2/1/16.
//  Copyright © 2016 webcash. All rights reserved.
//

import UIKit

class ConfirmTransactionViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {

    
    private struct CELLIDENFITIER {
        static let TRANSCATION_TYPE = "transcationTypeCell"
        static let INFO = "infoCell"
        static let MONEY = "moneyCell"
        static let SERVICE = "serviceCell"
    }
    
    //MARK: - Outlet
    @IBOutlet var tableView: UITableView!
    
    //label
    @IBOutlet var comfirmStepBar: UILabel!
    @IBOutlet var receiverInfoLabel: UILabel!
    @IBOutlet var receiverNameLabel: UILabel!
    @IBOutlet var nationalityLabel: UILabel!
    @IBOutlet var mailLabel: UILabel!
    @IBOutlet var phoneNumberLabel: UILabel!
    @IBOutlet var selectHowToPayLabel: UILabel!
    @IBOutlet var infoLabel: UILabel!
    @IBOutlet var payByBankLabel: UILabel!
    @IBOutlet var payByBankInfoLabel: UILabel!
    @IBOutlet var payDirectDebtLabel: UILabel!
    @IBOutlet var payDirectDebitInfoLabel: UILabel!
    
    @IBOutlet var recevingInfoLabel: UILabel!
    @IBOutlet var TranscationInfoTypeLabel: UILabel!
    
    //button
    @IBOutlet var nextStepButton: UIButton!
    @IBOutlet var directDebitButton: UIButton!
    @IBOutlet var virtualAccountButton: UIButton!
    
    //constraints
    @IBOutlet var topViewConstraintToButtomTableViewConstraint: NSLayoutConstraint!
    
    var transcation: Transaction?
    
    //MARK: - override
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("Confirm \(transcation?.serviceFeeInUSD)")
        
        if ((self.navigationController?.navigationBarHidden) != nil) {
            self.navigationController?.navigationBarHidden = false
        }
        
        //set localize
        self.localize()
        
        //set lefe button
        let leftNavigationImage = UIImage(named: "back_btn.png")
        let leftNavigationButton = UIButton(frame: CGRectMake(0,0,(leftNavigationImage?.size.width)! / 2 , (leftNavigationImage?.size.height)! / 2))
        leftNavigationButton.setBackgroundImage(leftNavigationImage, forState: .Normal)
        leftNavigationButton.addTarget(self, action: "leftNavigationClicked", forControlEvents: .TouchUpInside)
        
        let leftBarButtonItem = UIBarButtonItem(customView: leftNavigationButton)
        self.navigationItem.leftBarButtonItem = leftBarButtonItem
        
        self.receiverNameLabel.text = transcation?.recipient?.name
        self.nationalityLabel.text = transcation?.recipient?.nationality?.name
        self.phoneNumberLabel.text = transcation?.recipient?.telephone
        self.mailLabel.text = transcation?.recipient?.email
        self.virtualAccountButton.selected = true
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        if let transcation = self.transcation {
            topViewConstraintToButtomTableViewConstraint.constant = transcation.recipient?.recipientType == "1" ? 8 : -35
        }
    }
    override func localize() {
        self.receiverInfoLabel.text = Language.localizedStr("T01_02")
        self.infoLabel.text = Language.localizedStr("T01_13")
        self.selectHowToPayLabel.text = Language.localizedStr("T01_14")
        self.payByBankLabel.text = Language.localizedStr("T01_15")
        self.payByBankInfoLabel.text = Language.localizedStr("T01_16")
        self.payDirectDebtLabel.text = Language.localizedStr("T01_17")
        self.payDirectDebitInfoLabel.text = Language.localizedStr("T01_18")
        self.nextStepButton.setTitle(Language.localizedStr("T01_19"), forState: .Normal)
        self.TranscationInfoTypeLabel.text = transcation?.recipient?.recipientType == "1" ? Language.localizedStr("T03_05") : Language.localizedStr("T01_05")
        self.recevingInfoLabel.text = Language.localizedStr("T01_13")
        self.comfirmStepBar.text = "3." + Language.localizedStr("T10_03")
        
        self.navigationItem.title = Language.localizedStr("T01_01")
    }
    
    //MARK: - UITableViewDataSource
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        var cellIdentifier: String
        
        if transcation?.recipient?.recipientType == "1" {
            if indexPath.row == 0 || indexPath.row == 1 {
                cellIdentifier = CELLIDENFITIER.TRANSCATION_TYPE
            }else if indexPath.row == 2{
                cellIdentifier = CELLIDENFITIER.INFO
            }else if indexPath.row == 3{
                cellIdentifier = CELLIDENFITIER.MONEY
            }else{
                cellIdentifier = CELLIDENFITIER.SERVICE
            }
        }else{
            if indexPath.row == 0 {
                cellIdentifier = CELLIDENFITIER.TRANSCATION_TYPE
            }else if indexPath.row == 1{
                cellIdentifier = CELLIDENFITIER.INFO
            }else if indexPath.row == 2{
                cellIdentifier = CELLIDENFITIER.MONEY
            }else{
                cellIdentifier = CELLIDENFITIER.SERVICE
            }
        }
        
        
        let cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier)
        
        let recipeint = transcation?.recipient
        
        if cell is TranscationTypeTableViewCell {
            let text = [ Language.localizedStr( recipeint!.recipientType == "1" ? "T03_06" : "T01_06" ),
                        Language.localizedStr("T03_02")
                        ]

            
            let value = [ recipeint!.bankName,  recipeint!.name ]
            (cell as! TranscationTypeTableViewCell).fxTextLabel.text = text[indexPath.row]
            (cell as! TranscationTypeTableViewCell).fxValueLabel.text = value[indexPath.row]
        }else if cell is AccountTableViewCell {
            (cell as! AccountTableViewCell).fxTextLabel.text = recipeint!.recipientType == "1" ? Language.localizedStr("T03_08") : Language.localizedStr("T01_06")
            (cell as! AccountTableViewCell).fxValueLabel.text = recipeint!.recipientType == "1" ? recipeint!.accountNumber : Language.localizedStr("T01_07")
            (cell as! AccountTableViewCell).infoCellLabel.text = recipeint!.recipientType == "1" ? Language.localizedStr("T03_09") : Language.localizedStr("R06_05")
        }else if cell is MoneyTableViewCell {
            (cell as! MoneyTableViewCell).fxTextLabel.text = Language.localizedStr("T01_09")
            guard let sendAmount: String = transcation?.sendAmount else{
                print("sendAmount nil")
                return cell!
            }
            (cell as! MoneyTableViewCell).fxValueLabel.text = sendAmount.formatKRWCurrency()
            guard let receiveAmount: String = transcation?.receiveAmount else{
                print("receiveAmount nil")
                return cell!
            }

            (cell as! MoneyTableViewCell).fxRecevingTextLabel.text = Language.localizedStr("T01_10")
            

            (cell as! MoneyTableViewCell).fxRecevingValueLabel.text = receiveAmount.formatUSDCurrency()
        }else {
            
            (cell as! ServiceFeeTableViewCell).fxTextLabel.text = Language.localizedStr("T01_11")
            guard let foreignRate: String = transcation?.foreignRate?.formatUSDCurrency() else{
                print("foreignRate nil")
                return cell!
            }
//
            print("ceellllll \(foreignRate)")
                print("ceellllll \(transcation?.serviceFee)")
//            print("ceellllll \(foreignRate.formatKRWCurrency())")
////            print("tttttt \((transcation?.foreignCurrency)!)")
//            
            (cell as! ServiceFeeTableViewCell).fxValueLabel.text = "1USD=" + foreignRate + (transcation?.foreignCurrency)!
            
            guard let fee = transcation?.serviceFee?.formatUSDCurrency() else{
                print("service fee nil")
                return cell!
            }
            
            (cell as! ServiceFeeTableViewCell).fxSendingFeeTextLabel.text = Language.localizedStr("T01_12")
            (cell as! ServiceFeeTableViewCell).fxSendingFeeValueLabel.text = fee + (transcation?.foreignCurrency)!
        }
        
        return cell!
    }
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return transcation?.recipient?.recipientType == "1" ? 5 : 4
    }
    
//  MARK: - UITableViewDelegate
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerImage = UIImage(named: "con_rad_top01.png")
        let headerImageView = UIImageView(frame: CGRectMake(0, 0, CGRectGetWidth(tableView.frame), 9))
        
        headerImageView.image = headerImage
        return headerImageView
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 9.0
    }
    
    //custom footer
    func tableView(tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        
        let footerImage = UIImage(named: "con_rad_bottom01.png")
        let footerImageView = UIImageView(frame: CGRectMake(0, 0, CGRectGetWidth(tableView.frame), 9))
        footerImageView.image = footerImage
        
        return footerImageView
    }
    
    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 9.0
    }
    
    //cell hieght
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        
        if transcation?.recipient?.recipientType == "1" {
            if indexPath.row == 0 || indexPath.row == 1 {
                return 44
            }else if indexPath.row == 2 {
                return 88
            }else if indexPath.row == 3{
                return 80
            }else{
                return 65
            }
        }else{
            if indexPath.row == 0 {
                return 44
            }else if indexPath.row == 1 {
                return 88
            }else if indexPath.row == 2{
                return 80
            }else{
                return 65
            }
        }
    }
    
    
    
    //MARK: - Actions
    
    @IBAction func paymentTypeButtonClicked(sender: UIButton) {
        if sender == virtualAccountButton {
            virtualAccountButton.selected = true
            directDebitButton.selected = false
        }else{
            virtualAccountButton.selected = false
            directDebitButton.selected = true
        }
        
    }
    
    @IBAction func nextStepClicked(sender: UIButton) {

        if virtualAccountButton.selected {
            
            guard let reqTranscation = transcation else{
                print("transcaiton nil")
                return
            }
            
            let type:String = reqTranscation.recipient!.recipientType!
            
            var req: [String:String]
            
            
            if type == "1" {
                req = ["SEND_AMT": reqTranscation.sendAmount!,
                    "RECV_AMT" : reqTranscation.receiveAmount!,
                    "NATION_CD" : reqTranscation.nationlity!.code!,
                    "CURRENCY_CD": reqTranscation.currency!.code!,
                    "FX_RATE": reqTranscation.foreignRate!,
                    "SVC_FEE": reqTranscation.serviceFee!,
                    "SVC_USDFEE": String(format: "%.2f", Double(reqTranscation.serviceFeeInUSD!)!),
                    "RCPT_CD": reqTranscation.recipient!.recipientCode!,
                    "ACCT_NO": reqTranscation.recipient!.accountNumber!]
            }else{
            
                print("reqtranscation \(reqTranscation.recipient?.telephone)")
                
                guard let telephone = reqTranscation.recipient?.telephone else{
                    self.showEventAlert("Telephone nil", hasCancelButton: false, delegate: self, tag: 290000, confirmBlock: nil, cancelBlock: nil)
                    return
                }
                
                req = ["SEND_AMT": reqTranscation.sendAmount!,
                        "RECV_AMT" : reqTranscation.receiveAmount!,
                        "NATION_CD" : reqTranscation.nationlity!.code!,
                        "CURRENCY_CD": reqTranscation.currency!.code!,
                        "FX_RATE": reqTranscation.foreignRate!,
                        "SVC_FEE": reqTranscation.serviceFee!,
                        "SVC_USDFEE": String(format: "%.2f", Double(reqTranscation.serviceFeeInUSD!)!),
                        "RCPT_CD": reqTranscation.recipient!.recipientCode!,
                        "TEL_NO" : telephone,
                        "CPORG_CD": reqTranscation.recipient!.orgainzation!.code!]

            }

            TransactionClass().sendAPI( type == "1" ? APICode.REGISTER_TRASCATION_C001 : APICode.REGISTER_CASH_PICK_C001 , argument: req , success: { (success) -> Void in
                if let result = success["RSLT_CD"] where result as! String == "0000" { // everything's fine
                    if let data = success["RESP_DATA"] as? [String: String!] {
                        self.performSegueWithIdentifier("completedPaymentSegue", sender: data)
                    }else{
                        //TODO: alert messges
                    }
                }else{
                    self.showEventAlert(success["RSLT_MSG"]! as! String, hasCancelButton: false, delegate: self, tag: 1980, confirmBlock: nil, cancelBlock: nil)
                }
            })
        }else{
            //TODO: USING DEBIT
            TransactionClass().sendAPI(APICode.TRSCHK_R001, argument: ["":""], success: { (success) -> Void in
                guard let code = success["RSLT_CD"] where code as! String == "0000" else{
                    //Should be alert here
                    print("RESULT MESSAGE = \(success["RSLT_MSG"])")
                    return
                }
                if let data = success["RESP_DATA"]{
                    print("RESPONSE DATA = \(data)")
                    guard let chk_gb = data["CHK_GB"] as? String where chk_gb == "0" else{
                        //ALREADY SIGNUP
                        //GOTO T13 : Enter PIn
                        let viewcontroller = self.storyboard!.instantiateViewControllerWithIdentifier("EnterPinCodeViewController") as? EnterPinCodeViewController
                        viewcontroller?.customerinfo = data as! Dictionary<String,String>
                        viewcontroller?.trans = self.transcation
                        self.navigationController?.pushViewController(viewcontroller!, animated: true)
                        return
                    }
                    //NOT YET SIGN UP
                    //GOTO DEBIT SIGN UP
                    let viewcontroller = self.storyboard!.instantiateViewControllerWithIdentifier("DespositSignupViewController") as? DepositSignupViewController
                    self.navigationController?.pushViewController(viewcontroller!, animated: true)
                }
            })
        }
    }
    
    //MARK: - Navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "completedPaymentSegue" {
            let compeletedPaymentVC = segue.destinationViewController as? CompeletedPaymentViewController
            compeletedPaymentVC?.completedRequestDic = sender as? [String:String!]
        }
    }

    
    func leftNavigationClicked(){
        
        let viewControllers = self.navigationController?.viewControllers
        
        if let _ = viewControllers![(viewControllers?.count)! - 2] as? RecipientInfoViewController { // found the previeus vc
            for viewController in viewControllers! {
                if viewController is RecipientViewController {
                    let recipeintViewcontorller = viewControllers![(viewControllers?.count)! - 4] as? RecipientViewController// grab recipeinetViewController
                    self.navigationController?.popToViewController(recipeintViewcontorller!, animated: true)
                    break
                }
            }
        }else{
            self.navigationController?.popViewControllerAnimated(true)
        }        
    }
}
