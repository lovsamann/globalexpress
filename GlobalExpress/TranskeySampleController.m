//
//  TranskeySampleController.m
//  GlobalExpress
//
//  Created by JangWooil on 2016. 1. 20..
//  Copyright © 2016년 webcash. All rights reserved.
//

#import "TranskeySampleController.h"
#define kTextKeypadTag 9999
#define kNumberKeypadTag 8888

@interface TranskeySampleController ()

@end

@implementation TranskeySampleController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //텍스트 필드 클릭 시 쿼티 보안키패드 띄우기
    [self.textPassword addTarget:self action:@selector(showTextTranskey) forControlEvents:UIControlEventEditingDidBegin];
    //텍스트 필드 클릭 시 숫자 보안키패드 띄우기
    [self.numberPassword addTarget:self action:@selector(showNumberTranskey) forControlEvents:UIControlEventEditingDidBegin];
}

- (void)returnKeyboard:(NSUInteger)tagNumber inputlength:(NSUInteger)inputlength plainText:(NSString *)plainText encText:(NSString *)encText isCancel:(BOOL)isCancel
{
    if (isCancel == NO) {
        if (tagNumber == kTextKeypadTag) {
            self.textPassword.text = plainText;
        } else if (tagNumber == kNumberKeypadTag) {
            self.numberPassword.text = plainText;
        }
    }
}

- (void)showTextTranskey
{
    [[KeypadView sharedKeypadView] showQwertyTranskey:self minLength:8 maxLength:15 tagNumber:kTextKeypadTag title:@"입력"];
}

- (void)showNumberTranskey
{
    [[KeypadView sharedKeypadView] showNumberTranskey:self minLength:4 maxLength:4 tagNumber:kNumberKeypadTag title:@"숫자입력"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
