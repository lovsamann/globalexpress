//
//  NationPopupViewController.swift
//  GlobalExpress
//
//  Created by Ralex on 2/26/16.
//  Copyright © 2016 webcash. All rights reserved.
//

import UIKit

protocol NationViewDelegate{
    func selectedNation(nationGB:String)
}

class NationPopupViewController: MJPopupViewController,UITableViewDataSource,UITableViewDelegate{

    @IBOutlet weak var nationTableView: UITableView!
    @IBOutlet weak var lblSelectCountry: UILabel!
    
    @IBOutlet weak var viewHeader: UIView!
    var arrNation:Array<String>!
    var domestic:String = ""
    var foreigner:String = ""
    
    var delegate:NationViewDelegate?
    
    override func localize() {
        lblSelectCountry.text = Language.localizedStr("C10P_01")
        domestic = Language.localizedStr("C10P_02")
        foreigner = Language.localizedStr("C10P_03")
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        localize()
        self.nationTableView.registerNib(UINib(nibName: "NationTableViewCell", bundle: nil), forCellReuseIdentifier: "NationCell")
        arrNation = [domestic,foreigner]
        let headerMaskLayer = CAShapeLayer()
        headerMaskLayer.path = UIBezierPath(roundedRect: self.viewHeader.bounds, byRoundingCorners: UIRectCorner.TopLeft.union(.TopRight), cornerRadii: CGSizeMake(5, 5)).CGPath
        self.viewHeader.layer.mask = headerMaskLayer
        
        let maskLayer = CAShapeLayer()
        maskLayer.path = UIBezierPath(roundedRect: self.nationTableView.bounds, byRoundingCorners: UIRectCorner.BottomLeft.union(.BottomRight), cornerRadii: CGSizeMake(5, 5)).CGPath
        self.nationTableView.layer.mask = maskLayer
        
        self.view.layer.cornerRadius = 5
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrNation.count
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let nationcell = nationTableView.dequeueReusableCellWithIdentifier("NationCell", forIndexPath: indexPath) as! NationTableViewCell
        nationcell.lblString.text = arrNation[indexPath.row]
        
        //Configure color Cell selection
        let customColorView = UIView()
        customColorView.backgroundColor = UIColor(colorLiteralRed: 235.0/255.0, green: 240.0/255.0, blue: 242.0/255.0, alpha: 1.0)
        nationcell.selectedBackgroundView = customColorView
        
        return nationcell
    }
    
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        guard delegate != nil else{
            print("")
            return
        }
        self.delegate?.selectedNation("\(indexPath.row)")
        self.dismissPopupViewController(self, animationType: MJPopupViewAnimationFade)
    }
    
    
    @IBAction func ActionDismiss(sender: AnyObject) {
        self.dismissPopupViewController(self, animationType: MJPopupViewAnimationFade)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
