//
//  LangTableViewCell.swift
//  GlobalExpress
//
//  Created by JangWooil on 2016. 2. 17..
//  Copyright © 2016년 webcash. All rights reserved.
//

import UIKit

class LangTableViewCell: UITableViewCell {

    @IBOutlet var radioImage: UIImageView!
    
    @IBOutlet var descText: UILabel!
    
}
