//
//  Constants.m
//  GlobalExpress
//
//  Created by JangWooil on 2016. 1. 26..
//  Copyright © 2016년 webcash. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Constants.h"

@implementation Constants

+(NSString *)PushServerAddress{ return kPushServerAddress; }
+(NSString *)SM_GATEWAY_URL{ return _SM_GATEWAY_URL; }
+(NSString *)PushStartNotification{ return kPushStartNotification; }
+(NSString *)KoreaCode{ return kKoreaCode; }
+(NSString *)CambodiaCode{ return kCambodiaCode; }
+(NSString *)EnglishCode{ return kEnglishCode; }

@end