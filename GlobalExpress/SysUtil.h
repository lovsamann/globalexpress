//
//  SysUtil.h
//  GlobalExpress
//
//  Created by JangWooil on 2016. 1. 14..
//  Copyright © 2016년 webcash. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface SysUtil : NSObject
{
    
}

//Message와 delegate, 취소 버튼 유무에 따른 AlertView를 생성하여 반환한다.
+ (UIAlertView *) getAlertViewWithmessage:(NSString *)message delegate:(UIViewController *)alertDelegate hasCancelButton:(BOOL)hasCancelButton;
// Message만 받아서 확인 버튼만 있는 alertController를 생성하여 반환한다.
+(UIAlertController *)getAlertControllerWithmessage:(NSString *)message;
// Message와 취소 버튼 유무, 취소 버튼 유무, 확인 취소버튼 이벤트를 받아서 alertController를 생성하여 반환한다.
+(UIAlertController *)getAlertControllerWithmessage:(NSString *)message hasCancelButton:(BOOL)hasCancelButton
                                 confirmSelectBlock:(void (^)(NSString *))confirmSelectBlock cancelSelectBlock:(void (^)(NSString *))cancelSelectBlock;
//IOS 8 하위버전인지 여부
+(BOOL)IS_BEFORE_OS_8;
//IOS 9 하위버전인지 여부
+(BOOL)IS_BEFORE_OS_9;

@end
