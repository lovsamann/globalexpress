//
//  EnterPinCodeViewController.swift
//  GlobalExpress
//
//  Created by Ralex on 2/4/16.
//  Copyright © 2016 webcash. All rights reserved.
//

import UIKit

class EnterPinCodeViewController: UIViewController {

    @IBOutlet weak var lblBankAccount: UILabel!
    @IBOutlet weak var lblBankname: UILabel!
    @IBOutlet weak var lblSendingKOR: UILabel!
    @IBOutlet weak var lblReceivingUSD: UILabel!
    @IBOutlet weak var lblSendingFee: UILabel!
    @IBOutlet weak var lblExchangeRate: UILabel!
    
    @IBOutlet weak var debitBank: UILabel!
    @IBOutlet weak var debitAccount: UILabel!
    @IBOutlet weak var lblInformation: UILabel!
    
    @IBOutlet weak var passCodeTextField : UITextField!
    @IBOutlet weak var viewInset : UIView!
    @IBOutlet weak var lblStep: UILabel!
    
    let numOfImageInset = 5
    var startx:CGFloat = 0.0
    var isDelete:Bool = false
    //variable
    var customerinfo = Dictionary<String,String>()
    var navigationTitle = "Enter PIN No."
    var trans:Transaction?
    
    
    func initLoad(){
        lblBankname.text = customerinfo["BANK_NM"]!
        lblBankAccount.text = customerinfo["ACCT_NO"]!
        lblSendingKOR.text = trans?.sendAmount
        lblReceivingUSD.text = trans?.receiveAmount
        lblSendingFee.text = "\((trans?.serviceFee)!) KRW"
        lblExchangeRate.text = "1.00USD=\((trans?.foreignRate)!)KRW"
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController!.navigationBar.translucent = false
        
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController!.navigationBar.translucent = true
    }
    
    override func localize() {
        navigationTitle = Language.localizedStr("T13_01")
        debitBank.text = Language.localizedStr("T13_02")
        debitAccount.text = Language.localizedStr("T13_03")
        lblInformation.text = Language.localizedStr("T13_04")
        lblStep.text = "4. \(Language.localizedStr("M01_06"))"
        //NEED CONFIRM BUTTON FROM KEYBOARD
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //TODO: Localize
        localize()
        
        // TODO: initial view
        RLib.setNavigationTitle(self, title: navigationTitle)
        RLib.setLeftNavigationItem(self, image: "back_btn.png", action: Selector("handleLeftNavigationItemClick:"))
        self.initLoad()
        
        passCodeTextField.becomeFirstResponder()
        passCodeTextField.tintColor = UIColor.clearColor()
    }
    
    func initPinCharLayout(numOfNumberInput:Int){
        var count = 0
        if isDelete == true{
            for subview in viewInset.subviews{
                subview.removeFromSuperview()
            }
        }
        for index in 0...numOfImageInset{
            startx = CGFloat(index * 29)
            let imgPasscode = UIImageView()
            if numOfNumberInput == 0{
                imgPasscode.frame = CGRectMake(startx + 8, 19, 13 , 3)
                imgPasscode.image = UIImage(named: "pin_emp_char.png")
            }else{
                //Add it depend on index of inputted
                imgPasscode.frame = CGRectMake(startx + 8, 13, 13 , 13)
                imgPasscode.image = UIImage(named: "pin_char.png")
                count++
                if count > numOfNumberInput {
                    imgPasscode.frame = CGRectMake(startx + 8, 19, 13 , 3)
                    imgPasscode.image = UIImage(named: "pin_emp_char.png")
                }
            }
            viewInset.addSubview(imgPasscode)
        }
        
    }
    
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
        //LOOP IMAGE IN VIEW]
        initPinCharLayout(0)
        return true
        
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        let numOfNumberInput = (textField.text?.utf16.count)! + string.utf16.count - range.length
        //IF BACK KEY PRESS
        if range.length == 1{
            isDelete = true
            initPinCharLayout(numOfNumberInput)
        }else{
            isDelete = false
            initPinCharLayout(numOfNumberInput)
        }
        return numOfNumberInput > 6 ? false : true
    }

    
    func handleLeftNavigationItemClick(sender:UIBarButtonItem){
        navigationController?.popViewControllerAnimated(true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func handlePressOnView(sender: AnyObject) {
        passCodeTextField.resignFirstResponder()
    }

    @IBAction func ActionConfirm(sender: AnyObject) {
        guard passCodeTextField.text != "" else{
            #if _DEBUG_
                print("Please input pin code!")
            #endif
            return
        }
        
        
        let service_feeusd = (trans?.serviceFeeInUSD)!.formatUSDCurrency()
        
        var args:Dictionary<String,String>
        
        print("Transaction = \(trans?.serviceFee)")
        
        args = [
            "SEND_AMT": (trans?.sendAmount)!,
            "RECV_AMT": (trans?.receiveAmount)!,
            "NATION_CD": (trans?.nationlity?.code)!,
            "CURRENCY_CD": (trans?.currency?.code)!,
            "FX_RATE": (trans?.foreignRate)!,
            "SVC_FEE": (trans?.serviceFee)!,
            "SVC_USDFEE": service_feeusd!,
            "RCPT_GB":(trans?.recipient?.recipientType)!,
            "RCPT_CD":(trans?.recipient?.recipientCode)!,
            "ACCT_NO":customerinfo["ACCT_NO"]!,
            "ACCT_CD":customerinfo["ACCT_CD"]!,
            "SETL_PSWD":passCodeTextField.text!
        ]
        print("Request = \(args)")
        TransactionClass().sendAPI(APICode.TRANSFER_C001, argument: args, success: { (success) -> Void in
            print("Response = \(success)")
            guard let code = success["RSLT_CD"] where code as! String == "0000" else{
                //Should be alert here
                guard #available(iOS 9.0,*) else{
                    // Fallback on earlier versions
                    let alertView = UIAlertView(title: "", message: "\(success["RSLT_MSG"]!)", delegate: nil, cancelButtonTitle: "OK")
                    alertView.show()
                    return
                }
                let alertController : UIAlertController = UIAlertController(title: "", message: "\(success["RSLT_MSG"]!)", preferredStyle: .Alert)
                let confirmAction   : UIAlertAction     = UIAlertAction(title: "OK", style: .Cancel, handler:nil)
                alertController.addAction(confirmAction)
                self.presentViewController(alertController, animated: true, completion: nil)
                return

            }
            if let success_data = success["RESP_DATA"] as? Dictionary<String,String>{
                self.performSegueWithIdentifier("completetransersegue", sender: success_data)
            }
            
        })
        

    }
    
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "completetransersegue"{
            let destView = segue.destinationViewController as? CompleteTransferViewController
            destView?.data = sender as? Dictionary<String,String>
            destView?.trans = trans
        }
    }
    

}