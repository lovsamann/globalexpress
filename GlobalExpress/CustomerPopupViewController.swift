//
//  CustomerPopupViewController.swift
//  GlobalExpress
//
//  Created by Ralex on 3/7/16.
//  Copyright © 2016 webcash. All rights reserved.
//

import UIKit

class CustomerPopupViewController: MJPopupViewController{

    /*
        TODO: Outlet
    */
    @IBOutlet weak var lblCustomerTitle: UILabel!
    @IBOutlet weak var lblGMoneyTransInfo: UILabel!
    @IBOutlet weak var btnEmailInquiry: UIButton!
    @IBOutlet weak var btnARSHotline: UIButton!
    @IBOutlet weak var btnClose: UIButton!
    
    /*
        TODO: Localize string multi language
    */
    override func localize() {
        lblCustomerTitle.text = Language.localizedStr("C07P_01")
        lblGMoneyTransInfo.text = Language.localizedStr("C07P_02")
        btnEmailInquiry.setTitle(Language.localizedStr("C07P_03"), forState: .Normal)
        btnARSHotline.setTitle(Language.localizedStr("C07P_04"), forState: .Normal)

    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.layer.cornerRadius = 6
        btnClose.addTarget(self, action: "handleCustomerAction:", forControlEvents: .TouchUpInside)
        btnEmailInquiry.addTarget(self, action: "handleCustomerAction:", forControlEvents: .TouchUpInside)
        btnARSHotline.addTarget(self, action: "handleCustomerAction:", forControlEvents: .TouchUpInside)
        localize()
    }

    /*
        TODO: Handle Button Customer Click
    */
    func handleCustomerAction(sender:UIButton){
        if sender.tag == 0{
            //CLOSE
            self.dismissPopupViewController(self, animationType: MJPopupViewAnimationFade)
        }else if sender.tag == 1{
            //EMAIL INQUIRY
            TransactionClass().sendAPI(APICode.INQUIRY_R001, argument: ["" : ""], success: { (success) -> Void in
                guard let code = success["RSLT_CD"] where code as! String == "0000" else{
                    //Should be alert here
                    guard #available(iOS 9.0,*) else{
                        // Fallback on earlier versions
                        let alertView = UIAlertView(title: "", message: "\(success["RSLT_MSG"]!)", delegate: nil, cancelButtonTitle: "OK")
                        alertView.show()
                        return
                    }
                    let alertController : UIAlertController = UIAlertController(title: "", message: "\(success["RSLT_MSG"]!)", preferredStyle: .Alert)
                    let confirmAction   : UIAlertAction     = UIAlertAction(title: "OK", style: .Cancel, handler:nil)
                    alertController.addAction(confirmAction)
                    self.presentViewController(alertController, animated: true, completion: nil)
                    return

                }
                if let customerInfo = success["RESP_DATA"] as? Dictionary<String, String>{
                    let storyboard = UIStoryboard(name: "CustomerCenter", bundle: nil)
                    let viewcontroller = storyboard.instantiateViewControllerWithIdentifier("askquestionviewcontroller") as? AskQuestionViewController
                    viewcontroller?.customerInfo = customerInfo
                    let navigtion = UINavigationController(rootViewController: viewcontroller!)
                    //self.navigationController!.pushViewController(viewcontroller!, animated: true)
                    self.presentViewController(navigtion, animated: true, completion: nil)
                    self.dismissPopupViewController(viewcontroller, animationType: MJPopupViewAnimationFade)
                }
            })

        }
        else{
            //ARS HOTLINE
            guard #available(iOS 9.0,*) else{
                // Fallback on earlier versions
                let alertView = UIAlertView(title: "", message: "Connect to ARS!", delegate: nil, cancelButtonTitle: "OK")
                alertView.show()
                return
            }
            let alertController : UIAlertController = UIAlertController(title: "", message: "Connect to ARS!", preferredStyle: .Alert)
            let confirmAction   : UIAlertAction     = UIAlertAction(title: "OK", style: .Cancel, handler:nil)
            alertController.addAction(confirmAction)
            self.presentViewController(alertController, animated: true, completion: nil)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
