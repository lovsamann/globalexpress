//
//  PhoneAuthViewController.swift
//  GlobalExpress
//
//  Created by Ralex on 2/2/16.
//  Copyright © 2016 webcash. All rights reserved.
//

import UIKit

/*
    01: 회원등록
    02: :출금계좌등록,
    03: 결제비밀번호 재설정
    04: 출금계좌변경
*/
struct  CertificateTrading{
    static let REGISTRATION = "01"
    static let WIDRAWACC_REGISTER = "02"
    static let PAYMENTRESETPSWD = "03"
    static let WIDRAWACC_CHANGE = "04"
}
class PhoneAuthViewController: UIViewController,NationViewDelegate,GenderViewDelegate,UIScrollViewDelegate,UITextFieldDelegate{

    @IBOutlet weak var ViewContainter_top: UIView!
    @IBOutlet weak var viewNameKOR: UIView! //View Korean Name
    
    
    
    @IBOutlet weak var korNameLabel:UILabel!
    @IBOutlet weak var enNameLabel: UILabel!
    @IBOutlet weak var birthDateLabel: UILabel!
    @IBOutlet weak var mobielLabel: UILabel!
    @IBOutlet weak var NationORGenderLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var AgreeAllServiceLabel: UILabel!
    @IBOutlet weak var ServiceTermTop: UILabel!
    @IBOutlet weak var AgreeServiceTerms: UILabel!
    @IBOutlet weak var AgreePrivacyTerms: UILabel!
    @IBOutlet weak var PhoneAuthTermTop: UILabel!
    @IBOutlet weak var IdentifyVerification: UILabel!
    @IBOutlet weak var PersonalInfo: UILabel!
    @IBOutlet weak var UniqueIdentifyProcess: UILabel!
    @IBOutlet weak var TelecomTerms: UILabel!
    @IBOutlet weak var btnSendNo: UIButton!
    
    
    @IBOutlet weak var tfKoreaName: UITextField!
    @IBOutlet weak var tfEnglishName: UITextField!
    @IBOutlet weak var lblBirthDate: UILabel!
    @IBOutlet weak var lblNation: UILabel!
    @IBOutlet weak var lblGender: UILabel!
    @IBOutlet weak var tfMobilePhone:UITextField!
    
    @IBOutlet weak var btnShow1: UIButton!
    @IBOutlet weak var btnShow2: UIButton!
    @IBOutlet weak var btnShow3: UIButton!
    @IBOutlet weak var btnShow4: UIButton!
    @IBOutlet weak var btnShow5: UIButton!
    @IBOutlet weak var btnShow6: UIButton!
    
    @IBOutlet weak var segmentControl: ADVSegmentedControl!
    @IBOutlet weak var viewAgreeTerm: UIView!
    @IBOutlet weak var viewNameENG_constraint_Top: NSLayoutConstraint!
    
    @IBOutlet weak var viewNation: UIView!
    @IBOutlet weak var viewGender: UIView!
    
    @IBOutlet weak var viewContainerTop_constraint_height: NSLayoutConstraint!
    @IBOutlet weak var ContentMain_constraint_height: NSLayoutConstraint!
    
    @IBOutlet weak var btnAgreedAllService: UIButton!
    @IBOutlet weak var btnAgreedServiceTerm: UIButton!
    @IBOutlet weak var btnAgreedWithPrivacy: UIButton!
    @IBOutlet weak var btnAgreeIdVerify: UIButton!
    @IBOutlet weak var btnAgreePersonalInfo: UIButton!
    @IBOutlet weak var btnAgreeUniqueIdProcess: UIButton!
    @IBOutlet weak var btnAgreeTelecomTerms: UIButton!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var lblInternation_Code: UILabel!
    
    @IBOutlet weak var lblStep: UILabel!
    //Variable
    
    var custinfodictionary:Dictionary<String,AnyObject> = [:]
    var transerial:String?
    var nation_gb:String = ""
    var gender_str:String = ""
    var tradeStatus:Int?
    var attrs = [
        NSFontAttributeName : UIFont.systemFontOfSize(14.0),
        NSForegroundColorAttributeName : UIColor(red: 49/255, green: 51/255, blue: 51/255, alpha: 1),
        NSUnderlineStyleAttributeName : 1]
    
    var attributedString = NSMutableAttributedString(string:"")
    var arrayTelecom:Array<String> = []
    var navigationTitle = ""
    var allowNationPop:Bool = false
    var allowGenderPop:Bool = false
    //GESTURE
    let viewPress:UITapGestureRecognizer = UITapGestureRecognizer()
    
    
    override func localize() {
        navigationTitle = Language.localizedStr("T06_01")
        enNameLabel.text = Language.localizedStr("T06_02")
        korNameLabel.text = Language.localizedStr("T06_02")
        birthDateLabel.text = Language.localizedStr("T06_04")
        mobielLabel.text =  Language.localizedStr("T06_05")
        NationORGenderLabel.text = Language.localizedStr("T06_07")
        statusLabel.text = Language.localizedStr("T06_10")
        AgreeAllServiceLabel.text = Language.localizedStr("T06_11")
        ServiceTermTop.text = Language.localizedStr("T06_12")
        AgreeServiceTerms.text = Language.localizedStr("T06_13")
        AgreePrivacyTerms.text = Language.localizedStr("T06_14")
        PhoneAuthTermTop.text = Language.localizedStr("T06_15")
        IdentifyVerification.text = Language.localizedStr("T06_16")
        PersonalInfo.text = Language.localizedStr("T06_17")
        UniqueIdentifyProcess.text = Language.localizedStr("T06_18")
        TelecomTerms.text = Language.localizedStr("T06_19")
        btnSendNo.setTitle(Language.localizedStr("T06_20"), forState: .Normal)
        tfMobilePhone.attributedPlaceholder = NSAttributedString(string: Language.localizedStr("T06_06"))
        lblNation.text = Language.localizedStr("C10P_02")
        lblGender.text = Language.localizedStr("C11P_02")
        btnShow1.setTitle(Language.localizedStr("M02_15"), forState: .Normal)
        btnShow2.setTitle(Language.localizedStr("M02_15"), forState: .Normal)
        btnShow3.setTitle(Language.localizedStr("M02_15"), forState: .Normal)
        btnShow4.setTitle(Language.localizedStr("M02_15"), forState: .Normal)
        btnShow5.setTitle(Language.localizedStr("M02_15"), forState: .Normal)
        btnShow6.setTitle(Language.localizedStr("M02_15"), forState: .Normal)
        lblStep.text = "3. \(Language.localizedStr("M04_03"))"
    }
    
    func initLoad() -> Void{
        
        RLib.setNavigationTitle(self, title: navigationTitle)
        RLib.setLeftNavigationItem(self, image: "back_btn.png", action: Selector("handleLeftNavigationItemClick:"))
        
        self.ViewContainter_top.layer.cornerRadius = 6
        self.viewAgreeTerm.layer.cornerRadius = 6
        
        //Add Underline
        let buttonTitleStr = NSMutableAttributedString(string:Language.localizedStr("M02_15"), attributes:attrs)
        attributedString.appendAttributedString(buttonTitleStr)
        btnShow1.setAttributedTitle(attributedString, forState: .Normal)
        btnShow2.setAttributedTitle(attributedString, forState: .Normal)
        btnShow3.setAttributedTitle(attributedString, forState: .Normal)
        btnShow4.setAttributedTitle(attributedString, forState: .Normal)
        btnShow5.setAttributedTitle(attributedString, forState: .Normal)
        btnShow6.setAttributedTitle(attributedString, forState: .Normal)

        //PassData
        nationCheck((custinfodictionary["NATION_GB"] as? String)!)
        genderCheck((custinfodictionary["SEX_GB"] as? String)!)
        tfKoreaName.text = custinfodictionary["KOR_NM"] as? String
        tfEnglishName.text = custinfodictionary["ENG_NM"] as? String
        tfMobilePhone.text = custinfodictionary["HP_NO"] as? String
        lblBirthDate.text = RLib.formatDate(custinfodictionary["BIRTH_YMD"] as! String)
        lblInternation_Code.text = custinfodictionary["INTL_TELCD"] as? String
        
        //Segmemnt Control
        for index in 4...custinfodictionary["TELCOM_REC"]!.count!{
            arrayTelecom.append((custinfodictionary["TELCOM_REC"]![index-1]["TELCOM_NM"] as? String)!)
        }
        arrayTelecom.append("\(Language.localizedStr("C09P_02"))")
        segmentControl.items = arrayTelecom
        segmentControl.font = UIFont(name: "Helvetica Neue", size: 15)
        segmentControl.userInteractionEnabled = false
        segmentControl.selectedIndex = telecomSelected((custinfodictionary["TELCOM_CD"] as? String)!) //selected at index
    }

    func handleLeftNavigationItemClick(sender:UIBarButtonItem){
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    /*
        TODO: Check Nation
    */
    func nationCheck(nation:String){
        if nation != ""{
            allowNationPop = false
            lblNation.textColor = UIColor(red: 48/255, green: 50/255, blue: 51/255, alpha: 1)
            nation_gb = nation
            if nation == "0"{
                lblNation.text = Language.localizedStr("C10P_02")
                //SHOW KOREAN NAME TEXT FIELD
                viewNameKOR.hidden = false
                viewContainerTop_constraint_height.constant = 264
                viewNameENG_constraint_Top.constant = 0
                ContentMain_constraint_height.constant = 246
            }
            else if nation == "1"{
                lblNation.text = Language.localizedStr("C10P_03")
                //SHOW ENGLSIH NAME TEXT FIELD
                viewNameKOR.hidden = true
                viewContainerTop_constraint_height.constant = 264 - viewNameKOR.frame.size.height
                viewNameENG_constraint_Top.constant = 0 - viewNameKOR.frame.size.height
                ContentMain_constraint_height.constant = 246 - viewNameKOR.frame.size.height
            }
        }else{
            allowNationPop = true
        }

    }
    /*
        TODO: Check Gender
    */
    func genderCheck(gender:String){
        if gender != ""{
            lblGender.textColor = UIColor(red: 48/255, green: 50/255, blue: 51/255, alpha: 1)
            allowGenderPop = false
            gender_str = gender
            if gender == "1"{
                lblGender.text = Language.localizedStr("C11P_02")
            }
            else if gender == "2"{
                lblGender.text = Language.localizedStr("C11P_03")
            }
        }else{
            allowGenderPop = true
        }
    }
    
    /*
        TODO: Select Nation Gesture
    */
    @IBAction func handleNationPopupTap(sender: AnyObject) {
        tfEnglishName.resignFirstResponder()
        tfMobilePhone.resignFirstResponder()
        tfKoreaName.resignFirstResponder()
        if allowNationPop == true{
            let nation: NationPopupViewController = NationPopupViewController(nibName: "NationPopupViewController",bundle: NSBundle.mainBundle())
            nation.delegate = self
            presentPopupViewController(nation, animationType: MJPopupViewAnimationFade, contentInteraction: MJPopupViewContentInteractionDismissBackgroundOnly)
        }
    }
    /*
        TODO: Select Gender Gesture
    */
    @IBAction func handleGenderPopupTap(sender: AnyObject) {
        tfEnglishName.resignFirstResponder()
        tfMobilePhone.resignFirstResponder()
        tfKoreaName.resignFirstResponder()
        if allowGenderPop == true{
            let gender: GenderPopupViewController = GenderPopupViewController(nibName: "GenderPopupViewController",bundle: NSBundle.mainBundle())
            gender.delegate = self
            presentPopupViewController(gender, animationType: MJPopupViewAnimationFade, contentInteraction: MJPopupViewContentInteractionDismissBackgroundOnly)
        }
        
    }
    /*
        TODO: PressView Gesture to Hide Keyboard
    */
    @IBAction func handleViewTap(sender: AnyObject) {
        tfEnglishName.resignFirstResponder()
        tfKoreaName.resignFirstResponder()
        tfMobilePhone.resignFirstResponder()
    }
    /*
        TODO: ScrollDown to Hide Keyboard
    */
    func scrollViewDidScroll(scrollView: UIScrollView) {
        tfEnglishName.resignFirstResponder()
        tfKoreaName.resignFirstResponder()
        tfMobilePhone.resignFirstResponder()
    }
    
    //REMARK:
    /*
        - param telecomCode is the main value of the result
        example : custinfodictionary["TELCOM_CD"]
        ** Not from array of dic
    */
    func telecomSelected(telecomCode:String) -> Int{
        var selectedIndex = 0
        for index in 4...custinfodictionary["TELCOM_REC"]!.count!{
            if telecomCode == (custinfodictionary["TELCOM_REC"]![index-1]["TELCOM_CD"] as? String)!{
                selectedIndex = index - 4
            }
        }
        return selectedIndex
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController!.navigationBar.translucent = false
        viewPress.addTarget(self, action: "handleKeyboardHide")
        self.view.addGestureRecognizer(viewPress)
    }
    
    
    //Gesture handler
    func handleKeyboardHide(){
        tfMobilePhone.resignFirstResponder()
        tfEnglishName.resignFirstResponder()
        tfKoreaName.resignFirstResponder()
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController!.navigationBar.translucent = true
        
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        localize()
        self.initLoad()
//        print(self.custinfodictionary)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
 
    
    @IBAction func ActionService(sender: AnyObject) {
        //TODO: Subscribe Term of Service
        let condition:String = "0\(sender.tag)" as String
        TransactionClass().sendAPI(APICode.TERMS_R001, argument: ["TERMS_GB":condition], success: { (success) -> Void in
            guard let code = success["RSLT_CD"] where code as! String == "0000" else{
                //Should be alert here
                self.showSimpleAlert(success["RSLT_MSG"] as! String, delegate: self)
                return
            }
            
            if let weburl = success["RESP_DATA"]!["TERMS_URL"] as? String where weburl != "" {
                //TODO : Where TERMS_URL != ""
                let viewInput = Dictionary(dictionaryLiteral: ("viewName",weburl),("webviewYN","Y"), ("webviewTitle", "Terms"))
                let notification = NSNotificationCenter.defaultCenter()
                notification.postNotificationName("changeMainView", object: nil, userInfo: viewInput)
            }else{
                print("Not work")
            }

        })
    }
    
    
    func buttonStatus(arrButton:Array<UIButton>,status:Bool)->Bool{
        var index:Int = 0
        for index=0;index < arrButton.count;index++ {
            arrButton[index].selected = status
        }
        return status
    }
    
    func isButtonStatusTrue(arrButton:Array<UIButton>)->Bool{
        var index:Int = 0
        var count:Int = 0
        for index=0;index < arrButton.count;index++ {
            if arrButton[index].selected == true{
                count++
            }
        }
        if count == 5 {return true}
        else {return false}
    }
    
    func selecteButtonAtLeastOne(arrButton:Array<UIButton>)->Bool{
        var index:Int = 0
        var count:Int = 0
        for index=0;index < arrButton.count;index++ {
            if arrButton[index].selected == true{
                count++
            }
        }
        print("Count = \(count)")
        if count > 0 {return true}
        else {return false}
    }
    
    @IBAction func ActionTerm(aButton:UIButton){
        switch(aButton.tag){
        case 1:
            //TODO: btnAgreedAllService
            if aButton.selected{
                aButton.selected = false
                buttonStatus([btnAgreedServiceTerm,btnAgreedWithPrivacy,btnAgreeIdVerify,btnAgreePersonalInfo,btnAgreeUniqueIdProcess,btnAgreeTelecomTerms],status: false)

            }else{
                aButton.selected = true
                //Other 5 button are selected
                buttonStatus([btnAgreedServiceTerm,btnAgreedWithPrivacy,btnAgreeIdVerify,btnAgreePersonalInfo,btnAgreeUniqueIdProcess,btnAgreeTelecomTerms],status: true)
            }
            
            break
        case 2:
            //TODO: btnAgreedServiceTerm
            if aButton.selected{
                aButton.selected = false
                btnAgreedAllService.selected = false
            }else{
                aButton.selected = true
                let isTrue:Bool = isButtonStatusTrue([btnAgreedWithPrivacy,btnAgreeIdVerify,btnAgreePersonalInfo,btnAgreeUniqueIdProcess,btnAgreeTelecomTerms])
                guard isTrue == false else{
                    btnAgreedAllService.selected = true
                    return
                }
            }
            break
        case 3:
            //TODO:btnAgreedWithPrivacy
            if aButton.selected{
                aButton.selected = false
                btnAgreedAllService.selected = false
            }else{
                aButton.selected = true
                let isTrue:Bool = isButtonStatusTrue([btnAgreedServiceTerm,btnAgreeIdVerify,btnAgreePersonalInfo,btnAgreeUniqueIdProcess,btnAgreeTelecomTerms])
                guard isTrue == false else{
                    btnAgreedAllService.selected = true
                    return
                }
            }
            break
        case 4:
            //TODO: btnAgreeIdVerify
            if aButton.selected{
                aButton.selected = false
                btnAgreedAllService.selected = false
            }else{
                aButton.selected = true
                let isTrue:Bool = isButtonStatusTrue([btnAgreedServiceTerm,btnAgreedWithPrivacy,btnAgreePersonalInfo,btnAgreeUniqueIdProcess,btnAgreeTelecomTerms])
                guard isTrue == false else{
                    btnAgreedAllService.selected = true
                    return
                }
            }
            break
        case 5:
            //TODO: btnAgreePersonalInfo
            if aButton.selected{
                aButton.selected = false
                btnAgreedAllService.selected = false
            }else{
                aButton.selected = true
                let isTrue:Bool = isButtonStatusTrue([btnAgreedServiceTerm,btnAgreedWithPrivacy,btnAgreeIdVerify,btnAgreeUniqueIdProcess,btnAgreeTelecomTerms])
                guard isTrue == false else{
                    btnAgreedAllService.selected = true
                    return
                }
            }
            break
        case 6:
            //TODO : btnAgreeUniqueIdProcess
            if aButton.selected{
                aButton.selected = false
                btnAgreedAllService.selected = false
            }else{
                aButton.selected = true
                let isTrue:Bool = isButtonStatusTrue([btnAgreedServiceTerm,btnAgreedWithPrivacy,btnAgreeIdVerify,btnAgreePersonalInfo,btnAgreeTelecomTerms])
                guard isTrue == false else{
                    btnAgreedAllService.selected = true
                    return
                }
            }
            break
        case 7:
            //TODO: btnAgreeTelecomTerms
            if aButton.selected{
                aButton.selected = false
                btnAgreedAllService.selected = false
            }else{
                aButton.selected = true
                let isTrue:Bool = isButtonStatusTrue([btnAgreedServiceTerm,btnAgreedWithPrivacy,btnAgreeIdVerify,btnAgreePersonalInfo,btnAgreeUniqueIdProcess])
                guard isTrue == false else{
                    btnAgreedAllService.selected = true
                    return
                }
            }
            break
        default:
            //DEFUALT HERE
            break
        }
    }

    @IBAction func ActionGetVerifyCode(sender: AnyObject) {
        //TODO: Ger verification code
       if tfEnglishName.text?.characters.count == 0 ||
            tfKoreaName.text?.characters.count == 0 && nation_gb != "1" ||
            tfMobilePhone.text?.characters.count == 0 ||
            nation_gb == "" || gender_str == "" ||
            selecteButtonAtLeastOne([btnAgreedAllService,btnAgreedServiceTerm,btnAgreedWithPrivacy,btnAgreeIdVerify,btnAgreePersonalInfo,btnAgreeUniqueIdProcess,btnAgreeTelecomTerms]) == false{
               
            var message:String = ""
            if tfKoreaName.text?.characters.count == 0 && nation_gb != "1"{
                message = "Please Enter Korean full name."
                self.showSimpleAlert(message, delegate: self)
            }
            else if tfEnglishName.text?.characters.count == 0{
                message = "Please enter English full name."
                self.showSimpleAlert(message, delegate: self)
            }
            else if tfMobilePhone.text?.characters.count == 0{
                message = "Please Enter mobile phone number."
                self.showSimpleAlert(message, delegate: self)
            }
            else if nation_gb == "" {
                message = "Please select nation."
                self.showSimpleAlert(message, delegate: self)
            }else if gender_str == ""{
                message = "Please select gender."
                self.showSimpleAlert(message, delegate: self)
            }else if selecteButtonAtLeastOne([btnAgreedAllService,btnAgreedServiceTerm,btnAgreedWithPrivacy,btnAgreeIdVerify,btnAgreePersonalInfo,btnAgreeUniqueIdProcess,btnAgreeTelecomTerms]) == false{
                message = "Please agree service terms."
                self.showSimpleAlert(message, delegate: self)
            }
            
        }else{
            var cert_trade = ""
            let tradeProcess = NSUserDefaults.standardUserDefaults()
            switch tradeStatus!{
            case 1:
                //DEFAULT REGISTRATION
                cert_trade = CertificateTrading.REGISTRATION
                break
            case 2:
                //WID REG
                cert_trade = CertificateTrading.WIDRAWACC_REGISTER
                break
            case 3:
                //RESET PASSWORD
                cert_trade = CertificateTrading.PAYMENTRESETPSWD
                break
            case 4:
                // IF CHANGE DEBIT
                cert_trade = CertificateTrading.WIDRAWACC_CHANGE
                break
            default:
                break
            }
        
            //Use it in PinCode
            tradeProcess.setObject(tradeStatus, forKey: "TradeProcess")
            tradeProcess.synchronize()
        
            var args:Dictionary<String,String>
            args = [
                "CERT_TXGB":cert_trade,
                "CUST_NM": tfEnglishName.text!,
                "SEX_GB":gender_str,
                "BIRTH_YMD":self.custinfodictionary["BIRTH_YMD"]! as! String,
                "NATION_GB":nation_gb,
                "TELCOM_CD":self.custinfodictionary["TELCOM_CD"]! as! String,
                "HP_NO": tfMobilePhone.text!
            ]
        
            TransactionClass().sendAPI(APICode.HPCERT_R001, argument: args, success: { (success) -> Void in
                print("Data Success = \(success)")
                guard let code = success["RSLT_CD"] where code as! String == "0000" else{
                    //Should be alert here
                    self.showSimpleAlert(success["RSLT_MSG"] as! String, delegate: self)
                    return
                }
                
                if let tran_serial = success["RESP_DATA"]!["CERT_TXNO"] as? String{
                    self.transerial = tran_serial
                    self.performSegueWithIdentifier("verifycodesegue", sender: self)
                }
            })
        }
    }
    
    /*
        TODO: Popup handle
    */
    func selectedNation(nationGB: String) {
        nation_gb = nationGB
        lblNation.textColor = UIColor(red: 48/255, green: 50/255, blue: 51/255, alpha: 1)
        if nation_gb == "0"{
            lblNation.text = Language.localizedStr("C10P_02")
            //SHOW KOREAN NAME TEXT FIELD
            viewNameKOR.hidden = false
            viewContainerTop_constraint_height.constant = 264
            viewNameENG_constraint_Top.constant = 0
            ContentMain_constraint_height.constant = 246
        }
        else if nation_gb == "1"{
            lblNation.text = Language.localizedStr("C10P_03")
            //SHOW ENGLSIH NAME TEXT FIELD
            viewNameKOR.hidden = true
            viewContainerTop_constraint_height.constant = 264 - viewNameKOR.frame.size.height
            viewNameENG_constraint_Top.constant = 0 - viewNameKOR.frame.size.height
            ContentMain_constraint_height.constant = 246 - viewNameKOR.frame.size.height
        }
    }
    
    func selectedGender(gender: String) {
        gender_str = gender
        lblGender.textColor = UIColor(red: 48/255, green: 50/255, blue: 51/255, alpha: 1)
        if gender_str == "1"{
            lblGender.text = Language.localizedStr("C11P_02")
        }
        else if gender_str == "2"{
            lblGender.text = Language.localizedStr("C11P_03")
        }
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        //TODO : Pass transerial to VerificationViewController
        if segue.identifier == "verifycodesegue"{
            let verifyViewCtl = segue.destinationViewController as! VerificationViewController
            verifyViewCtl.transerial = self.transerial
            verifyViewCtl.nation_gb = self.nation_gb
            verifyViewCtl.custname = [self.custinfodictionary["ENG_NM"]! as! String,self.custinfodictionary["KOR_NM"]! as! String]
        }
        
    }
    
    /*
        TODO: Limit Character input
    */
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        let newLength = (textField.text?.utf16.count)! + string.utf16.count - range.length
        if textField == tfEnglishName || textField == tfKoreaName {
            return newLength > 50 ? false : true
        }else if textField == tfMobilePhone{
            return newLength > 20 ? false : true
        }
        return newLength > 20 ? false : true
    }
}