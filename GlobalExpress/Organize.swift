//
//  Organize.swift
//  GlobalExpress
//
//  Created by Ann on 3/11/16.
//  Copyright © 2016 webcash. All rights reserved.
//

import UIKit

class Organize: NSObject,PrintableCell {
    var name: String!
    var image: String!
    var numberOfLocations: String!
    var code: String!
    var remark: String!
    
    init?(data: NSDictionary?) {
        guard data != nil else{
            print("dictionary bank failed")
            return
        }
        
        name = data!["ORG_NM"] as? String
        image = data!["ORG_LOGO"] as? String
        numberOfLocations = data!["LOCATION_CNT"] as? String
        code = data!["ORG_CD"] as? String
        remark = data!["ORG_BIGO"] as? String
    }
}
