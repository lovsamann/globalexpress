//
//  MainNavigationController.swift
//  GlobalExpress
//
//  Created by JangWooil on 2016. 3. 21..
//  Copyright © 2016년 webcash. All rights reserved.
//

import UIKit

struct CustomColor {
    init(){
        
    }
    
    static func rgba(red: Float, green: Float, blue: Float, alpha: Float) -> UIColor {
        return UIColor(colorLiteralRed: red/255.0, green: green/255.0, blue: blue/255.0, alpha: alpha)
    }
    
    static func rgb(red: Float, green: Float, blue: Float) -> UIColor {
        return UIColor(colorLiteralRed: red/255.0, green: green/255.0, blue: blue/255.0, alpha: 1)
    }
}

class MainNavigationController: UINavigationController {

    var popupBgView : UIView? = nil
    var leftpopupView : LeftPopupView? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()

        NSNotificationCenter.defaultCenter().addObserver(self, selector: "openLeftPopup:", name: "openLeftPopup", object: nil)
    }
    
    //팝업 띄우기
    func openLeftPopup(note : NSNotification) {
        if let noteDic = note.userInfo {
            let height = UIScreen.mainScreen().bounds.size.height
            let width  = UIScreen.mainScreen().bounds.size.width
            
            if(popupBgView == nil){
                popupBgView = UIView(frame: CGRectMake(0, 0, width, height))
                popupBgView!.backgroundColor = CustomColor.rgba(0, green: 0, blue: 0, alpha: 0.5)
                self.view.addSubview(popupBgView!)
            } else {
                popupBgView!.hidden = false
            }
            
            
            let letPopupVCList = NSBundle.mainBundle().loadNibNamed("LeftPopUpViewController", owner: nil, options: nil)
            if(leftpopupView == nil){
                leftpopupView = letPopupVCList[0] as! LeftPopupView
                
                let x = (UIScreen.mainScreen().bounds.size.width - 249) / 2
                let y = (UIScreen.mainScreen().bounds.size.height - 171) / 2
                leftpopupView!.frame = CGRectMake(x, y, leftpopupView!.bounds.size.width, leftpopupView!.bounds.size.height)
                
                self.view.addSubview(leftpopupView!)
                //팝업에서 취소 버튼, x 버튼 클릭 시의 처리
                leftpopupView!.popupCloseBtn.addTarget(self, action: "closeLeftPopup", forControlEvents: .TouchUpInside)
                leftpopupView!.popupNoBtn.addTarget(self, action: "closeLeftPopup", forControlEvents: .TouchUpInside)
                //확인 버튼 클릭 시의 처리
                leftpopupView!.popupYesBtn.addTarget(self, action: "gotoSignup", forControlEvents: .TouchUpInside)
            } else {
                leftpopupView!.hidden = false
            }
            
            //팝업 뷰
            if let titleLabel = noteDic["title_label"] as? String {
                leftpopupView!.popupTitle.text = titleLabel
            }
            
            if let label1 = noteDic["label1"] as? String {
                leftpopupView!.popupLabel1.text = label1
            }
            
            if let buttonL = noteDic["buttonL"] as? String  {
                leftpopupView!.popupNoBtn.setTitle(buttonL, forState: .Normal)
            }
            
            if let buttonR = noteDic["buttonR"] as? String  {
                leftpopupView!.popupYesBtn.setTitle(buttonR, forState: .Normal)
            }
        }
    }
    
    //회원가입으로 이동
    func gotoSignup(){
        NSNotificationCenter.defaultCenter().postNotificationName("gotoSignup", object: nil)
        self.closeLeftPopup()
    }
    
    func closeLeftPopup(){
        leftpopupView!.hidden = true
        popupBgView!.hidden   = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
