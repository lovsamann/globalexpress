/*==============================================================================
 - TransKey.h (컨트롤 분리 secureKeyboard)
 ==============================================================================*/

#import <Foundation/Foundation.h>
@class TransKey;

enum {
    NormalKey,
    DeleteKey,
    ClearallKey
};

enum{
    CancelCmdType,
    CompleteCmdType,
    NaviCancelCmdType,
    NaviCompleteCmdType,
    NaviBeforeCmdType,
    NaviNextCmdType,
};

/*==============================================================================
 secureKeyboard(컨트롤분리) keypad type
 ==============================================================================*/
enum {
    TransKeyKeypadTypeText                      = 1,
	TransKeyKeypadTypeNumber                    = 2,
	TransKeyKeypadTypeTextWithPassword          = -1,
    TransKeyKeypadTypeTextWithPasswordOnly      = -3,
	TransKeyKeypadTypeNumberWithPassword        = -2,
    TransKeyKeypadTypeNumberWithPasswordOnly    = -4,
    TransKeyKeypadTypeTextWithPasswordLastShow  = -5,
    TransKeyKeypadTypeNumberWithPasswordLastShow  = -6
};
typedef NSInteger TransKeyKeypadType;

/*=============================================================================================================
 secureKeyboard(컨트롤분리) delegate
 =============================================================================================================*/
@protocol TransKeyDelegate<NSObject>

@optional

- (void)TransKeyDidBeginEditing:(TransKey *)transKey;
- (void)TransKeyDidEndEditing:(TransKey *)transKey;
- (void)TranskeyWillBeginEditing:(TransKey *)transKey;
- (void)TranskeyWillEndEditing:(TransKey *)transKey;
- (void)TransKeyInputKey:(NSInteger)keytype;
- (BOOL)TransKeyShouldInternalReturn:(TransKey *)transKey btnType:(NSInteger)type;

@required
- (BOOL)TransKeyShouldReturn:(TransKey *)transKey;
@end

/*=============================================================================================================
 secureKeyboard(컨트롤분리) interface
 =============================================================================================================*/
@interface TransKey : UIView
@property (nonatomic, assign) id <TransKeyDelegate>delegate;
@property (nonatomic, retain) id parent;

/*=============================================================================================================
    MTranskey 기본 API
 =============================================================================================================*/
// 암호키 설정한다. 128bit
-(void)setSecureKey:(NSData*)securekey;
// 컨트롤 분리모드에서 디바이스지원 범위 설정
// secureKeyboard(컨트롤분리) support device 참조
- (void)mTK_SupportedByDeviceOrientation:(NSInteger)supported_;
// 보안 키패드의 타입 및 최대/최소 길이를 설정한다.
- (void)setKeyboardType:(TransKeyKeypadType)type maxLength:(NSInteger)maxlength minLength:(NSInteger)minlength;
// 커서 사용유무
- (void)mTK_UseCursor:(BOOL)bUse;
// 전체삭제 버튼 사용유무
- (void)mTK_UseAllDeleteButton:(BOOL)bUse;
// 입력에디트필드의 백그라운드 이미지 적용(transkey_inputbox.png)
- (void)mTK_SetInputEditboxImage:(BOOL)bUse;
// 입력에디트필드의 초기 문구를 설정
- (void)mTK_SetHint:(NSString*)desc font:(UIFont*)font;
- (void)mTK_SetHint:(NSString*)desc font:(UIFont*)font textAlignment:(UITextAlignment)alignment;
// VoiceOver 사용
- (void)mTK_UseVoiceOver:(BOOL)bUse;
// Navi Bar 설정
- (void)mTK_ShowNaviBar:(BOOL)show;
// Navi Bar 이전/다음/완료 버튼 숨김,보이스오버 설정
- (void)mTK_SetHiddenBeforeButton:(BOOL)hidden;
- (void)mTK_SetEnableBeforeButton:(BOOL)enable;
- (void)mTK_SetDisableBeforeButtonImageName:(NSString *)name;
- (void)mTK_SetBeforeButtonAccessibilityLabel:(NSString *)label;

- (void)mTK_SetHiddenNextButton:(BOOL)hidden;
- (void)mTK_SetEnableNextButton:(BOOL)enable;
- (void)mTK_SetDisableNextButtonImageName:(NSString *)name;
- (void)mTK_SetNextButtonAccessibilityLabel:(NSString *)label;

- (void)mTK_SetHiddenCompleteButton:(BOOL)hidden;
- (void)mTK_SetEnableCompleteButton:(BOOL)enable;
- (void)mTK_SetDisableCompleteButtonImageName:(NSString *)name;
- (void)mTK_SetCompleteButtonAccessibilityLabel:(NSString *)label;

- (void)mTK_textAlignCenter:(BOOL)bCenter;

- (void)mTK_UseShiftOptional:(BOOL)bUse;
// 보안키패드의 언어설정(0:한글(default), 1:영어)
// 한글 문구가 들어간 버튼만 이미지 변경(입력완료, 취소 등)
- (void)mTK_SetLanguage:(NSInteger)languageType;

/*=============================================================================================================
 MTranskey additional API
 =============================================================================================================*/
- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration;
// textfield에 입력길이만큼 '*' 출력
- (void)setDummyCharacterWithLength:(NSInteger)length;
// 보안 키패드로 입력한 데이터 길이를 얻는다.
- (NSInteger)length;
// 암호화 키를 얻는다.
-(NSData*)getSecureKey;
// 보안 키패드를 사용하여 입력된 암호화 값을  얻는다.
- (NSString *)getCipherData;
- (NSString *)getCipherDataEx;
- (NSString *)getCipherDataExWithPadding;
// 보안키패드 랜덤키 설정
- (void)makeSecureKey;
// 보안 키패드를 사용하여 입력된 원문 값을 얻는다.
- (void)getPlainDataWithKey:(NSData*)key cipherString:(NSString*)cipherString plainString:(char*)plainData length:(NSInteger)length;
- (void)getPlainDataExWithKey:(NSData*)key cipherString:(NSString*)cipherString plainString:(char*)plainData length:(NSInteger)length;
- (void)getPlainDataExWithPaddingWithKey:(NSData*)key cipherString:(NSString*)cipherString plainString:(char*)plainData length:(NSInteger)length;

// TransKey의 키패드를 띄운다.
- (void)TranskeyBecomeFirstResponder;

// TransKey의 키패드를 내린다.
- (void)TranskeyResignFirstResponder;

// TransKey를 통해 입력한 데이터를 초기화한다.
- (void)clear;

// 입력문자의 사이즈 조절(텍스트모드)
- (void)mTK_SetSizeOfInputKeyImage:(CGFloat)width height:(CGFloat)height_;

// 입력문자의 사이즈 조절(암호모드)
- (void)mTK_SetSizeOfInputPwKeyImage:(CGFloat)width height:(CGFloat)height_;

// 팝오버 사용(아이패드 전용)
- (void)mTK_UsePopOver:(BOOL)bUse;

// 현재 키패드가 팝오버 인지 확인.
- (void)mTK_IsPopOver;


// 현재 보안키패드에 설정된 언어 리턴
- (NSInteger)mTK_GetLanguage;

// 허용입력 초과시 메세지 박스 사용
- (void)mTK_ShowMessageIfMaxLength:(NSString*)message;

// 최소입력 미만시 메세지 박스 사용
- (void)mTK_ShowMessageIfMinLength:(NSString*)message;

// 버전
- (NSString*)mTK_GetVersion;

// navigation bar 사용할 경우 셋팅
- (void)mTK_UseNavigationBar:(BOOL)bUse;

// 숫자입력 4가지로 표현
- (void)mTK_SetFixed;

// 팝오버 옵션
- (void)mTK_SetPopOverOpt:(UIColor*)labelBackColor textColor:(UIColor*)labelColor alignment:(UITextAlignment)textAlignment;

// 암호화 데이터 값
- (NSString*)mTK_GetSecureData;
- (NSString*)CK_GetSecureData;

// Navi Bar 백그라운드 색상 설정
- (void)mTK_SetNaviBarBackgroundColor:(UIColor *)color;
// Navi Bar 백그라운드 이미지 설정
- (void)mTK_SetNaviBarBackgroundImage:(NSString *)name;

// Navi Bar 마지막 입력값 보여주기 설정
- (void)mTK_ShowLastInput:(BOOL)show;

//최상위 버튼의 Top 마진
- (void)mTK_SetHighestTopMargin:(int)height;

// NaviBar 마지막 글자 커스터마이징 설정
- (void)mTK_SetNaviLastInputImageNamePrefix:(NSString*)prefixStr;
- (NSString*)GetLastInputKeyImg;

- (void)mTK_ClearDelegateSubviews;

- (NSInteger)mTK_GetInputLength;

- (void)mTK_EnableSamekeyInputDataEncrypt:(BOOL)bEnable;

- (void)mTK_setHideInputPasswordDelay:(NSInteger)delaySecond;

- (void)mTK_setReArrangeKeypad:(BOOL)bReArrange;

- (void)mTK_UseKeypadAnimation:(BOOL)bUseAnimation;

- (void)mTK_setVerticalKeypadPosition:(int)position;

- (void)mTK_InputDone;
- (void)mTK_InputCancel;
- (void)mTK_InputBackSpace;

- (void)mTK_UseAtmMode:(BOOL)bUseAtm;

- (void)mTK_setCustomPostfixInputBoxImage:(NSString*)postfixImgName;

- (void)mTK_SetInputBoxTransparent:(BOOL)bTransparent;

- (void)mTK_SetPBKDF_RandKey:(NSData*)randkey;

- (NSString*)getPBKDF2DataEncryptCipherData;
- (NSString*)getPBKDF2DataEncryptCipherDataEx;
- (NSString*)getPBKDF2DataEncryptCipherDataExWithPadding;

- (void)mTK_UseFocusColor:(BOOL)isUse;
- (void)mTK_SetAlertTitle:(NSString*)title;

// 입력필드 중앙정렬 유지하기 위해  - (void)viewDidDisappear:(BOOL)animated 하단에 선언
- (void)mTK_RemainTextAlignCenter;
// 말풍선(balloon) 사용유무
- (void)mTK_SetUseBalloonImageButton:(BOOL)bUse;
@end
