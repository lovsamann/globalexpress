//
//  RecipientInfoViewController.swift
//  GlobalExpress
//
//  Created by Ann on 2/1/16.
//  Copyright © 2016 webcash. All rights reserved.
//

import UIKit

class RecipientInfoViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {

    
    //MARK: - Outlet
    
    @IBOutlet var tableView: UITableView!
    
    @IBOutlet var receiverInfoLabel: UILabel!
    @IBOutlet var infoLabel: UILabel!
    
    @IBOutlet var topConstraintViewCustom: NSLayoutConstraint!
    @IBOutlet var viewCustom: ViewCustom!
    var recipientDic: [String:String]?
    var titles: [[String:String]]!{
        didSet{
            if recipientDic?.count == 7 {
                titles.removeAtIndex(3)
            }
            self.tableView.reloadData()
        }
    }
    var transcation: Transaction?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        titles = loadTitle()
        
        self.navigationController?.navigationBarHidden = true
    }
    
    override func localize() {
       self.receiverInfoLabel.text = Language.localizedStr("R04_01")
        self.infoLabel.text = Language.localizedStr("R04_02")
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        let controllers = self.navigationController?.viewControllers
        guard let viewControllers = controllers else{
            print("nothing on stack navigation")
            return
        }
        
        if let index: Int = viewControllers.count - 3 where index >= 0 {
            
            let previousViewController = viewControllers[index]
            
            if previousViewController is RecipientListViewController {
                
                topConstraintViewCustom.constant = -30
                viewCustom.hidden = true
            }else{
                topConstraintViewCustom.constant = 15
                viewCustom.hidden = false
            }
        }
        
    }
    
    //MARK: - UITableViewDataSource
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("cell") as? RecipientInfoTableViewCell

        let dic = titles[indexPath.row]
        cell?.titleLabel.text = dic.keys.first
        cell?.valueLabel.text = dic.values.first
        
        
        
        return cell!
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return titles == nil ? 0 : titles.count
    }
    
    //MARK: - UITableViewDelegate
    //header
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerImage = UIImage(named: "con_rad_top01.png")
        let headerImageView = UIImageView(frame: CGRectMake(0, 0, CGRectGetWidth(tableView.frame), 9))
        
        headerImageView.image = headerImage
        return headerImageView
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 9.0
    }
    //footer
    func tableView(tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footerImage = UIImage(named: "con_rad_bottom01.png")
        let footerImageView = UIImageView(frame: CGRectMake(0, 0, CGRectGetWidth(tableView.frame), 1))
        footerImageView.image = footerImage
        
        let clearSeparatorView = UIView(frame: CGRectMake(8, -1 , CGRectGetWidth(footerImageView.frame) - 12 , 1))
        clearSeparatorView.backgroundColor = UIColor.whiteColor()
        footerImageView.addSubview(clearSeparatorView)
        
        return footerImageView
    }
    
    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 9
    }
    //MARK: - private methods
    
    func loadTitle() -> [[String:String]]! {
        
        guard let dic = recipientDic else{
            print("reciepint nil. nothing to load titles")
            return nil
        }
        
        return [ [Language.localizedStr("R04_03") //Country
                 : dic["NATION_NM"]!],
                 [Language.localizedStr("R04_04") //Receiving Method
                    : dic.count == 7 ? "Bank Account" : "Cash Pick up"],
                 [Language.localizedStr("R04_05") //Receiving Bank
                    : dic["BANK_NM"] == nil ? dic["CPORG_NM"]! : dic["BANK_NM"]!],
                [Language.localizedStr("R04_06") //Account No.
                    : dic["ACCT_NO"] == nil ? "" : dic["ACCT_NO"]!],
                [Language.localizedStr("R04_07") //Account Holder.
                    : dic["RCPT_NM"]!],
                [Language.localizedStr("R04_08") //Mobile Phone
                    : dic["TEL_NO"]!],
                [Language.localizedStr("R07_08") //E-mail
                    : dic["E_MAIL"] == nil ? "" : dic["E_MAIL"]! ],
                ]
    }
    
    func getPreviousViewController() -> UIViewController! {
        
        let viewControllers = self.navigationController?.viewControllers
        
        guard let controllers = viewControllers else{
            print("all controllers on stack = nil")
            return nil
        }
        
        let numberOfControllers = controllers.count
        let previousController = controllers[numberOfControllers - 2]
        
        return previousController
    }

    
    //MARK: - Action
    @IBAction func confirmClicked(sender: AnyObject) {
        
        if transcation != nil { //s.th wrong here
             print("errrrrrrrr")
            let transcationStoryboard = UIStoryboard(name: "Transaction", bundle: NSBundle.mainBundle())
            let confirmTranscationVC = transcationStoryboard.instantiateInitialViewController() as? ConfirmTransactionViewController
            confirmTranscationVC?.transcation = self.transcation
            
            self.navigationController?.pushViewController(confirmTranscationVC!, animated: true)
        }
    }
    
}
