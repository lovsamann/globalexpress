//
//  ViewCostom.swift
//  Khmernote
//
//  Created by Ralex on 1/22/16.
//  Copyright © 2016 japcoding. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable class ViewCustom:UIControl{
    @IBInspectable var borderColor:UIColor = UIColor.clearColor(){
        didSet{
            layer.borderColor = borderColor.CGColor
        }
    }
    
    @IBInspectable var borderWidth:CGFloat = 0{
        didSet{
            layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable var cornerRadius:CGFloat = 0{
        didSet{
            layer.cornerRadius = cornerRadius
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}