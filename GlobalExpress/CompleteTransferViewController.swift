//
//  CompleteTransferViewController.swift
//  GlobalExpress
//
//  Created by Ralex on 2/4/16.
//  Copyright © 2016 webcash. All rights reserved.
//

import UIKit

class CompleteTransferViewController: UIViewController {
    
    @IBOutlet weak var lblReceiverName: UILabel!
    @IBOutlet weak var lblReceiverCountry: UILabel!
    @IBOutlet weak var lblBank: UILabel!
    @IBOutlet weak var lblReceiver: UILabel!
    @IBOutlet weak var lblBankAccountNO: UILabel!
    @IBOutlet weak var lblSendingKOR: UILabel!
    @IBOutlet weak var lblReceivingUSD: UILabel!
    @IBOutlet weak var lblExchangeRate: UILabel!
    @IBOutlet weak var lblSendingFee: UILabel!
    @IBOutlet weak var lblDiscount: UILabel!
    @IBOutlet weak var lblActualSendingFee: UILabel!
    @IBOutlet weak var lblPhone: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var lblStep: UILabel!
    
    
    var data:Dictionary<String,String>?
    
    @IBOutlet weak var thankLabel: UILabel!
    @IBOutlet weak var moneyInfoLabel: UILabel!
    @IBOutlet weak var recieverNameLabel: UILabel!
    @IBOutlet weak var remittanceResultLabel: UILabel!
    @IBOutlet weak var receiverBankLabel: UILabel!
    @IBOutlet weak var receiverLabel: UILabel!
    @IBOutlet weak var receiverBankAccLabel: UILabel!
    @IBOutlet weak var remittMoneyInfo: UILabel!
    @IBOutlet weak var finalMoneyInfo: UILabel!
    @IBOutlet weak var SendingFee: UILabel!
    @IBOutlet weak var ActualSendingFeeLabel: UILabel!
    @IBOutlet weak var ActualExchangeRateLabel: UILabel!
    @IBOutlet weak var DiscountLabel: UILabel!
    @IBOutlet weak var btnConfirm: UIButton!
    @IBOutlet weak var btnTransaction: UIButton!
    var trans:Transaction?
    
    override func localize() {
        lblStep.text = "4. \(Language.localizedStr("M01_06"))"
        thankLabel.text = Language.localizedStr("T14_01")
        moneyInfoLabel.text = Language.localizedStr("T14_02")
        receiverLabel.text = Language.localizedStr("T14_03")
        remittanceResultLabel.text = Language.localizedStr("T14_06")
        receiverBankLabel.text = Language.localizedStr("T14_011")
        recieverNameLabel.text = Language.localizedStr("T14_12")
        receiverBankAccLabel.text = Language.localizedStr("T14_13")
        remittMoneyInfo.text = Language.localizedStr("T14_14")
        ActualExchangeRateLabel.text = Language.localizedStr("T14_17")
        SendingFee.text = Language.localizedStr("T14_18")
        DiscountLabel.text = Language.localizedStr("T14_19")
        ActualSendingFeeLabel.text = Language.localizedStr("T14_20")
        finalMoneyInfo.text = Language.localizedStr("T14_21")
        btnConfirm.setTitle(Language.localizedStr("T14_22"), forState: .Normal)
        btnTransaction.setTitle(Language.localizedStr("T14_23"), forState: .Normal)
    }
    func initLoad(){
        
        if data != nil{
            let sendingfee:String = data!["SVC_FEE"]! as String
            let actualSendingFee:String = data!["PAY_FEE"]! as String
            
            lblReceiverName.text = trans?.recipient?.name
            lblReceiverCountry.text = trans?.recipient?.nationality?.name
            lblPhone.text = trans?.recipient?.telephone
            lblEmail.text = trans?.recipient?.email
            
            lblReceiver.text = data!["RCP_NAME"]!
            lblBank.text = data!["RCP_BANK"]
            lblBankAccountNO.text = data!["RCP_ACCT"]
            lblSendingKOR.text = data!["REC_AMT"]
            lblReceivingUSD.text = data!["REC_USDAMT"]
            lblExchangeRate.text = "1.00USD=\(data!["FX_RATE"]!)"
            lblSendingFee.text = sendingfee
            lblActualSendingFee.text = actualSendingFee
            lblDiscount.text = data!["FEE_DISAMT"]!
            lblActualSendingFee.text = actualSendingFee

        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //TODO: Localize
        localize()
        initLoad()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBarHidden = true
    }
    override func viewWillDisappear(animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBarHidden = false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func ActionDisplayTran(sender: AnyObject) {
//        TODO: Display all transaction
        self.performSegueWithIdentifier("mytransactionsegue", sender: nil)
    }
    @IBAction func ActionDisplayMain(sender: AnyObject) {
        //TODO: Go to main
//        let storyboard = UIStoryboard(name: "Main", bundle: nil)
//        let viewcontroller = storyboard.instantiateViewControllerWithIdentifier("HomeViewController") as? HomeViewController
//        self.navigationController?.pushViewController(viewcontroller!, animated: true)
        navigationController?.popToRootViewControllerAnimated(true)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
