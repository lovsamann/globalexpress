//
//  IdentityVerificationViewController.swift
//  GlobalExpress
//
//  Created by UDAM on 1/29/16.
//  Copyright © 2016 webcash. All rights reserved.
//

import UIKit

class IdentityVerificationViewController: UIViewController, CountryViewControllerDelegate, BankSelectViewControllerDelegate, UITextFieldDelegate, UIScrollViewDelegate {
    
    // MARK: - IBOutlet
    // -------------------------------------------------------------------------------
    //	IBOutlet UIScrollView
    // -------------------------------------------------------------------------------
    @IBOutlet weak var Scroller         : UIScrollView!
    
    // -------------------------------------------------------------------------------
    //	IBOutlet UILabel
    // -------------------------------------------------------------------------------
    @IBOutlet weak var topTitleLabel    : UILabel!
    @IBOutlet weak var bankLabel        : UILabel!
    @IBOutlet weak var accNoLabel       : UILabel!
    @IBOutlet weak var dobLabel         : UILabel!
    @IBOutlet weak var nameKOLabel      : UILabel!
    @IBOutlet weak var nameENLabel      : UILabel!
    @IBOutlet weak var nationalityLabel : UILabel!
    @IBOutlet weak var serviceLabel     : UILabel!
    @IBOutlet weak var privacyLabel     : UILabel!
    @IBOutlet weak var termTitleLabel   : UILabel!
    @IBOutlet weak var stepTitleLabel   : UILabel!
    
    // -------------------------------------------------------------------------------
    // IBOutlet UIButton
    // -------------------------------------------------------------------------------
    @IBOutlet weak var nextButton           : UIButton!
    @IBOutlet weak var showServiceButton    : UIButton!
    @IBOutlet weak var showPrivacyButton    : UIButton!
    @IBOutlet weak var checkServiceButton   : UIButton!
    @IBOutlet weak var checkPrivacyButton   : UIButton!
    
    // -------------------------------------------------------------------------------
    // IBOutlet UITextField
    // -------------------------------------------------------------------------------
    @IBOutlet weak var bankTextField    : UITextField!
    @IBOutlet weak var accNoTextField   : UITextField!
    @IBOutlet weak var dobTextField     : UITextField!
    @IBOutlet weak var nameKOTextField  : UITextField!
    @IBOutlet weak var nameENTextField  : UITextField!
    @IBOutlet weak var nationalityTextField : UITextField!
    
    // -------------------------------------------------------------------------------
    // IBOutlet NSLayoutConstraint
    // -------------------------------------------------------------------------------
    @IBOutlet weak var splitRightConstraint             : NSLayoutConstraint!
    @IBOutlet weak var splitLeftConstraint              : NSLayoutConstraint!
    @IBOutlet weak var bottomNextButtonConstraint       : NSLayoutConstraint!
    @IBOutlet weak var heightLabelNameEngConstraint     : NSLayoutConstraint!
    @IBOutlet weak var heightTextFieldNameEngConstraint : NSLayoutConstraint!
    @IBOutlet weak var centerViewHeightConstraint       : NSLayoutConstraint!

    // -------------------------------------------------------------------------------
    // IBOutlet UIView
    // -------------------------------------------------------------------------------
    @IBOutlet weak var stepView         : UIView!
    @IBOutlet weak var selectBankView   : UIView!
    @IBOutlet weak var selectCountryView: UIView!
    @IBOutlet weak var checkServiceView : UIView!
    @IBOutlet weak var checkPrivacyView : UIView!
    
    // MARK: - Property
    // -------------------------------------------------------------------------------
    //	Store/Receive data
    // -------------------------------------------------------------------------------
    var fxBankRecord    : NSArray?
    var totBankRecord   : NSArray?
    var telcomRecord    : NSArray?
    var countryRecord   : NSArray?
    
    var signupInfo      : NSMutableDictionary?
    var selectedBankCode: NSString?
    var nationCode      : NSString?
    
    // MARK: - Private function
    // -------------------------------------------------------------------------------
    // Localizing language
    // -------------------------------------------------------------------------------
    override func localize() {
        
        self.navigationItem.title   = Language.localizedStr("M02_01")
        self.stepTitleLabel.text    = "1. \(Language.localizedStr("M01_01"))"
        self.topTitleLabel.text     = Language.localizedStr("M02_02")
        self.bankLabel.text         = Language.localizedStr("M02_03")
        self.accNoLabel.text        = Language.localizedStr("M02_04")
        self.dobLabel.text          = Language.localizedStr("M02_05")
        self.nationalityLabel.text  = Language.localizedStr("M02_11")
        self.nameKOLabel.text       = Language.localizedStr("M02_07")
        self.nameENLabel.text       = Language.localizedStr("M02_08")
        self.termTitleLabel.text    = Language.localizedStr("M02_12")
        self.serviceLabel.text      = Language.localizedStr("M02_13")
        self.privacyLabel.text      = Language.localizedStr("M02_14")
        
        self.bankTextField.placeholder          = Language.localizedStr("M02_17")
        self.accNoTextField.placeholder         = Language.localizedStr("M02P_06")//Localize data look like missing
        self.dobTextField.placeholder           = Language.localizedStr("M02_18")
        self.nationalityTextField.placeholder   = Language.localizedStr("M02_20")
        self.nameKOTextField.placeholder        = Language.localizedStr("M02_09")
        self.nameENTextField.placeholder        = Language.localizedStr("M02_10")
        
        self.showServiceButton.setTitle(Language.localizedStr("M02_15"), forState: .Normal)
        self.showPrivacyButton.setTitle(Language.localizedStr("M02_15"), forState: .Normal)
        self.nextButton.setTitle(Language.localizedStr("M02_16"), forState: .Normal)
        
        //Show service and privacy button title
        let attrs = [
            NSFontAttributeName : UIFont.systemFontOfSize(14.0),
            NSForegroundColorAttributeName : UIColor(red: 49/255, green: 51/255, blue: 51/255, alpha: 1),
            NSUnderlineStyleAttributeName : 1]
        let attributedString = NSMutableAttributedString(string:"")
        
        let buttonTitleStr = NSMutableAttributedString(string:Language.localizedStr("M02_15"), attributes:attrs)
        attributedString.appendAttributedString(buttonTitleStr)
        showServiceButton.setAttributedTitle(attributedString, forState: .Normal)
        showPrivacyButton.setAttributedTitle(attributedString, forState: .Normal)
        
        
    }
    
    // -------------------------------------------------------------------------------
    // Customize View Layout
    // -------------------------------------------------------------------------------
    func customizeViewLayout(){
        
        stepView.layer.cornerRadius = 8.0
        
        //Customize navigation item ,barButton
        let closeImage = UIImage(imageLiteral: "title_close_icon")
        let rightNavButton = UIButton(frame: CGRectMake(269.0,20.0,closeImage.size.width/2,closeImage.size.height/2))
        rightNavButton.setBackgroundImage(closeImage, forState: .Normal)
        rightNavButton.addTarget(self, action: "popUpViewController", forControlEvents:UIControlEvents.TouchUpInside)
        let barButton = UIBarButtonItem(customView: rightNavButton)
        self.navigationItem.rightBarButtonItem = barButton
        navigationItem.setHidesBackButton(true, animated: false)
        
        //Customize button position
//        if UIDevice().modelName == "iPhone 6" {
//            bottomNextButtonConstraint.constant = 17 - 36
//        }
//        
//        if UIDevice().modelName == "iPhone 6 Plus" {
//            bottomNextButtonConstraint.constant = 17 - 105
//        }
        
//        if DeviceType.IS_IPHONE_4_OR_LESS {
//            
//            splitRightConstraint.constant       = 58 + 7
//            splitLeftConstraint.constant        = 167 + 8
//        }
        
        if DeviceType.IS_IPHONE_6 {
            
            splitRightConstraint.constant       = 58 + 18
            splitLeftConstraint.constant        = 167 + 16
            
            bottomNextButtonConstraint.constant = 14 + 35
            Scroller.scrollEnabled = false
        }
        
        if DeviceType.IS_IPHONE_6P {
            
            splitRightConstraint.constant       = 58 + 29
            splitLeftConstraint.constant        = 167 + 28
            
            bottomNextButtonConstraint.constant = 14 + 104
            Scroller.scrollEnabled              = false
        }
        
        //DismissKeyboard
        let recognizer              = UITapGestureRecognizer(target: self, action:Selector("dismissKeyboard"))
        view.addGestureRecognizer(recognizer)
        
        //Tap to View SelectBank Popup
        let tapGestureBank          = UITapGestureRecognizer(target: self, action: Selector("selectBankClicked:"))
        selectBankView.addGestureRecognizer(tapGestureBank)
        
        //Tap to view SelectCountry Popup
        let tapGestureCountry       = UITapGestureRecognizer(target: self, action: Selector("selectCountryClicked:"))
        selectCountryView.addGestureRecognizer(tapGestureCountry)
        
        //Tap to check Service image
        let tapGestureCheckService  = UITapGestureRecognizer(target: self, action: Selector("agreeServiceClicked:"))
        checkServiceView.addGestureRecognizer(tapGestureCheckService)
        
        //Tap to check Service image
        let tapGestureCheckPrivacy  = UITapGestureRecognizer(target: self, action: Selector("agreePrivacyClicked:"))
        checkPrivacyView.addGestureRecognizer(tapGestureCheckPrivacy)
        
    }

    
    // -------------------------------------------------------------------------------
    // PopupViewController
    // -------------------------------------------------------------------------------
    func popUpViewController() {
        navigationController?.popViewControllerAnimated(true)
    }

    // MARK: - View Life Cycle
    // -------------------------------------------------------------------------------
    // viewDidLoad
    // -------------------------------------------------------------------------------
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Localize
        localize()
        
        //Customize View Layout
        customizeViewLayout()
        
    }
    
    // -------------------------------------------------------------------------------
    // viewDidAppear
    // -------------------------------------------------------------------------------
    override func viewDidAppear(animated: Bool) {
        
         if NSUserDefaults.standardUserDefaults().objectForKey("FIRST_USE") == nil {
            let notePopup :NotePopup = NotePopup(nibName: "NotePopup", bundle: NSBundle.mainBundle())
            presentPopupViewController(notePopup, animationType: MJPopupViewAnimationFade, contentInteraction: MJPopupViewContentInteractionNone, setAlpha: 0.5)
            NSUserDefaults.standardUserDefaults().setObject("Y", forKey: "FIRST_USE")
            NSUserDefaults.standardUserDefaults().synchronize()
        }
        
    }
    
    // MARK: - CountryViewController Delegate Method
    // -------------------------------------------------------------------------------
    // getCountry dictionary
    // -------------------------------------------------------------------------------
    func getCountryData(dict: NSDictionary!) {
        self.nationalityTextField.text         = "\(dict.objectForKey("NATION_NM")!)"
        self.nationCode = "\(dict.objectForKey("NATION_CD")!)"
        self.nationalityTextField.textColor    = UIColor(colorLiteralRed: 48.0/255.0, green: 50.0/255.0, blue: 51.0/255.0, alpha: 1.0)
        
        //For foreigner
        if nationCode != "410"  {
            centerViewHeightConstraint.constant         = 267 - 47
            heightLabelNameEngConstraint.constant       = 18 - 43
            heightTextFieldNameEngConstraint.constant   = 18 - 43
            nameKOLabel.hidden      = true
            nameKOTextField.hidden  = true
            
//            if UIDevice().modelName == "iPhone 6" {
//                bottomNextButtonConstraint.constant = 17 - 36 - 47
//            }
//            
//            if UIDevice().modelName == "iPhone 6 Plus"{
//                bottomNextButtonConstraint.constant = 17 - 105 - 47
//            }
//            
//            if UIDevice().modelName == "iPhone 5" {
//                bottomNextButtonConstraint.constant = 17 + 17
//                Scroller.scrollEnabled = false
//            }
            
            if DeviceType.IS_IPHONE_6 {
                bottomNextButtonConstraint.constant = 14 + 35 + 47
            }
            
            if DeviceType.IS_IPHONE_6P {
                bottomNextButtonConstraint.constant = 14 + 104 + 47
            }
            
            if DeviceType.IS_IPHONE_5 {
////                bottomNextButtonConstraint.constant = 17 + 17
//                Scroller.scrollEnabled = false
            }
            
            
        }
        //For Korean
        else {
            centerViewHeightConstraint.constant         = 267
            heightLabelNameEngConstraint.constant       = 18
            heightTextFieldNameEngConstraint.constant   = 18
            nameKOLabel.hidden      = false
            nameKOTextField.hidden  = false
//            Scroller.scrollEnabled  = true
            
//            if UIDevice().modelName == "iPhone 6" {
//                bottomNextButtonConstraint.constant = 17 - 36
//            }
//            
//            if UIDevice().modelName == "iPhone 6 Plus" {
//                bottomNextButtonConstraint.constant = 17 - 105
//            }
            
            if DeviceType.IS_IPHONE_6 {
                bottomNextButtonConstraint.constant = 14 + 35
               
            }
            
            if DeviceType.IS_IPHONE_6P {
                bottomNextButtonConstraint.constant = 14 + 104
                
            }
            
        }
    }
    
    // MARK: - BankSelectViewController Delegate Method
    // -------------------------------------------------------------------------------
    // getBank dictionary
    // -------------------------------------------------------------------------------
    func getBankData(dict: NSDictionary!) {
        selectedBankCode                = "\(dict.objectForKey("BANK_CD")!)"
        self.bankTextField.text         = "\(dict.objectForKey("BANK_NM")!)"
        self.bankTextField.textColor    = UIColor(colorLiteralRed: 48.0/255.0, green: 50.0/255.0, blue: 51.0/255.0, alpha: 1.0)
    }
    
    // MARK: - Handle Button
    // -------------------------------------------------------------------------------
    // Click next go to RegisterInfoViewController
    // -------------------------------------------------------------------------------
    @IBAction func nextClicked(sender: UIButton) {

        dismissKeyboard()
        
        if self.selectedBankCode == nil || nationCode == nil || accNoTextField.text == "" || dobTextField.text == "" {
            view.userInteractionEnabled = true
            let noticePopup = NoticePopup()
            self.presentPopupViewController(noticePopup, contentInteraction: MJPopupViewContentInteractionNone)
            return
        }

        //Korean
        if nationCode == "410"  {
            if nameKOTextField.text == "" || nameENTextField.text == "" {
                let noticePopup = NoticePopup()
                self.presentPopupViewController(noticePopup, contentInteraction: MJPopupViewContentInteractionNone)
                return
            }
        }else {
            if nameENTextField.text == "" {
                let noticePopup = NoticePopup()
                self.presentPopupViewController(noticePopup, contentInteraction: MJPopupViewContentInteractionNone)
                return
            }
        }
        
        if checkServiceButton.selected != true || checkPrivacyButton.selected != true {
            view.userInteractionEnabled = true
            showEventAlert("서비스 약관에 동의해 주세요", hasCancelButton: false, delegate: self, tag: nil, confirmBlock: nil, cancelBlock: nil)
            return
        }
        
        var suffixDic = ["" : ""]
        
        if nationCode == "410" {
            suffixDic = ["CUST_NM":"\(self.nameKOTextField.text!)"]
        }
        else{
            suffixDic = ["CUST_NM":"\(self.nameENTextField.text!)"]
        }
        
        //Prepare Data to server
        suffixDic.updateValue("\(self.selectedBankCode!)"       , forKey: "BANK_CD")
        suffixDic.updateValue("\(self.accNoTextField.text!)"    , forKey: "ACCT_NO")
        suffixDic.updateValue("\(self.dobTextField.text!)"      , forKey: "BIRTH_YMD")
        
        ///////// USE THIS FORMAT
        let transaction = TransactionClass()
        transaction.sendAPI(APICode.ACTCERT_R001, argument: suffixDic,
            success: { (success) -> Void in
                
                if let rslt_cd = success["RSLT_CD"] as? String {
                    
                    if rslt_cd != "0000" { //오류
                        if let rsltMsg = success["RSLT_MSG"] as? String {
                            
                            print("Error Code : \(rslt_cd), Error Message : \(rsltMsg)")
                            self.showEventAlert("Error Message :\(rsltMsg)", hasCancelButton: false, delegate: self, tag: nil, confirmBlock: nil, cancelBlock: nil)
                        }
                    } else { //정상
                        if let respData = success["RESP_DATA"] as? NSDictionary {
                            print("Success Code : \(rslt_cd), Data  : \(respData)")
                            
                            self.signupInfo = ["CERT_DT":respData["CERT_DT"]!, "CERT_TXNO":respData["CERT_TXNO"]!, "BANK_CD":self.selectedBankCode!, "BIRTH_YMD":self.dobTextField.text!,"NATION_NM":self.nationalityTextField.text!, "NATION_CD":self.nationCode!, "KOR_NM":self.nameKOTextField.text!, "ENG_NM":self.nameENTextField.text!]

                           self.performSegueWithIdentifier("ShowRegistration", sender: self.signupInfo)
                        }
                        
                    }
                }
                
        })

//        self.performSegueWithIdentifier("ShowRegistration", sender: nil)
        
    }
    
    // -------------------------------------------------------------------------------
    // Select Country
    // -------------------------------------------------------------------------------
    @IBAction func selectCountryClicked(sender: UIButton) {
        dismissKeyboard()
        if countryRecord != nil {
            let country: CountryViewController = CountryViewController(nibName: "CountryViewController", bundle: NSBundle.mainBundle())
            country.delegate = self
            country.countryArray = self.countryRecord
            presentPopupViewController(country, animationType: MJPopupViewAnimationFade, contentInteraction: MJPopupViewContentInteractionNone)
        }
    }
    
    // -------------------------------------------------------------------------------
    // Select Bank
    // -------------------------------------------------------------------------------
    @IBAction func selectBankClicked(sender: AnyObject) {
        dismissKeyboard()
        if totBankRecord != nil {
            let bank : BankSelectViewController = BankSelectViewController(nibName: "BankSelectViewController", bundle: NSBundle.mainBundle())
            bank.delegate = self
            bank.bankArray = self.totBankRecord
            presentPopupViewController(bank, animationType: MJPopupViewAnimationFade, contentInteraction: MJPopupViewContentInteractionNone)
        }
    
    }
    
    // -------------------------------------------------------------------------------
    // agreeServiceClicked
    // -------------------------------------------------------------------------------
    @IBAction func agreeServiceClicked(sender: AnyObject) {
        dismissKeyboard()
        if checkServiceButton.selected == true {
            checkServiceButton.selected = false
        }else{
            checkServiceButton.selected = true
        }
        
    }
    
    // -------------------------------------------------------------------------------
    // agreePrivacyClicked
    // -------------------------------------------------------------------------------
    @IBAction func agreePrivacyClicked(sender: AnyObject) {
        dismissKeyboard()
        if checkPrivacyButton.selected == true {
            checkPrivacyButton.selected = false
        }else{
            checkPrivacyButton.selected = true
        }
        
    }

    @IBAction func showService(sender: AnyObject) {
        
        let condition:String = "0\(sender.tag)" as String
        ///////// USE THIS FORMAT
        let transaction = TransactionClass()
        transaction.sendAPI(APICode.TERMS_R001, argument: ["TERMS_GB" : condition],
            success: { (success) -> Void in
                
                if let rslt_cd = success["RSLT_CD"] as? String {
                    
                    if rslt_cd != "0000" { //오류
                        if let rsltMsg = success["RSLT_MSG"] as? String {
                            
                            print("Error Code : \(rslt_cd), Error Message : \(rsltMsg)")
                            self.showEventAlert("Error Message :\(rsltMsg)", hasCancelButton: false, delegate: self, tag: nil, confirmBlock: nil, cancelBlock: nil)
                        }
                    } else { //정상
                        if let respData = success["RESP_DATA"] as? NSDictionary {
                            print("Success Code : \(rslt_cd), Data  : \(respData)")
                            
                            if let weburl = success["RESP_DATA"]!["TERMS_URL"] as? String where weburl != "" {
                            
                                print("Data = \(success["RESP_DATA"])")
                                print("Tag = 0\(sender.tag)")
                                print("WebURL = \(weburl)")
                            
                                //TODO : Where TERMS_URL != ""
                                let viewInput = Dictionary(dictionaryLiteral: ("viewName",weburl),("webviewYN","Y"),("webviewTitle","Terms"))
                                let notification = NSNotificationCenter.defaultCenter()
                                notification.postNotificationName("changeMainView", object: nil, userInfo: viewInput)
                            }else{
                                print("Not work")
                            }

                        }
                        
                    }
                }
                
        })
    }

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
       
        if(segue.identifier == "ShowRegistration"){
            
            if sender == nil {
                return
            }
            
            let registerInfo                = segue.destinationViewController as! RegisterInfoViewController
            registerInfo.signupInfo         = sender as? NSMutableDictionary
            registerInfo.passDataTelecom    = self.telcomRecord
        }
    }
    
    //MARK: - UITextFieldDelegate
    // -------------------------------------------------------------------------------
    // textFieldShouldReturn
    // -------------------------------------------------------------------------------
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        accNoTextField.resignFirstResponder()
        dobTextField.resignFirstResponder()
        nameKOTextField.resignFirstResponder()
        nameENTextField.resignFirstResponder()
        return true
    }

    
    // -------------------------------------------------------------------------------
    // shouldChangeCharactersInRange: For max length in textfield
    // -------------------------------------------------------------------------------
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
    
        //Max Length
        guard let text = textField.text else { return true }
        
        let newLength = text.characters.count + string.characters.count - range.length
        
        if textField == dobTextField {
            
            return newLength <= 8
        }
        
        else if textField == nameKOTextField {
        
            return newLength <= 30
        }
            
        else if textField == nameENTextField {
            
            return newLength <= 20
        }
            
        else {
            
            return newLength <= 20
        }
    }
    
    // -------------------------------------------------------------------------------
    // dismissKeyboard
    // -------------------------------------------------------------------------------
    func dismissKeyboard()    {
        accNoTextField?.resignFirstResponder()
        dobTextField?.resignFirstResponder()
        nameKOTextField.resignFirstResponder()
        nameENTextField.resignFirstResponder()
    }
    
    // -------------------------------------------------------------------------------
    // textFieldDidBeginEditing
    // -------------------------------------------------------------------------------
    func textFieldDidBeginEditing(textField: UITextField) {

        if textField == nameKOTextField {
            TextFieldMoveUp(true, txtfield: nameKOTextField)
        }
        
        if textField == nameENTextField {
            TextFieldMoveUp(true, txtfield: nameENTextField)
        }

    }
    
    // -------------------------------------------------------------------------------
    // textFieldDidEndEditing
    // -------------------------------------------------------------------------------
    func textFieldDidEndEditing(textField: UITextField) {
            
        if textField == nameKOTextField {
            TextFieldMoveUp(false, txtfield: nameKOTextField)
        }
        
        if (textField == nameENTextField){
            self.TextFieldMoveUp(false, txtfield: nameENTextField)
        }
        
    }
    
    // MARK: - Text Feild Animation Up
    // -------------------------------------------------------------------------------
    // TextFieldMoveUp
    // -------------------------------------------------------------------------------
    func TextFieldMoveUp(move:Bool , txtfield:UITextField) {
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDelegate(self)
        UIView.setAnimationDuration(0.2)
        UIView.setAnimationBeginsFromCurrentState(true)
        var rect = self.view.frame
        if(move){
            if DeviceType.IS_IPHONE_4_OR_LESS {
                rect.origin.y -= 140.0
            }
            
            if DeviceType.IS_IPHONE_5{
                rect.origin.y -= 60.0
            }
            
        }else{
            if DeviceType.IS_IPHONE_4_OR_LESS {
                rect.origin.y += 140.0
            }
            if DeviceType.IS_IPHONE_5{
                rect.origin.y += 60.0
            }
        }
      
        self.view.frame=rect
        UIView.commitAnimations()
    }
    
    //IBAction: - UITextField dobTextField
    // -------------------------------------------------------------------------------
    // textFieldEditing
    // -------------------------------------------------------------------------------
//    @IBAction func textFieldEditing(sender: UITextField) {
//        
//        let datePickerView:UIDatePicker = UIDatePicker()
//        datePickerView.datePickerMode = UIDatePickerMode.Date
//        datePickerView.backgroundColor = UIColor.clearColor()
//        sender.inputView = datePickerView
//        
//        datePickerView.addTarget(self, action: Selector("datePickerValueChanged:"), forControlEvents: UIControlEvents.ValueChanged)
//    }

    //MARK: - DatePickerValue
    // -------------------------------------------------------------------------------
    // datePickerValueChanged
    // -------------------------------------------------------------------------------
//    func datePickerValueChanged(sender:UIDatePicker) {
//        
//        let dateFormatter = NSDateFormatter()
//        
//        if #available(iOS 8.0, *) {
//            dateFormatter.setLocalizedDateFormatFromTemplate("yyyyMMdd")
//        } else {
//            // Fallback on earlier versions
//            dateFormatter.dateStyle = NSDateFormatterStyle.MediumStyle
//            
//            dateFormatter.timeStyle = NSDateFormatterStyle.NoStyle
//        }
//        
//        //Fill Birthday data into Label
//        let stringDate = dateFormatter.stringFromDate(sender.date) as NSString
//        let year    = stringDate.substringWithRange(NSRange(location: 6, length: 4))
//        let month   = stringDate.substringWithRange(NSRange(location: 0, length: 2))
//        let day     = stringDate.substringWithRange(NSRange(location: 3, length: 2))
//        
//        dobTextField.text = "\(year)\(month)\(day)"
//        
//    }
}
