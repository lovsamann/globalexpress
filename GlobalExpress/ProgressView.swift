//
//  ProgressView.swift
//  GlobalExpress
//
//  Created by JangWooil on 2016. 2. 18..
//  Copyright © 2016년 webcash. All rights reserved.
//

import UIKit

// 로딩바 뷰이다
@objc class ProgressView: UIView {
    let viewHeight = SessionManager.sharedSessionManager().screenHeight
    var viewWidth  = SessionManager.sharedSessionManager().screenWidth
    
    //초기화
    @objc
    init() {
        let yPoint = (viewHeight - 111.0) / 2
        let xPoint = (viewWidth - 249.0) / 2
        
        super.init(frame: CGRectMake(xPoint, yPoint, 249.0, 111.0))
        
        internalInit()
    }
    
    func internalInit() {
        let viewHeight = SessionManager.sharedSessionManager().screenHeight
        var viewWidth  = SessionManager.sharedSessionManager().screenWidth
        let yLocation  = (viewHeight - 150.0) / 2 //y 값 위치
        
        viewWidth = viewWidth - 60
        
        let view : UIView = UIView(frame: CGRectMake(30.0, yLocation, viewWidth, 150.0)) //가로 센터에 위치하기 위해 30(공백) - viewWidth - 30(공백) 으로 세팅한다
        view.backgroundColor = UIColor.whiteColor()
        view.layer.borderWidth = 1
        view.layer.borderColor = UIColor.blackColor().CGColor
        
        let descLabel : UILabel = UILabel(frame: CGRectMake(0.0, 20.0, viewWidth, 21.0))
        descLabel.textAlignment = .Center
        descLabel.text = Language.localizedStr("C04P_01") //ARS 인증 요청 중입니다.
        view.addSubview(descLabel)
        
        self.addSubview(view)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //로딩바보이기
    func show() {
        self.frame = CGRectMake(0.0, 20, self.frame.size.width, self.frame.size.height)
    }
    
    //로딩바가리기
    func close() {
        self.frame = CGRectMake(0.0, 480, self.frame.size.width, self.frame.size.height)
    }
    
}
