//
//  Bank.swift
//  GlobalExpress
//
//  Created by Ann on 3/7/16.
//  Copyright © 2016 webcash. All rights reserved.
//

import Foundation

class Bank: NSObject, PrintableCell {

    var name: String!
    var image: String!
    var code: String!
    
    init?(data: NSDictionary?){
        guard data != nil else{
            print("dictionary Bank failed")
            return
        }
        
        name = data!["BANK_NM"] as? String
        code = data!["BANK_CD"] as? String
        image = nil
    }
}
