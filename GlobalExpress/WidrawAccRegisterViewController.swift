//
//  WidrawAccRegisterViewController.swift
//  GlobalExpress
//
//  Created by Ralex on 2/2/16.
//  Copyright © 2016 webcash. All rights reserved.
//

import UIKit

class WidrawAccRegisterViewController: UIViewController,BankSelectViewControllerDelegate, ARSViewControllerDelegate,UITextFieldDelegate{
    //Variable
    var customerinfo:Dictionary<String,String> = ["":""]
    var custname = Array<String>()
    var bankList = Array<AnyObject>()
    var selectedbank = Dictionary<String,String>()
    var isConfirm:Bool = false
    var inq_txno:String?
    var navigationTitle = "Debit Account Designation"
    var nation_gb:String?
    //Outlet
    @IBOutlet weak var lblKoreanName: UILabel!
    @IBOutlet weak var lblEnglishName: UILabel!
    
    @IBOutlet weak var tfAccountNo: UITextField!
    @IBOutlet weak var lblBank: UILabel!
    @IBOutlet weak var lblProcessStatus: UILabel!
    
    @IBOutlet weak var korNameLabel:UILabel!
    @IBOutlet weak var enNameLabel:UILabel!
    @IBOutlet weak var bankLabel:UILabel!
    @IBOutlet weak var bankAccLabel:UILabel!
    @IBOutlet weak var btnConfirm: UIButton!
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var lblStep: UILabel!
    
    @IBOutlet weak var viewNameKOR: UIView!
    
    @IBOutlet weak var viewEng_constraint_top: NSLayoutConstraint!
    @IBOutlet weak var viewContent_constraint_height: NSLayoutConstraint!
    @IBOutlet weak var contentMain_constraint_hieght: NSLayoutConstraint!
    
    func initLoad() -> Void{
        lblKoreanName.text = custname[1]
        lblEnglishName.text = self.custname[0]
    }
    
    override func localize() {
        navigationTitle = Language.localizedStr("T08_01")
        enNameLabel.text =  Language.localizedStr("T08_02")
        korNameLabel.text = Language.localizedStr("T08_02")
        bankLabel.text = Language.localizedStr("T08_03")
        bankAccLabel.text = Language.localizedStr("T08_04")
        lblBank.text = Language.localizedStr("T08_05")
        tfAccountNo.attributedPlaceholder = NSAttributedString(string: Language.localizedStr("T08_06"))
        btnConfirm.setTitle(Language.localizedStr("T08_07"), forState: .Normal)
        btnNext.setTitle(Language.localizedStr("T08_08"), forState: .Normal)
        lblStep.text = "4. \(Language.localizedStr("M01_06"))"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        localize()
        RLib.setNavigationTitle(self, title: navigationTitle)
        RLib.setLeftNavigationItem(self, image: "back_btn.png", action: Selector("handleLeftNavigationItemClick:"))
        
        //TODO:
        if nation_gb != ""{
            viewNameKOR.hidden = true
            viewEng_constraint_top.constant = 0 - viewNameKOR.frame.size.height
            viewContent_constraint_height.constant = 176 - viewNameKOR.frame.size.height
            contentMain_constraint_hieght.constant = 158 - viewNameKOR.frame.size.height
        }
    }
    
    func handleLeftNavigationItemClick(sender:UIBarButtonItem){
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.initLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func handleSelectOnView(sender: AnyObject) {
        tfAccountNo.resignFirstResponder()
    }
   
    
    @IBAction func ActionConfirm(sender: AnyObject) {
        tfAccountNo.resignFirstResponder()
        if tfAccountNo.text?.characters.count == 0 {
            showSimpleAlert("Please enter account number.", delegate: self)
            return
        }else{
            if isConfirm == false{
                var args : Dictionary<String,String>
                args = ["CUST_NM":self.custname[0],"BANK_CD":self.selectedbank["BANK_CD"]!,"ACCT_NO":self.tfAccountNo.text!]
                TransactionClass().sendAPI(APICode.ACTING_ROO1, argument: args, success: { (success) -> Void in
                    print("Arg = \(success)")
                    guard let code = success["RSLT_CD"] where code as! String == "0000" else{
                        self.showSimpleAlert(success["RSLT_MSG"] as! String, delegate: self)
                        self.isConfirm = false
                        return
                    }
                    self.lblProcessStatus.text = Language.localizedStr("T08_09")
                    //TODO: Transaction authorize number should be here ~_~
                    if let auth = success["RESP_DATA"]!["INQ_TXNO"] as? String{
                        self.inq_txno = auth
                        print("Auth number = \(self.inq_txno!)")
                    }
                    self.isConfirm = true
                })

            }
        }
    }
    
    @IBAction func handleSelectBankTap(sender: AnyObject) {
        //TODO: Handle Bank Selection
        tfAccountNo.resignFirstResponder()
        TransactionClass().sendAPI(APICode.CODE_R001, argument: ["":""], success: { (success) -> Void in
            guard let code = success["RSLT_CD"] where code as! String == "0000" else{
                //Should be alert here
                print("RESULT MESSAGE = \(success["RSLT_MSG"])")
                return
            }
            if let data = success["RESP_DATA"], let bankList = data["TOTBANK_REC"] as? Array<AnyObject>{
                self.bankList = bankList
                let bank : BankSelectViewController = BankSelectViewController(nibName: "BankSelectViewController", bundle: NSBundle.mainBundle())
                bank.delegate = self
                bank.bankArray = self.bankList
                self.presentPopupViewController(bank, animationType: MJPopupViewAnimationFade, contentInteraction: MJPopupViewContentInteractionNone)
            }
        })
        
    }
    
    /* 
        Limit number account input
    */
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        let newLength = (textField.text?.utf16.count)! + string.utf16.count - range.length
        return newLength > 15 ? false : true
    }
    
    @IBAction func ActionNext(sender: AnyObject) {
        //TODO: Goto ARS인증 안내
        if selectedbank["BANK_NM"] != nil && tfAccountNo.text?.characters.count != 0 && isConfirm == true{
            //SUCCESS
            // Go to T09 by get auth number from ActionConfirm section
            // End of my task 
            // jokachi hane cbal
            self.performSegueWithIdentifier("showARS", sender: self)
        }else{
            //FAIL
            var message = ""
            if selectedbank["BANK_NM"] == nil{
                message = "Please select a bank."
            }else if tfAccountNo.text?.characters.count == 0{
                message = "Please enter account number."
            }else if isConfirm == false{
                message = "Please confirm account number."
            }
            self.showSimpleAlert(message, delegate: self)
        }
    }
    
    
    /*
        + getBankData Delegate method
        - param: dict return dictionary data
    */
    func getBankData(dict: NSDictionary!) {
        selectedbank = dict as! Dictionary<String, String>
        lblBank.textColor = UIColor(red: 48/255, green: 50/255, blue: 51/255, alpha: 1)
        lblBank.text = dict["BANK_NM"] as? String
    }
 
    //TODO: YOU HAVE TO UPDATE THIS METHODS - START
    func arsSuccess() {
        let tradeProcess = NSUserDefaults.standardUserDefaults()
        if let tradeCode = tradeProcess.objectForKey("TradeProcess"){
            if tradeCode as! Int == 4{
                //GOTO CHANGE SUCCESS SCREEN
                let storyboard = UIStoryboard(name: "MyInfoManager", bundle: nil)
                let viewcontroller = storyboard.instantiateViewControllerWithIdentifier("DebitAccountChangedViewController") as? DebitAccountChangedViewController
                //Send Data
                self.navigationController?.pushViewController(viewcontroller!, animated: true)
            }else{
                let viewcontroller = self.storyboard!.instantiateViewControllerWithIdentifier("PinCodeViewController") as? PinCodeViewController
                viewcontroller?.tradeStatus = tradeCode as? Int
                self.navigationController?.pushViewController(viewcontroller!, animated: true)
            }
        }
    }
    
    func arsFailure() {
        print("Failure!!")
        
    }
    //TODO: YOU HAVE TO UPDATE THIS METHODS - END
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let segueId = segue.identifier {
            if(segueId == "showARS") {
                if let arsVC = segue.destinationViewController as? ARSViewController {
                   if let i_InqTxno = self.inq_txno, i_custNm = self.custname[0] as? String, i_bankCd =
                    self.selectedbank["BANK_CD"], i_acctNo = self.tfAccountNo.text {
                        arsVC.inq_txno = i_InqTxno
                        arsVC.cust_nm = i_custNm
                        arsVC.bank_cd = i_bankCd
                        arsVC.acct_no = i_acctNo
                        arsVC.delegate = self
                    }
                }
            }
        }
    }
    
}
