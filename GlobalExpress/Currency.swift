//
//  Currency.swift
//  GlobalExpress
//
//  Created by Ann on 1/28/16.
//  Copyright © 2016 webcash. All rights reserved.
//

import Foundation

class Currency: NSObject,PrintableCell {

    var code: String?
    var name: String!
    var image: String!
    
    init?(data: NSDictionary?) {
        
        guard let rawData = data else{
            print("Currency data nil. ")
            return
        }
        
        code = rawData["CURRENCY_CD"] as? String
        name = rawData["CURRENCY_NM"] as? String
        image = nil
    }
    
}
