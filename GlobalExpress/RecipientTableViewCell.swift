//
//  RecipientTableViewCell.swift
//  GlobalExpress
//
//  Created by Ann on 2/27/16.
//  Copyright © 2016 webcash. All rights reserved.
//

import UIKit

class RecipientTableViewCell: UITableViewCell {

    @IBOutlet var onOffButton: UIButton!
    @IBOutlet var recipientNameLabel: UILabel!
    @IBOutlet var BankNameLabel: UILabel!
    @IBOutlet var NationalityLabel: UILabel!
    @IBOutlet var accountNoLabel: UILabel!
    
    override func setSelected(selected: Bool, animated: Bool) {
        onOffButton.selected = selected
    }
    

}
