//
//  RegisterInfoViewController.swift
//  GlobalExpress
//
//  Created by UDAM on 2/3/16.
//  Copyright © 2016 webcash. All rights reserved.
//

import UIKit

class RegisterInfoViewController: UIViewController, UITextFieldDelegate, TelecomPopupDelegate {

    // MARK: - IBOutlet
    // -------------------------------------------------------------------------------
    // IBOulet UIScrollView
    // -------------------------------------------------------------------------------
    @IBOutlet weak var Scroller         : UIScrollView!
    
    // -------------------------------------------------------------------------------
    // IBOulet UILabel
    // -------------------------------------------------------------------------------
    @IBOutlet weak var topTitleLabel    : UILabel!
    @IBOutlet weak var stepTitleLabel   : UILabel!
    @IBOutlet weak var nameKOLabel      : UILabel!
    @IBOutlet weak var nameENLabel      : UILabel!
    @IBOutlet weak var dobLabel         : UILabel!
    @IBOutlet weak var countryLabel     : UILabel!
    @IBOutlet weak var phoneNoLabel     : UILabel!
    @IBOutlet weak var passNoLabel      : UILabel!
    @IBOutlet weak var emailLabel       : UILabel!
    @IBOutlet weak var expireDateLabel  : UILabel!
    @IBOutlet weak var promoCodeLabel   : UILabel!
    @IBOutlet weak var requiredLabel    : UILabel!

    
    @IBOutlet weak var nameKOResult     : UILabel!
    @IBOutlet weak var nameENResult     : UILabel!
    @IBOutlet weak var dateOfBirth      : UILabel!
    @IBOutlet weak var country          : UILabel!
    
    // -------------------------------------------------------------------------------
    // IBOulet UIImageView
    // -------------------------------------------------------------------------------
    @IBOutlet weak var redIconExpireDate: UIImageView!
    
    // -------------------------------------------------------------------------------
    // IBOulet UITextField
    // -------------------------------------------------------------------------------
    @IBOutlet weak var phoneNoTextField : UITextField!
    @IBOutlet weak var passNoTextField  : UITextField!
    @IBOutlet weak var emailTextField   : UITextField!
    @IBOutlet weak var expireDateTextField: UITextField!
    @IBOutlet weak var proCodeTextField : UITextField!
    @IBOutlet weak var intl_Telecom     : UITextField!
    
    // -------------------------------------------------------------------------------
    // IBOulet UIView
    // -------------------------------------------------------------------------------
    @IBOutlet weak var stepView                 : UIView!
    @IBOutlet weak var lastLineLine8View        : UIView!
    @IBOutlet weak var line7View                : UIView!
    
    // -------------------------------------------------------------------------------
    // IBOulet UIButton
    // -------------------------------------------------------------------------------
    @IBOutlet weak var nextButton       : UIButton!
    @IBOutlet weak var Item1Button      : UIButton!
    @IBOutlet weak var Item2Button      : UIButton!
    @IBOutlet weak var Item3Button      : UIButton!
    @IBOutlet weak var Item4Button      : UIButton!
    
    // -------------------------------------------------------------------------------
    // IBOulet ADVSegmentController
    // -------------------------------------------------------------------------------
    @IBOutlet weak var telecomView      : ADVSegmentedControl!
    
    // -------------------------------------------------------------------------------
    // IBOulet NSLayoutConstraint
    // -------------------------------------------------------------------------------
    @IBOutlet weak var splitRightConstraint                 : NSLayoutConstraint!
    @IBOutlet weak var splitLeftConstraint                  : NSLayoutConstraint!
    @IBOutlet weak var bottomNextButtonConstraint           : NSLayoutConstraint!
    @IBOutlet weak var centerViewHeightConstraint           : NSLayoutConstraint!
    @IBOutlet weak var heightLabelNameEngConstraint         : NSLayoutConstraint!
    @IBOutlet weak var heightTextFieldNameEngConstraint     : NSLayoutConstraint!
    @IBOutlet weak var heightLabelPromotionCodeConstraint   : NSLayoutConstraint!
    @IBOutlet weak var heightTextFieldPromoCodeConstraint   : NSLayoutConstraint!
    @IBOutlet weak var positionEmailLabelConstraint         : NSLayoutConstraint!
    @IBOutlet weak var positionEmailTextFieldConstraint     : NSLayoutConstraint!
    
    // MARK: - Propertie
    // ------------------------------------------------------------------------------
    // NSArray, NSMutableDictionary
    // -------------------------------------------------------------------------------
    var arrayTelecom        : Array<String> = []
    var passDataTelecom     : NSArray?
    var signupInfo          : NSMutableDictionary?

    private var isKorean    : Bool      = false
    
    // ------------------------------------------------------------------------------
    // String for input and request to server
    // -------------------------------------------------------------------------------
    var firstLengthPasNo    : String    = ""
    var lastLengthPasNo     : String    = ""
    var telecomCode         : String?
    
    // MARK: - Private function
    // -------------------------------------------------------------------------------
    // Localizing language
    // -------------------------------------------------------------------------------
    override func localize() {
        
        self.navigationItem.title       = Language.localizedStr("M03_2_01")
        self.stepTitleLabel.text        = "1. \(Language.localizedStr("M01_01"))"
        self.topTitleLabel.text         = Language.localizedStr("M03_2_02")
        self.nameKOLabel.text           = Language.localizedStr("M03_2_03")
        self.nameENLabel.text           = Language.localizedStr("M03_2_03")
        self.dobLabel.text              = Language.localizedStr("M03_2_04")
        self.countryLabel.text          = Language.localizedStr("M03_2_05")
        self.phoneNoLabel.text          = Language.localizedStr("M03_2_06")
        self.emailLabel.text            = Language.localizedStr("M03_2_17")
        self.expireDateLabel.text       = Language.localizedStr("M03_2_16")
        self.promoCodeLabel.text        = Language.localizedStr("M03_2_09")
        self.requiredLabel.text         = Language.localizedStr("M03_2_10")
        
        self.phoneNoTextField.placeholder       = Language.localizedStr("M03_2_12")
        self.passNoTextField.placeholder        = Language.localizedStr("M03_2_12")
        self.emailTextField.placeholder         = Language.localizedStr("M03_2_17")
        self.expireDateTextField.placeholder    = Language.localizedStr("M03_2_16")
        self.proCodeTextField.placeholder       = Language.localizedStr("M03_2_14")
        
        self.nextButton.setTitle(Language.localizedStr("M03_2_15"), forState: .Normal)
        
        self.passNoLabel.text               = Language.localizedStr("M03_2_07")
        
    }
    
    // -------------------------------------------------------------------------------
    // Customize View Layout
    // -------------------------------------------------------------------------------
    func customizeViewLayout(){
        
        stepView.layer.cornerRadius = 5.0
        
//        if UIDevice().modelName == "iPhone 6" {
//            splitRightConstraint.constant       = 58 + 18
//            splitLeftConstraint.constant        = 167 + 16
//        }
//        
//        if UIDevice().modelName == "iPhone 6 Plus" {
//            splitRightConstraint.constant       = 58 + 29
//            splitLeftConstraint.constant        = 167 + 28
//        }

        if DeviceType.IS_IPHONE_6 {
            splitRightConstraint.constant       = 58 + 18
            splitLeftConstraint.constant        = 167 + 16
        }
        
        if DeviceType.IS_IPHONE_6P {
            splitRightConstraint.constant       = 58 + 29
            splitLeftConstraint.constant        = 167 + 28
        }
        
        //Customize navigation item ,barButton
        let backImage = UIImage(imageLiteral: "back_btn")
        let leftNavButton = UIButton(frame: CGRectMake(269.0, 20.0, backImage.size.width/2, backImage.size.height/2))
        leftNavButton.setBackgroundImage(backImage, forState: .Normal)
        leftNavButton.addTarget(self, action: "popUpViewController", forControlEvents:UIControlEvents.TouchUpInside)
        let barButton = UIBarButtonItem(customView: leftNavButton)
        self.navigationItem.leftBarButtonItem = barButton
        navigationItem.setHidesBackButton(true, animated: false)
        
        //Add Gesture for dismissKeyboard
        let recognizer      = UITapGestureRecognizer(target: self, action:Selector("dismissKeyboard"))
        view.addGestureRecognizer(recognizer)
        
    }
    
    // -------------------------------------------------------------------------------
    // setResultToView: Main function to set view which is communicated with server
    // -------------------------------------------------------------------------------
    func setResultToView() {
        
        emailTextField.keyboardType = UIKeyboardType.ASCIICapable
        
        //Localize string with Passport/Social Security No
        if isKorean {
            
            self.passNoLabel.text               = Language.localizedStr("M03_1_08")
            self.passNoTextField.keyboardType   = UIKeyboardType.NumberPad
            
            nameKOResult.text   = "\(self.signupInfo!["KOR_NM"]!)"
            nameENResult.text   = "\(self.signupInfo!["ENG_NM"]!)"
            
//            centerViewHeightConstraint.constant         = 394 - 46
            expireDateLabel.hidden                      = true
            expireDateTextField.hidden                  = true
//            emailLabel.hidden                           = true
//            emailTextField.hidden                       = true
            redIconExpireDate.hidden                    = true
//            line7View.hidden                            = true
            
//            if UIDevice().modelName == "iPhone 6" {
//                bottomNextButtonConstraint.constant = 17 - 36 - 47
//            }
//
//            if UIDevice().modelName == "iPhone 6 Plus" {
//                bottomNextButtonConstraint.constant = 17 - 105 - 47
//            }
            
            if DeviceType.IS_IPHONE_6 {
                bottomNextButtonConstraint.constant = 17 - 36
            }
            
            if DeviceType.IS_IPHONE_6P {
                bottomNextButtonConstraint.constant = 17 - 105
            }
            
            //Additional Email in Korean Signup
            positionEmailTextFieldConstraint.constant   = 18 - 44
            heightTextFieldPromoCodeConstraint.constant = 18 + 44
            positionEmailLabelConstraint.constant       = 18 - 44
            heightLabelPromotionCodeConstraint.constant = 18 + 44
            
        }else{
            
            self.passNoLabel.text                   = Language.localizedStr("M03_2_07")
            self.passNoTextField.keyboardType       = UIKeyboardType.Default
            self.passNoTextField.autocorrectionType = UITextAutocorrectionType.No
            
             nameENResult.text = "\(self.signupInfo!["ENG_NM"]!)"
            
            heightLabelNameEngConstraint.constant       = 18 - 43
            heightTextFieldNameEngConstraint.constant   = 18 - 43
            nameKOLabel.hidden                          = true
            nameKOResult.hidden                         = true
            expireDateLabel.hidden                      = false
            expireDateTextField.hidden                  = false
            redIconExpireDate.hidden                    = false
            heightLabelPromotionCodeConstraint.constant = 18 + 86
            heightTextFieldPromoCodeConstraint.constant = 18 + 86
            lastLineLine8View.hidden                    = false
            
//            if UIDevice().modelName == "iPhone 6" {
//                bottomNextButtonConstraint.constant = 17 - 36
//            }
//
//            if UIDevice().modelName == "iPhone 6 Plus" {
//                bottomNextButtonConstraint.constant = 17 - 105
//            }
            
            if DeviceType.IS_IPHONE_6 {
                bottomNextButtonConstraint.constant = 17 - 36
            }
            
            if DeviceType.IS_IPHONE_6P {
                bottomNextButtonConstraint.constant = 17 - 105
            }
        }
        
        //Fill Birthday data into Label
        let stringDate = self.signupInfo!["BIRTH_YMD"] as! NSString
        
        let year    = stringDate.substringWithRange(NSRange(location: 0, length: 4))
        let month   = stringDate.substringWithRange(NSRange(location: 4, length: 2))
        let day     = stringDate.substringWithRange(NSRange(location: 6, length: 2))

        self.dateOfBirth.text         = "\(year).\(month).\(day)"
        
        //Country
        self.country.text     = "\(self.signupInfo!["NATION_NM"]!)"
        
        //Telecom
        for index in 4...self.passDataTelecom!.count{
            if index == 4 {
                //Default Value Telecomcode beware nil
                telecomCode = self.passDataTelecom![index-1]["TELCOM_CD"] as? String
            }
            self.arrayTelecom.append((self.passDataTelecom![index - 1]["TELCOM_NM"] as? String)!)
        }
        self.arrayTelecom.append("\(Language.localizedStr("C09P_02"))")
        self.telecomView.items                   = self.arrayTelecom
        self.telecomView.font                    = UIFont.systemFontOfSize(12.0)
//        self.telecomView.addTarget(self, action: Selector("handleSelectedTelecomView"), forControlEvents: .ValueChanged)
        
        //Add gesture to HideViewTelecomView
//        let recognizerTelecom      = UITapGestureRecognizer(target: self, action:Selector("selectTelecomClicked"))
//        hideViewTelecomTapView.addGestureRecognizer(recognizerTelecom)
        
    }
    
    // -------------------------------------------------------------------------------
    // TelecomPopupDelegate
    // -------------------------------------------------------------------------------
    func getTelecomData(dict: NSDictionary!) {
        telecomCode = dict["TELCOM_CD"] as? String
        telecomView.selectedIndex = 3
        print(telecomCode!)
    }
    
    // MARK: - IBAction
    // -------------------------------------------------------------------------------
    // Click to view TelecomPopup
    // -------------------------------------------------------------------------------
    func selectTelecomClicked() {
        
        dismissKeyboard()
        let telecom : TelecomPopup = TelecomPopup(nibName: "TelecomPopup", bundle: NSBundle.mainBundle())
        telecom.delegate = self
        telecom.telecomArray = self.passDataTelecom as! [AnyObject]
        presentPopupViewController(telecom, animationType: MJPopupViewAnimationFade, contentInteraction: MJPopupViewContentInteractionNone)
        
    }
    
    @IBAction func clickedIndexButton(sender: UIButton) {
        
        if sender.tag == 0 {
            telecomView.selectedIndex = 0
            telecomCode = self.passDataTelecom![sender.tag+3]["TELCOM_CD"] as? String
        }
        else if sender.tag == 1 {
            telecomView.selectedIndex = 1
            telecomCode = self.passDataTelecom![sender.tag+3]["TELCOM_CD"] as? String
        }
        else if sender.tag == 2 {
            telecomView.selectedIndex = 2
            telecomCode = self.passDataTelecom![sender.tag+3]["TELCOM_CD"] as? String
        }
        else if sender.tag == 3 {
            selectTelecomClicked()
        }

        print(telecomCode!)
    }
    
    // -------------------------------------------------------------------------------
    // PopupViewController
    // -------------------------------------------------------------------------------
    func popUpViewController() {
        navigationController?.popViewControllerAnimated(true)
    }
    
    // MARK: - View Life Cycle
    // -------------------------------------------------------------------------------
    // ViewDidLoad
    // -------------------------------------------------------------------------------
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Localized
        self.localize()
        
        //Customize
        customizeViewLayout()
        
        //Check signupInfo nil
        if signupInfo == nil {
            return
        }
        
        //Set isKorean true/false
        (signupInfo!["NATION_CD"]! as! String != "410") ? (isKorean = false) : (isKorean = true)

        //SetResultToView
        setResultToView()
        
    }
    
    // -------------------------------------------------------------------------------
    // viewDidDisappear
    // -------------------------------------------------------------------------------
    override func viewDidDisappear(animated: Bool) {
         navigationItem.setHidesBackButton(false, animated: false)
    }
    
    // -------------------------------------------------------------------------------
    // Click to complete SignupStep
    // -------------------------------------------------------------------------------
    @IBAction func nextCliked(sender: UIButton) {
        
        dismissKeyboard()
        
        //Dictionary request to server
        var suffixDic = ["" : ""]
        
        //Check Phone number is correct
        if phoneNoTextField.text?.characters.count < 9 {
            self.showEventAlert("Error Message :Your Cell Phone No is incorrect", hasCancelButton: false, delegate: self, tag: nil, confirmBlock: nil, cancelBlock: nil)
            return
        }
        
        //Check Passport/ Social Security Number is correct
        if isKorean {
            
            intl_Telecom.text = "82"
            
            if passNoTextField.text?.characters.count < 13 {
                self.showEventAlert("Error Message :Your Social Security No is incorrect", hasCancelButton: false, delegate: self, tag: nil, confirmBlock: nil, cancelBlock: nil)
                return
            }
            
            suffixDic = [
 
                "PASSPORT_NO":"",
//                "RESIDENT_NO":"\(passNoTextField.text!)",
                "RESIDENT_NO" : "\(firstLengthPasNo)\(lastLengthPasNo)",
                "PASSPORT_EXPRDT":"",
            ]
            
        }
        else{
            
            if intl_Telecom.text == "" {
                intl_Telecom.text = "82"
            }
            
            if passNoTextField.text?.characters.count < 10 {
                self.showEventAlert("Error Message :Your Passport No is incorrect", hasCancelButton: false, delegate: self, tag: nil, confirmBlock: nil, cancelBlock: nil)
                return
            }
            
            //Check Passport expire date is correct
            if expireDateTextField.text?.characters.count < 8 {
                self.showEventAlert("Error Message :Your Passport Expire Date is incorrect", hasCancelButton: false, delegate: self, tag: nil, confirmBlock: nil, cancelBlock: nil)
                return
            }
            
            suffixDic = [
//                "PASSPORT_NO":"\(passNoTextField.text!)",
                "PASSPORT_NO" : "\(firstLengthPasNo)\(lastLengthPasNo)",
                "RESIDENT_NO" : "",
                "PASSPORT_EXPRDT" : "\(expireDateTextField.text!)",
            ]

            
        }
        
        //Prepare Data to server
        suffixDic.updateValue("\(signupInfo!["CERT_DT"]!)"      , forKey: "CERT_DT")
        suffixDic.updateValue("\(signupInfo!["CERT_TXNO"]!)"    , forKey: "CERT_TXNO")
        suffixDic.updateValue("\(signupInfo!["ENG_NM"]!)"       , forKey: "ENG_NM")
        suffixDic.updateValue("\(signupInfo!["KOR_NM"]!)"       , forKey: "KOR_NM")
        suffixDic.updateValue("\(signupInfo!["BIRTH_YMD"]!)"    , forKey: "BIRTH_YMD")
        suffixDic.updateValue("\(signupInfo!["NATION_CD"]!)"    , forKey: "NATION_CD")
        suffixDic.updateValue("\(telecomCode!)"                 , forKey: "TELCOM_CD")
        suffixDic.updateValue("\(phoneNoTextField.text!)"       , forKey: "HP_NO")
        suffixDic.updateValue("\(signupInfo!["BANK_CD"]!)"      , forKey: "FXBANK_CD")
        suffixDic.updateValue("\(proCodeTextField.text!)"       , forKey: "RCMD_ID")
        suffixDic.updateValue("1"                               , forKey: "APP_TYPE")
        suffixDic.updateValue("1"                               , forKey: "PUSH_ID")
        suffixDic.updateValue("1"                               , forKey: "DEVICE_ID")
        suffixDic.updateValue(UIDevice().modelName              , forKey: "MODEL_NM")
        suffixDic.updateValue("1"                               , forKey: "OS")
        suffixDic.updateValue("\(emailTextField.text!)"         , forKey: "E_MAIL")
        suffixDic.updateValue("\(intl_Telecom.text!)"           , forKey: "INTL_TELCD")

        
//        var suffixDic = [
//            "CERT_DT"   : "\(signupInfo!["CERT_DT"]!)",
//            "CERT_TXNO" : "\(signupInfo!["CERT_TXNO"]!)",
//            "ENG_NM"    : "\(signupInfo!["ENG_NM"]!)",
//            "KOR_NM"    : "\(signupInfo!["KOR_NM"]!)",
//            "BIRTH_YMD" : "\(signupInfo!["BIRTH_YMD"]!)",
//            "NATION_CD" : "\(signupInfo!["NATION_CD"]!)",
//            "TELCOM_CD" : "\(telecomCode!)",
//            "HP_NO"     : "\(phoneNoTextField.text!)",
//            "FXBANK_CD" : "\(signupInfo!["BANK_CD"]!)",
//            "RCMD_ID"   : "\(proCodeTextField.text!)",
//            "APP_TYPE"  : "1",
//            "PUSH_ID"   : "1",
//            "DEVICE_ID" : "1",
//            "MODEL_NM"  : UIDevice().modelName,
//            "OS"        : "1",
//            "E_MAIL"    : "\(emailTextField.text!)",
//            "INTL_TELCD": "\(intl_Telecom.text!)"
//        ]
        
        print("Print send SuffixDic--->\(suffixDic)")
        
        /////////// USE THIS FORMAT
        let transaction = TransactionClass()
        transaction.sendAPI(APICode.USER_C001, argument: suffixDic,
            success: { (success) -> Void in
                
            //20160310 - 장우일
            if let rslt_cd = success["RSLT_CD"] as? String {
                if rslt_cd != "0000" { //오류
                    if let rsltMsg = success["RSLT_MSG"] as? String {
                        print("Error Code : \(rslt_cd), Error Message : \(rsltMsg)")
                        self.showEventAlert("Error Message :\(rsltMsg)", hasCancelButton: false, delegate: self, tag: nil, confirmBlock: nil, cancelBlock: nil)
                    }
                } else { //정상
                    if let respData = success["RESP_DATA"] as? NSDictionary {
                        print("Sucess Code : \(rslt_cd),Response Message : \(respData)")
                        // -------------- NO EDIT ---------------------
                        //USER_NO, USER_NM을 저장해서 사용한다.
                        if let userNo = respData["USER_NO"] as? String {
                            NSUserDefaults.standardUserDefaults().setObject(userNo, forKey: "USER_NO")
                            
                            //Push Notification 정보 등록
                            let regDic = Dictionary(dictionaryLiteral: ("registerKey", userNo))
                            
                            NSNotificationCenter.defaultCenter().postNotificationName("PushStartNotification", object: self, userInfo: regDic)
                        }
                        if let userNm = respData["USER_NM"] as? String {
                            NSUserDefaults.standardUserDefaults().setObject(userNm, forKey: "USER_NM")
                        }
                        //회원가입여부 Y
                        NSUserDefaults.standardUserDefaults().setObject("Y", forKey: "SIGNUP_YN")
                        
                        NSUserDefaults.standardUserDefaults().synchronize()
                        // -------------- NO EDIT ---------------------
                    }
                    
                    self.performSegueWithIdentifier("ShowCompleted", sender: nil)

                }

            }
            //20160310 - 장우일
            
        })
        /////////// USE THIS FORMAT
        
//        self.performSegueWithIdentifier("ShowCompleted", sender: nil)
    }
    
    //MARK: - UITextFieldDelegate
    // -------------------------------------------------------------------------------
    // textFieldShouldReturn
    // -------------------------------------------------------------------------------
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        phoneNoTextField.resignFirstResponder()
        passNoTextField.resignFirstResponder()
        proCodeTextField.resignFirstResponder()
        expireDateTextField.resignFirstResponder()
        emailTextField.resignFirstResponder()
        return true
    }
    
    // -------------------------------------------------------------------------------
    // textFieldDidBeginEditing
    // -------------------------------------------------------------------------------
    func textFieldDidBeginEditing(textField: UITextField) {
        
        if textField == phoneNoTextField {
            TextFieldMoveUp(true, txtfield: phoneNoTextField)
        }
            
        if textField == passNoTextField {
            TextFieldMoveUp(true, txtfield: passNoTextField)
            
            if isKorean {
                //Korean
                if textField.text?.characters.count == 14 {
                    
                   textField.text = "\(firstLengthPasNo)\(lastLengthPasNo)"
                    
                }
            } else {
                //Foreigner
                
                if textField.text?.characters.count >= 8 {
                    
                    textField.text = "\(firstLengthPasNo)\(lastLengthPasNo)"
                    
                }
                
            }
        }
        
        if textField == expireDateTextField {
            TextFieldMoveUp(true, txtfield: expireDateTextField)
        }
        
        if textField == emailTextField {
            
            TextFieldMoveUp(true, txtfield: emailTextField)
        }
            
        if textField == proCodeTextField {
            TextFieldMoveUp(true, txtfield: proCodeTextField)
        }
        
    }
    
    // -------------------------------------------------------------------------------
    // textFieldDidEndEditing
    // -------------------------------------------------------------------------------
    func textFieldDidEndEditing(textField: UITextField) {

        if textField == phoneNoTextField {
            TextFieldMoveUp(false, txtfield: phoneNoTextField)
        }
            
        if textField == passNoTextField {
            TextFieldMoveUp(false, txtfield: passNoTextField)
            
            if isKorean {
                
                if textField.text?.characters.count == 13 {
                    textField.text = "\(firstLengthPasNo)-\u{25CF}\u{25CF}\u{25CF}\u{25CF}\u{25CF}\u{25CF}\u{25CF}"
                }
            }
            else {
                
                if textField.text?.characters.count >= 7 {
                    textField.text = "\(firstLengthPasNo) \u{25CF}\u{25CF}\u{25CF}\u{25CF}\u{25CF}"
                }
            }
        }
        
        if textField == expireDateTextField {
            TextFieldMoveUp(false, txtfield: expireDateTextField)
        }
        
        if textField == emailTextField {
            TextFieldMoveUp(false, txtfield: emailTextField)
        }
        
        if textField == proCodeTextField {
            TextFieldMoveUp(false, txtfield: proCodeTextField)
        }
    
    }
    
    // -------------------------------------------------------------------------------
    // shouldChangeCharactersInRange : Customize make PassNo/SocialNo hidden with dot
    // -------------------------------------------------------------------------------
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        
        //Max Length
        guard let text = textField.text else { return true }
        
        let newLength = text.characters.count + string.characters.count - range.length
        
        if textField == phoneNoTextField {
            
            return newLength <= 13
            
        }
        
        else if textField == expireDateTextField {
            
            return newLength <= 8
            
        }
        
        else if textField == emailTextField {
            
            return newLength <= 30
            
        }
        
        else if textField == proCodeTextField {
            
            return newLength <= 10
            
        }
        
        else if textField == passNoTextField {
            
            if isKorean {
                //Korean Social Security Number only 13 Digit
                let fullString   = (textField.text! as NSString).stringByReplacingCharactersInRange(range, withString: string) as NSString
                
                if newLength == 13 {
                    firstLengthPasNo  = fullString.substringWithRange(NSRange(location: 0, length: 6))
                    lastLengthPasNo   = fullString.substringWithRange(NSRange(location: 6, length: 7))
                }

                return newLength <= 13
                
            }
            else {
                //Foreigner Passport Number over 7 Digit
                let fullString   = (textField.text! as NSString).stringByReplacingCharactersInRange(range, withString: string) as NSString
                
                for var i = 7 ; i <= newLength ; i++ {
                    if i == 11 {
                        break
                    }
                    let subString = i - 5
                    
                    firstLengthPasNo  = fullString.substringWithRange(NSRange(location: 0, length: 5))
                    lastLengthPasNo   = fullString.substringWithRange(NSRange(location: 5, length: subString))
                }

//                if newLength >= 7 {
//                    firstLengthPasNo  = fullString.substringWithRange(NSRange(location: 0, length: 5))
//                    lastLengthPasNo   = fullString.substringWithRange(NSRange(location: 5, length: 2))
//                }
                
//                if newLength >= 10 {
//                    firstLengthPasNo  = fullString.substringWithRange(NSRange(location: 0, length: 5))
//                    lastLengthPasNo   = fullString.substringWithRange(NSRange(location: 5, length: 5))
//                }
                
                return newLength <= 10
                
            }
        }

        return true
    }
    
    // MARK: - Text Feild Animation Up
    // -------------------------------------------------------------------------------
    // TextFieldMoveUp
    // -------------------------------------------------------------------------------
    func TextFieldMoveUp(move:Bool , txtfield:UITextField)         {
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDelegate(self)
        UIView.setAnimationDuration(0.2)
        UIView.setAnimationBeginsFromCurrentState(true)
        var rect = self.view.frame
        if(move){

            if DeviceType.IS_IPHONE_4_OR_LESS{
                rect.origin.y -= 200.0
            }
        
            if DeviceType.IS_IPHONE_5{
                rect.origin.y -= 180.0
            }
            
            if DeviceType.IS_IPHONE_6{
                rect.origin.y -= 80.0
            }
            
            if DeviceType.IS_IPHONE_6P{
                rect.origin.y -= 30.0
            }

        }else{
            if DeviceType.IS_IPHONE_4_OR_LESS{
                rect.origin.y += 200.0
            }
            
            if DeviceType.IS_IPHONE_5{
                rect.origin.y += 180.0
            }
            
            if DeviceType.IS_IPHONE_6{
                rect.origin.y += 80.0
            }
            
            if DeviceType.IS_IPHONE_6P{
                rect.origin.y += 30.0
            }
        }
        
        self.view.frame=rect
        UIView.commitAnimations()
    }
    
    
    // -------------------------------------------------------------------------------
    // dismissKeyboard
    // -------------------------------------------------------------------------------
    func dismissKeyboard()    {
        phoneNoTextField.resignFirstResponder()
        passNoTextField.resignFirstResponder()
        proCodeTextField.resignFirstResponder()
        expireDateTextField.resignFirstResponder()
        emailTextField.resignFirstResponder()
    }
    
    // MARK: - Navigation
    // -------------------------------------------------------------------------------
    // In a storyboard-based application, you will often want to do a little
    // preparation before navigation
    // -------------------------------------------------------------------------------
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if segue.identifier == "ShowCompleted" {
            
            if sender == nil {
                return
            }
            
            let signedup = segue.destinationViewController as! SignedupViewController
            signedup.userInfo = sender as? NSDictionary
        }
        
    }

}
