//
//  SignedupViewController.swift
//  GlobalExpress
//
//  Created by UDAM on 1/25/16.
//  Copyright © 2016 webcash. All rights reserved.
//

import UIKit

class SignedupViewController: UIViewController {

    //MARK: - IBOutlet
    // -------------------------------------------------------------------------------
    // IBOutlet UIView
    // -------------------------------------------------------------------------------
    @IBOutlet weak var stepView: UIView!
    
    // -------------------------------------------------------------------------------
    // IBOutlet UILabel
    // -------------------------------------------------------------------------------
    @IBOutlet weak var topTitleLabel    : UILabel!
    @IBOutlet weak var stepTitleLabel   : UILabel!
    @IBOutlet weak var descriptionLabel : UILabel!
    @IBOutlet weak var nextButton       : UIButton!
    
    // -------------------------------------------------------------------------------
    // IBOutlet NSLayoutConstraint
    // -------------------------------------------------------------------------------
    @IBOutlet weak var splitRightConstraint     : NSLayoutConstraint!
    @IBOutlet weak var splitLeftConstraint      : NSLayoutConstraint!
    @IBOutlet weak var heightCenterViewConstraint: NSLayoutConstraint!
    @IBOutlet weak var heightImageViewConstraint: NSLayoutConstraint!
    
    // MARK: - Propertie
    // -------------------------------------------------------------------------------
    // NSDictionary
    // -------------------------------------------------------------------------------
    var userInfo  : NSDictionary?
    
    //MARK: - Private function
    // -------------------------------------------------------------------------------
    // Localize
    // -------------------------------------------------------------------------------
    override func localize() {
        
        self.topTitleLabel.text     = Language.localizedStr("M04_01")
        self.stepTitleLabel.text    = "1. \(Language.localizedStr("M01_01"))"
        self.descriptionLabel.text  = Language.localizedStr("M04_02")
        self.nextButton.setTitle(Language.localizedStr("M03_2_15"), forState: .Normal)
        
    }
    
    // -------------------------------------------------------------------------------
    // Customize View Layout
    // -------------------------------------------------------------------------------
    func customizeViewLayout() {
        
        self.navigationController?.navigationBarHidden=true
        
        stepView.layer.cornerRadius = 5.0
        
//        if UIDevice().modelName == "iPhone 4" || UIDevice().modelName == "iPhone 4s"{
//            splitRightConstraint.constant       = 58 + 5
//            splitLeftConstraint.constant        = 167 + 4
//        }
//        
//        if UIDevice().modelName == "iPhone 6" {
//            splitRightConstraint.constant       = 58 + 7
//            splitLeftConstraint.constant        = 167 + 26
//        }
//        
//        if UIDevice().modelName == "iPhone 6 Plus" {
//            splitRightConstraint.constant       = 58 + 34
//            splitLeftConstraint.constant        = 167 + 18
//        }

        if DeviceType.IS_IPHONE_4_OR_LESS {
            splitRightConstraint.constant       = 58 - 3
            splitLeftConstraint.constant        = 167 + 4
            heightCenterViewConstraint.constant = 44 - 30
            heightImageViewConstraint.constant  = 100 - 20
        }
        
        if DeviceType.IS_IPHONE_6 {
            splitRightConstraint.constant       = 58
            splitLeftConstraint.constant        = 167 + 26
        }
        
        if DeviceType.IS_IPHONE_6P {
            splitRightConstraint.constant       = 58 + 34
            splitLeftConstraint.constant        = 167 + 18
        }
        
    }
    
    // MARK: - View Life Cycle
    // -------------------------------------------------------------------------------
    // ViewDidLoad
    // -------------------------------------------------------------------------------
    override func viewDidLoad() {
        super.viewDidLoad()

        //Localized
        self.localize()
        
        //Customize
        customizeViewLayout()
    }
    
    override func viewDidDisappear(animated: Bool) {
        self.navigationController?.navigationBarHidden=false
    }
    
    // MARK: - IBAction
    // -------------------------------------------------------------------------------
    // Click to view CompletedSignupViewController
    // -------------------------------------------------------------------------------
    @IBAction func nextClicked(sender: UIButton) {
        
        /////////// USE THIS FORMAT
        let transaction = TransactionClass()
        transaction.sendAPI(APICode.INIT_R001, argument: ["" : ""],
            success: { (success) -> Void in
                
                if let rslt_cd = success["RSLT_CD"] as? String {
                    
                    if rslt_cd != "0000" { //오류
                        if let rsltMsg = success["RSLT_MSG"] as? String {
                            
                            print("Error Code : \(rslt_cd), Error Message : \(rsltMsg)")
                            self.showEventAlert("Error Message :\(rsltMsg)", hasCancelButton: false, delegate: self, tag: nil, confirmBlock: nil, cancelBlock: nil)
                            
                        }
                    } else { //정상
                        if let respData = success["RESP_DATA"] as? NSDictionary {
//                            let mainStoryboard      : UIStoryboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
//                            let homeViewController  : UIViewController = mainStoryboard.instantiateViewControllerWithIdentifier("KYDrawerController")
//                            
//                            self.navigationController?.pushViewController(homeViewController, animated: true)
                            self.dismissViewControllerAnimated(true, completion: nil)
                        }

                    }
                }
        })

//        let mainStoryboard      : UIStoryboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
//        let homeViewController  : UIViewController = mainStoryboard.instantiateViewControllerWithIdentifier("KYDrawerController")
//
//        self.navigationController?.pushViewController(homeViewController, animated: true)
        
//        self.performSegueWithIdentifier("ShowSecondSignedup", sender: nil)
    }

}
