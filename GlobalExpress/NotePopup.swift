//
//  NotePopup.swift
//  GlobalExpress
//
//  Created by UDAM on 3/14/16.
//  Copyright © 2016 webcash. All rights reserved.
//

import UIKit

class NotePopup: MJPopupViewController {

    // MARK: - IBOutlet
    // -------------------------------------------------------------------------------
    //	IBOutlet Label, Button
    // -------------------------------------------------------------------------------
    @IBOutlet weak var noteTitleLabel: UILabel!
    @IBOutlet weak var noteDescriptionLabel: UILabel!
    
    // MARK: - IBAction
    // -------------------------------------------------------------------------------
    // Close popup when clicked
    // -------------------------------------------------------------------------------
    @IBAction func noteCloseButtonClicked(sender: UIButton) {
        dismissPopupViewController(self, animationType: MJPopupViewAnimationFade)
    }
    
    // MARK: - Private function
    // -------------------------------------------------------------------------------
    // Localize language
    // -------------------------------------------------------------------------------
    override func localize() {
        noteTitleLabel.text         = Language.localizedStr("M02P_01")
        noteDescriptionLabel.text   = Language.localizedStr("M02P_02")
    }
    
    // MARK: - View Life Cycle
    // -------------------------------------------------------------------------------
    // viewDidLoad
    // -------------------------------------------------------------------------------
    override func viewDidLoad() {
        super.viewDidLoad()

        //Localize
        localize()

    }

}
