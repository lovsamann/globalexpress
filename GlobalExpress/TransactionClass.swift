//
//  TransactionProtocol.swift
//  GlobalExpress
//
//  Created by JangWooil on 2016. 1. 5..
//  Copyright © 2016년 webcash. All rights reserved.
//

import Foundation

struct constantStruct {
    static let showWatingView : String = "ShowWaitingViewNotification"
    static let closeWatingView : String = "CloseWaitingViewNotification"
}

@objc class TransactionClass : NSObject {
    
    private var urlString: String!{
        didSet{
            LoadingView.show()
            showNetworkIndicator(true)
        }
    }
    
    private func showNetworkIndicator(isShowing: Bool){
        UIApplication.sharedApplication().networkActivityIndicatorVisible = isShowing
    }
    
    func showProgressBar(viewC : UIViewController){
        SessionManager.sharedSessionManager().showProgressBar(viewC);
//        NSNotificationCenter.defaultCenter().postNotificationName(constantStruct.showWatingView, object: self, userInfo: nil)
    }
    
    func closeProgressBar(viewC : UIViewController){
        SessionManager.sharedSessionManager().closeProgressBar(viewC);
//        NSNotificationCenter.defaultCenter().postNotificationName(constantStruct.closeWatingView, object: self, userInfo: nil)
    }
    
    //콘텐츠 전문을 호출한다.
    func sendAPI(apiName: String, argument: Dictionary<String, String>, success: NSDictionary -> Void) {
        
#if _DEBUG_
        let chnl_cd = "01"
        let ver_cd = SessionManager.sharedSessionManager().serverLanguageCd()
        let user_no = "146"
#else
        let chnl_cd = SessionManager.sharedSessionManager().chnl_cd
        let user_no = SessionManager.sharedSessionManager().user_no()
        let ver_cd = SessionManager.sharedSessionManager().serverLanguageCd()
#endif
//        let user_no = SessionManager.sharedSessionManager().user_no()
//        let ver_cd = SessionManager.sharedSessionManager().serverLanguageCd()
     
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyyMMddhhmmss"
        let now_time = dateFormatter.stringFromDate(NSDate())
        
        
        
        var params  = "{\"SVC_CD\":\"\(apiName)\",\"CHNL_CD\":\"\(chnl_cd)\",\"USER_NO\":\"\(user_no)\",\"VER_CD\":\"\(ver_cd)\",\"TR_TIME\":\"\(now_time)\",\"REQ_DATA\":\(self.makeURLString(argument, jsonYN: true))}"
        
        urlString = "\(SessionManager.sharedSessionManager().contentServerURL)"
//        print(urlString); //http://fxremit.wecambodia.com/gateway/gauus_api.jsp?
                          //http://192.168.178.83:20000/gateway/gauus_api.jsp?JSONData=
//        let urlString = _SM_GATEWAY_URL
//        urlString = _SM_GATEWAY_URL
        let encodedURLstr : String
        
        if #available(iOS 9, *) {
            params = params.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLHostAllowedCharacterSet())!
        } else {
            //IOS 9보다 하위 버전인 경우
            params = params.stringByAddingPercentEscapesUsingEncoding(NSUTF8StringEncoding)!
        }
//        encodedURLstr = "\(urlString)\(params)"
        encodedURLstr = "\(urlString)JSONData=\(params)"
        let all_url = NSURL(string: encodedURLstr)
        
        if let url_ok = all_url {
            self.sendTransaction(url_ok, apiCode: apiName, params: argument, method: "POST", success: success, decodeYN: false, mainThreadYn: true)
        } else {
            print("URL 오류")
        }
    }
    
    //MG 전문을 호출한다.
    func sendMGTransaction(success: NSDictionary -> Void) {
        var rsltDic : Dictionary<String, AnyObject> = ["c_minimum_ver":"", "c_update_act":"", "c_update_close":"",
                                                    "c_appstore_url":""]
        
        let inputDic : Dictionary<String, String> = Dictionary(dictionaryLiteral: ("master_id","I_GEXP_G_1"))
//        let url = "\(_SM_GATEWAY_URL)\(_SM_GATEWAY_PATH)?master_id=I_GEXP_G_1"
        let url = "\(_MG_GATE_URL)master_id=I_GEXP_G_1"
        
        if let urlData = NSURL(string: url) {
            self.sendTransaction(urlData, apiCode: nil, params: inputDic, method: "GET", success: { dictionary in
                print("MG \(dictionary)")
                if let rsltData = dictionary["RESP_DATA"] {
                    if let arrData = rsltData["_tran_res_data"] {
                        //프로그램 최소 버전 확인
                        if let rsltJson = arrData![0] {
                            if let minVer = rsltJson["c_minimum_ver"] {
                                rsltDic["c_minimum_ver"] = "\(minVer!)"
                            }
                            if let updateDesc = rsltJson["c_update_act"] { //업데이트 메시지
                                rsltDic["c_update_act"]  = "\(updateDesc!)"
                            }
                            if let appstore_url = rsltJson["c_appstore_url"] {
                                rsltDic["c_appstore_url"] = "\(appstore_url!)"
                            }
                            if let update_close = rsltJson["c_update_close"] {
                                rsltDic["c_update_close"] = self.numberToYn("\(update_close!)")
                            }
                            if let bizplay_url  = rsltJson["c_bizplay_url"] {
                                rsltDic["c_bizplay_url"]  = "\(bizplay_url!)"
                            }
                            if let act_yn = rsltJson["c_act_yn"] { //공지 메시지 관련
                                rsltDic["c_act_yn"] = self.numberToYn("\(act_yn!)")
                            }
                            if let act    = rsltJson["c_act"] {
                                rsltDic["c_act"] = "\(act!)"
                            }
                            if let act_close = rsltJson["c_act_close"] {
                                rsltDic["c_act_close"] = self.numberToYn("\(act_close!)")
                            }
                            if let session_time = rsltJson["c_session_time"] { //세션 타임아웃 시간
                                rsltDic["c_session_time"] = "\(session_time!)"
                            }
                            if let left_menu_info = rsltJson["_left_menu_info"] {
                                rsltDic["left_menu_info"] = left_menu_info!
                            }
                            
                            success(rsltDic)
                        }
                    }
                }
            }, decodeYN: true, mainThreadYn: true)
        }
        
        
        
    }
    
    //숫자 String 으로 된 값을 YN 으로 변환하여 반환한다
    func numberToYn(number: String) -> String {
        if(number == "0") {
            return "N"
        } else {
            return "Y"
        }
    }
    
    //API 요청을 보낸다
    func sendTransaction(url: NSURL, apiCode: String?, params: Dictionary<String, String>, var method: String?, success: NSDictionary -> Void, decodeYN: Bool, mainThreadYn: Bool){
        let requestURL = url //NSURL(string: url)

        let request = NSMutableURLRequest(URL: requestURL)
        print("request \(request)")
        
        let session = NSURLSession.sharedSession()
        if method == nil {
            method = "POST"
        } else {
            if(method != "POST" && method != "GET"){
                method = "POST"
            }
        }
        
        request.HTTPMethod = method!
        
        if method == "POST" {
            //파라미터 세팅
            var requestString = ""
            if apiCode?.isEmpty == false {
                requestString = self.makeURLString(params, jsonYN: true)
            } else {
                requestString = self.makeURLString(params, jsonYN: false)
            }
            
            if apiCode?.isEmpty == false {
                request.HTTPBody   = "JSONData={\"_tran_cd\":\"\(apiCode)\",\"req_data\":[\(requestString)]}".dataUsingEncoding(NSUTF8StringEncoding)
            } else {
                request.HTTPBody   = requestString.dataUsingEncoding(NSUTF8StringEncoding)
            }
            
             print("Input Value : \(requestString) and \(request)")
        }
        
       
        // 통신 후 처리
        let task = session.dataTaskWithRequest(request, completionHandler: { data, response, error in
            guard data != nil else {
                dispatch_async(dispatch_get_main_queue(), {
                    LoadingView.hide()
                })
                print("no data found: \(error)")
                return
            }
            
            do {
                let jsonStr = NSString(data: data!, encoding: NSUTF8StringEncoding)
                var okState = false
                let decodedStr: String?
                
                //반환된 결과값에 문자열 decode 가 필요한 경우
                if decodeYN == true {
                    decodedStr = jsonStr?.stringByReplacingPercentEscapesUsingEncoding(NSUTF8StringEncoding)
                } else {
                    decodedStr = "\(jsonStr!)"
                }
                
                //디코딩된 문자열
                if decodedStr != nil {
                    if let decodedData = decodedStr!.dataUsingEncoding(NSUTF8StringEncoding) {
                        if let json = try NSJSONSerialization.JSONObjectWithData(decodedData, options: []) as? NSDictionary {
                            okState = true
                            if(mainThreadYn == true){
                                dispatch_async(dispatch_get_main_queue(), {
                                    success(json)
                                    LoadingView.hide()
                                })
                            } else {
                                success(json)
                                LoadingView.hide()
                            }
                        }
                    }
                }
                
                if okState == false {
                    //JSON 파싱 오류
                    let jsonStr = NSString(data: data!, encoding: NSUTF8StringEncoding)
                    print("Error could not parse JSON: \(jsonStr)")
                    LoadingView.hide()
                }
            } catch let parseError {
                //JSON 파싱 오류
                print(parseError)
                let jsonStr = NSString(data: data!, encoding: NSUTF8StringEncoding)
                print("Error could not parse JSON: \(jsonStr)")
                self.showNetworkIndicator(false)
                LoadingView.hide()
            }
            
            //dismiss network indicator
            self.showNetworkIndicator(false)
//            LoadingView.hide()
        })
        
        task.resume()
        
    }
    
    func makeURLString(input : Dictionary<String, String>, jsonYN : Bool) -> String {
        var URLString = ""
        if jsonYN == true {
            URLString = "{"
            for (key, value) in input {
                URLString += "\"\(key)\":\"\(value)\","
            }
            let index1 = URLString.endIndex.advancedBy(-1)
            URLString = URLString.substringToIndex(index1) //끝자리 제거
            URLString += "}"
        } else {
            for (key, value) in input {
                URLString += "\(key)=\(value)&"
            }
            let index1 = URLString.endIndex.advancedBy(-1)
            URLString = URLString.substringToIndex(index1) //끝자리 제거
        }
        
        return URLString
    }
}