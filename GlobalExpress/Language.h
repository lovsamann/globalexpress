//
//  Language.h
//  GlobalExpress
//
//  Created by JangWooil on 2016. 1. 14..
//  Copyright © 2016년 webcash. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SessionManager.h"

@interface Language : NSObject

+(NSString *)localizedStr: (NSString *)key;

@end
