//
//  ARSViewController.swift
//  GlobalExpress
//
//  Created by JangWooil on 2016. 2. 11..
//  Copyright © 2016년 webcash. All rights reserved.
//

import Foundation
import UIKit

//ARS의 성공, 실패에 따라 할 처리를 protocol로 지정한다
protocol ARSViewControllerDelegate {
    //ARS가 성공할 경우의 처리 - 거래
    func arsSuccess()
    //ARS가 실패할 경우의 처리 - 거래 종료
    func arsFailure()
}

class ARSViewController : UIViewController, UIAlertViewDelegate {
    
    @IBOutlet var descLabel1: UILabel!
    @IBOutlet var descLabel2: UILabel!
    @IBOutlet var payLabel: UILabel!
    @IBOutlet var ArsDescLabel: UILabel!
    @IBOutlet var arsDescLabel2: UILabel!
    
    @IBOutlet var statusBar: UIView!
    
    //Input 값 --- 전달되어야 함
    var inq_txno : String = ""
    var cust_nm  : String = ""
    var bank_cd  : String = ""
    var acct_no  : String = ""
    var timer : NSTimer?
    var selectedLang = ""
    var delegate : ARSViewControllerDelegate? = nil
    var statusBarHidden = false //statusBar를 보이고 싶지 않은 경우 true로 변경
    
    @IBOutlet var progressBar: UIView!
    @IBOutlet var englishRadio: UIButton!
    @IBOutlet var koreanRadio: UIButton!
    @IBOutlet var camboRadio: UIButton!
    @IBOutlet var englishLabel: UILabel!
    @IBOutlet var koreanLabel: UILabel!
    @IBOutlet var camboLabel: UILabel!
    @IBOutlet var arsConnectBtn: UIButton!
    
    var englishAvailable = true // ARS 지원여부
    var koreanAvailable  = true
    var camboAvailable   = true
    
    //언어 라디오 처리 start
    @IBAction func setEnglish(sender: AnyObject) {
        setEngRadio()
    }
    
    @IBAction func setKorean(sender: AnyObject) {
        setKorRadio()
    }
    
    @IBAction func setCambo(sender: AnyObject) {
        setCamboRadio()
    }
    
    func setEngRadio() {
        if(englishAvailable == false){
            return
        }
        
        englishRadio.selected = true
        koreanRadio.selected  = false
        camboRadio.selected   = false
        
        selectedLang = ServerLangCode().English
    }
    
    func setKorRadio() {
        if(koreanAvailable == false){
            return
        }
        
        englishRadio.selected = false
        koreanRadio.selected  = true
        camboRadio.selected   = false
        
        selectedLang = ServerLangCode().Korea
    }
    
    func setCamboRadio() {
        if(camboAvailable == false){
            return
        }
        
        englishRadio.selected = false
        koreanRadio.selected  = false
        camboRadio.selected   = true
        
        selectedLang = ServerLangCode().Cambodia
    }
    //언어 라디오 처리 end
    
    struct ARSAlertTag {
        static let arsErrorAlert = 9999
        static let noResponseAlert = 8888
    }
    
    //다국어 처리
    override func localize() {
        self.title = Language.localizedStr("T09_01")
        self.descLabel1.text = Language.localizedStr("T09_02")
        self.descLabel2.text = Language.localizedStr("T09_03")
        self.ArsDescLabel.text = Language.localizedStr("C04P_01")
        self.payLabel.text = "4. \(Language.localizedStr("M01_06"))"
        self.arsDescLabel2.text = Language.localizedStr("T09_06")
        
        //언어 세팅
        englishLabel.text = Language.localizedStr("T09_07") //영어
        koreanLabel.text  = Language.localizedStr("T09_08") //한국어
        camboLabel.text   = Language.localizedStr("T09_09") //크메르어
        
        arsConnectBtn.setTitle(Language.localizedStr("T09_04"), forState: .Normal)
    }
    
    override func viewDidLoad() {
        self.localize()
        self.cf_setBackButton()
        
        //지원언어 조회
        self.getLanguage()
        
        //
        //라디오버튼 Selected 되었을 때의 이미지 설정
        koreanRadio.setBackgroundImage(UIImage(named: "on_radio_btn"), forState: .Selected)
        englishRadio.setBackgroundImage(UIImage(named: "on_radio_btn"), forState: .Selected)
        camboRadio.setBackgroundImage(UIImage(named: "on_radio_btn"), forState: .Selected)
        
        //Label 클릭 시 라디오 선택되게 처리
        let engLabelGesture = UITapGestureRecognizer(target: self, action: "setEngRadio")
        let korLabelGesture = UITapGestureRecognizer(target: self, action: "setKorRadio")
        let camLabelGesture = UITapGestureRecognizer(target: self, action: "setCamboRadio")
        
        self.englishLabel.addGestureRecognizer(engLabelGesture)
        self.koreanLabel.addGestureRecognizer(korLabelGesture)
        self.camboLabel.addGestureRecognizer(camLabelGesture)
        
        //Back button 세팅
        self.cf_setBackButton()
        
        //Description bold
        self.descLabel1.font = UIFont.boldSystemFontOfSize(16.0)
        
        self.statusBar.hidden = self.statusBarHidden

    }
    
    func rgb(r:Float, g:Float, b: Float) -> UIColor{
        return UIColor(colorLiteralRed: r/255.0, green: g/255.0, blue: b/255.0, alpha: 1)
    }
    
    func getLanguage(){
        
        let inputDic : Dictionary<String, String> = ["":""]
        
        TransactionClass().sendAPI("frt_vercd_r001", argument: inputDic, success: {
            (resultDic : NSDictionary) in
            if let rslt_cd = resultDic["RSLT_CD"] as? String {
                if rslt_cd != "0000" { //오류
                    if let rsltMsg = resultDic["RSLT_MSG"] as? String {
                        print("Error Code : \(rslt_cd), Error Message : \(rsltMsg)")
                    }
                } else { //정상
                    if let respData = resultDic["RESP_DATA"] as? NSDictionary {
                        if let verRec = respData["VER_REC"] as? Array<Dictionary<String, String>> {
                            //ARS 결과조회
                            print("ARS Record :::: \(verRec)")
                            for arsDic : Dictionary in verRec {
                                if let arsSts = arsDic["ARS_STS"], verCd = arsDic["VER_CD"] {
                                    if(arsSts == "0"){ //미지원
                                        if(verCd == "01"){
                                            self.koreanAvailable = false
                                            self.koreanLabel.textColor = self.rgb(168, g: 168, b: 170)
                                        } else if(verCd == "02"){
                                            self.englishAvailable = false
                                            self.englishLabel.textColor = self.rgb(168, g: 168, b: 170)
                                        } else if(verCd == "03"){
                                            self.camboAvailable   = false
                                            self.camboLabel.textColor = self.rgb(168, g: 168, b: 170)
                                        }
                                    }
                                }
                            }
                            
                            //사용자 언어 확인
                            if let userSavedLanguage = NSUserDefaults.standardUserDefaults().objectForKey("UserSavedLanguage") as? String {
                                if(userSavedLanguage == LanguageCode().Korea){
                                    self.setKorRadio()
                                } else if(userSavedLanguage == LanguageCode().Cambodia){
                                    self.setCamboRadio()
                                } else {
                                    self.setEngRadio()
                                }
                            } else {
                                self.setEngRadio()
                            }
                            //
                        }
                    }
                }
            }
        })
    }
    
    //ARS 연결요청
    @IBAction func requestARS(sender: AnyObject) {
        if(selectedLang == ""){
            return
        }
        
        //        TransactionClass().showProgressBar(self)
        progressBar.hidden = false
        
        //ARS 지원언어 항목 추가 - 20150311
        let inputDic : Dictionary<String, String> = ["INQ_TXNO":inq_txno, "CUST_NM":cust_nm, "BANK_CD":bank_cd, "ACCT_NO":acct_no, "ARS_VERCD":selectedLang]
        
        TransactionClass().sendAPI("frt_ars_c001", argument: inputDic, success: {
            (resultDic : NSDictionary) in
            if let rslt_cd = resultDic["RSLT_CD"] as? String {
                if rslt_cd != "0000" { //오류
                    if let rsltMsg = resultDic["RSLT_MSG"] as? String {
                        print("Error Code : \(rslt_cd), Error Message : \(rsltMsg)")
                    }
                } else { //정상
                    if let respData = resultDic["RESP_DATA"] as? NSDictionary {
                        if let certSeq = respData["CERT_SEQ"] as? String {
                            //ARS 결과조회
                            self.getARSResult(certSeq)
                        }
                    }
                }
            }
            self.progressBar.hidden = true
            //            TransactionClass().closeProgressBar(self)
        })
    }
    
    func delay(delay: Double, closure:()->()) {
        dispatch_after(
            dispatch_time(
                DISPATCH_TIME_NOW,
                Int64(delay * Double(NSEC_PER_SEC))
            ),
            dispatch_get_main_queue(), closure)
    }
    
    //ARS 결과조회
    func getARSResult(certSeq : String) {
        let inputDic : Dictionary<String, String> = ["CERT_SEQ":certSeq]
        
        TransactionClass().sendAPI("frt_ars_r001", argument: inputDic, success: {
            (resultDic : NSDictionary) in
            if let rslt_cd = resultDic["RSLT_CD"] as? String {
                if rslt_cd != "0000" { //오류
                    if let rsltMsg = resultDic["RSLT_MSG"] as? String {
                        print("Error Code : \(rslt_cd), Error Message : \(rsltMsg)")
                    }
                } else { //정상
                    if let respData = resultDic["RESP_DATA"] as? NSDictionary {
                        if let certSeq = respData["TX_STS"] as? String { //거래상태
                            if certSeq == "1" { //인증정상
                                // TODO: 출금계좌등록 또는 출금계좌변경 거래 진행 처리를 작성한다.
                                self.delegate?.arsSuccess()
                                //self.navigationController?.popViewControllerAnimated(true)
                            } else if certSeq == "2" { //인증오류
                                if let rsltMsg = resultDic["RSLT_MSG"] as? String {
                                    //결과메시지 출력 후 거래종료
                                    self.showEventAlert(rsltMsg, hasCancelButton: false, delegate: self, tag: ARSAlertTag.arsErrorAlert, confirmBlock: {
                                        _ in
                                        // TODO: 거래를 종료하는 처리를 작성한다.
                                        self.quitTransaction()
                                        }, cancelBlock: nil)
                                }
                            } else if certSeq == "3" { //처리중
                                if let rpt_gb = respData["RPT_GB"] as? String { //조회반복구분
                                    if rpt_gb == "0" { //반복조회
                                        //5초후 반복조회
                                        self.delay(5.0, closure: {
                                            self.getARSResult(certSeq)
                                        })
                                    } else if rpt_gb == "1" { //조회종료
                                        //ARS 무응답 오류처리 후 거래종료
                                        let rsltMsg = Language.localizedStr("T09_05")
                                        self.showEventAlert(rsltMsg, hasCancelButton: false, delegate: self, tag: ARSAlertTag.arsErrorAlert, confirmBlock: {
                                            _ in
                                            // TODO: 거래를 종료하는 처리를 작성한다.
                                            self.quitTransaction()
                                            }, cancelBlock: nil)
                                        
                                    }
                                }
                            }
                        }
                    }
                }
            }
        })
    }
    
    //거래를 종료한다.
    func quitTransaction() {
        self.navigationController?.popViewControllerAnimated(true)
        self.delegate?.arsFailure()
    }
    
    func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int) {
        if(buttonIndex == 0){
            if(alertView.tag == ARSAlertTag.arsErrorAlert){
                // TODO: 거래를 종료하는 처리를 작성한다.
                self.quitTransaction()
            }
        } else if(buttonIndex == 1){
            
        }
    }
    
}