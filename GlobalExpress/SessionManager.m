//
//  SessionManager.m
//  GlobalExpress
//
//  Created by JangWooil on 2015. 12. 29..
//  Copyright © 2015년 webcash. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SessionManager.h"
#import "GlobalExpress-Swift.h"

@implementation SessionManager

static SessionManager *sessionMgr = nil;


+ (SessionManager *)sharedSessionManager {
    if (sessionMgr == nil){
        sessionMgr = [[SessionManager alloc] init];
        
        
        //ProgressView값을 초기세팅한다
        ProgressView *progressBar = [[ProgressView alloc] init];
        progressBar.tag           = progressViewTag;
        sessionMgr.progressBarDic = [NSDictionary dictionaryWithObjectsAndKeys:progressBar, @"progress", nil];
    }
        
        
        return sessionMgr;
}

- (NSString *)signupYn
{
    //회원가입여부를 반환한다
    NSString *strSignUpYN = [[NSUserDefaults standardUserDefaults] objectForKey:@"SIGNUP_YN"];
    if (strSignUpYN == nil || [strSignUpYN isEqualToString:@"Y"] == NO) {
        strSignUpYN = @"N";
    }
    
    return strSignUpYN;
}

- (void)showProgressBar : (UIViewController *)viewC
{
    ProgressView *progressBar = [sessionMgr.progressBarDic objectForKey:@"progress"];
    
    [viewC.view addSubview:progressBar];
    [progressBar show];
    viewC.view.userInteractionEnabled = NO;
}

- (void)closeProgressBar : (UIViewController *)viewC
{
    ProgressView *progressBar = [sessionMgr.progressBarDic objectForKey:@"progress"];
    
    if (progressBar == nil) {
        return;
    }
    
    [progressBar close];
    UIView *view = [viewC.view viewWithTag:progressViewTag];
    
    if (view != nil) {
        [view removeFromSuperview];
    }
    
    viewC.view.userInteractionEnabled = YES;
}

- (NSString *)serverLanguageCd
{
    NSString *serverLangCd = @"";
    NSString *clientLangCd    = sessionMgr.language;
    
    if ([clientLangCd isEqualToString:kCambodiaCode]) { // 캄보디아어
        serverLangCd = @"03";
    } else if([clientLangCd isEqualToString:kEnglishCode]){ //영어
        serverLangCd = @"02";
    } else if([clientLangCd isEqualToString:kKoreaCode]) { //한국어
        serverLangCd = @"01";
    }
    
    return serverLangCd;
}

- (NSString *)user_no{
    NSString *userNo = [[NSUserDefaults standardUserDefaults] objectForKey:@"USER_NO"];
    
    if (userNo == nil) {
        userNo = @"";
    }
    
    return userNo;
}

//- (NSString *)user_nm{
//    NSString *userNm = [[NSUserDefaults standardUserDefaults] objectForKey:@"USER_NM"];
//    
//    if (userNm == nil) {
//        userNm = @"";
//    }
//    
//    return userNm;
//}

@end