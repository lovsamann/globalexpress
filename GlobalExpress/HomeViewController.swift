//
//  HomeViewController.swift
//  GlobalExpress
//
//  Created by JangWooil on 2015. 12. 29..
//  Copyright © 2015년 webcash. All rights reserved.
//

import UIKit

extension String{
    
    func formatUSDCurrency() -> String!{
            let number = NSDecimalNumber(string: self)
            
            let formatter = NSNumberFormatter()
        
                formatter.numberStyle = .CurrencyStyle
                formatter.locale = NSLocale(localeIdentifier: "en_US")
                formatter.currencySymbol = ""
        
            let newString = formatter.stringFromNumber(number)
            print("format text new \(newString)")
            return newString!
    }
    
    func formatKRWCurrency() -> String!{
        let currencyFormatter = NSNumberFormatter()
        currencyFormatter.numberStyle = .CurrencyStyle
        currencyFormatter.currencyCode = "KRW"
        currencyFormatter.negativeFormat = "-¤#,##0.00"
        let currencyStr = currencyFormatter.stringFromNumber(Int(self)!)!
        let str:NSString = currencyStr
        return str.substringWithRange(NSRange(location: 1, length: (currencyStr.characters.count - 1)))
    }
    
}
//MARK: - Constants
private let MAX_LENGTH = 9


struct APICode {
    struct RECIPIENT {
        static let RECIPIENT_R001 = "frt_recipient_r001"
        static let RECIPIENT_ROO2 = "frt_recipient_r002"
        static let RECIPIENT_D001 =  "frt_recipient_d001"
    }
    
    static let CALA_R001 = "frt_calc_r001"
    static let CURRENCY_R001 = "frt_currency_r001"
    static let CODE_R001 = "frt_code_r001"
    static let RCPBANK_R001 = "frt_rcpbank_r001"
    static let INQUIRY_R001 = "frt_inquiry_r001"
    static let INQUIRY_C001 = "frt_inquiry_c001"
    static let RCPACCT_C001 = "frt_rcpacct_c001"
    static let RCPINFO_COO1 = "frt_rcpinfo_c001"
    static let CPORG_R001 = "frt_cporg_r001"
    static let TRSCHK_R001 = "frt_trschk_r001"
    static let TERMS_R001 = "frt_terms_r001"
    static let HPCERT_R001 = "frt_hpcert_r001"
    static let HPCERT_C001 = "frt_hpcert_c001"
    static let ACTING_ROO1 = "frt_actinq_r001"
    static let TRANSFER_C001 = "frt_transfer_c001"
    static let ACTCERT_R001     = "frt_actcert_r001"
    static let USER_C001        = "frt_user_c001"
    static let INIT_R001        = "frt_init_r001"
    static let HIST_R001        = "frt_hist_r001"
    static let PSWD_C001        = "frt_pswd_c001"
    static let HIST_R002        = "frt_hist_r002"
    static let PROFILE_R001     = "frt_profile_r001"
    static let PROFILE_R002     = "frt_profile_r002"
    static let PROFILE_U001     = "frt_profile_u001"
    static let RECOMMEND_C001   = "frt_rcmd_c001"
    static let REGISTER_TRASCATION_C001 = "frt_regs_c001"
    static let REGISTER_CASH_PICK_C001 = "frt_cpregs_c001"
    static let SVC_D001         = "frt_svc_d001"
    static let PSWDCHK_R001     = "frt_pswdchk_r001"
    static let PSWD_U001        = "frt_pswd_u001"
    static let PSWD_INTL_U001   = "frt_pswdintl_u001"
    static let HISTORY_D001     = "frt_hist_d001"
}

struct StoryboardName {
    static let SIGNUP = "Signup"
    static let RECIPIENT = "Recipient"
    static let INTROSCREEN = "IntroScreen"
    struct Segue {
        static let NATIONALITY_SEGUE = "nationalitySegue"
    }

}

public let USEREXIST = "USER_NM"

class HomeViewController: UIViewController, UIAlertViewDelegate,UITextFieldDelegate,CountryViewControllerDelegate {
    
    
    //Outlet
    //labels
    @IBOutlet var centerText: UILabel!
    @IBOutlet var countryLabel: UILabel!
    @IBOutlet var sendingLabel: UILabel!
    @IBOutlet var recevingLabel: UILabel!
    @IBOutlet var exchangeRateLabel: UILabel!
    @IBOutlet var sendingFeeLabel: UILabel!
    @IBOutlet var couponLabel: UILabel!
    @IBOutlet var descriptionLabel: UILabel!
    @IBOutlet var secondDescriptionLabel: UILabel!
    @IBOutlet var currencyLabel: UILabel!
    @IBOutlet var exchangeRateValuelabel: UILabel!
    @IBOutlet var sendingFeeValueLabel: UILabel!
    @IBOutlet var couponValueLabel: UILabel!
    
    //buttons
    @IBOutlet var countryButton: UIButton!
    @IBOutlet var sendMoney: UIButton!
    @IBOutlet var currencyTypeButton: UIButton!
    @IBOutlet var couponButton: UIButton!
    
    @IBOutlet var remittanceButton: UIButton!
    //textFields
    @IBOutlet var senderTextField: UITextField!
    @IBOutlet var receiverTextField: UITextField!
    
    //imageview
    @IBOutlet var currencyImageView: UIImageView!

    
    //constraints
    @IBOutlet var hieghtConstraints: NSLayoutConstraint!
    @IBOutlet var topConstraints: NSLayoutConstraint!

    

    
    var nationalityList: [Nationality]?{
        didSet{
            if nationalityList != nil {
                selectedNationality = nationalityList?.first
            }
        }
    }
    
    var currencies: [Currency]?{
        didSet {
            if currencies != nil && currencies?.count > 0 {
                selectedCurrency = currencies?.first
                self.currencyLabel.text = selectedCurrency?.name
                let imageName = selectedNationality?.image
                print("vvvvv \(imageName!)")
                self.currencyImageView.image = UIImage(named: imageName!)
            }
        }
    }
    
    var selectedNationality: Nationality?{
        didSet{
            if selectedNationality != nil {
                    fetchCurrency(selectedNationality!)
                    updateUI((selectedNationality?.name)!)
                }
        }
    }
//    }
    
//        didSet{
//            if selectedNationality != nil {
//                if selectedNationality?.name != oldValue?.name {
//                    print("selectedNationality \(selectedNationality?.name)")
//                    fetchCurrency(selectedNationality!)
//                    updateUI((selectedNationality?.name)!) // default
//                }
//            }
//        }
//    }
    
    var selectedCurrency: Currency?{
        didSet{
            if selectedCurrency != nil {
                print("vaaaa \(selectedNationality?.name)")
                print("whahah \(selectedCurrency?.name)")
                self.currencyImageView.image = UIImage(named: (selectedNationality?.image)!)
                self.currencyLabel.text = selectedCurrency?.name
            }
        }
    }
    
    var transcation: Transaction?
    
    var isPush:Bool = false
    var webViewURL: String = ""
    var webViewTitle : String = ""
    var couponCount: String! {
        didSet {
            self.couponButton.hidden = couponCount == nil ? true : Int(couponCount) > 0 ? false: true
            
            let getLocalized = Language.localizedStr("S05_07")
            let splitLocalized = getLocalized.characters.split { $0 == "0" }.map(String.init)
            let quantity = splitLocalized.last
            
            let attrString = NSMutableAttributedString(string: couponCount == nil ? "0 " + quantity! : "\(couponCount) " + quantity!)
            let color = UIColor(red: 242 / 255.0, green: 78 / 255.0, blue: 78 / 255.0, alpha: 1.0)
            print("5555 \(couponCount)")
            attrString.addAttribute(NSForegroundColorAttributeName, value: color, range: NSMakeRange(0, (quantity?.characters.count)!))
            self.couponValueLabel.attributedText = attrString
        }
    }
    
//    var userExist = NSUserDefaults.standardUserDefaults().valueForKey(USEREXIST) as? String {
//        didSet{
//            guard let uExist = userExist else{
//                print("uExist nil")
//                return
//            }
//            
//            let couponWithName = Language.localizedStr("S05_07").stringByReplacingOccurrencesOfString("@@", withString: uExist)
//            let splitName = couponWithName.characters.split { $0 == "0" }.map(String.init)
//            let freeCouponName = splitName.first
//            
//            let attrString = NSMutableAttributedString(string: freeCouponName == nil ? "" : freeCouponName!)
//            let color = UIColor(red: 36 / 255.0, green: 74 / 255.0, blue: 99 / 255.0, alpha: 1.0)
//            attrString.addAttribute(NSForegroundColorAttributeName, value: color, range: NSMakeRange(0,uExist.characters.count))
//            attrString.addAttribute(NSFontAttributeName, value: UIFont.boldSystemFontOfSize(11.5), range: NSMakeRange(0, uExist.characters.count))
//            
//            self.couponLabel.attributedText = attrString
//        }
//    }
    
    var userName: String! {
        didSet {
            guard let uName = userName else{
                print("uName nil")
                return
            }
            
            let couponWithName = Language.localizedStr("S05_07").stringByReplacingOccurrencesOfString("@@", withString: uName)
            let splitName = couponWithName.characters.split { $0 == "0" }.map(String.init)
            let freeCouponName = splitName.first
            
            let attrString = NSMutableAttributedString(string: freeCouponName == nil ? "" : freeCouponName!)
            let color = UIColor(red: 36 / 255.0, green: 74 / 255.0, blue: 99 / 255.0, alpha: 1.0)
            attrString.addAttribute(NSForegroundColorAttributeName, value: color, range: NSMakeRange(0,uName.characters.count))
            attrString.addAttribute(NSFontAttributeName, value: UIFont.boldSystemFontOfSize(11.5), range: NSMakeRange(0, uName.characters.count))
//
            self.couponLabel.attributedText = attrString
        }
    }
    private var transactionClass = TransactionClass()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func localize() {
        self.countryLabel.text = Language.localizedStr("S05_01")
        self.sendMoney.setTitle(Language.localizedStr("S05_10"), forState: .Normal)
        self.sendingLabel.text = Language.localizedStr("S05_02")
        self.recevingLabel.text = Language.localizedStr("S05_03")
        self.exchangeRateLabel.text = Language.localizedStr("S05_04")
        self.sendingFeeLabel.text = Language.localizedStr("S05_05")
        self.descriptionLabel.text = Language.localizedStr("S05_08")
        self.secondDescriptionLabel.text = Language.localizedStr("S05_09")
        
        let freeCoupon = Language.localizedStr("S05_07")
        let splitFreeCoupon = freeCoupon.characters.split { $0 == "0" }.map(String.init)
//        self.couponLabel.text = splitFreeCoupon.first!.stringByReplacingOccurrencesOfString("@@", withString: self.userName == nil ? "" : self.userName)
        
        initFetch()
    }
    
    
    //MARK: - View life cycle
    override func viewDidLoad() {
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "NotiAPNS:", name: "NotiAPNS", object: nil) //push message
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "changeMainView:", name: "changeMainView", object: nil) //메인뷰 변경
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "showHomeViewAlert:", name: "showHomeViewAlert", object: nil) //홈 화면에서 Alert 메시지 띄우기
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "refreshHomeVC:", name: "refreshHomeVC", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "gotoSignup", name: "gotoSignup", object: nil)
        
//        self.view.alpha = CGFloat(0.7)
    
        self.senderTextField.rightView = doPaddingView()
        self.senderTextField.rightViewMode = .Always
        self.receiverTextField.rightView = doPaddingView()
        self.receiverTextField.rightViewMode = .Always
        
    }
    
    override func viewWillAppear(animated: Bool) {
        SessionManager.sharedSessionManager().mainYn = "Y" //세션에 메인화면여부를 저장한다. - 사이드바 표시 여부를 정함
        
        //localizing
        self.localize()
        
        
        remittanceButton.selected = false
        
        if remittanceButton.selected {
            setActiveButton(true)
        }else{
            setActiveButton(false)
        }
        
        self.view.endEditing(true)
        
    }
    
    override func viewWillDisappear(animated: Bool) {
        SessionManager.sharedSessionManager().mainYn = "N" //세션에 메인화면여부를 저장한다. - 사이드바 표시 여부를 정함
        self.view.endEditing(true)
    }
    
    
    //MARK: - UITextFieldDelegate
    func textFieldDidBeginEditing(textField: UITextField) {
        if senderTextField.text == "" && receiverTextField.text == "" {
            setActiveButton(false)
        }else{
            setActiveButton(true)
        }
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        
        let textLength = textField.text?.characters.count ?? 0
        if range.length + range.location > textLength {
            return false
        }
        
        let newLegth = textLength + string.characters.count - range.length
        let isMaxLength = textField.tag == 1 ? newLegth <= MAX_LENGTH : newLegth <= MAX_LENGTH - 1

        
        
        if range.length == 1 && string.length == 0 { //delete key is pressed
            return true
        }
        
        
        var upToDateText: String
        guard let text = textField.text else{ // grab text from UITextFields
            print("text of textField error")
            return false
        }
        
        if isMaxLength {
            let textNoStyle = text.stringByReplacingOccurrencesOfString(",", withString: "")   //filter thousand seperator to none style
            upToDateText = (textNoStyle as NSString).stringByReplacingCharactersInRange(NSMakeRange(textNoStyle.characters.count, 0), withString: string) // get up to date text
            
            
            if upToDateText == "" {
                print("uptodDateText nil")
            }
            
            if textField == senderTextField {
                textField.text = formatTextField(upToDateText, type: 1)
            }else{
                textField.text = upToDateText
            }
            
        }
        return false    // keep old text
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
        print("textFieldDidEndEditing \(textField == senderTextField ? "SenderTF" : "RecevicerTF" )")
        print("textfeild \(textField.text)")
        guard let text = textField.text else{
            setActiveButton(false)
            return
        }
        
        if text != "" && !text.isEmpty {
            print("sender \(self.transcation?.sendAmount) receving \(self.transcation?.receiveAmount)")
            if self.transcation != nil {
                if senderTextField.text == self.transcation?.sendAmount && receiverTextField.text == self.transcation?.receiveAmount {
                    return
                }
            }
            
            if senderTextField.text == "" && receiverTextField.text == "" {
                return
            }
            
            let senderOrRecevier = textField == senderTextField ? senderTextField.tag : receiverTextField.tag
            let reqText = text.stringByReplacingOccurrencesOfString(",", withString: "")
            let recevingText = formatTextField(reqText, type: 2).stringByReplacingOccurrencesOfString(",", withString: "")
            
            guard let nationality = selectedNationality else{
                return
            }
            
            guard let currency = selectedCurrency else{
                return
            }
            
            let calculateDic = ["CALC_GB" : "\(senderOrRecevier)",
                "SEND_AMT":reqText,
                "RECV_AMT": recevingText,
                "NATION_CD": nationality.code!,
                "CURRENCY_CD": currency.code!]
            
            print("calcuate Dic \(calculateDic)")
            
            transactionClass.sendAPI(APICode.CALA_R001, argument: calculateDic, success: { (success) -> Void in
                
                if let data = success["RSLT_CD"] where data as! String == "0000" { //everything is fine, let's update UI
                    if let transcationDic: NSDictionary = success["RESP_DATA"] as? NSDictionary { //save it to use later
                        self.transcation = Transaction(currency: self.selectedCurrency, nationality: self.selectedNationality, data: transcationDic)
                        print("transcation sending \(self.transcation?.sendAmount)")
                        self.updateUIAfterFetching(self.transcation!)
                    }
                }else{
                    self.showEventAlert(success["RSLT_MSG"] as! String, hasCancelButton: false, delegate: self, tag: 1973, confirmBlock: nil, cancelBlock: nil)
                    self.setActiveButton(false)
                }
                
                //dismission keyboard
                self.view.endEditing(true)
            })
        }else{
            self.senderTextField.text = ""
            self.receiverTextField.text = ""
        }

    }

    //MARK: - Action
    @IBAction func tapDismissKeyboard(sender: AnyObject) {
        self.view.endEditing(true)
    }
    
    
    @IBAction func currencyButtonClicked(sender: UIButton) {
//        if #available(iOS 8.0, *) {
//            let popOver = PopupController<Currency>(title: Language.localizedStr("C03P_01"), inView: self.view, preferredStyle: UIPopupControllerStyle.TableView)
//            popOver.delegate = self
//            popOver.data = self.currencies
//            presentViewController(popOver.navigationController!, animated: true, completion: nil)
//        } else {
//            // Fallback on earlier versions
//            
//        }
        
        
        guard let currencyList = currencies else{
            print("currency nil")
            return
        }
        
        
        let country: CountryViewController = CountryViewController(nibName: "CountryViewController", bundle: NSBundle.mainBundle())
        country.delegate = self
        country.countryArray = currencyList
        presentPopupViewController(country, animationType: MJPopupViewAnimationFade, contentInteraction: MJPopupViewContentInteractionDismissBackgroundOnly)
    }
    
    @IBAction func remittanceClicked(sender: AnyObject) {
        
        //Go to Signup Screen
        if NSUserDefaults.standardUserDefaults().objectForKey("SIGNUP_YN") == nil {
//            let signupStoryboard = UIStoryboard(name: "Signup", bundle: NSBundle.mainBundle())
//            let signupViewController = signupStoryboard.instantiateInitialViewController()
//            
//            guard let signupRootViewController = signupViewController else{
//                print("instanitateViewController nil. please check it out.")
//                return
//            }
//            
//            self.navigationController?.pushViewController(signupRootViewController, animated: true)
            gotoSignup()
        
        }else {
        
            if self.senderTextField.text == "" || self.receiverTextField.text == "" {
                return
            }
        

        
            let recipientStoryboard = UIStoryboard(name: StoryboardName.RECIPIENT, bundle: NSBundle.mainBundle())
            let recipientViewContoller = recipientStoryboard.instantiateViewControllerWithIdentifier("RecipientViewController") as? RecipientViewController
            
            
            guard let recipientRootViewController = recipientViewContoller else{
                print("instanitateViewController nil. RecipientVC.")
                return
            }
            
            
            transactionClass.sendAPI(APICode.RECIPIENT.RECIPIENT_R001, argument: ["" : ""] , success: { (success) -> Void in
                
                if let response = success["RSLT_CD"] as? String where response != "0000" {
                    self.showEventAlert(success["RSLT_MSG"] as! String, hasCancelButton: false, delegate: self, tag: 1923, confirmBlock: nil, cancelBlock: nil)
                    return
                }else{
                    guard let responseData = success["RESP_DATA"] else{
                        print("responseData nil.---->")
                        return
                    }
                    
                    print("vvvvv \(responseData)")
                    
                    guard let recipients: [AnyObject] = responseData["RECIPIENT_REC"] as? [AnyObject] else{
                        print("recipients nil.----->")
                        return
                    }
                    var recipientList = [Recipient]()
                    
                    for item in recipients {
                        guard let recipientDic = item as? [String: AnyObject] else{
                            print("Dictionary parse error.")
                            return
                        }
                        
                        let recipient = Recipient(data: recipientDic)
                        recipientList.append(recipient!)
                    }
                    print("HomeViewController - \(self.transcation)")
                    recipientRootViewController.recipients = recipientList
                    recipientRootViewController.transcation = self.transcation
                    self.navigationController?.pushViewController(recipientRootViewController, animated: true)
                }
            })
        }
    }
    
    @IBAction func popupNationality(sender: AnyObject) {
//        if #available(iOS 8.0, *) {
//            let popOver = PopupController<Nationality>(title: Language.localizedStr("C02P_01"), inView: self.view, preferredStyle: UIPopupControllerStyle.TableView)
//            popOver.delegate = self
//            popOver.data = self.nationalityList
//            presentViewController(popOver.navigationController!, animated: true, completion: nil)
//        } else {
//            // Fallback on earlier versions
//            let country: CountryViewController = CountryViewController(nibName: "CountryViewController", bundle: NSBundle.mainBundle())
//            country.countryArray = self.nationalityList! as [AnyObject]
//            presentPopupViewController(country, animationType: MJPopupViewAnimationFade, contentInteraction: MJPopupViewContentInteractionDismissBackgroundOnly)
//        }
        
        guard let nationalities = self.nationalityList else{
            print("s.th's wrong")
            return
        }
        
        let country: CountryViewController = CountryViewController(nibName: "CountryViewController", bundle: NSBundle.mainBundle())
        country.delegate = self
        country.countryArray = nationalities
        presentPopupViewController(country, animationType: MJPopupViewAnimationFade, contentInteraction: MJPopupViewContentInteractionDismissBackgroundOnly)
    }
    
    
    //MARK: - private methods
    func setActiveButton(isActive: Bool) {
        self.remittanceButton.enabled = isActive
        self.remittanceButton.selected = isActive
    }
    
    func clearText(){
        self.senderTextField.text = nil
        self.receiverTextField.text = nil
        self.exchangeRateValuelabel.text = nil
        self.sendingFeeValueLabel.text = nil
    }
    
    
    func hiddenViews(isHidden: Bool) {
        self.couponLabel.hidden = isHidden
        self.couponValueLabel.hidden = isHidden
    }
    
    func initFetch(){
        let sessionId : String
        let token : String
        let push_id : String
        
        if(SessionManager.sharedSessionManager().sessionId == nil){
            sessionId = "1"
        } else {
            sessionId = SessionManager.sharedSessionManager().sessionId
        }
        
        if(SessionManager.sharedSessionManager().token == nil){
            token = "1"
        } else {
            token = SessionManager.sharedSessionManager().token
        }
        
        if(NSUserDefaults.standardUserDefaults().objectForKey("_deviceToken") != nil){
            push_id = NSUserDefaults.standardUserDefaults().objectForKey("_deviceToken") as! String
        } else {
            push_id = "1"
        }
        
//        self.showSimpleAlert("Test pushID [\(push_id)] token[\(token)] sessionId \(sessionId)", delegate: self)
        
        
        let suffixDic = ["SESSION_ID" : sessionId,
            "TOKEN": token,
            "APP_TYPE": "2",
            "PUSH_ID": push_id,
            "DEVICE_ID": "1",
            "MODEL_NM": "1",
            "OS": "1"]
        
        transactionClass.sendAPI("frt_login_r001", argument: suffixDic, success: { (success) -> Void in
            
            
            if let noError = success["RSLT_CD"] where noError as! String == "0000" {
                
                if let getCoupon = success["RESP_DATA"]!["FREE_CNT"] as? String {
                    self.couponCount = getCoupon
                }
                
                if let uName = success["RESP_DATA"]!["USER_NM"] as? String where uName != ""{
                    self.userName = uName
                }
                
                print("initFecth \(self.userName)")
                
                
                if let respData = success["RESP_DATA"] {
                    guard let rawData = respData["NATION_REC"] as? [AnyObject] else{
                        //alert s.th here
                        print("HomeViewController-Nation List nil")
                        return
                    }
                    
                    var nationalities = [Nationality]()
                    for item in rawData {
                        let national = Nationality(data: item as? NSDictionary)
                        nationalities.append(national!)
                    }
                    
                    self.nationalityList = nationalities
                    
                    if let userNm = respData["USER_NM"] as? String {
                        if(userNm != ""){
                            SessionManager.sharedSessionManager().user_nm = userNm
                            //사이드바에 이름 업데이트
                            NSNotificationCenter.defaultCenter().postNotificationName("updateSideUserNm", object: nil)
                        }
                    }
                }

            }
        })
        
    }
    
    
    func doPaddingView() -> UIView{
        let view = UIView(frame: CGRectMake(0,0,14,30))
        view.backgroundColor = UIColor.clearColor()
        return view
    }
    
    func updateUI(name: String) {
        self.countryButton.setTitle(name, forState: .Normal)
//        self.currencyImageView.image = UIImage(named: (selectedNationality?.image)!)
//        self.currencyLabel.text = selectedCurrency?.name
    }
    
    func updateUIAfterFetching(transaction: Transaction){
        self.exchangeRateValuelabel.text = "1 USD = " + transaction.foreignRate! + transcation!.foreignCurrency!
        self.sendingFeeValueLabel.text = formatTextField(transaction.serviceFee!, type: 1)  + " " + transcation!.foreignCurrency!
        self.senderTextField.text = formatTextField(transaction.sendAmount!, type:1)
        self.receiverTextField.text = formatTextField(transaction.receiveAmount!, type: 2)
//        let userName = userExist
        
        var getUser:String
        if let user = userName {
            getUser = user
        }else{
            if let anotherName = self.userName {
                getUser = anotherName
            }else{
                getUser = "Your"
            }
            
        }
        
        let couponWithName = Language.localizedStr("S05_07").stringByReplacingOccurrencesOfString("@@", withString: getUser)
        let splitString = couponWithName.characters.split{ $0 == "0" }.map(String.init)
        let freeCoupon = splitString.first
        self.couponLabel.text = freeCoupon
        
        if senderTextField.text == "" && receiverTextField.text == "" {
            setActiveButton(false)
        }else{
            setActiveButton(true)
        }

    }
    
    func formatTextField(text: String, type: Int) -> String!{
        if !text.isEmpty {
            print("format text \(text)")

            let number = NSDecimalNumber(string: text)

            let formatter = NSNumberFormatter()
            if type == 1 {
                formatter.numberStyle = .DecimalStyle
            }else if type == 2{

                formatter.numberStyle = .CurrencyStyle
                formatter.locale = NSLocale(localeIdentifier: "en_US")
                formatter.currencySymbol = ""
            }else {
                formatter.numberStyle = .NoStyle
            }
            
            let newString = formatter.stringFromNumber(number)
            print("format text new \(newString)")
//            return type == 2 ? newString!.stringByReplacingOccurrencesOfString(",", withString: "") : newString
            return newString
        }
        
        return nil
    }
    
    //MARK: - override
    override func updateViewConstraints() {
        super.updateViewConstraints()
//        print("user ex \(userExist) name \(self.userName) ")
        if self.userName == nil {
            hieghtConstraints.constant = 0
            topConstraints.constant = 13
            hiddenViews(true)
        }else{ //default
            hieghtConstraints.constant = 60
            topConstraints.constant = 0
            hiddenViews(false)
        }
    }
    
    func fetchCurrency(nationality: Nationality){
        
        //request currency
        transactionClass.sendAPI(APICode.CURRENCY_R001, argument: ["NATION_CD" : nationality.code!], success: { (success) -> Void in
            
            if let noError = success["RSLT_CD"] where noError as! String == "0000" {
                
                guard let data = success["RESP_DATA"]!["CURRENCY_REC"] as? [AnyObject] else{
                    print("HomeViewController-currency fetch failed")
                    return
                }
                
                var tempCurrencies = [Currency]()
                
                for item in data {
                    let currency = Currency(data: item as? NSDictionary)
                    
                    tempCurrencies.append(currency!)
                }
                
                self.currencies = tempCurrencies
            }
        })
        
    }
    
    
    
    
    
    func updateTextField(textField: UITextField, withText text: String) {
        textField.text = text
    }
    
    func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int) {
        if buttonIndex == 0 {
            print("확인 버튼을 클릭하였습니다.")
        } else {
            print("취소 버튼을 클릭하였습니다.")
        }
    }
    
    
    func showHomeViewAlert(note : NSNotification) {
        if let message = note.userInfo?["message"] as? String {
            //self.showSimpleAlert(message, delegate: self)
            let leftPopUp:LeftPopUpViewController = LeftPopUpViewController(nibName: "LeftPopUpViewController",bundle: NSBundle.mainBundle())
            presentPopupViewController(leftPopUp, animationType: MJPopupViewAnimationFade, contentInteraction: MJPopupViewContentInteractionDismissBackgroundOnly)
        }
    }
    
    func refreshHomeVC(note: NSNotification) {
        initFetch()
    }
    
    //MARK: - Navigation
    func changeMainView(note : NSNotification) {
        let viewName = note.userInfo?["viewName"] as? String
        let webViewYn = note.userInfo?["webviewYN"] as? String
        let webViewTitle = note.userInfo?["webviewTitle"] as? String
print("----->\(viewName)")
        self.view.endEditing(true)
        if viewName != "" {
            if webViewYn != nil && webViewYn == "Y" {
                //웹뷰
                //테스트중
                webViewURL = viewName! as String;
                self.webViewTitle = webViewTitle!
                self.performSegueWithIdentifier("ShowWebView", sender: self)
            } else {
                
                if viewName == String(RecipientListViewController) {
                    transactionClass.sendAPI(APICode.RECIPIENT.RECIPIENT_R001, argument: ["" : ""], success: { (success) -> Void in
                        
                        if let data = success["RSLT_CD"]! as? String where data != "0000" {
                            self.showEventAlert((success["RSLT_MSG"]! as? String)!, hasCancelButton: false, delegate: self, tag: 200000, confirmBlock: { _ in
                                    //do nothing
                                }, cancelBlock: nil)
                        }else{
                            guard let data = success["RESP_DATA"]!["RECIPIENT_REC"] as? [AnyObject] else {
                                print("Recipeint R001 nil.....")
                                return
                            }
                            
                            var recipients = [Recipient]()
                            for item in data {
                                let recipient = Recipient(data: item as? [String: AnyObject])
                                recipients.append(recipient!)
                            }
                            
                            let recipientStoryboard = UIStoryboard(name: "Recipient", bundle: NSBundle.mainBundle())
                            let recipientListViewController =  recipientStoryboard.instantiateViewControllerWithIdentifier("RecipientListViewController") as? RecipientListViewController
                            recipientListViewController?.nationlityList = self.nationalityList
                            recipientListViewController?.recipients = recipients
                            print("homeview navigaiton \(self.navigationController)")
                            self.navigationController?.pushViewController(recipientListViewController!, animated: true)
//                            self.presentViewController(recipientListViewController!, animated: true, completion: nil)
                        }
                    })

                }
                //MyInfoManagerViewController
                else if viewName == String("MyInfoManagerController"){

                    //Go to MyInfoManager Screen
                    if NSUserDefaults.standardUserDefaults().objectForKey("SIGNUP_YN") != nil {
                        
                            /////////// USE THIS FORMAT
                            let transaction = TransactionClass()
                            transaction.sendAPI(APICode.PROFILE_R001, argument: ["" : ""],
                                success: { (success) -> Void in
                                
                                    if let rslt_cd = success["RSLT_CD"] as? String {
                                    
                                        if rslt_cd != "0000" { //오류
                                            if let rsltMsg = success["RSLT_MSG"] as? String {
                                            
                                                print("Error Code : \(rslt_cd), Error Message : \(rsltMsg)")
                                                
                                            }
                                        } else { //정상
                                            if let respData = success["RESP_DATA"] as? NSDictionary {
                                                
                                                print("Sucess Code : \(rslt_cd), Message Data : \(respData)")
                                                
                                                let myInfoManagerStroyboard     = UIStoryboard(name: "MyInfoManager", bundle: NSBundle.mainBundle())
                                                let myInfoManagerController = myInfoManagerStroyboard.instantiateInitialViewController() as? MyInfoManagerViewController
                                                guard let myInfoManagerRootViewController = myInfoManagerController else{
                                                    print("instanitateViewController nil. please check it out.")
                                                    return
                                                }
                                                
                                                myInfoManagerController!.responseData = respData
                                                
                                                self.navigationController?.pushViewController(myInfoManagerRootViewController, animated: true)
                                                
                                        }
                                    }
                                        
                                }
                            })
                        
                    }
                    //Go to Signup Screen
                    else {
                        self.gotoSignup()
                    }
                    
                }
                    
                //ServiceUnRegister
                else if viewName == String(ServiceUnRegisterViewController) {
                    
                    //Go to ServiceUnregister Screen
                    if NSUserDefaults.standardUserDefaults().objectForKey("SIGNUP_YN") != nil {
                        let serviceUnregisterStroyboard     = UIStoryboard(name: "ServiceUnregister", bundle: NSBundle.mainBundle())
                        let serviceUnregisterViewController = serviceUnregisterStroyboard.instantiateInitialViewController()
                    
                        guard let serviceUnregisterRootViewController = serviceUnregisterViewController else{
                            print("instanitateViewController nil. please check it out.")
                            return
                        }
                        self.navigationController?.pushViewController(serviceUnregisterRootViewController, animated: true)
                    }
                    else{
                        print("No User Signedup")
                    }
                    
                }
                //
                else if viewName == String(RecommendFriendViewController) {
                    let customerCenterStoryboard = UIStoryboard(name: "CustomerCenter", bundle: NSBundle.mainBundle())
                    let recommendFriendVC = customerCenterStoryboard.instantiateInitialViewController() as? RecommendFriendViewController
                    self.navigationController?.pushViewController(recommendFriendVC!, animated: true)
                }else if viewName == String(MyTransViewController){
                    let storyboard = UIStoryboard(name: "Transaction", bundle: nil)
                    let viewcontroller = storyboard.instantiateViewControllerWithIdentifier("MyTransViewController") as? MyTransViewController
                    self.navigationController?.pushViewController(viewcontroller!, animated: true)
                }else if viewName == String("http://211.217.152.244/supark/notice/notice_0002_01_view.jsp"){
                    let customer:CustomerPopupViewController = CustomerPopupViewController(nibName: "CustomerPopupViewController",bundle: NSBundle.mainBundle())
                    presentPopupViewController(customer, animationType: MJPopupViewAnimationFade, contentInteraction: MJPopupViewContentInteractionDismissBackgroundOnly)
                }else {
                    //네이티브 뷰
                    self.performSegueWithIdentifier(viewName!, sender: self)
                }
            }
        }
    }
    
    func gotoSignup(){
        let signupStoryboard = UIStoryboard(name: "Signup", bundle: NSBundle.mainBundle())
        let signupViewController = signupStoryboard.instantiateInitialViewController()
        
//        guard let signupRootViewController = signupViewController else{
//            print("instanitateViewController nil. please check it out.")
//            return
//        }
//        
//        self.navigationController?.pushViewController(signupRootViewController, animated: true)
        
        self.presentViewController(signupViewController!, animated: true, completion: nil)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier == "ShowWebView") {
            let svc = segue.destinationViewController as! WebViewController
            svc.destURL = webViewURL
            svc.webViewTitle = self.webViewTitle
        }
    }
    
    @IBAction func loginAction(sender: UIButton) {
        let token = NSUserDefaults.standardUserDefaults().objectForKey(kDeviceToken) as? String
        let reqData = ["SESSION_ID" : "1",
                       "TOKEN" : "1",
                       "APP_TYPE" : "1",
                       "PUSH_ID" : "1",
                       "DEVICE_ID" : "1",
                       "MODEL_NM" : UIDevice().modelName,
                       "OS" : "1"]
        transactionClass.sendAPI("frt_login_r001", argument: reqData, success: { (rawData) -> Void in
            
            print("response data \(rawData)")
        })

        
    }
    
    func LeftBtnClicked(sender: AnyObject) {
        if let drawController = navigationController?.parentViewController as? KYDrawerController {
            drawController.setDrawerState( .Opened, animated: true)
            self.view.endEditing(true)
        }
    }
    
    //Push 메시지를 받았을 때의 처리
    func NotiAPNS(note: NSNotification) {
        isPush = true
        NSUserDefaults.standardUserDefaults().setObject(nil, forKey: "notiDic")
        NSUserDefaults.standardUserDefaults().synchronize()
        
        //뱃지 추가
        if let userInfoDic:NSDictionary = note.userInfo {
            if let badge:NSDictionary  = userInfoDic.objectForKey("aps") as? NSDictionary {
                SessionManager.sharedSessionManager().strNumberAlam = badge.objectForKey("badge") as? String
                
                // 푸시 뱃지 수 셋팅
                if let badgeString: String = badge.objectForKey("badge") as? String {
                    if let badgeNumber: Int = Int(badgeString) {
                        UIApplication.sharedApplication().applicationIconBadgeNumber = badgeNumber
                    }
                }
                
            }
        }
    }
    
    //MARK: - PopupControllerDelegate
    
    func getCountryData(dict: NSDictionary!){
        // do nothing
    }
    
    func getCountryObject(indexPath: NSIndexPath) {
        
        selectedNationality = self.nationalityList![indexPath.row]
        print("i'm about to get $ ---> \(selectedNationality?.name)")
        fetchCurrency(selectedNationality!)
        
    }

    func getCurrencyObject(indexPath: NSIndexPath) {
        selectedCurrency = self.currencies![indexPath.row]
    }
    

//    @available(iOS 8.0, *)
//    func popupController<T: PrintableCell>(popupController: PopupController<T>, didSelectRowAtIndexPath indexPath: NSIndexPath){
//        
//        if T.self == Nationality.self {
//            selectedNationality = self.nationalityList![indexPath.row]
//            fetchCurrency(selectedNationality!)
//            updateUI(selectedNationality!.name)
//        }else{ // currency selected
//            let selectedCurrency = self.currencies![indexPath.row]
//            self.currencyLabel.text = selectedCurrency.name
//        }
//    }
    
}
