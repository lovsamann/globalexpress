//
//  KeypadView.h
//  GlobalExpress
//
//  Created by JangWooil on 2016. 1. 18..
//  Copyright © 2016년 webcash. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TransKey.h"
#import "TransKeyView.h"

@protocol KeypadViewDelegate;

@interface KeypadView : NSObject<TransKeyViewDelegate>

+ (KeypadView *)sharedKeypadView;
-(void)showNumberTranskey: (UIViewController *)ownerView minLength:(int)minLength maxLength:(int)maxLength tagNumber:(NSInteger)tagNumber title:(NSString *)title;
-(void)showQwertyTranskey: (UIViewController *)ownerView minLength:(int)minLength maxLength:(int)maxLength tagNumber:(NSInteger)tagNumber title:(NSString *)title;

@property(nonatomic, assign) id<KeypadViewDelegate> delegate;

@end

@protocol KeypadViewDelegate <NSObject>

- (void) returnKeyboard:(NSUInteger)tagNumber inputlength:(NSUInteger)inputlength plainText:(NSString *)plainText encText:(NSString *)encText isCancel:(BOOL)isCancel;

@end