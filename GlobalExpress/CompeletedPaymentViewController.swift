//
//  CompeletedPaymentViewController.swift
//  GlobalExpress
//
//  Created by Ann on 3/11/16.
//  Copyright © 2016 webcash. All rights reserved.
//

import UIKit

class CompeletedPaymentViewController: UIViewController {

    
    var completedRequestDic: [String:String!]?
    
    
    //MARK: - Outlet
    //label
    @IBOutlet var infTitleLabel: UILabel!
    @IBOutlet var subInfoTitleLabel: UILabel!
    @IBOutlet var payToTheBelowLabel: UILabel!
    @IBOutlet var bankTextLabel: UILabel!
    @IBOutlet var accountNoTextLabel: UILabel!
    @IBOutlet var accoutNameTextLabel: UILabel!
    @IBOutlet var amountTextLabel: UILabel!
    @IBOutlet var periodTextLabel: UILabel!
    @IBOutlet var noticeLabel: UILabel!
    
    @IBOutlet var bankValueLabel: UILabel!
    @IBOutlet var accountValueLabel: UILabel!
    @IBOutlet var accoutNameValueLabel: UILabel!
    @IBOutlet var amountValueLabel: UILabel!
    @IBOutlet var periodValueLabel: UILabel!
    
    
    @IBOutlet var myTranscationButton: UIButton!
    @IBOutlet var confirmButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.localize()
        
        self.navigationItem.backBarButtonItem = nil
        self.navigationItem.hidesBackButton = true
        
        
        
        guard let dic = completedRequestDic else{
            print("error compeltedREquestDic")
            return
        }
        
        self.bankValueLabel.text = dic["REC_BANK"]
        self.accountValueLabel.text = dic["REC_ACCT"]
        self.accoutNameValueLabel.text = dic["REC_ACCTNM"]
        self.amountValueLabel.text = ("\(dic["REC_AMT"]!) \(dic["REC_CURRENCY"]!)")
        let formatter = NSDateFormatter()
        formatter.dateFormat = "yyyyMMddHHmmss"
        let date = formatter.dateFromString(dic["REC_DUETIME"]!)
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        self.periodValueLabel.text = formatter.stringFromDate(date!)
        
        
        
    }
    
    override func localize() {
        self.infTitleLabel.text = Language.localizedStr("T04_02")
        self.subInfoTitleLabel.text = Language.localizedStr("T04_03")
        self.payToTheBelowLabel.text = Language.localizedStr("T04_04")
        self.bankTextLabel.text = Language.localizedStr("T04_05")
        self.accountNoTextLabel.text = Language.localizedStr("T04_06")
        self.accoutNameTextLabel.text = Language.localizedStr("T04_07")
        self.amountTextLabel.text = Language.localizedStr("T04_08")
        self.periodTextLabel.text = Language.localizedStr("T04_09")
        self.noticeLabel.text = Language.localizedStr("T04_10")
        
        
        self.confirmButton.setTitle(Language.localizedStr("T04_11"), forState: .Normal)
        self.myTranscationButton.setTitle(Language.localizedStr("T04_12"), forState: .Normal)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Navigation
    
    @IBAction func buttonClick(sender: AnyObject) {
        
        if sender.tag == 2000 { //confirm clicked
            self.navigationController?.popToRootViewControllerAnimated(true)
        }else{  // My transcation clicked
            self.performSegueWithIdentifier("myTranscationSeuge", sender: nil)
        }
    }
    

}
