//
//  BSAppIron.h
//  BSAppIron
//
//  Created by Cho JeongHyeon on 2015. 3. 25..
//  Copyright (c) 2015년 Barunsoft. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol BSAppIronServiceCallback <NSObject>

-(void)onAppIronResultReceive:(NSString *)code sid:(NSString *)sid token:(NSString *)token;

@end

@interface BSAppIron : NSObject

@property (nonatomic, retain)NSString *url;
@property (nonatomic, retain)NSString *appId;
@property (nonatomic, retain)NSString *appVer;
@property (nonatomic, retain)NSString *sessionId;

@property (nonatomic, retain)NSString *sSessionId;
@property (nonatomic, retain)NSString *sToken;

+ (BSAppIron *)getInstance;


- (NSString *)authApp:(NSString *)url appId:(NSString *)appId appVer:(NSString *)appVer;
- (NSString *)getSessionId;
- (NSString *)getToken;

/* 기기등록 프로세스 추가 - 20131107 */
- (NSString *)registDevice:(NSString *)registKey reqUrl:(NSString *)reqUrl;

/* ipa파일 자동등록기능추가로 별도로 앱아이디와 버전을 넘기지 않는 authApp 메소드 추가 - 20131129 */
- (NSString *)authApp:(NSString *)url;


- (NSString *)getAppIronDeviceId;

- (void)startService:(NSString *) url callback:(id<BSAppIronServiceCallback>)callback;
- (void)startService:(NSString *) url interval:(NSTimeInterval)interval callback:(id<BSAppIronServiceCallback>)callback;
- (void)startService:(NSString *) url interval:(NSTimeInterval)interval timeout:(NSTimeInterval)timeout callback:(id<BSAppIronServiceCallback>)callback;


@end
