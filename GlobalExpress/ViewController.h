//
//  ViewController.h
//  GlobalExpress
//
//  Created by JangWooil on 2015. 12. 29..
//  Copyright © 2015년 webcash. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITextField *userId;
@property (strong, nonatomic) IBOutlet UILabel *popup_desc;
@property (strong, nonatomic) IBOutlet UILabel *englishLabel;
@property (strong, nonatomic) IBOutlet UILabel *koreanLabel;
@property (strong, nonatomic) IBOutlet UILabel *camboLabel;
@property (strong, nonatomic) IBOutlet UIButton *confirmButton;
@property (strong, nonatomic) IBOutlet UIButton *englishRadio;
@property (strong, nonatomic) IBOutlet UIButton *koreanRadio;
@property (strong, nonatomic) IBOutlet UIButton *camboRadio;
@property (strong, nonatomic) IBOutlet UIView *popupView;

@end

