//
//  ProfileTableViewCell.swift
//  GlobalExpress
//
//  Created by Ann on 3/16/16.
//  Copyright © 2016 webcash. All rights reserved.
//

import UIKit

class ProfileTableViewCell: UITableViewCell {

    
    //MARK: - Outlet
    
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var nationalLabel: UILabel!
    @IBOutlet var statusImageView: UIImageView!
    @IBOutlet var statusLabel: UILabel!
    @IBOutlet var telLabel: UILabel!
    @IBOutlet var emailLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
