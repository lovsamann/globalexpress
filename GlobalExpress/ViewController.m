//
//  ViewController.m
//  GlobalExpress
//
//  Created by JangWooil on 2015. 12. 29..
//  Copyright © 2015년 webcash. All rights reserved.
//


#import "ViewController.h"
#import "SysUtil.h"
#import "AppDelegate.h"
#import "Constants.h"
#import "SessionManager.h"
#import "Language.h"
#import "GlobalExpress-Swift.h"

@interface ViewController ()
{
    NSString *clientLangCd;
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showAlert:) name:kShowAlertMain object:nil]; //Alert 메시지 띄우기
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loginSuccess) name:kIntroEnded object:nil]; //인트로 화면 끝났을 때 메인으로 이동한다
    
    //네비게이션 바를 hide 처리한다.
    [[self navigationController] setNavigationBarHidden:YES];
    
    [SessionManager sharedSessionManager].screenWidth = self.view.frame.size.width;
    [SessionManager sharedSessionManager].screenHeight = self.view.frame.size.height;
    
    // Radio Event
    UIImage *on_image = [UIImage imageNamed:@"on_radio_btn"];
    [self.englishRadio setBackgroundImage:on_image forState:UIControlStateSelected];
    [self.koreanRadio setBackgroundImage:on_image forState:UIControlStateSelected];
    [self.camboRadio setBackgroundImage:on_image forState:UIControlStateSelected];
    
    UITapGestureRecognizer *engLabelGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(setEnglishOn)];
    UITapGestureRecognizer *korLabelGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(setKoreanOn)];
    UITapGestureRecognizer *camLabelGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(setCamboOn)];
    
    [self.englishLabel addGestureRecognizer:engLabelGesture];
    [self.koreanLabel addGestureRecognizer:korLabelGesture];
    [self.camboLabel addGestureRecognizer:camLabelGesture];
    
    //사용자 언어 확인
    NSString *userSavedLang = [[NSUserDefaults standardUserDefaults] objectForKey:kUserSavedLanguage];
    
    
    if (userSavedLang != nil) {
        if ([userSavedLang isEqualToString:kKoreaCode] || [userSavedLang isEqualToString:kEnglishCode] || [userSavedLang isEqualToString:kCambodiaCode]) {
            [self languageSuccess];
        } else {
            //언어설정이 되지 않은 경우 언어선택 팝업을 보여준다. 영어 기본
            self.popupView.hidden = NO;
            [self setEnglishOn];
        }
    } else {
        //언어설정이 되지 않은 경우 언어선택 팝업을 보여준다. 영어 기본
        self.popupView.hidden = NO;
        [self setEnglishOn];
    }
    
    //다국어 처리
    [self localize];
}

- (void) localize{
    self.popup_desc.text = [Language localizedStr:@"S01P_01"];
    self.englishLabel.text = [Language localizedStr:@"S24_03"];
    self.koreanLabel.text  = [Language localizedStr:@"S24_04"];
    self.camboLabel.text   = [Language localizedStr:@"S24_05"];
    [self.confirmButton setTitle:[Language localizedStr:@"S24_06"] forState:UIControlStateNormal];
}

- (void)showAlert: (NSNotification *)note {
    if ([SysUtil IS_BEFORE_OS_8]) {
        
    } else {
        UIAlertController *alertController = [[note userInfo] objectForKey:@"alertController"];
        if (alertController != nil) {
            [self presentViewController:alertController animated:true completion:nil];
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setEnglishOn {
    clientLangCd = kEnglishCode;
    
    self.englishRadio.selected = YES;
    self.koreanRadio.selected  = NO;
    self.camboRadio.selected   = NO;
}

-(void)setKoreanOn{
    clientLangCd = kKoreaCode;
    
    self.englishRadio.selected = NO;
    self.koreanRadio.selected  = YES;
    self.camboRadio.selected   = NO;
}

-(void)setCamboOn{
    clientLangCd = kCambodiaCode;
    
    self.englishRadio.selected = NO;
    self.koreanRadio.selected  = NO;
    self.camboRadio.selected   = YES;
}

- (IBAction)englishRadioClicked:(id)sender {
    [self setEnglishOn];
}

- (IBAction)koreanRadioClicked:(id)sender {
    [self setKoreanOn];
}

- (IBAction)camboRadioClicked:(id)sender {
    [self setCamboOn];
}

- (IBAction)confirmBtnClicked:(id)sender {
    //언어설정 업데이트가 사라진 관계로 세션 값과 앱 캐시에만 저장한다.
    [SessionManager sharedSessionManager].language = clientLangCd;
    
    //사용자 언어 확인
    [[NSUserDefaults standardUserDefaults] setObject:clientLangCd forKey:kUserSavedLanguage];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    //팝업 종료
    self.popupView.hidden = YES;
    
    //사이드메뉴 refresh 처리
//    [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshSideMenu" object:nil userInfo:nil];
    
    //Intro 화면을 보여줄 지를 확인한다
    [self languageSuccess];
}

- (IBAction)loginButton:(id)sender {
    NSString *strUserId = @"testUserId";

    
    //Push 사용자 등록
    NSDictionary *regDic = [NSDictionary dictionaryWithObjectsAndKeys:strUserId, @"registerKey", nil];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"PushStartNotification" object:self userInfo:regDic];
//    NSNotificationCenter.defaultCenter().postNotificationName("PushStartNotification", object: self, userInfo: regDic)
}

//언어 선택이 다 된 경우 Intro를 봤는지를 확인한다
-(void)languageSuccess {
   NSString *strIntroWatched = [[NSUserDefaults standardUserDefaults] objectForKey:@"IntroWatchedYN"];
    
    if (strIntroWatched == nil || [strIntroWatched isEqualToString:@"Y"] == NO) {
        //인트로 화면을 안 본 경우, 인트로 스토리보드를 보여준다
        UIStoryboard *introStoryboard = [UIStoryboard storyboardWithName:@"IntroScreen" bundle:[NSBundle mainBundle]];
        IntroScrollViewController *introViewC = introStoryboard.instantiateInitialViewController;
        
        [self presentViewController:introViewC animated:YES completion:nil];
    } else {
        [self loginSuccess];
    }
}

//메인으로 이동하기
- (void)loginSuccess {
    [[NSUserDefaults standardUserDefaults] setObject:@"Y" forKey:@"IntroWatchedYN"];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"loginSuccess" object:nil userInfo:nil];
    
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self.userId resignFirstResponder];
}
@end
