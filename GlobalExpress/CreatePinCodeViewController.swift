//
//  PinCodeViewController.swift
//  GlobalExpress
//
//  Created by Ralex on 2/4/16.
//  Copyright © 2016 webcash. All rights reserved.
//

import UIKit

class PinCodeViewController: UIViewController,UITextFieldDelegate{

    @IBOutlet weak var lblPinInfo: UILabel!
    
    @IBOutlet weak var passCodeTextField : UITextField!
    @IBOutlet weak var viewInset : UIView!
    @IBOutlet weak var lblStep: UILabel!
    @IBOutlet weak var btnConfirm:UIButton!
    @IBOutlet weak var viewStep: ViewCustom!
    @IBOutlet weak var viewStep_constraint_height: NSLayoutConstraint!
    
    
    let numOfImageInset = 5
    var startx:CGFloat = 0.0
    var isDelete:Bool = false
    var beforePassword:String?
    var numOfNumberInput:Int = 0
    var navigationTitle = "Set up PIN Number"
    var tradeStatus:Int?
    
    @IBOutlet weak var processConstraint: NSLayoutConstraint!
    @IBOutlet weak var labelInfoTopConstraint: NSLayoutConstraint!
    
    override func localize() {
        lblStep.text = "4. \(Language.localizedStr("M01_06"))"
        btnConfirm.setTitle(Language.localizedStr("T10_03"), forState: .Normal)
       
        print("TradeStatus = \(tradeStatus)")
       
        if tradeStatus == 0{
            navigationTitle = Language.localizedStr("T10_08")
            lblPinInfo.text = Language.localizedStr("T10_09")
            viewStep.hidden = true
        }
        else if tradeStatus == 3{
            navigationTitle = Language.localizedStr("T10_11")
            lblPinInfo.text = Language.localizedStr("T10_12")
            viewStep.hidden = true
        }else{
            navigationTitle = Language.localizedStr("T10_01")
            lblPinInfo.text = Language.localizedStr("T10_02")
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        localize()
        
        RLib.setNavigationTitle(self, title: navigationTitle)
        RLib.setLeftNavigationItem(self, image: "back_btn.png", action: Selector("handleLeftNavigationItemClick:"))
        
        passCodeTextField.becomeFirstResponder()
        passCodeTextField.tintColor = UIColor.clearColor()
    }
    
    func handleLeftNavigationItemClick(sender:UIBarButtonItem){
        navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func handleViewPress(sender: AnyObject) {
        passCodeTextField.resignFirstResponder()
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func initPinCharLayout(numOfNumberInput:Int){
        var count = 0
        if isDelete == true{
            for subview in viewInset.subviews{
                subview.removeFromSuperview()
            }
        }
        for index in 0...numOfImageInset{
            startx = CGFloat(index * 29)
            let imgPasscode = UIImageView()
            if numOfNumberInput == 0{
                imgPasscode.frame = CGRectMake(startx + 8, 19, 13 , 3)
                imgPasscode.image = UIImage(named: "pin_emp_char.png")
            }else{
                //Add it depend on index of inputted
                imgPasscode.frame = CGRectMake(startx + 8, 13, 13 , 13)
                imgPasscode.image = UIImage(named: "pin_char.png")
                count++
                if count > numOfNumberInput {
                    imgPasscode.frame = CGRectMake(startx + 8, 19, 13 , 3)
                    imgPasscode.image = UIImage(named: "pin_emp_char.png")
                }
            }
            viewInset.addSubview(imgPasscode)
        }
        
    }
    
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
        //LOOP IMAGE IN VIEW]
        initPinCharLayout(numOfNumberInput)
        return true
        
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        numOfNumberInput = (textField.text?.utf16.count)! + string.utf16.count - range.length
        //IF BACK KEY PRESS
        if range.length == 1{
            isDelete = true
            initPinCharLayout(numOfNumberInput)
        }else{
            isDelete = false
            initPinCharLayout(numOfNumberInput)
        }
        return numOfNumberInput > 6 ? false : true
    }

    

    @IBAction func ActionNext(sender: AnyObject) {
        guard passCodeTextField.text != "" && numOfNumberInput > 5 else{
            self.showSimpleAlert("Please enter PIN No.", delegate: self)
            return

        }
        self.performSegueWithIdentifier("confirmpincodesegue", sender: self)
    }
    
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        //TODO: pass data
        if segue.identifier == "confirmpincodesegue" {
            let conf = segue.destinationViewController as? ConfirmPinCodeViewController
            conf?.pinCode = self.passCodeTextField.text!
            conf?.tradeStatus = self.tradeStatus
            conf?.beforePassword = self.beforePassword
        }
    }
    

}
