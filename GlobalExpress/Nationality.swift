//
//  Nationality.swift
//  GlobalExpress
//
//  Created by Ann on 1/26/16.
//  Copyright © 2016 webcash. All rights reserved.
//

import Foundation

struct NationalityProperty {
    static let CODE = "NATION_CD"
    static let DIGIT = "NATION_CD1"
    static let NAME = "NATION_NM"
    static let IMAGE = "FLAG_IMG"
}


@objc class Nationality: NSObject,PrintableCell {
    
    var code: String?
    var digi: String?
    var name: String!
    var image: String!
    
    init?(data: NSDictionary?) {
        
        guard data != nil else {
            print("dictionary is nil")
            return
        }
        
        code = data![NationalityProperty.CODE] as? String
        digi = data![NationalityProperty.DIGIT] as? String
        name = data![NationalityProperty.NAME] as? String
        image = data![NationalityProperty.IMAGE] as? String
        
    }
    
    init?(nameNM: String){
        name = nameNM
    }
}