//
//  DebitSignupFinalViewController.swift
//  GlobalExpress
//
//  Created by Ralex on 2/4/16.
//  Copyright © 2016 webcash. All rights reserved.
//

import UIKit

class DebitSignupFinalViewController: UIViewController {
    @IBOutlet weak var lblCompleteStatus1:UILabel!
    @IBOutlet weak var lblCompleteStatus2:UILabel!
    @IBOutlet weak var btnConfirm: UIButton!
    @IBOutlet weak var lblStep:UILabel!
    
    var navigationTitle:String?
    
    override func localize() {
        lblStep.text = "4. \(Language.localizedStr("M01_06"))"
        lblCompleteStatus1.text = Language.localizedStr("T12_01")
        lblCompleteStatus2.text = Language.localizedStr("T12_02")
        btnConfirm.setTitle(Language.localizedStr("T12_03"), forState: .Normal)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //TODO : Localize
        localize()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func ActionConfirm(sender: AnyObject) {
        //TODO: DO STH HERE 
        //GOTO T03
        let viewcontroller = self.storyboard?.instantiateViewControllerWithIdentifier("ConfirmTransactionViewController") as? ConfirmTransactionViewController
        self.navigationController?.pushViewController(viewcontroller!, animated: true)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBarHidden = true
    }
    override func viewWillDisappear(animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBarHidden = false
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
