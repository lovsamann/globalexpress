//
//  SysUtil.m
//  GlobalExpress
//
//  Created by JangWooil on 2016. 1. 14..
//  Copyright © 2016년 webcash. All rights reserved.
//

#import "SysUtil.h"
#import "Language.h"
#import "Constants.h"

@implementation SysUtil

+(BOOL)IS_BEFORE_OS_8
{
    return IS_OS_8_BEFORE;
}

+(BOOL)IS_BEFORE_OS_9
{
    return IS_OS_9_BEFORE;
}


+(UIAlertView *)getAlertViewWithmessage:(NSString *)message delegate:(UIViewController *)alertDelegate hasCancelButton:(BOOL)hasCancelButton
{
    UIAlertView *alert = nil;
    
    if (hasCancelButton) {
        alert = [[UIAlertView alloc] initWithTitle:[Language localizedStr:@"MSG_01"] //@"안내"
                                           message:message
                                          delegate:alertDelegate
                                 cancelButtonTitle:[Language localizedStr:@"S23_05"] //@"확인"
                                 otherButtonTitles:[Language localizedStr:@"cancel"], nil]; //취소
    } else {
        alert = [[UIAlertView alloc] initWithTitle:[Language localizedStr:@"MSG_01"] //@"안내"
                                                        message:message
                                                       delegate:alertDelegate
                                              cancelButtonTitle:[Language localizedStr:@"S23_05"] //@"확인"
                                              otherButtonTitles:nil];
    }
    
    return alert;
}

+(UIAlertController *)getAlertControllerWithmessage:(NSString *)message
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:[Language localizedStr:@"MSG_01"] message:message preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okButton  = [UIAlertAction actionWithTitle:[Language localizedStr:@"S23_05"] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        [alert dismissViewControllerAnimated:YES completion:nil];
    }];
    
    [alert addAction:okButton];
    
    return alert;
}

+(UIAlertController *)getAlertControllerWithmessage:(NSString *)message hasCancelButton:(BOOL)hasCancelButton
                           confirmSelectBlock:(void (^)(NSString *))confirmSelectBlock cancelSelectBlock:(void (^)(NSString *))cancelSelectBlock
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:[Language localizedStr:@"MSG_01"] message:message preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okButton  = [UIAlertAction actionWithTitle:[Language localizedStr:@"S23_05"] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        if (confirmSelectBlock != nil) {
            confirmSelectBlock(message);
        }
        [alert dismissViewControllerAnimated:YES completion:nil];
    }];
    
    if (hasCancelButton) {
        UIAlertAction *cancel    = [UIAlertAction actionWithTitle:[Language localizedStr:@"cancel"] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            if (cancelSelectBlock != nil) {
                cancelSelectBlock(message);
            }
            [alert dismissViewControllerAnimated:YES completion:nil];
        }];
        
        [alert addAction:okButton];
        [alert addAction:cancel];
    } else {
        [alert addAction:okButton];
    }
    
    return alert;
}

@end
