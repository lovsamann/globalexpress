//
//  AskQuestionViewController.swift
//  GlobalExpress
//
//  Created by Ralex on 1/29/16.
//  Copyright © 2016 webcash. All rights reserved.
//

import UIKit

class AskQuestionViewController: UIViewController,UITextViewDelegate{

    @IBOutlet weak var ViewKR: UIView!
    @IBOutlet weak var ViewCustom: UIView!
    @IBOutlet weak var tvContent: UITextView!
    @IBOutlet weak var lblEnFullName: UILabel!
    @IBOutlet weak var lblKorFullName: UILabel!
    @IBOutlet weak var lblMobilePhone: UILabel!
    @IBOutlet weak var btnConfirm: UIButton!
    
    @IBOutlet weak var enFullName: UILabel!
    @IBOutlet weak var korFullName: UILabel!
    @IBOutlet weak var mobilePhone: UILabel!
    @IBOutlet weak var inquiryStatus: UILabel!
    
    var customerInfo:Dictionary<String,String>?
    var argument =  Dictionary<String, String>()
    var isKOR:Bool = false
    var isAllowConfirmed:Bool = false
    var PLACEHOLDER_TEXT:String = ""
    var navigationTitle:String = ""
    
    @IBOutlet weak var constraints_ViewKOR_Top: NSLayoutConstraint!
    @IBOutlet weak var constraints_ViewCustom_Hieght: NSLayoutConstraint!
    @IBOutlet weak var constraints_TvContent_Height: NSLayoutConstraint!
    
    func initLoad(){
        self.ViewCustom.layer.cornerRadius = 6
        self.tvContent.layer.cornerRadius = 6
        self.tvContent.delegate = self
        isAllowConfirmed = applyPlaceholderStyle(tvContent, placeholderText: PLACEHOLDER_TEXT)
        
//        if (isKOR == true){
//            self.lblEnFullName.text = customer_name!
//            self.lblMobilePhone.text = customer_tel!
//            
//            //IF Device is iPhone 4 or 4s
//            if DeviceType.IS_IPHONE_4_OR_LESS{
//                //NOT YET FIX
//            }
//            if DeviceType.IS_IPHONE_5{
//                constraints_TvContent_Height.constant = 232
//            }
//            if DeviceType.IS_IPHONE_6{
//                constraints_TvContent_Height.constant = 318
//            }
//            if DeviceType.IS_IPHONE_6P{
//                //NOT YET FIX
//            }
//            
//        }else{
//            
//            self.ViewKR.hidden = true //IF no kroean name...hide korean view
//            constraints_ViewKOR_Top.constant = -48
//            constraints_ViewCustom_Hieght.constant = CGFloat(self.ViewCustom.frame.height - self.ViewKR.frame.height)
//            self.lblEnFullName.text = customer_name!
//            self.lblMobilePhone.text = customer_tel!
//            
//            //IF Device is Iphone 4 or 4s
//            if DeviceType.IS_IPHONE_4_OR_LESS{
//                //NOT YET FIX
//            }
//            if DeviceType.IS_IPHONE_5{
//                constraints_TvContent_Height.constant = 232
//            }
//            if DeviceType.IS_IPHONE_6{
//                constraints_TvContent_Height.constant = 318
//            }
//            if DeviceType.IS_IPHONE_6P{
//                //NOT YET FIX
//            }
//        }
        
        self.lblEnFullName.text = customerInfo!["CUST_NM"]
        self.lblMobilePhone.text = customerInfo!["TEL_NO"]
        btnConfirm.addTarget(self, action: "ActionConfirm:", forControlEvents: .TouchUpInside)
        
    }

    override func localize() {
        navigationTitle = Language.localizedStr("S20_01")
        enFullName.text = Language.localizedStr("S20_02")
        korFullName.text = Language.localizedStr("S20_02")
        mobilePhone.text = Language.localizedStr("S20_03")
        PLACEHOLDER_TEXT = Language.localizedStr("S20_04")
        inquiryStatus.text = Language.localizedStr("S20_05")
        btnConfirm.setTitle(Language.localizedStr("S20_06"), forState: .Normal)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // TODO: initial view
        localize()
        RLib.setNavigationTitle(self, title: navigationTitle)
        RLib.setLeftNavigationItem(self, image: "back_btn.png", action: Selector("handleLeftNavigationItemClick:"))
        RLib.setNavigationBackground(self, red: 42, green: 87, blue: 118, alpha: 1)
        self.initLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func handleLeftNavigationItemClick(sender:UIBarButtonItem){
        navigationController?.popViewControllerAnimated(true)
    }
    
    func ActionConfirm(sender: AnyObject) {
        var argument = Dictionary<String,String>()
        guard isAllowConfirmed == true else{
            //Should alert message here
            return
        }
        argument = ["CUST_NM":customerInfo!["CUST_NM"]!,
            "TEL_NO":customerInfo!["TEL_NO"]!,
            "INQUIRY_TXT":self.tvContent.text]
        print("Data = \(argument)")
        //TRUE DO HERE
        TransactionClass().sendAPI(APICode.INQUIRY_C001, argument: argument, success: { (success) -> Void in
            guard let code = success["RSLT_CD"] where code as! String == "0000" else{
                //Should be alert here
                guard #available(iOS 9.0,*) else{
                    // Fallback on earlier versions
                    let alertView = UIAlertView(title: "", message: "\(success["RSLT_MSG"]!)", delegate: nil, cancelButtonTitle: "OK")
                    alertView.show()
                    return
                }
                let alertController : UIAlertController = UIAlertController(title: "", message: "\(success["RSLT_MSG"]!)", preferredStyle: .Alert)
                let confirmAction   : UIAlertAction     = UIAlertAction(title: "OK", style: .Cancel, handler:nil)
                alertController.addAction(confirmAction)
                self.presentViewController(alertController, animated: true, completion: nil)
                return

            }
            self.performSegueWithIdentifier("successsegue", sender: nil)
        })
    }
    
    func applyPlaceholderStyle(aTextView:UITextView,placeholderText:String) -> Bool{
        aTextView.textColor = UIColor(red: 194/255, green: 199/255, blue: 204/255, alpha: 1)
        aTextView.text = placeholderText
        return false
    }
    
    func applyNonPlaceholderStyle(aTextView:UITextView) -> Bool{
        aTextView.textColor = UIColor(red: 48/255, green: 50/255, blue: 51/255, alpha: 1)
        return true
    }
    
    func textViewShouldBeginEditing(textView: UITextView) -> Bool {
        if textView == tvContent && tvContent.text == PLACEHOLDER_TEXT{
            //MOVE CURSOR TO START
            moveCursorToStart(textView)
        }
        return true
    }
    
    
    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        // remove the placeholder text when they start typing
        let newLength = textView.text.utf16.count + text.utf16.count - range.length
        if newLength > 0 // have text, so don't show the placeholder
        {
            // check if the only text is the placeholder and remove it if needed
            // unless they've hit the delete button with the placeholder displayed
            if textView == tvContent && textView.text == PLACEHOLDER_TEXT
            {
                if text.utf16.count == 0 // they hit the back button
                {
                    return false // ignore it
                }
                isAllowConfirmed = applyNonPlaceholderStyle(textView)
                textView.text = ""
            }
            return true
        }
        else  // no text, so show the placeholder
        {
            isAllowConfirmed = applyPlaceholderStyle(textView, placeholderText: PLACEHOLDER_TEXT)
            moveCursorToStart(textView)
            return false
        }
    }
    
    func moveCursorToStart(aTextView: UITextView)
    {
        dispatch_async(dispatch_get_main_queue(), {
            aTextView.selectedRange = NSMakeRange(0, 0);
        })
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}