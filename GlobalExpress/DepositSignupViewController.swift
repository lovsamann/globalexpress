//
//  DepositSignupViewController.swift
//  GlobalExpress
//
//  Created by Ralex on 2/2/16.
//  Copyright © 2016 webcash. All rights reserved.
//

import UIKit

class DepositSignupViewController: UIViewController {
    
    @IBOutlet weak var lblT05Signup: UILabel!
    @IBOutlet weak var lblT05DebitProcess: UILabel!
    @IBOutlet weak var lblT05Identify: UILabel!
    @IBOutlet weak var lblT05DebitAccountDesignation: UILabel!
    @IBOutlet weak var lblT05CreatePin: UILabel!
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var lblStep: UILabel!
    


    
    //Variable
    var custinfodictionary:Dictionary<String,AnyObject> = [:]
    var  navigationTitle = "Pay With Direct Debit"
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController!.navigationBar.translucent = false
        
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController!.navigationBar.translucent = true
    }
    
    override func localize() {
        navigationTitle = Language.localizedStr("T05_01")
        lblStep.text = "3. \(Language.localizedStr("M04_03"))"
        lblT05Signup.text = Language.localizedStr("T05_02")
        lblT05DebitProcess.text = Language.localizedStr("T05_03")
        lblT05Identify.text = Language.localizedStr("T05_04")
        lblT05DebitAccountDesignation.text = Language.localizedStr("T05_05")
        lblT05CreatePin.text = Language.localizedStr("T05_06")
        btnNext.setTitle(Language.localizedStr("T05_07"), forState: .Normal)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        //TODO: Localize
        localize()
        
        // TODO: initial view
        RLib.setNavigationTitle(self, title: navigationTitle)
        RLib.setLeftNavigationItem(self, image: "back_btn.png", action: Selector("handleLeftNavigationItemClick:"))
    }
    
    func handleLeftNavigationItemClick(sender:UIBarButtonItem){
        navigationController?.popViewControllerAnimated(true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func ActionNext(sender: AnyObject) {
        
        //TODO: USE API CODE_R001
        TransactionClass().sendAPI(APICode.CODE_R001, argument: ["":""], success: { (success) -> Void in
            print("Success Data = \(success)")
            guard let code = success["RSLT_CD"] where code as! String == "0000" else{
                //Should be alert here
                self.showSimpleAlert(success["RSLT_MSG"] as! String, delegate: self)
                return
            }
            if let eng_name = success["RESP_DATA"]!["ENG_NM"] as? String,
                let kor_name = success["RESP_DATA"]!["KOR_NM"] as? String,
                let birth_ymd = success["RESP_DATA"]!["BIRTH_YMD"] as? String,
                let tel_cd = success["RESP_DATA"]!["TEL_CD"] as? String,
                let hp_no = success["RESP_DATA"]!["HP_NO"] as? String,
                let sex_gb = success["RESP_DATA"]!["SEX_GB"] as? String,
                let nation_gb = success["RESP_DATA"]!["NATION_GB"] as? String,
                let intl_cd = success["RESP_DATA"]!["INTL_TELCD"] as? String,
                let tel_rec = success["RESP_DATA"]!["TELCOM_REC"] as? Array<Dictionary<String,String>>
            {
                self.custinfodictionary = [
                    "ENG_NM": eng_name,
                    "KOR_NM": kor_name,
                    "BIRTH_YMD": birth_ymd,
                    "TELCOM_CD":tel_cd,
                    "HP_NO":hp_no,
                    "SEX_GB":sex_gb,
                    "NATION_GB":nation_gb,
                    "INTL_TELCD":intl_cd,
                    "TELCOM_REC":tel_rec
                ]
                
               self.performSegueWithIdentifier("debitsignupsegue", sender: self)
            }
        })
    }
   
    // MARK: - Navigation
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        //TODO : Pass data via segue
        if segue.identifier == "debitsignupsegue" {
            let phoneAuthViewCtl = segue.destinationViewController as! PhoneAuthViewController
            phoneAuthViewCtl.custinfodictionary = self.custinfodictionary
            phoneAuthViewCtl.tradeStatus = 1
        }
    }
}
