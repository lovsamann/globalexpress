//
//  AppDelegate.h
//  GlobalExpress
//
//  Created by JangWooil on 2015. 12. 29..
//  Copyright © 2015년 webcash. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WCPushService.h"
#import "Constants.h"
#import "Reachability.h"
//#import "GlobalExpress-Swift.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate, WCPushServiceDelegate, UIAlertViewDelegate>
{
    Reachability*			hostReach;
    Reachability*			wifiReach;
}

@property (strong, nonatomic) UIWindow *window;
//@property (nonatomic, retain) KYDrawerController *kyDrawerController;

@end

