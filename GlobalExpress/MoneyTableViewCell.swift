//
//  MoneyTableViewCell.swift
//  GlobalExpress
//
//  Created by Ann on 3/10/16.
//  Copyright © 2016 webcash. All rights reserved.
//

import UIKit

class MoneyTableViewCell: UITableViewCell {

    @IBOutlet var fxTextLabel: UILabel!
    @IBOutlet var fxValueLabel: UILabel!
    @IBOutlet var fxRecevingTextLabel: UILabel!
    @IBOutlet var fxRecevingValueLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
