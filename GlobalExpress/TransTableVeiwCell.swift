//
//  TransTableVeiwCell.swift
//  GlobalExpress
//
//  Created by Ralex on 2/8/16.
//  Copyright © 2016 webcash. All rights reserved.
//

import UIKit

class TransTableVeiwCell: UITableViewCell {

    @IBOutlet weak var lblTransdate: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblBank: UILabel!
    @IBOutlet weak var lblKRWMoney: UILabel!
    @IBOutlet weak var lblUSDMoney: UILabel!
    @IBOutlet weak var imgBackground: UIImageView!
    @IBOutlet weak var btnStatus: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
