//
//  NoticePopup.m
//  GlobalExpress
//
//  Created by UDAM on 2/16/16.
//  Copyright © 2016 webcash. All rights reserved.
//

#import "NoticePopup.h"

@interface NoticePopup ()

@end

@implementation NoticePopup

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.layer.cornerRadius = 8.0f;
    
    //Localize
    self.titleLabel.text         = [Language localizedStr:@"M02P_01"];
    self.descriptionLabel.text   = [Language localizedStr:@"M02_21"];
    
    [self.confirmButton setTitle:[Language localizedStr:@"M02_22"] forState:UIControlStateNormal];
    
}

//MARK: IBAction
- (IBAction)shareButtonClicked:(UIButton *)sender {
    [self dismissPopupViewController];
}
@end
