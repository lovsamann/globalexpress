//
//  ViewCircle.swift
//  GlobalExpress
//
//  Created by Ann on 2/28/16.
//  Copyright © 2016 webcash. All rights reserved.
//

import UIKit

@IBDesignable class ViewCircle: UIControl {
    
    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet {
            layer.cornerRadius = cornerRadius
        }
    }
    
    @IBInspectable var fillColor: UIColor = UIColor.whiteColor() {
        didSet {
            self.backgroundColor = fillColor
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

}
