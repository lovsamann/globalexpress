//
//  RecipientViewController.swift
//  GlobalExpress
//
//  Created by Ann on 1/27/16.
//  Copyright © 2016 webcash. All rights reserved.
//

import UIKit

class RecipientViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    var recipients: [Recipient]?
    var nationlityList: [Nationality]?
    var transcation: Transaction?
    @IBOutlet var tableView: UITableView!
    @IBOutlet var addButton: UIButton!
    @IBOutlet var selectReceiverLabel: UILabel!
    @IBOutlet var nextButton: UIButton!
    @IBOutlet var receiverStepBarLabel: UILabel!
    
    
    //MARK: - View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.localize()
        
        let leftNavigationImage = UIImage(named: "back_btn.png")
        let leftNavigationButton = UIButton(frame: CGRectMake(0,0,(leftNavigationImage?.size.width)! / 2 , (leftNavigationImage?.size.height)! / 2))
        leftNavigationButton.setBackgroundImage(leftNavigationImage, forState: .Normal)
        leftNavigationButton.addTarget(self, action: "leftNavigationClicked", forControlEvents: .TouchUpInside)
        
        let leftBarButtonItem = UIBarButtonItem(customView: leftNavigationButton)
        self.navigationItem.leftBarButtonItem = leftBarButtonItem

    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
//        let imageView = UIImageView(image: UIImage(named: "con_main01.png"))
////        imageView.frame = self.tableView.frame
//        self.tableView.backgroundView = backgroundImageView()
        
        print("RecipientViewContorller - \(self.transcation)")
    }
    
    //MARK: - UITableViewDataSource
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  recipients == nil ? 0 : (recipients?.count)!
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("uitableviewcell") as? RecipientTableViewCell
        
        
        let recipient = recipients![indexPath.row]
        
        cell?.recipientNameLabel.text = recipient.name
        cell?.BankNameLabel.text = recipient.bankName
        cell?.NationalityLabel.text = recipient.nationality?.name
        cell?.accountNoLabel.text = recipient.accountNumber
        
        
        return cell!
    }
    
    //MARK: - UITableViewDelegate
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let recipient = recipients![indexPath.row]
        print("ddddd \(recipient)")
        transcation?.recipient = recipient
        print("did selecte d \(transcation?.recipient)")
        
    }

    //custom header
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerImage = UIImage(named: "con_rad_top01.png")
        let headerImageView = UIImageView(frame: CGRectMake(0, 0, CGRectGetWidth(tableView.frame), 9))

        headerImageView.image = headerImage
        return recipients == nil || recipients?.count == 0 ? nil : headerImageView
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return recipients == nil || recipients?.count == 0 ? 0.00000000000000000000001 : 9.0
//        return 0.00000000000000000000001
    }

////    //custom footer
    func tableView(tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        
        let frame =  CGRectMake(0,0,CGRectGetWidth(tableView.frame),recipients == nil || recipients?.count == 0 ? 45 : 62)
        let footerView = UIView(frame: frame)
        footerView.backgroundColor = UIColor.clearColor()

        let addButton = addReceiverButton(CGRectMake(0,recipients == nil || recipients?.count == 0 ? 0 : 18,CGRectGetWidth(tableView.frame),45))
        addButton.addTarget(self, action: "navigationToAddRecipeint", forControlEvents: .TouchUpInside)
        if recipients?.count > 0 { //add both footerView & add recipeint view
            let footerImageView = addFooterImageView(CGRectMake(0, 0, CGRectGetWidth(tableView.frame), 9))
            footerView.addSubview(footerImageView)
        }
        
        footerView.addSubview(addButton)
    
        return footerView
    }

    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return recipients == nil || recipients?.count == 0 ? 45 : 62
    }
    
    
    //MARK: - private methods
    
    func backgroundImageView() -> UIImageView {
        return UIImageView(image: UIImage(named: "con_main01.png"))
        //        imageView.frame = self.tableView.frame
    }
    
    override func localize() {
        self.selectReceiverLabel.text = Language.localizedStr("R05_02")
        self.nextButton.setTitle(Language.localizedStr("R05_04"), forState: .Normal)
        self.receiverStepBarLabel.text = "2." + Language.localizedStr("R07_05")
        self.navigationItem.title = Language.localizedStr("R05_01")
    }
    
    
    func addFooterImageView(frame: CGRect) -> UIImageView {
        //footerImage
        let footerImage = UIImage(named: "con_rad_bottom01.png")
        let footerImageView = UIImageView(frame: frame)
        footerImageView.image = footerImage
        
        let clearSeparatorView = UIView(frame: CGRectMake(8, -1 , CGRectGetWidth(footerImageView.frame) - 12 , 1))
        clearSeparatorView.backgroundColor = UIColor.whiteColor()
        footerImageView.addSubview(clearSeparatorView)
        
        return footerImageView
    }
    
    func addReceiverButton(frame: CGRect) -> UIButton {
        let addButton = UIButton(frame: frame)
        addButton.setBackgroundImage(UIImage(named: "w_btn.png"), forState: .Normal)
        addButton.setTitle(Language.localizedStr("R05_03"), forState: .Normal)

        addButton.setTitleColor(UIColor(red: 48 / 255, green: 50 / 255, blue: 51 / 255, alpha: 1.0), forState: .Normal)
        
        let addIcon = UIImage(named: "add_icon.png")
        let newAddIcon = UIImage.customResizeImage(addIcon!, newSize: CGSizeMake(18,18))
        addButton.setImage(newAddIcon, forState: .Normal)

        addButton.imageEdgeInsets = UIEdgeInsetsMake(0, -8, -1, 0)
        
        return addButton
    }
    
    //MARK:  Action
    @IBAction func nextStepClicked(sender: AnyObject) {
        print("trasss \(transcation?.recipient)")
        if transcation?.recipient != nil {
            let transcationStoryboard = UIStoryboard(name: "Transaction", bundle: NSBundle.mainBundle())
            let confirmTranscationViewController = transcationStoryboard.instantiateInitialViewController() as? ConfirmTransactionViewController
            confirmTranscationViewController?.transcation = self.transcation
            self.navigationController?.pushViewController(confirmTranscationViewController!, animated: true)
        }else{
            self.showEventAlert("Please Select recipient or add recipient", hasCancelButton: false, delegate: self, tag: 1998, confirmBlock: nil, cancelBlock: nil)
        }
        

    }
    
    func leftNavigationClicked(){
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    func navigationToAddRecipeint() {
        self.performSegueWithIdentifier("addRecipientSegue", sender: nil)
    }
    
    //MARK: - Navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if addButton === sender {
                TransactionClass().sendAPI(APICode.CODE_R001, argument: ["" : ""], success: {
                    (success) -> Void in
                    
                    var nationalityList = [Nationality]()
                    
                    if let respondData = success["RESP_DATA"], nationalities = respondData["NATION_REC"] as? [AnyObject] {
                        for item in nationalities {
                            let nationlity = Nationality(data: item as? NSDictionary)
                            nationalityList.append(nationlity!)
                        }
                    }
                    
                    self.nationlityList = nationalityList
                    
                    if let destinationViewController = segue.destinationViewController as? AddRecipientViewController {
                        destinationViewController.nationalityList = self.nationlityList
                        destinationViewController.transcation = self.transcation
                    }
                })
        }
    }
    
}

extension UIImage {
    class func customResizeImage(image: UIImage, newSize: CGSize) -> UIImage{
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0)
        image.drawInRect(CGRectMake(0, 0, newSize.width, newSize.height))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage
    }
}
