//
//  CountryViewController.swift
//  GlobalExpress
//
//  Created by UDAM on 2/23/16.
//  Copyright © 2016 webcash. All rights reserved.
//

import UIKit

@objc protocol CountryViewControllerDelegate {
    func getCountryData(dict: NSDictionary!)
    optional func getCountryObject(indexPath: NSIndexPath)
    optional func getCurrencyObject(indexPath: NSIndexPath)
}

class CountryViewController: MJPopupViewController, UITableViewDataSource, UITableViewDelegate {

    // MARK: - IBOutlet
    // -------------------------------------------------------------------------------
    //	IBOutlet UITableView, UILabel, UIImageView
    // -------------------------------------------------------------------------------
    @IBOutlet weak var tableView        : UITableView!
    @IBOutlet weak var titleLabel       : UILabel!
    
    // MARK: - Property
    // -------------------------------------------------------------------------------
    // Store country as NSArray
    // -------------------------------------------------------------------------------
    var countryArray    : NSArray!
    var delegate        : CountryViewControllerDelegate?
    
    let cellHeight      : CGFloat   = 44.0
    let fixedTableRow   : Int       = 7
    
    // MARK: - IBAction
    // -------------------------------------------------------------------------------
    // Click for closing Popup CountryViewController
    // -------------------------------------------------------------------------------
    @IBAction func closeButton(sender: UIButton) {
        
        self.dismissPopupViewController(self, animationType: MJPopupViewAnimationFade)
        
    }
    
    // MARK: - Private function
    // -------------------------------------------------------------------------------
    // Localizing language
    // -------------------------------------------------------------------------------
    override func localize() {
        if countryArray[0] is Currency {
            titleLabel.text = Language.localizedStr("C03P_01")
        }else{
            titleLabel.text = Language.localizedStr("C02P_01")
        }
    }
    
    // MARK: - View Life Cycle
    // -------------------------------------------------------------------------------
    // viewDidLoad customize view and table view
    // -------------------------------------------------------------------------------
    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.layer.cornerRadius = 8.0
        
        if countryArray.count <= fixedTableRow {
            self.view.frame = CGRectMake(view.frame.origin.x, view.frame.origin.y, self.view.frame.width, (CGFloat(countryArray.count) * cellHeight) + 40)
            
            let tableMaskLayer = CAShapeLayer()
            tableMaskLayer.path = UIBezierPath(roundedRect: self.tableView.bounds, byRoundingCorners: UIRectCorner.BottomLeft.union(.BottomRight), cornerRadii: CGSizeMake(8, 8)).CGPath
            self.tableView.layer.mask = tableMaskLayer

        }
        
        tableView.registerNib(UINib.init(nibName: "CurrencyTableViewCell", bundle: NSBundle.mainBundle()), forCellReuseIdentifier: "CurrencyTableViewCell")
        
        //Localize 
        localize()
        
    }
    
    // -------------------------------------------------------------------------------
    // viewDidLayoutSubviews
    // -------------------------------------------------------------------------------
    override func viewDidLayoutSubviews(){
        
        if countryArray.count <= fixedTableRow {
            tableView.frame = CGRectMake(tableView.frame.origin.x, tableView.frame.origin.y, tableView.frame.size.width, CGFloat(countryArray.count) * cellHeight)

            let tableMaskLayer = CAShapeLayer()
            tableMaskLayer.path = UIBezierPath(roundedRect: self.tableView.bounds, byRoundingCorners: UIRectCorner.BottomLeft.union(.BottomRight), cornerRadii: CGSizeMake(8, 8)).CGPath
            self.tableView.layer.mask = tableMaskLayer

            tableView.scrollEnabled = false
            tableView.reloadData()
        }
        
        if tableView.respondsToSelector("setSeparatorInset:") {
            tableView.separatorInset = UIEdgeInsetsZero
        }
        
        if tableView.respondsToSelector("setLayoutMargins:") {
            
            //## Swift2.0 수정부분 - 테스트 필요
            if #available(iOS 8.0, *) {
                tableView.layoutMargins = UIEdgeInsetsZero
            }
        }
        
    }
    
    // MARK: - UITableViewDelegate
    // -------------------------------------------------------------------------------
    // Set tableView for full width each cell and color of separator
    // -------------------------------------------------------------------------------
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {

        if cell.respondsToSelector("setSeparatorInset:") {
            cell.separatorInset = UIEdgeInsetsZero
        }
        
        if cell.respondsToSelector("setLayoutMargins:") {

            //## Swift2.0 수정부분 - 테스트 필요
            if #available(iOS 8.0, *) {
                cell.layoutMargins = UIEdgeInsetsZero
            }
        }
        tableView.separatorColor = UIColor(red: 213 / 255, green: 219 / 255, blue: 224 / 255, alpha: 1)
        
    }
    
    // -------------------------------------------------------------------------------
    // didSelectRowAtIndexPath: Get Country Data After select for each cell
    // -------------------------------------------------------------------------------
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if countryArray[indexPath.row] is NSDictionary {
            self.delegate?.getCountryData(countryArray[indexPath.row] as! NSDictionary)
        }else if countryArray[indexPath.row] is Nationality{
            print("sent delegate \(indexPath.row)")
            self.delegate?.getCountryObject!(indexPath)
        }else if countryArray[indexPath.row] is Currency{
            self.delegate?.getCurrencyObject!(indexPath)
        }
        
        self.dismissPopupViewController(self, animationType: MJPopupViewAnimationFade)
        print(countryArray[indexPath.row])
    }
    
    // MARK: - UITableViewDataSource
    // -------------------------------------------------------------------------------
    // Customize the number of rows in table view
    // -------------------------------------------------------------------------------
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return countryArray.count
    }
    
    // -------------------------------------------------------------------------------
    // UITableView cellForRowAtIndexPath
    // -------------------------------------------------------------------------------
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        if countryArray[indexPath.row] is Currency {
            let cell = tableView.dequeueReusableCellWithIdentifier("CurrencyTableViewCell")
            let currency = countryArray[indexPath.row] as! Currency
            (cell as! CurrencyTableViewCell).currencyLabel.text = currency.name
            return cell!
        }else{
            tableView.registerNib(UINib.init(nibName: "CountryCell", bundle: NSBundle.mainBundle()), forCellReuseIdentifier: "countryCell")
            
            let cell = tableView.dequeueReusableCellWithIdentifier("countryCell") as! CountryCell
            
            //Configure color Cell selection
            let popBG5_P            = UIImage(imageLiteral: "pop_bg5_p")
            let customSelectImage   = UIImageView(image: popBG5_P)
            cell.selectedBackgroundView = customSelectImage
            
            if countryArray[indexPath.row] is NSDictionary {
//                if (countryArray[indexPath.row]["NATION_CD"] as! String == "156") {
//                    cell.imageCountry.image     = UIImage(imageLiteral: "fl_sg_icon.png")
//                }
//                else if (countryArray[indexPath.row]["NATION_CD"] as! String == "410") {
//                    cell.imageCountry.image     = UIImage(imageLiteral: "fl_kor_icon.png")
//                }
//                else if (countryArray[indexPath.row]["NATION_CD"] as! String == "704") {
//                    cell.imageCountry.image     = UIImage(imageLiteral: "fl_th_icon.png")
//                }
//                else{
//                    cell.imageCountry.image = UIImage(imageLiteral: "\(countryArray[indexPath.row]["FLAG_IMG"])")
//                }
                guard let imageString: String = countryArray[indexPath.row]["FLAG_IMG"] as? String else{
                    print("imageString nil")
                    return cell
                }
                
                guard let image: UIImage = UIImage(imageLiteral: imageString)  else{
                    print("image nil")
                    return cell
                }
                
                cell.imageCountry.image = image
                cell.titleCountry.text = "\(countryArray[indexPath.row]["NATION_NM"])"
            }else{
                
                if countryArray[indexPath.row] is Nationality {
                    let nationality = countryArray[indexPath.row] as! Nationality
//                    if (nationality.code! == "156") {
//                        cell.imageCountry.image     = UIImage(imageLiteral: "fl_sg_icon.png")
//                    }
//                    else if (nationality.code! == "410") {
//                        cell.imageCountry.image     = UIImage(imageLiteral: "fl_kr_icon.png")
//                    }
//                    else if (nationality.code! == "704") {
//                        cell.imageCountry.image     = UIImage(imageLiteral: "fl_th_icon.png")
//                    }
//                    else{
//                        cell.imageCountry.image = UIImage(imageLiteral: nationality.image)
//                    }
                    
                    guard let imageString = nationality.image else{
                        print("iamgeString nil")
                        return cell
                    }
                    
                    guard let image: UIImage = UIImage(imageLiteral: imageString) else{
                        print("image nil")
                        return cell
                    }
                    
                    cell.imageCountry.image = image
                    cell.titleCountry.text = nationality.name
                }
            }
            
            
            
            //        cell.imageCountry.image     = UIImage(imageLiteral: "fl_cam_icon.png")
            
            
            return cell
        }
    }
    
    // -------------------------------------------------------------------------------
    // UITableView heightForFooterInSection : set height for Footer of UITableView
    // -------------------------------------------------------------------------------
    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.01
    }
    
    // -------------------------------------------------------------------------------
    // UITableView viewForFooterInSection : set view nil for Footer of UITableView
    // -------------------------------------------------------------------------------
    func tableView(tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    
}
