//
//  Transaction.swift
//  GlobalExpress
//
//  Created by Ann on 2/3/16.
//  Copyright © 2016 webcash. All rights reserved.
//

import Foundation


class Transaction {
    
    var recipient: Recipient?
    var currency: Currency?
    var nationlity: Nationality?
    
    var sendAmount: String?
    var receiveAmount: String?
    var foreignRate: String?
    var serviceFee: String?
    var serviceFeeInUSD: String?
    var couponFree: String?
    var foreignCurrency: String?
    
    init(data: NSDictionary?) {
        guard let rawData = data else{
            recipient = nil
            currency  = nil
            nationlity = nil
            sendAmount = nil
            receiveAmount = nil
            foreignRate = nil
            serviceFeeInUSD = nil
            couponFree = nil
            print("Occured data \(data) in Transcation")
            return
        }
        
        sendAmount = rawData["SEND_AMT"] as? String
        receiveAmount = rawData["RECV_AMT"] as? String
        foreignRate = rawData["FX_RATE"] as? String
        serviceFee = rawData["SVC_FEE"] as? String
        serviceFeeInUSD = rawData["SVC_USDFEE"] as? String
        couponFree = rawData["FREE_CNT"] as? String
        foreignCurrency = rawData["FX_CURR"] as? String
    }
    
    convenience init(currency:Currency?, nationality: Nationality?,data: NSDictionary?) {
        
        if currency == nil || nationality == nil {
            self.init(data: data)
        }else{
            self.init(data: data)
            self.currency = currency
            self.nationlity = nationality
        }
    }
    

    
    
}