//
//  CountryCell.swift
//  GlobalExpress
//
//  Created by UDAM on 2/23/16.
//  Copyright © 2016 webcash. All rights reserved.
//

import UIKit

class CountryCell: UITableViewCell {

    // MARK: - IBOutlet
    // -------------------------------------------------------------------------------
    // Customize cell image and title
    // -------------------------------------------------------------------------------
    @IBOutlet weak var imageCountry: UIImageView!
    @IBOutlet weak var titleCountry: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
