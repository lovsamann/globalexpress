//
//  TransHistoryViewController.swift
//  GlobalExpress
//
//  Created by Ralex on 2/5/16.
//  Copyright © 2016 webcash. All rights reserved.
//

import UIKit

class TransWaitingViewController: UIViewController,UITableViewDataSource,UITableViewDelegate{

    @IBOutlet weak var viewNocontent: UIView!
    @IBOutlet weak var tranWaitingTableView: UITableView!
    @IBOutlet weak var lblNoTrans: UILabel!
    
    //variable
    var waitingArr = Array<AnyObject>()
    var transDetail = Dictionary<String,String>()
    
    override func localize() {
        lblNoTrans.text = Language.localizedStr("S07_11")
    }
    
    func initLoad()->Void{
        let args = [
            "INQ_GB":"0",
            "MORE_GB":"0",
            "LAST_ORDNO":"10"
        ]
        TransactionClass().sendAPI(APICode.HIST_R001, argument: args, success: { (success)->Void in
            print("Data = \(success)")
            guard let code = success["RSLT_CD"] where code as! String == "0000" else{
                //Should be alert here
                print("RESULT MESSAGE = \(success["RSLT_MSG"])")
                return
            }
            if let data = success["RESP_DATA"]!["INQ_REC"] as? Array<AnyObject>{
                self.waitingArr = data
                guard self.waitingArr.count > 0 else{
                    //TODO: Display no content
                    self.viewNocontent.hidden = false
                    self.tranWaitingTableView.hidden = true
                    return
                }
                //TODO: Display data in table view
                self.viewNocontent.hidden = true
                self.tranWaitingTableView.hidden = false
                
                self.tranWaitingTableView.reloadData()
            }
        })
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        localize()
        self.initLoad()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tranWaitingTableView.registerNib(UINib(nibName: "TransTableVeiwCell", bundle: nil), forCellReuseIdentifier: "TransCell")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.waitingArr.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tranWaitingTableView.dequeueReusableCellWithIdentifier("TransCell", forIndexPath: indexPath) as! TransTableVeiwCell
        if self.waitingArr.count > 0{
            var imgStrBackgroud:String = ""
            var imgStatus:String = ""
            if waitingArr[indexPath.row]["TX_STS"] as? String == "0"{
                imgStrBackgroud = "l_waiting_bg"
                imgStatus = "waiting_icon"
            }else if waitingArr[indexPath.row]["TX_STS"] as? String == "1"{
                imgStrBackgroud = "l_depo_bg"
                imgStatus = "depo_icon"
            }else if waitingArr[indexPath.row]["TX_STS"] as? String == "3"{
                imgStrBackgroud = "l_transfr_bg"
                imgStatus = "transfr_icon"
            }
            if imgStrBackgroud != ""{
                cell.imgBackground.image = UIImage(named:imgStrBackgroud)
                cell.btnStatus.setImage(UIImage(named: imgStatus), forState: UIControlState.Normal)
            }
            if waitingArr[indexPath.row]["RCPT_GB"] as? String == "1"{
                cell.lblBank.text = "Bank account"
            }else if waitingArr[indexPath.row]["RCPT_GB"] as? String == "2"{
                cell.lblBank.text = "Cash Pick-up"
            }
            cell.lblName.text = self.waitingArr[indexPath.row]["RCP_NM"] as? String
            cell.lblTransdate.text = RLib.formatDate(self.waitingArr[indexPath.row]["TX_DT"] as! String)
            cell.lblKRWMoney.text = (self.waitingArr[indexPath.row]["TX_WONAMT"] as! String).formatKRWCurrency()
            cell.lblUSDMoney.text = (self.waitingArr[indexPath.row]["TX_AMT"] as? String)?.formatUSDCurrency()
            
            cell.btnStatus.tag = indexPath.row
            cell.btnStatus.addTarget(self, action: "ActionClick:", forControlEvents: .TouchUpInside)
        }
        
        return cell
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 80.0
    }
    
    func ActionClick(sender:AnyObject){
        print("Tag = \(sender.tag)")
        if let orderno = self.waitingArr[sender.tag]["ORDER_NO"] as? String {
            print("ORDER_NO = \(orderno)")
            TransactionClass().sendAPI(APICode.HIST_R002, argument: ["ORDER_NO": orderno], success: { (success) -> Void in
                    print(success)
                  guard let code = success["RSLT_CD"] where code as! String != "0000" else{
                    if let data = success["RESP_DATA"] {
                        self.transDetail = data as! Dictionary<String, String>
                        //transDetailSegueprint("Detail Trans = \(self.transDetail)")
                        let storyboard = UIStoryboard(name: "Transaction", bundle: nil)
                        let viewcontroller = storyboard.instantiateViewControllerWithIdentifier("TransDetailViewController") as? TransDetailViewController
                        self.transDetail["ORDER_NO"] = orderno
                        viewcontroller?.tranDetail = self.transDetail
                        self.navigationController?.pushViewController(viewcontroller!, animated: true)
                    }
                    return
                }
                print("RESULT MESSAGE = \(success["RSLT_MSG"]!)")
            })
        }
    }
    
    
    // MARK: - Navigation
    //In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // TODO: Pass data
        if segue.identifier == "transDetailSegue"{
            let detailViewCtl = segue.destinationViewController as? TransDetailViewController
            detailViewCtl?.tranDetail = self.transDetail
        }
    }

}
