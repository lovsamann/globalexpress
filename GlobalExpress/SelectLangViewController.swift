//
//  SelectLangViewController.swift
//  GlobalExpress
//
//  Created by JangWooil on 2016. 2. 16..
//  Copyright © 2016년 webcash. All rights reserved.
//

import UIKit


struct LanguageCode {
    let Korea : String
    let Cambodia : String
    let English : String
    
    init(){
        Korea = "ko"
        Cambodia = "km-KH"
        English  = "Base"
    }
}

struct ServerLangCode {
    let Korea : String
    let Cambodia : String
    let English : String
    
    init(){
        Korea = "01"
        English = "02"
        Cambodia = "03"
    }
}

class SelectLangViewController: UIViewController, UIAlertViewDelegate {
    @IBOutlet var descText: UILabel!
    @IBOutlet var submitBtn: UIButton!
    
    @IBOutlet var EnglishLabel: UILabel!
    @IBOutlet var KoreanLabel: UILabel!
    @IBOutlet var CamboLabel: UILabel!
    @IBOutlet var EnglishRadio: UIButton!
    @IBOutlet var KoreanRadio: UIButton!
    @IBOutlet var CamboRadio: UIButton!
    
    var selectedLang : String = "" //선택한 언어코드
    private let confirmAlertTag = 7777
    
    var arrLangList : [String] = [] //영어(0), 한국어(1), 크메르어(2)
    var selectedIndexrow : Int?
    
    
    //언어변경
    @IBAction func submitBtnClicked(sender: AnyObject) {
        let rsltMsg : String
        let LangCd = LanguageCode()
        
        if(selectedLang != LangCd.Korea && selectedLang != LangCd.English && selectedLang != LangCd.Cambodia){
            return
        }
        
        //언어설정 업데이트가 사라진 관계로 세션 값과 앱 캐시에만 저장한다.
        SessionManager.sharedSessionManager().language = selectedLang
        
        //사용자 언어 확인
        NSUserDefaults.standardUserDefaults().setObject(selectedLang, forKey: "UserSavedLanguage")
        NSUserDefaults.standardUserDefaults().synchronize()
        
        if(selectedLang == LangCd.Korea){
            rsltMsg = Language.localizedStr("S24_09")
        } else if(selectedLang == LangCd.English){
            rsltMsg = Language.localizedStr("S24_07")
        } else if(selectedLang == LangCd.Cambodia){
            rsltMsg = Language.localizedStr("S24_08")
        } else {
            return;
        }
        
        //사이드메뉴 refresh 처리
        NSNotificationCenter.defaultCenter().postNotificationName("refreshSideMenu", object: nil, userInfo: nil)
        
//        self.showSimpleAlert(rsltMsg, delegate: self)
        self.showEventAlert(rsltMsg, hasCancelButton: false, delegate: self, tag: confirmAlertTag, confirmBlock: {
            msg in
                self.navigationController?.popViewControllerAnimated(true) //뒤로가기
            }, cancelBlock: nil)
    }
    
    @IBAction func showTest(sender: AnyObject) {
//        IntroScrollViewController
        let recipientStoryboard = UIStoryboard(name: "IntroScreen", bundle: NSBundle.mainBundle())
        let recipientListViewController =  recipientStoryboard.instantiateViewControllerWithIdentifier("IntroScrollViewController") as? IntroScrollViewController
        self.navigationController?.pushViewController(recipientListViewController!, animated: true)
    }
    
    //언어 라디오 처리 start
    @IBAction func setEnglish(sender: AnyObject) {
        setEngRadio()
    }
    
    @IBAction func setKorean(sender: AnyObject) {
        setKorRadio()
    }
    
    @IBAction func setCambo(sender: AnyObject) {
        setCamboRadio()
    }
    
    func setEngRadio() {
        EnglishRadio.selected = true
        KoreanRadio.selected  = false
        CamboRadio.selected   = false
        
        selectedLang = LanguageCode().English
    }
    
    func setKorRadio() {
        EnglishRadio.selected = false
        KoreanRadio.selected  = true
        CamboRadio.selected   = false
        
        selectedLang = LanguageCode().Korea
    }
    
    func setCamboRadio() {
        EnglishRadio.selected = false
        KoreanRadio.selected  = false
        CamboRadio.selected   = true
        
        selectedLang = LanguageCode().Cambodia
    }
    //언어 라디오 처리 end
    
    //UIAlertView Delegate
    
    func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int) {
        if(buttonIndex == 0){ //확인
            if(alertView.tag == confirmAlertTag){
                self.navigationController?.popViewControllerAnimated(true) //뒤로가기
            }
        } else if(buttonIndex == 1){ //취소
            
        }
    }
    
    //UIAlertView Delegate
    
    //다국어 처리
    override func localize() {
        self.descText.text = "\(Language.localizedStr("S24_02"))"
        
        //언어 세팅
        arrLangList.append(Language.localizedStr("S24_03")) //영어
        arrLangList.append(Language.localizedStr("S24_04")) //한국어
        arrLangList.append(Language.localizedStr("S24_05")) //크메르어
        
        submitBtn.setTitle(Language.localizedStr("S23_05"), forState: UIControlState.Normal)
        EnglishLabel.text = Language.localizedStr("S24_03")
        KoreanLabel.text  = Language.localizedStr("S24_04")
        CamboLabel.text   = Language.localizedStr("S24_05")
    }
    
    override func viewDidLoad() {
        localize();
        
        self.title = Language.localizedStr("S24_01")
        
        //라디오버튼 Selected 되었을 때의 이미지 설정
        KoreanRadio.setBackgroundImage(UIImage(named: "on_radio_btn"), forState: .Selected)
        EnglishRadio.setBackgroundImage(UIImage(named: "on_radio_btn"), forState: .Selected)
        CamboRadio.setBackgroundImage(UIImage(named: "on_radio_btn"), forState: .Selected)
        
        //Label 클릭 시 라디오 선택되게 처리
        let engLabelGesture = UITapGestureRecognizer(target: self, action: "setEngRadio")
        let korLabelGesture = UITapGestureRecognizer(target: self, action: "setKorRadio")
        let camLabelGesture = UITapGestureRecognizer(target: self, action: "setCamboRadio")
        
        self.EnglishLabel.addGestureRecognizer(engLabelGesture)
        self.KoreanLabel.addGestureRecognizer(korLabelGesture)
        self.CamboLabel.addGestureRecognizer(camLabelGesture)
        
        //Back button 세팅
        self.cf_setBackButton()
        
        //사용자 언어 확인
        if let userSavedLanguage = NSUserDefaults.standardUserDefaults().objectForKey("UserSavedLanguage") as? String {
            if(userSavedLanguage == LanguageCode().Korea){
                setKorRadio()
            } else if(userSavedLanguage == LanguageCode().Cambodia){
                setCamboRadio()
            } else {
                setEngRadio()
            }
        } else {
            setEngRadio()
        }
    }
}
