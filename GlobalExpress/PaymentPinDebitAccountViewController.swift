//
//  PaymentPinDebitAccountViewController.swift
//  GlobalExpress
//
//  Created by UDAM on 3/7/16.
//  Copyright © 2016 webcash. All rights reserved.
//

import UIKit

class PaymentPinDebitAccountViewController: UIViewController {
    
    //MARK: - IBOutlet
    // -------------------------------------------------------------------------------
    //	IBOutlet UILabel, UIView
    // -------------------------------------------------------------------------------
    @IBOutlet weak var changePaymentPin         : UILabel!
    @IBOutlet weak var resetPaymentPin          : UILabel!
    @IBOutlet weak var debitAccountDesignation  : UILabel!
    @IBOutlet weak var changeDebitAccount       : UILabel!
    
    // -------------------------------------------------------------------------------
    //  UIView
    // -------------------------------------------------------------------------------
    @IBOutlet weak var changePINView            : UIView!
    @IBOutlet weak var resetPINView             : UIView!
    @IBOutlet weak var debitDesignationView     : UIView!
    @IBOutlet weak var changeDebitView          : UIView!
    
    // MARK: - Private function
    // -------------------------------------------------------------------------------
    // Localizing language
    // -------------------------------------------------------------------------------
    override func localize() {
        
        self.navigationItem.title       = Language.localizedStr("S13_1_01")
        changePaymentPin.text           = Language.localizedStr("S12_15")
        resetPaymentPin.text            = Language.localizedStr("S12_16")
        debitAccountDesignation.text    = Language.localizedStr("S12_17")
        changeDebitAccount.text         = Language.localizedStr("S12_18")
        
    }
    
    /*
        tradeStatus = 0 ==> CHANGE PASSWORD
        tradeStatus = 1 ==> REGISTER
        tradeStatus = 2 ==> WIDRAW ACCOUNT DESIGNATION
        tradeStatus = 3 ==> PAYMENT RESET PINCODE
        tradeStatus = 4 ==> CHANGE DEBIT ACCOUNT
    
    */
    var tradeStatus:Int = 0
    var custinfodictionary:Dictionary<String,AnyObject> = [:]
    
    // -------------------------------------------------------------------------------
    // Customize View Layout
    // -------------------------------------------------------------------------------
    func customizeViewLayout(){
        
        //Customize navigation item ,barButton
        let backImage = UIImage(imageLiteral: "back_btn")
        let leftNavButton = UIButton(frame: CGRectMake(269.0, 20.0, backImage.size.width/2, backImage.size.height/2))
        leftNavButton.setBackgroundImage(backImage, forState: .Normal)
        leftNavButton.addTarget(self, action: "popUpViewController", forControlEvents:UIControlEvents.TouchUpInside)
        let barButton = UIBarButtonItem(customView: leftNavButton)
        self.navigationItem.leftBarButtonItem = barButton
        
       
        let changePinGesture = UITapGestureRecognizer(target: self , action: "handleEditProcessGesture:")
        let resetPinGesture = UITapGestureRecognizer(target: self , action: "handleEditProcessGesture:")
        let designationPinGesture = UITapGestureRecognizer(target: self , action: "handleEditProcessGesture:")
        let debitChangeGesture = UITapGestureRecognizer(target: self , action: "handleEditProcessGesture:")
        
        changePINView.tag = 0
        resetPINView.tag = 1
        debitDesignationView.tag = 2
        changeDebitView.tag = 3
        
        changePINView.addGestureRecognizer(changePinGesture)
        resetPINView.addGestureRecognizer(resetPinGesture)
        debitDesignationView.addGestureRecognizer(designationPinGesture)
        changeDebitView.addGestureRecognizer(debitChangeGesture)
        
    }
    
    // -------------------------------------------------------------------------------
    // PopupViewController
    // -------------------------------------------------------------------------------
    func popUpViewController() {
         navigationController?.popViewControllerAnimated(true)
//        let myInfoManagerStroyboard     = UIStoryboard(name: "MyInfoManager", bundle: NSBundle.mainBundle())
//        let myInfoManagerController = myInfoManagerStroyboard.instantiateInitialViewController()
//        
//        self.navigationController?.pushViewController(myInfoManagerController!, animated: true)
    }
    
    //MARK: - View Life Cycle
    // -------------------------------------------------------------------------------
    // ViewDidLoad
    // -------------------------------------------------------------------------------
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Localize
        localize()
        
        //Customize
        customizeViewLayout()
    }
    
    // MARK: - Navigatio
    // -------------------------------------------------------------------------------
    // unwindToMainView
    // -------------------------------------------------------------------------------
    @IBAction func unwindToPaymentDebit(storyboard: UIStoryboardSegue ) {
        self.navigationController?.navigationBarHidden=false
        navigationItem.setHidesBackButton(false, animated: false)
    }
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "ShowChangePIN" {
            let destView = segue.destinationViewController as? ChangePinCodeViewController
            destView!.tradeStatus = self.tradeStatus
        }
    }
    
    func handleEditProcessGesture(sender:UITapGestureRecognizer){
        switch (sender.view?.tag)!{
        case 0:
            tradeStatus = 0
            performSegueWithIdentifier("ShowChangePIN", sender: self)
            return
        case 1:
            tradeStatus = 3 //PAYMENT RESET
            break
        case 2:
            tradeStatus = 2 //DEBIT ACCOUNT DESINATION
            break
        case 3:
            tradeStatus = 4 //DEBIT CHANGE
            break
        default:
            break
        }
        
        //Use it in PinCode
        let tradeProcess = NSUserDefaults.standardUserDefaults()
        tradeProcess.setObject(tradeStatus, forKey: "TradeProcess")
        tradeProcess.synchronize()
        
        TransactionClass().sendAPI(APICode.CODE_R001, argument: ["":""], success: { (success) -> Void in
            guard let code = success["RSLT_CD"] where code as! String == "0000" else{
                //Should be alert here
                print("RESULT MESSAGE = \(success["RSLT_MSG"])")
                return
            }
            if let eng_name = success["RESP_DATA"]!["ENG_NM"] as? String,
                let kor_name = success["RESP_DATA"]!["KOR_NM"] as? String,
                let birth_ymd = success["RESP_DATA"]!["BIRTH_YMD"] as? String,
                let tel_cd = success["RESP_DATA"]!["TEL_CD"] as? String,
                let hp_no = success["RESP_DATA"]!["HP_NO"] as? String,
                let sex_gb = success["RESP_DATA"]!["SEX_GB"] as? String,
                let nation_gb = success["RESP_DATA"]!["NATION_GB"] as? String,
                let intl_cd = success["RESP_DATA"]!["INTL_TELCD"] as? String,
                let tel_rec = success["RESP_DATA"]!["TELCOM_REC"] as? Array<Dictionary<String,String>>
            {
                self.custinfodictionary = [
                    "ENG_NM": eng_name,
                    "KOR_NM": kor_name,
                    "BIRTH_YMD": birth_ymd,
                    "TELCOM_CD":tel_cd,
                    "HP_NO":hp_no,
                    "TELCOM_REC":tel_rec,
                    "SEX_GB":sex_gb,
                    "NATION_GB":nation_gb,
                    "INTL_TELCD":intl_cd
                ]
                
                let storyboard = UIStoryboard(name: "Transaction", bundle: nil)
                let viewcontroller = storyboard.instantiateViewControllerWithIdentifier("PhoneAuthViewController") as? PhoneAuthViewController
                viewcontroller?.custinfodictionary = self.custinfodictionary
                viewcontroller?.tradeStatus = self.tradeStatus
                self.navigationController?.pushViewController(viewcontroller!, animated: true)
            }
        })

    }
}
