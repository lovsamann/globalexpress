//
//  CompletedUnRegisterViewController.swift
//  GlobalExpress
//
//  Created by UDAM on 3/9/16.
//  Copyright © 2016 webcash. All rights reserved.
//

import UIKit

class CompletedUnRegisterViewController: UIViewController {

    //MARK: - IBOutlet
    // -------------------------------------------------------------------------------
    //	IBOutlet label and button , Constraint
    // -------------------------------------------------------------------------------
    @IBOutlet weak var titleLabel   : UILabel!
    @IBOutlet weak var text1        : UILabel!
    @IBOutlet weak var text2        : UILabel!
    @IBOutlet weak var text3        : UILabel!
    @IBOutlet weak var confirmButton : UIButton!
    
    // -------------------------------------------------------------------------------
    //IBOutlet NSLayoutConstraint
    // -------------------------------------------------------------------------------
    @IBOutlet weak var bottomCenterViewConstraint   : NSLayoutConstraint!
    
    //MARK: - Private function
    // -------------------------------------------------------------------------------
    // Localize
    // -------------------------------------------------------------------------------
    override func localize() {
        
        self.titleLabel.text    = Language.localizedStr("S22_01")
        self.text1.text         = Language.localizedStr("S23_01")
        self.text2.text         = Language.localizedStr("S23_02")
        self.text3.text         = Language.localizedStr("S23_03")
        self.confirmButton.setTitle(Language.localizedStr("S23_05"), forState: .Normal)
        
    }
    
    // -------------------------------------------------------------------------------
    // Customize View Layout
    // -------------------------------------------------------------------------------
    func customizeViewLayout() {
        
        self.navigationController?.navigationBarHidden=true
        navigationItem.setHidesBackButton(false, animated: false)
        
        if DeviceType.IS_IPHONE_4_OR_LESS {

            bottomCenterViewConstraint.constant = 148 - 74
        }
        
    }
    
    // MARK: - View Life Cycle
    // -------------------------------------------------------------------------------
    // ViewDidLoad
    // -------------------------------------------------------------------------------
    override func viewDidLoad() {
        super.viewDidLoad()

        //Localize
        localize()
        
        //Customize
        customizeViewLayout()
    }
    
    // MARK: - Handle Button
    // -------------------------------------------------------------------------------
    //	Click confirm will go to HomeViewController
    // -------------------------------------------------------------------------------
    @IBAction func confirmButtonClicked(sender: UIButton) {
        
        /////////// USE THIS FORMAT
        let transaction = TransactionClass()
        transaction.sendAPI(APICode.INIT_R001, argument: ["" : ""],
            success: { (success) -> Void in
                
                if let rslt_cd = success["RSLT_CD"] as? String {
                    
                    if rslt_cd != "0000" { //오류
                        if let rsltMsg = success["RSLT_MSG"] as? String {
                            
                            self.showSimpleAlert("Error Message \(rsltMsg) ", delegate: self)
                            print("Error Code : \(rslt_cd), Error Message : \(rsltMsg)")
                        }
                    } else { //정상
                        if let respData = success["RESP_DATA"] as? NSDictionary {
                            let mainStoryboard      : UIStoryboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
                            let homeViewController  : UIViewController = mainStoryboard.instantiateViewControllerWithIdentifier("KYDrawerController")
                            
                            self.navigationController?.pushViewController(homeViewController, animated: true)
                        }
                    }
                }
        })
//        let mainStoryboard      : UIStoryboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
//        let homeViewController  : UIViewController = mainStoryboard.instantiateViewControllerWithIdentifier("KYDrawerController")
//        
//        self.navigationController?.pushViewController(homeViewController, animated: true)
        
    }

}
