//
//  MyTransViewController.swift
//  GlobalExpress
//
//  Created by Ah Pov on 1/18/16.
//  Copyright © 2016 webcash. All rights reserved.
//

import UIKit

@objc public protocol CAPSPageMenuDelegate {
    optional func willMoveToPage(controller: UIViewController, index: Int)
    optional func didMoveToPage(controller: UIViewController, index: Int)
}

class MenuItemView: UIView {
    var titleLabel : UILabel?
    func initNavMenuItemView(menuItemWidth: CGFloat, menuScrollViewHeight: CGFloat, indicatorHeight: CGFloat) {
        titleLabel = UILabel(frame: CGRectMake(0.0, 0.0, menuItemWidth, menuScrollViewHeight - indicatorHeight))
        self.addSubview(titleLabel!)
    }
}

enum RPageMenuOption {
    case SelectionIndicatorHeight(CGFloat)
    case ScrollMenuBackgroundColor(UIColor)
    case ContentScrollMenuBackgroundColor(UIColor)
    case ViewBackgroundColor(UIColor)
    case SelectionIndicatorColor(UIColor)
    case MenuMargin(CGFloat)
    case MenuHeight(CGFloat)
    case SelectedMenuItemLabelColor(UIColor)
    case UnselectedMenuItemLabelColor(UIColor)
    case MenuItemWidth(CGFloat)
    case EnableHorizontalBounce(Bool)
    case ScrollAnimationDurationOnMenuItemTap(Int)
    case CenterMenuItems(Bool)
}

class RPageMenu: UIViewController, UIScrollViewDelegate, UIGestureRecognizerDelegate {
    // MARK: - Properties
    
    var contentScrollView = UIScrollView()
    let menuScrollView = UIScrollView()
    var menuItems = Array<MenuItemView>()
    var numOfController = Array<UIViewController>()
    var menuItemWidths : [CGFloat] = []
    
    var menuHeight : CGFloat = 34.0
    var menuMargin : CGFloat = 0.0
    var menuItemWidth : CGFloat = 111.0
    var selectionIndicatorHeight : CGFloat = 2.0
    var totalMenuItemWidthIfDifferentWidths : CGFloat = 0.0
    var scrollAnimationDurationOnMenuItemTap : Int = 500 // Millisecons
    var startingMenuMargin : CGFloat = 0.0
    
    var selectionIndicatorView : UIView = UIView()
    
    var currentPageIndex : Int = 0
    var lastPageIndex : Int = 0
    
    var selectionIndicatorColor : UIColor = UIColor.whiteColor()
    var selectedMenuItemLabelColor : UIColor = UIColor.whiteColor()
    var unselectedMenuItemLabelColor : UIColor = UIColor.lightGrayColor()
    var scrollMenuBackgroundColor : UIColor = UIColor.blackColor()
    var controllerScrollBackgroundColor : UIColor = UIColor.whiteColor()
    var viewBackgroundColor : UIColor = UIColor.whiteColor()
    
    var menuItemFont : UIFont = UIFont.systemFontOfSize(15.0)
    
    var centerMenuItems : Bool = false
    var enableHorizontalBounce : Bool = true
    
    var currentOrientationIsPortrait : Bool = true
    var pageIndexForOrientationChange : Int = 0
    var didLayoutSubviewsAfterRotation : Bool = false
    var didScrollAlready : Bool = false
    
    var lastControllerScrollViewContentOffset : CGFloat = 0.0
    
    var lastScrollDirection : CAPSPageMenuScrollDirection = .Other
    var startingPageForScroll : Int = 0
    var didTapMenuItemToScroll : Bool = false
    
    var pagesAddedDictionary : [Int : Int] = [:]
    
    weak var delegate : CAPSPageMenuDelegate?
    
    var tapTimer : NSTimer?
    
    enum CAPSPageMenuScrollDirection : Int {
        case Left
        case Right
        case Other
    }
    
    // MARK: - View life cycle
    /**
        Initialize PageMenu with view controllers
    
        :param: viewControllers List of view controllers that must be subclasses of UIViewController
        :param: frame Frame for page menu view
        :param: options Dictionary holding any customization options user might want to set
    */
    
    init(viewControllers: [UIViewController], frame: CGRect, options: Dictionary<String,AnyObject>?) {
        super.init(nibName: nil, bundle: nil)
        numOfController = viewControllers
        self.view.frame = frame
    }
    
    convenience init(viewControllers: Array<UIViewController>, frame: CGRect, pageMenuOptions: Array<RPageMenuOption>?) {
        self.init(viewControllers:viewControllers, frame:frame, options:nil)
        
        if let options = pageMenuOptions {
            for option in options {
                switch (option) {
                case let .SelectionIndicatorHeight(value):
                    selectionIndicatorHeight = value
                case let .ScrollMenuBackgroundColor(value):
                    scrollMenuBackgroundColor = value
                case let .ContentScrollMenuBackgroundColor(value):
                    controllerScrollBackgroundColor = value
                case let .ViewBackgroundColor(value):
                    viewBackgroundColor = value
                case let .SelectionIndicatorColor(value):
                    selectionIndicatorColor = value
                case let .MenuMargin(value):
                    menuMargin = value
                case let .MenuHeight(value):
                    menuHeight = value
                case let .SelectedMenuItemLabelColor(value):
                    selectedMenuItemLabelColor = value
                case let .UnselectedMenuItemLabelColor(value):
                    unselectedMenuItemLabelColor = value
                case let .MenuItemWidth(value):
                    menuItemWidth = value
                case let .EnableHorizontalBounce(value):
                    enableHorizontalBounce = value
                case let .ScrollAnimationDurationOnMenuItemTap(value):
                    scrollAnimationDurationOnMenuItemTap = value
                case let .CenterMenuItems(value):
                    centerMenuItems = value
                }
            }
        }
        
        initialUserInterface()
        if menuScrollView.subviews.count == 0 {
            configureUserInterface()
        }
       
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    // MARK: - UI Setup
    
    func initialUserInterface() {
        
        // Set up controller scroll view
        contentScrollView.pagingEnabled = true
        contentScrollView.translatesAutoresizingMaskIntoConstraints = false
        contentScrollView.alwaysBounceHorizontal = enableHorizontalBounce
        contentScrollView.bounces = enableHorizontalBounce
        
        contentScrollView.frame = CGRectMake(0.0, menuHeight, self.view.frame.width, self.view.frame.height)
        self.view.addSubview(contentScrollView)
        
        
        let viewsDictionary = [
            "menuScrollView":menuScrollView,
            "contentScrollView":contentScrollView
        ]
        let contentScrollView_constraint_H:Array = NSLayoutConstraint.constraintsWithVisualFormat("H:|[contentScrollView]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: viewsDictionary)
        let contentScrollView_constraint_V:Array = NSLayoutConstraint.constraintsWithVisualFormat("V:|[contentScrollView]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: viewsDictionary)
        self.view.addConstraints(contentScrollView_constraint_H)
        self.view.addConstraints(contentScrollView_constraint_V)
        
        // Set up menu scroll view
        menuScrollView.translatesAutoresizingMaskIntoConstraints = false
        menuScrollView.frame = CGRectMake(0.0, 0.0, self.view.frame.width, menuHeight)
        self.view.addSubview(menuScrollView)
        
        let menuScrollView_constraint_H:Array = NSLayoutConstraint.constraintsWithVisualFormat("H:|[menuScrollView]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: viewsDictionary)
        let menuScrollView_constraint_V:Array = NSLayoutConstraint.constraintsWithVisualFormat("V:[menuScrollView(\(menuHeight))]", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: viewsDictionary)
        self.view.addConstraints(menuScrollView_constraint_H)
        self.view.addConstraints(menuScrollView_constraint_V)
        
        // Disable scroll bars
        menuScrollView.showsHorizontalScrollIndicator = false
        menuScrollView.showsVerticalScrollIndicator = false
        menuScrollView.backgroundColor = scrollMenuBackgroundColor
        contentScrollView.showsHorizontalScrollIndicator = false
        contentScrollView.showsVerticalScrollIndicator = false
        contentScrollView.backgroundColor = controllerScrollBackgroundColor
        
        // Set background color behind scroll views and for menu scroll view
        self.view.backgroundColor = viewBackgroundColor
        
    }
    
    func configureUserInterface() {
        
        let menuItemTapGestureRecognizer = UITapGestureRecognizer(target: self, action: Selector("handleMenuItemTap:"))
        menuItemTapGestureRecognizer.numberOfTapsRequired = 1
        menuItemTapGestureRecognizer.numberOfTouchesRequired = 1
        menuItemTapGestureRecognizer.delegate = self
        menuScrollView.addGestureRecognizer(menuItemTapGestureRecognizer)
        
        //Set delegate for controller scroll view
        contentScrollView.delegate = self
        
        // When the user taps the status bar, the scroll view beneath the touch which is closest to the status bar will be scrolled to top,
        // but only if its `scrollsToTop` property is YES, its delegate does not return NO from `shouldScrollViewScrollToTop`, and it is not already at the top.
        // If more than one scroll view is found, none will be scrolled.
        // Disable scrollsToTop for menu and controller scroll views so that iOS finds scroll views within our pages on status bar tap gesture.
        menuScrollView.scrollsToTop = false;
        contentScrollView.scrollsToTop = false;
        
        menuScrollView.contentSize = CGSizeMake((menuItemWidth + menuMargin) * CGFloat(numOfController.count) + menuMargin, menuHeight)
        
        
        // Configure controller scroll view content size
        contentScrollView.contentSize = CGSizeMake(self.view.frame.width * CGFloat(numOfController.count), 0.0)
        
        var index : CGFloat = 0.0
        
        for controller in numOfController {
            if index == 0.0 {
                addPageAtIndex(0)// Add first two controllers to scrollview and as child view controller
            }
            
            // Set up menu item for menu scroll view
            var menuItemFrame : CGRect = CGRect()
                if centerMenuItems && index == 0.0  {
                    startingMenuMargin = ((self.view.frame.width - ((CGFloat(numOfController.count) * menuItemWidth) + (CGFloat(numOfController.count - 1) * menuMargin))) / 2.0) -  menuMargin
                    
                    if startingMenuMargin < 0.0 {
                        startingMenuMargin = 0.0
                    }
                    
                    menuItemFrame = CGRectMake(startingMenuMargin + menuMargin, 0.0, menuItemWidth, menuHeight)
                } else {
                    menuItemFrame = CGRectMake(menuItemWidth * index + menuMargin * (index + 1) + startingMenuMargin, 0.0, menuItemWidth, menuHeight)
                }
            
            
            let menuItemView : MenuItemView = MenuItemView(frame: menuItemFrame)
           
            menuItemView.initNavMenuItemView(menuItemWidth, menuScrollViewHeight: menuHeight, indicatorHeight: selectionIndicatorHeight)
           
            // Configure menu item label font if font is set by user
            menuItemView.titleLabel!.font = menuItemFont
            
            menuItemView.titleLabel!.textAlignment = NSTextAlignment.Center
            menuItemView.titleLabel!.textColor = unselectedMenuItemLabelColor
          
            // Set title depending on if controller has a title set
            if controller.title != nil {
                menuItemView.titleLabel!.text = controller.title!
            } else {
                menuItemView.titleLabel!.text = "Menu \(Int(index) + 1)"
            }
            
            // Add menu item view to menu scroll view
            menuScrollView.addSubview(menuItemView)
            menuItems.append(menuItemView)
            
            index++
        }
        
      
        
        // Set selected color for title label of selected menu item
        if menuItems.count > 0 {
            if menuItems[currentPageIndex].titleLabel != nil {
                menuItems[currentPageIndex].titleLabel!.textColor = selectedMenuItemLabelColor
            }
        }
        
        // Configure selection indicator view
        var selectionIndicatorFrame : CGRect = CGRect()
        
        if centerMenuItems  {
            selectionIndicatorFrame = CGRectMake(startingMenuMargin + menuMargin, menuHeight - selectionIndicatorHeight, menuItemWidth, selectionIndicatorHeight)
        } else {
            selectionIndicatorFrame = CGRectMake(menuMargin, menuHeight - selectionIndicatorHeight, menuItemWidth, selectionIndicatorHeight)
        }
        
        selectionIndicatorView = UIView(frame: selectionIndicatorFrame)
        selectionIndicatorView.backgroundColor = selectionIndicatorColor
        menuScrollView.addSubview(selectionIndicatorView)
    }
    
    
    // MARK: - Scroll view delegate
    func scrollViewDidScroll(scrollView: UIScrollView) {
        if !didLayoutSubviewsAfterRotation {
            if scrollView.isEqual(contentScrollView) {
                if scrollView.contentOffset.x >= 0.0 && scrollView.contentOffset.x <= (CGFloat(numOfController.count - 1) * self.view.frame.width) {
                    if (currentOrientationIsPortrait && UIApplication.sharedApplication().statusBarOrientation.isPortrait) || (!currentOrientationIsPortrait && UIApplication.sharedApplication().statusBarOrientation.isLandscape) {
                        // Check if scroll direction changed
                        if !didTapMenuItemToScroll {
                            if didScrollAlready {
                                var newScrollDirection : CAPSPageMenuScrollDirection = .Other
                                
                                if (CGFloat(startingPageForScroll) * scrollView.frame.width > scrollView.contentOffset.x) {
                                    newScrollDirection = .Right
                                } else if (CGFloat(startingPageForScroll) * scrollView.frame.width < scrollView.contentOffset.x) {
                                    newScrollDirection = .Left
                                }
                                
                                if newScrollDirection != .Other {
                                    if lastScrollDirection != newScrollDirection {
                                        let index : Int = newScrollDirection == .Left ? currentPageIndex + 1 : currentPageIndex - 1
                                        
                                        if index >= 0 && index < numOfController.count {
                                            // Check dictionary if page was already added
                                            if pagesAddedDictionary[index] != index {
                                                addPageAtIndex(index)
                                                pagesAddedDictionary[index] = index
                                            }
                                        }
                                    }
                                }
                                
                                lastScrollDirection = newScrollDirection
                            }
                            
                            if !didScrollAlready {
                                if (lastControllerScrollViewContentOffset > scrollView.contentOffset.x) {
                                    if currentPageIndex != numOfController.count - 1 {
                                        // Add page to the left of current page
                                        let index : Int = currentPageIndex - 1
                                        
                                        if pagesAddedDictionary[index] != index && index < numOfController.count && index >= 0 {
                                            addPageAtIndex(index)
                                            pagesAddedDictionary[index] = index
                                        }
                                        
                                        lastScrollDirection = .Right
                                    }
                                } else if (lastControllerScrollViewContentOffset < scrollView.contentOffset.x) {
                                    if currentPageIndex != 0 {
                                        // Add page to the right of current page
                                        let index : Int = currentPageIndex + 1
                                        
                                        if pagesAddedDictionary[index] != index && index < numOfController.count && index >= 0 {
                                            addPageAtIndex(index)
                                            pagesAddedDictionary[index] = index
                                        }
                                        
                                        lastScrollDirection = .Left
                                    }
                                }
                                
                                didScrollAlready = true
                            }
                            
                            lastControllerScrollViewContentOffset = scrollView.contentOffset.x
                        }
                        
                        var ratio : CGFloat = 1.0
                        
                        
                        // Calculate ratio between scroll views
                        ratio = (menuScrollView.contentSize.width - self.view.frame.width) / (contentScrollView.contentSize.width - self.view.frame.width)
                        
                        if menuScrollView.contentSize.width > self.view.frame.width {
                            var offset : CGPoint = menuScrollView.contentOffset
                            offset.x = contentScrollView.contentOffset.x * ratio
                            menuScrollView.setContentOffset(offset, animated: false)
                        }
                        
                        // Calculate current page
                        let width : CGFloat = contentScrollView.frame.size.width;
                        let page : Int = Int((contentScrollView.contentOffset.x + (0.5 * width)) / width)
                        
                        // Update page if changed
                        if page != currentPageIndex {
                            lastPageIndex = currentPageIndex
                            currentPageIndex = page
                            
                            if pagesAddedDictionary[page] != page && page < numOfController.count && page >= 0 {
                                addPageAtIndex(page)
                                pagesAddedDictionary[page] = page
                            }
                            
                            if !didTapMenuItemToScroll {
                                // Add last page to pages dictionary to make sure it gets removed after scrolling
                                if pagesAddedDictionary[lastPageIndex] != lastPageIndex {
                                    pagesAddedDictionary[lastPageIndex] = lastPageIndex
                                }
                                
                                // Make sure only up to 3 page views are in memory when fast scrolling, otherwise there should only be one in memory
                                let indexLeftTwo : Int = page - 2
                                if pagesAddedDictionary[indexLeftTwo] == indexLeftTwo {
                                    pagesAddedDictionary.removeValueForKey(indexLeftTwo)
                                    removePageAtIndex(indexLeftTwo)
                                }
                                let indexRightTwo : Int = page + 2
                                if pagesAddedDictionary[indexRightTwo] == indexRightTwo {
                                    pagesAddedDictionary.removeValueForKey(indexRightTwo)
                                    removePageAtIndex(indexRightTwo)
                                }
                            }
                        }
                        
                        // Move selection indicator view when swiping
                        moveSelectionIndicator(page)
                    }
                } else {
                    var ratio : CGFloat = 1.0
                    ratio = (menuScrollView.contentSize.width - self.view.frame.width) / (contentScrollView.contentSize.width - self.view.frame.width)
                    if menuScrollView.contentSize.width > self.view.frame.width {
                        var offset : CGPoint = menuScrollView.contentOffset
                        offset.x = contentScrollView.contentOffset.x * ratio
                        menuScrollView.setContentOffset(offset, animated: false)
                    }
                }
            }
        } else {
            didLayoutSubviewsAfterRotation = false
            //Move selection indicator view when swiping
            moveSelectionIndicator(currentPageIndex)
        }
    }
    
    func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
        if scrollView.isEqual(contentScrollView) {
            let currentController = numOfController[currentPageIndex]
            delegate?.didMoveToPage?(currentController, index: currentPageIndex) // Call didMoveToPage delegate function
            // Remove all but current page after decelerating
            for key in pagesAddedDictionary.keys {
                if key != currentPageIndex {
                    removePageAtIndex(key)
                }
            }
            didScrollAlready = false
            startingPageForScroll = currentPageIndex
            pagesAddedDictionary.removeAll(keepCapacity: false)// Empty out pages in dictionary
        }
    }
    
    func scrollViewDidEndTapScrollingAnimation() {
        let currentController = numOfController[currentPageIndex]
        delegate?.didMoveToPage?(currentController, index: currentPageIndex) // Call didMoveToPage delegate function
        // Remove all but current page after decelerating
        for key in pagesAddedDictionary.keys {
            if key != currentPageIndex {
                removePageAtIndex(key)
            }
        }
        startingPageForScroll = currentPageIndex
        didTapMenuItemToScroll = false
        pagesAddedDictionary.removeAll(keepCapacity: false)// Empty out pages in dictionary
    }
    
    
    // MARK: - Handle Selection Indicator
    func moveSelectionIndicator(pageIndex: Int) {
        if pageIndex >= 0 && pageIndex < numOfController.count {
            UIView.animateWithDuration(0.15, animations: { () -> Void in
                let selectionIndicatorWidth : CGFloat = self.selectionIndicatorView.frame.width
                var selectionIndicatorX : CGFloat = 0.0
                
                if pageIndex == 0 {
                    selectionIndicatorX = 0.0
                } else {
                    selectionIndicatorX = self.menuItemWidth * CGFloat(pageIndex)
                }
                
                self.selectionIndicatorView.frame = CGRectMake(selectionIndicatorX, self.selectionIndicatorView.frame.origin.y, selectionIndicatorWidth, self.selectionIndicatorView.frame.height)
                
                // Switch newly selected menu item title label to selected color and old one to unselected color
                if self.menuItems.count > 0 {
                    if self.menuItems[self.lastPageIndex].titleLabel != nil && self.menuItems[self.currentPageIndex].titleLabel != nil {
                        self.menuItems[self.lastPageIndex].titleLabel!.textColor = self.unselectedMenuItemLabelColor
                        self.menuItems[self.currentPageIndex].titleLabel!.textColor = self.selectedMenuItemLabelColor
                    }
                }
            })
        }
    }
    
    
    // MARK: - Tap gesture recognizer selector
    
    func handleMenuItemTap(gestureRecognizer : UITapGestureRecognizer) {
        let tappedPoint : CGPoint = gestureRecognizer.locationInView(menuScrollView)
        
        if tappedPoint.y < menuScrollView.frame.height {
            
            // Calculate tapped page
            var itemIndex : Int = 0
            let rawItemIndex : CGFloat = ((tappedPoint.x - startingMenuMargin) - menuMargin / 2) / (menuMargin + menuItemWidth)
                
            // Prevent moving to first item when tapping left to first item
            if rawItemIndex < 0 {
                itemIndex = -1
            } else {
                itemIndex = Int(rawItemIndex)
            }
            
            if itemIndex >= 0 && itemIndex < numOfController.count {
                // Update page if changed
                if itemIndex != currentPageIndex {
                    startingPageForScroll = itemIndex
                    lastPageIndex = currentPageIndex
                    currentPageIndex = itemIndex
                    didTapMenuItemToScroll = true
                    
                    // Add pages in between current and tapped page if necessary
                    let smallerIndex : Int = lastPageIndex < currentPageIndex ? lastPageIndex : currentPageIndex
                    let largerIndex : Int = lastPageIndex > currentPageIndex ? lastPageIndex : currentPageIndex
                    
                    if smallerIndex + 1 != largerIndex {
                        for index in (smallerIndex + 1)...(largerIndex - 1) {
                            if pagesAddedDictionary[index] != index {
                                addPageAtIndex(index)
                                pagesAddedDictionary[index] = index
                            }
                        }
                    }
                    
                    addPageAtIndex(itemIndex)
                    
                    // Add page from which tap is initiated so it can be removed after tap is done
                    pagesAddedDictionary[lastPageIndex] = lastPageIndex
                }
                
                // Move controller scroll view when tapping menu item
                let duration : Double = Double(scrollAnimationDurationOnMenuItemTap) / Double(1000)
                
                UIView.animateWithDuration(duration, animations: { () -> Void in
                    let xOffset : CGFloat = CGFloat(itemIndex) * self.contentScrollView.frame.width
                    self.contentScrollView.setContentOffset(CGPoint(x: xOffset, y: self.contentScrollView.contentOffset.y), animated: false)
                })
                
                if tapTimer != nil {
                    tapTimer!.invalidate()
                }
                
                let timerInterval : NSTimeInterval = Double(scrollAnimationDurationOnMenuItemTap) * 0.001
                tapTimer = NSTimer.scheduledTimerWithTimeInterval(timerInterval, target: self, selector: "scrollViewDidEndTapScrollingAnimation", userInfo: nil, repeats: false)
            }
        }
    }
    
    
    // MARK: - Remove/Add Page
    func addPageAtIndex(index : Int) {
        // Call didMoveToPage delegate function
        let currentController = numOfController[index]
        delegate?.willMoveToPage?(currentController, index: index)
        
        let newVC = numOfController[index]
        newVC.willMoveToParentViewController(self)
        newVC.view.frame = CGRectMake(self.view.frame.width * CGFloat(index), menuHeight, self.view.frame.width, self.view.frame.height - menuHeight)
        
        self.addChildViewController(newVC)
        self.contentScrollView.addSubview(newVC.view)
        newVC.didMoveToParentViewController(self)
    }
    
    func removePageAtIndex(index : Int) {
        let oldVC = numOfController[index]
        oldVC.willMoveToParentViewController(nil)
        
        oldVC.view.removeFromSuperview()
        oldVC.removeFromParentViewController()
    }
    
    
}

class MyTransViewController: UIViewController {
    
    @IBOutlet weak var waitingContainer: UIView!
    @IBOutlet weak var completedContainer: UIView!
    @IBOutlet weak var cancelledContainer: UIView!
    
    var rPageMenu : RPageMenu?
    var numOfController:Array<UIViewController>!
    var navigationTitle:String = "My Transaction"
    var waitingTitle:String = "Waiting"
    var completedTitle:String = "Completed"
    var cancelledTitle:String = "Cancelled"
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController!.navigationBar.translucent = false
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController!.navigationBar.translucent = true
    }
   
    override func localize() {
        navigationTitle=Language.localizedStr("S07_01")
        waitingTitle = Language.localizedStr("S07_03")
        completedTitle = Language.localizedStr("S07_04")
        cancelledTitle = Language.localizedStr("S07_05")
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        localize()
        
        RLib.setNavigationTitle(self, title: navigationTitle)
        RLib.setLeftNavigationItem(self, image: "back_btn.png", action: Selector("handleLeftNavigationItemClick:"))
        RLib.setRightNavigationItem(self, image: "home_btn.png", action: Selector("handleRightNavigationItemClick:"))

        
        //TODO: Transaction
        /*
        0: 진행내역,        1: 완료내역,        2: 취소내역
        */
        numOfController = Array<UIViewController>()
        
        let waitingController : TransWaitingViewController = TransWaitingViewController(nibName: "TransWaitingViewController", bundle: nil)
        let completedController : TransCompletedViewController = TransCompletedViewController(nibName: "TransCompletedViewController", bundle: nil)
        let cancelledController : TransCancelViewController = TransCancelViewController(nibName: "TransCancelViewController", bundle: nil)
        
        waitingController.title = waitingTitle
        completedController.title = completedTitle
        cancelledController.title = cancelledTitle
        
        numOfController.append(waitingController)
        numOfController.append(completedController)
        numOfController.append(cancelledController)
        
        
        let parameters: Array<RPageMenuOption> = [
            .ContentScrollMenuBackgroundColor(UIColor(red: 223/255, green: 227/255, blue: 229/255, alpha: 1.0)),
            .ScrollMenuBackgroundColor(UIColor.whiteColor()),
            .SelectedMenuItemLabelColor(UIColor(red: 42/255, green: 87/255, blue: 118/255, alpha: 1.0)),
            .UnselectedMenuItemLabelColor(UIColor(red: 121/255, green: 124/255, blue: 128/255, alpha: 1.0)),
            .SelectionIndicatorColor(UIColor(red: 42/255, green: 87/255, blue: 118/255, alpha: 1.0)),
            .MenuHeight(40.0),
            .MenuItemWidth(self.view.frame.width / 3),
            .CenterMenuItems(true),
            .SelectionIndicatorHeight(2.0)
        ]
        
        rPageMenu = RPageMenu(viewControllers: numOfController, frame: CGRectMake(0.0, 0.0, self.view.frame.width, self.view.frame.height), pageMenuOptions: parameters)
        self.addChildViewController(rPageMenu!)
        self.view.addSubview(rPageMenu!.view)
        rPageMenu!.didMoveToParentViewController(self)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func handleLeftNavigationItemClick(sender:UIBarButtonItem){
        //POP TO ROOT
        navigationController?.popToRootViewControllerAnimated(true)
    }
    
    /*
        TODO: Right navigation item
    */
    func handleRightNavigationItemClick(sender:UIBarButtonItem){
        //POP TO ROOT
        navigationController?.popToRootViewControllerAnimated(true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
