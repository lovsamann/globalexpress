//
//  EditedInfoManagerViewController.swift
//  GlobalExpress
//
//  Created by UDAM on 2/19/16.
//  Copyright © 2016 webcash. All rights reserved.
//

import UIKit

class EditedInfoManagerViewController: UIViewController {

    //MARK: - IBOutlet
    // -------------------------------------------------------------------------------
    //	IBOutlet label and button , UIView, Constraint
    // -------------------------------------------------------------------------------
    @IBOutlet weak var topTitleLabel    : UILabel!
    @IBOutlet weak var topLabel         : UILabel!
    @IBOutlet weak var nameKOLabel      : UILabel!
    @IBOutlet weak var nameENLabel      : UILabel!
    @IBOutlet weak var nameKOResult     : UILabel!
    @IBOutlet weak var nameENResult     : UILabel!
    @IBOutlet weak var dobLabel         : UILabel!
    @IBOutlet weak var dobResult        : UILabel!
    @IBOutlet weak var countryLabel     : UILabel!
    @IBOutlet weak var countryResult    : UILabel!
    @IBOutlet weak var phoneLabel       : UILabel!
    @IBOutlet weak var phoneResult      : UILabel!
    @IBOutlet weak var securityNoLabel  : UILabel!
    @IBOutlet weak var securityNoResult : UILabel!
    @IBOutlet weak var expireDateLabel  : UILabel!
    @IBOutlet weak var expireDateResult : UILabel!
    @IBOutlet weak var line6View        : UIView!
    
    // -------------------------------------------------------------------------------
    //	IBOutlet UIScrollView
    // -------------------------------------------------------------------------------
    @IBOutlet weak var Scroller         : UIScrollView!
    
    // -------------------------------------------------------------------------------
    // UIButton
    // -------------------------------------------------------------------------------
    @IBOutlet weak var confirmButton: UIButton!
    
    // -------------------------------------------------------------------------------
    // IBOutlet UIImageView
    // -------------------------------------------------------------------------------
    @IBOutlet weak var img6: UIImageView!
    @IBOutlet weak var img7: UIImageView!
    
    // -------------------------------------------------------------------------------
    // NSLayoutConstraint
    // -------------------------------------------------------------------------------
    @IBOutlet weak var bottomNextButtonConstraint   : NSLayoutConstraint!
    @IBOutlet weak var heightCenterViewConstraint   : NSLayoutConstraint!
    @IBOutlet weak var heightNameENLabelConstraint  : NSLayoutConstraint!
    @IBOutlet weak var heightNameENResultConstraint : NSLayoutConstraint!
    @IBOutlet weak var widthViewPwdConstraint       : NSLayoutConstraint!
    
    //MARK: - Properties
    // -------------------------------------------------------------------------------
    // NSDicionary
    // -------------------------------------------------------------------------------
    var passData            : NSDictionary?
    var phoneString         : NSString?
    private var isKorean: Bool = false
    
    // MARK: - Private function
    // -------------------------------------------------------------------------------
    // Localizing language
    // -------------------------------------------------------------------------------
    override func localize() {
    
        topTitleLabel.text          = Language.localizedStr("S14_01")
        topLabel.text               = Language.localizedStr("S14_02")
        nameKOLabel.text            = Language.localizedStr("S14_03")
        nameENLabel.text            = Language.localizedStr("S14_03")
        dobLabel.text               = Language.localizedStr("S14_04")
        countryLabel.text           = Language.localizedStr("S14_05")
        phoneLabel.text             = Language.localizedStr("S14_06")
//        expireDateLabel.text         = Language.localizedStr("")
        confirmButton.setTitle(Language.localizedStr("S14_09"), forState: .Normal)
        
        securityNoLabel.text        = Language.localizedStr("S14_07")
        
    }
    
    // -------------------------------------------------------------------------------
    // Customize View Layout
    // -------------------------------------------------------------------------------
    func customizeViewLayout(){
        
        self.navigationController?.navigationBarHidden=true
        
        //Customize button position
//        if UIDevice().modelName == "iPhone 6" {
//            bottomNextButtonConstraint.constant = 17 - 36 - 47
//        }
//        
//        if UIDevice().modelName == "iPhone 6 Plus" {
//            bottomNextButtonConstraint.constant = 17 - 105 - 47
//        }

        if DeviceType.IS_IPHONE_5 {
            bottomNextButtonConstraint.constant = 507 - 26
        }
        
        if DeviceType.IS_IPHONE_6 {
            bottomNextButtonConstraint.constant = 507 + 83
            Scroller.scrollEnabled = false
        }
        
        if DeviceType.IS_IPHONE_6P {
            bottomNextButtonConstraint.constant = 507 + 152
            Scroller.scrollEnabled = false
        }
        
        heightCenterViewConstraint.constant     = 311 - 44
        line6View.hidden                        = true
    }
    
    // -------------------------------------------------------------------------------
    // setResultToView: Main function to set view which is communicated with server
    // -------------------------------------------------------------------------------
    func setResultToView() {
        
        //Format PassportNo and Social SecurityNumber
        if isKorean {
            
            securityNoLabel.text        = Language.localizedStr("S14_07")
            
             //Format PassportNo and Social SecurityNumber
            let securityNo  = passData!["RESIDENT_NO"]! as! NSString

            if securityNo.length < 6 {
                
                self.securityNoResult.text  = "\(securityNo)-"
            }else{
                
                let result      = securityNo.substringWithRange(NSRange(location: 0, length: 6))
                img6.hidden = false
                img7.hidden = false
                widthViewPwdConstraint.constant    = 85
                self.securityNoResult.text  = "\(result)-"
                
            }
        }
        else{
            
            securityNoLabel.text        = Language.localizedStr("M03_2_07")
            
            //Customize viewLayout
            heightNameENLabelConstraint.constant    = 18 - 44
            heightNameENResultConstraint.constant   = 18 - 44
            nameKOLabel.hidden                      = true
            nameKOResult.hidden                     = true
            
             //Format PassportNo and Social SecurityNumber
            let passportNo  = passData!["PASSPORT_NO"]! as! NSString

            if passportNo.length < 5 {
                
                self.securityNoResult.text  = "\(passportNo)-"
            }else{
                
                let result1     = passportNo.substringWithRange(NSRange(location: 0, length: 5))
                
                img6.hidden = true
                img7.hidden = true
                widthViewPwdConstraint.constant    = 85 - 23
                self.securityNoResult.text  = "\(result1)-"
                
            }
            
        }

        //Fill dateOfBirth Label
        let stringDate = passData!["BIRTH_YMD"] as! NSString
        
        if stringDate.length < 8 {
            self.dobResult.text = "\(self.passData!["BIRTH_YMD"])"
        }
        
        let year    = stringDate.substringWithRange(NSRange(location: 0, length: 4))
        let month   = stringDate.substringWithRange(NSRange(location: 4, length: 2))
        let day     = stringDate.substringWithRange(NSRange(location: 6, length: 2))
        
        //Passport ExpireDate
        let stringDate1 = passData!["PASSPORT_EXPRDT"] as! NSString
        
        if stringDate1.length < 8 {
            self.expireDateResult.text = "\(self.passData!["PASSPORT_EXPRDT"])"
        }
        
        let year1   = stringDate1.substringWithRange(NSRange(location: 0, length: 4))
        let month1  = stringDate1.substringWithRange(NSRange(location: 4, length: 2))
        let day1    = stringDate1.substringWithRange(NSRange(location: 6, length: 2))

        self.nameKOResult.text      = "\(passData!["KOR_NM"]!)"
        self.nameENResult.text      = "\(passData!["CUST_NM"]!)"
        self.dobResult.text         = "\(year).\(month).\(day)"
        self.countryResult.text     = "\(passData!["NATION_NM"]!)"
        self.phoneResult.text       = "\(passData!["INTL_TELCD"]!)-\(self.phoneString!)"
        self.expireDateResult.text  = "\(year1).\(month1).\(day1)"
        
    }
    
    //MARK: - View Life Cycle
    // -------------------------------------------------------------------------------
    // ViewDidLoad
    // -------------------------------------------------------------------------------
    override func viewDidLoad() {
        super.viewDidLoad()

        //Localize
        localize()
        
        //Customize
        customizeViewLayout()
        
    }
    
    // -------------------------------------------------------------------------------
    // viewWillAppear
    // -------------------------------------------------------------------------------
    override func viewWillAppear(animated: Bool) {
        
        if passData == nil {
            return
        }

        print("-----\(passData)")
        //Set isKorean true/false
        (passData!["NATION_GB"]! as! String != "0") ? (isKorean = false) : (isKorean = true)
        
        //SetResultToView
        setResultToView()
        
    }

    //MARK: - IBAction
    // -------------------------------------------------------------------------------
    //	IBOutlet Click to view MyInfoManagerViewController
    // -------------------------------------------------------------------------------
    @IBAction func confirmClicked(sender: UIButton) {
        
        /////////// USE THIS FORMAT
        let transaction = TransactionClass()
        transaction.sendAPI(APICode.PROFILE_R001, argument: ["" : ""],
            success: { (success) -> Void in
                
                if let rslt_cd = success["RSLT_CD"] as? String {
                    
                    if rslt_cd != "0000" { //오류
                        if let rsltMsg = success["RSLT_MSG"] as? String {
                            
                            print("Error Code : \(rslt_cd), Error Message : \(rsltMsg)")
                            self.showEventAlert("Error Message :\(rsltMsg)", hasCancelButton: false, delegate: self, tag: nil, confirmBlock: nil, cancelBlock: nil)
                            
                        }
                    } else { //정상
                        if let respData = success["RESP_DATA"] as? NSDictionary {
                            
                            print("Sucess Code : \(rslt_cd), Message Data : \(respData)")
                            
                            let myInfoManagerStroyboard     = UIStoryboard(name: "MyInfoManager", bundle: NSBundle.mainBundle())
                            let myInfoManagerController = myInfoManagerStroyboard.instantiateInitialViewController() as? MyInfoManagerViewController
                            guard let myInfoManagerRootViewController = myInfoManagerController else{
                                print("instanitateViewController nil. please check it out.")
                                return
                        }
                            
                        myInfoManagerController!.responseData = respData
                        
                        self.navigationController?.pushViewController(myInfoManagerRootViewController, animated: true)
                            
                    }
                }
                    
            }
        })
    }

}
