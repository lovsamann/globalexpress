//
//  VerificationViewController.swift
//  GlobalExpress
//
//  Created by Ah Pov on 1/19/16.
//  Copyright © 2016 webcash. All rights reserved.
//

import UIKit

class VerificationViewController: UIViewController,UITextFieldDelegate{
    //OUTLET
    @IBOutlet weak var tfVerifyNo: UITextField!
    @IBOutlet weak var lbTimer: UILabel!
    @IBOutlet weak var btnResend: UIButton!
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var lblStep: UILabel!
    
    //GESTURE
    let viewPress:UITapGestureRecognizer = UITapGestureRecognizer()
    
    //Variable
    var transerial:String?
    var custname = [String]()
    var totalRemaining:Int?
    var minute:Int?
    var second:Int?
    var timer:NSTimer?
    var newLength:Int?
    var navigationTitle:String = "Enter Varification No."
    var isTimer:Bool = true
    var nation_gb:String?
    
    override func localize() {
        navigationTitle = Language.localizedStr("T07_01")
        lblStep.text = "3. \(Language.localizedStr("M01_06"))"
        lbTimer.text = Language.localizedStr("T07_03")
        btnNext.setTitle(Language.localizedStr("T07_04"), forState: .Normal)
        btnResend.setTitle(Language.localizedStr("T07_02"), forState: .Normal)
    }
    
    override func viewWillAppear(animated: Bool) {
        viewPress.addTarget(self, action: "handleKeyboardHide")
        self.view.addGestureRecognizer(viewPress)
        self.navigationController!.navigationBar.translucent = true
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        isTimer = false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        localize()
        
        self.navigationController!.navigationBar.translucent = false
        RLib.setNavigationTitle(self, title: navigationTitle)
        RLib.setLeftNavigationItem(self, image: "back_btn.png", action: Selector("handleLeftNavigationItemClick:"))
        self.timerStart()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func handleLeftNavigationItemClick(sender:UIBarButtonItem){
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    
    //Gesture handler
    func handleKeyboardHide(){
        self.tfVerifyNo.resignFirstResponder()
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        tfVerifyNo.resignFirstResponder()
        return true
    }
    
    //func timer start
    func timerStart(){
        tfVerifyNo.userInteractionEnabled = true
        tfVerifyNo.becomeFirstResponder()
        tfVerifyNo.text = ""
        btnResend.userInteractionEnabled = false
        //btnNext.userInteractionEnabled = false
        totalRemaining = 180
        timer = NSTimer.scheduledTimerWithTimeInterval(1, target: self, selector: "timerCounter", userInfo: nil, repeats: true)
    }
    
    func timerRestart(){
        tfVerifyNo.userInteractionEnabled = true
        totalRemaining = 0
        
        //REQUEST FOR RE-SEND
        self.timerStart()
    }
    
    func timerCounter(){
        totalRemaining! -= 1
        minute = totalRemaining! / 60
        second = totalRemaining! - (minute! * 60)
        /*
            KOR = 남은 시간 2분 53초
            ENG = Remaining Time 2 mins 53 secs
        */
        let LangCd = LanguageCode()
        let selectedLanguage = NSUserDefaults().objectForKey("UserSavedLanguage")
        if let selectedLang = selectedLanguage{
            if selectedLang as! String == LangCd.Korea{
                lbTimer.text = "남은 시간 \(minute!)분 \(second!)초"
            }else if selectedLang as! String == LangCd.English{
                lbTimer.text = "Remaining Time \(minute!) mins \(second!) secs"
            }else if selectedLang as! String == LangCd.Cambodia{
                lbTimer.text = "Remaining Time \(minute!) mins \(second!) secs"
            }
        }
        if isTimer==true{
            if totalRemaining == 0{
                timer?.invalidate()
                tfVerifyNo.resignFirstResponder()
                self.showSimpleAlert("Please click Re-send to get certification number.", delegate: self)
                lbTimer.text = "Please click resend!"
                btnResend.userInteractionEnabled = true
                tfVerifyNo.userInteractionEnabled = false
            }
        }
    }
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
        //buttonStatusCheck()
        return true
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        newLength = (textField.text?.utf16.count)! + string.utf16.count - range.length
        //IF BACK KEY PRESS
        //buttonStatusCheck()
        if range.length == 1{
            //buttonStatusCheck()
        }
        return newLength > 6 ? false : true
    }
    
    func buttonStatusCheck(){
        if newLength >= 6{
            btnNext.userInteractionEnabled = true
        }
        else{
            btnNext.userInteractionEnabled = false
        }
    }
    
    @IBAction func actionResend(sender: AnyObject) {
        self.timerRestart()
    }
    
    @IBAction func ActionSubmit(sender: AnyObject) {
        guard newLength >= 6 else{
            self.showSimpleAlert("This certification number input is required.", delegate: self)
            return
        }
        
        //TODO : Submit verification code
        TransactionClass().sendAPI(APICode.HPCERT_C001, argument:["CERT_TXNO":self.transerial! ,"AUTH_NO":self.tfVerifyNo.text!], success: { (success) -> Void in
            guard let code = success["RSLT_CD"] where code as! String == "0000" else{
                //Should be alert here
                self.showSimpleAlert(success["RSLT_MSG"] as! String, delegate: self)
                self.tfVerifyNo.becomeFirstResponder()
                return
            }
            print("RESP DATA = \(success)")
            self.performSegueWithIdentifier("windrawaccregistersegue", sender: self)
        })
    }
    
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        //TODO : Pass customer name to windrawal
        if segue.identifier == "windrawaccregistersegue"{
            let windraw = segue.destinationViewController as? WidrawAccRegisterViewController
            windraw?.custname = self.custname
            windraw?.nation_gb = self.nation_gb
            
        }
    }
    

   
    
}
