//
//  RecipientInfoTableViewCell.swift
//  GlobalExpress
//
//  Created by Ann on 3/2/16.
//  Copyright © 2016 webcash. All rights reserved.
//

import UIKit

class RecipientInfoTableViewCell: UITableViewCell {

    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var valueLabel: UILabel!
}
