//
//  ListRecipientTableViewCell.swift
//  GlobalExpress
//
//  Created by Ann on 3/1/16.
//  Copyright © 2016 webcash. All rights reserved.
//

import UIKit

class ListRecipientTableViewCell: UITableViewCell {

    @IBOutlet var recipientNameLabel: UILabel!
    @IBOutlet var nationalityLabel: UILabel!
    @IBOutlet var recevingType: UILabel!
    @IBOutlet var acountNoLabel: UILabel!
}
