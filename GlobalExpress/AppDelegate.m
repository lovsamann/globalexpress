//
//  AppDelegate.m
//  GlobalExpress
//
//  Created by JangWooil on 2015. 12. 29..
//  Copyright © 2015년 webcash. All rights reserved.
//

#import "AppDelegate.h"
//#import "MFSideMenuContainerViewController.h"
#import "SessionManager.h"
#import "Language.h"
#import "GlobalExpress-Swift.h"
#import "mVaccine.h"
#import "SysUtil.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import "BSAppIron.h"

#define kUpdateAlertTag 9999
#define kNoticeAlertTag 9998
#define kSystemErrAlertTag 8888

@interface AppDelegate ()
@end

@implementation AppDelegate
{
    NSString *notice_close_yn; //공지사항 보여주고 강제종료할 지 여부
    KYDrawerController *kyDrawerController;
    
}


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    // Push Delegate를 먼저 설정 해주자.
    [WCPushService sharedPushService].delegate = self;
    if (!IS_OS_8_BEFORE) { //iOS 8 이후 버전인 경우 UIUserNotification으로 Push 설정을 세팅한다.
        UIUserNotificationType userNotificationTypes = (UIUserNotificationTypeAlert |
                                                        UIUserNotificationTypeBadge |
                                                        UIUserNotificationTypeSound);
        UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:userNotificationTypes categories:nil];
        
        [application registerUserNotificationSettings:settings];
        [application registerForRemoteNotifications];
    }
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(PushStartNotification:) name:kPushStartNotification object:nil];
    [[WCPushService sharedPushService] startPushService:launchOptions serverAddress:kPushServerAddress types:(UIRemoteNotificationTypeBadge|UIRemoteNotificationTypeSound|UIRemoteNotificationTypeAlert)];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loginSuccess) name:@"loginSuccess" object:nil]; //로그인
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showAlert:) name:kShowAlert object:nil]; // Alert 메시지
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(closeLeftMenu) name:@"closeLeftMenu" object:nil]; //사이드 메뉴 닫기
    
    // 탈옥 및 위변조 검증
#if TARGET_IPHONE_SIMULATOR
    
#else
//    [self authApp];
#endif
    
    //사용자 언어 확인
    NSString *userSavedLang = [[NSUserDefaults standardUserDefaults] objectForKey:kUserSavedLanguage];
    
    if (userSavedLang != nil) {
        if ([userSavedLang isEqualToString:kKoreaCode]) {
            [SessionManager sharedSessionManager].language = kKoreaCode;
        } else if([userSavedLang isEqualToString:kCambodiaCode]){
            [SessionManager sharedSessionManager].language = kCambodiaCode;
        } else {
            [SessionManager sharedSessionManager].language = kEnglishCode;
        }
    } else { //영어를 Default로 설정
        [SessionManager sharedSessionManager].language = kEnglishCode;
    }
    
    // Reachability 체크 (인터넷 되는지 여부)
    if ([self canConnectNetwork] == NO) {
        NSDictionary   *userInfo = [[NSDictionary alloc] initWithObjectsAndKeys:[Language localizedStr:@"MSG_02"], @"alert_msg", [NSString stringWithFormat:@"%d", kSystemErrAlertTag], @"tag", nil];
        [[NSNotificationCenter defaultCenter] postNotificationName:kShowAlert object:nil userInfo:userInfo];
        
        return NO;
    }
    
    //탈옥체크 후 앱 자동 종료
    if ([[mVaccine sharedInstance] isJailBreak]) {
        NSDictionary   *userInfo = [[NSDictionary alloc] initWithObjectsAndKeys:[Language localizedStr:@"MSG_03"], @"alert_msg", [NSString stringWithFormat:@"%d", kSystemErrAlertTag], @"tag", nil];
        [[NSNotificationCenter defaultCenter] postNotificationName:kShowAlert object:nil userInfo:userInfo];
        
        return NO;
    }
    
    //MG 전문 체크
    TransactionClass *transClass = [[TransactionClass alloc] init];
    [transClass sendMGTransaction:^(NSDictionary *rsltDic){
        
        //현재 버전
        NSString *strBuildVer = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"];
        int nBuildVer = [self versionToInteger:strBuildVer];
        int minBuildVer = [self versionToInteger:[rsltDic objectForKey:@"c_minimum_ver"]];
        NSString *strUpdMsg  = [rsltDic objectForKey:@"c_update_act"];
        strUpdMsg            = [strUpdMsg stringByReplacingOccurrencesOfString:@"+" withString:@" "];
        NSString *strNoticeMsg = [rsltDic objectForKey:@"c_act"];
        strNoticeMsg           = [strNoticeMsg stringByReplacingOccurrencesOfString:@"+" withString:@" "];
        
        [SessionManager sharedSessionManager].appStoreURL = [rsltDic objectForKey:@"c_appstore_url"];
        [SessionManager sharedSessionManager].contentServerURL = [rsltDic objectForKey:@"c_bizplay_url"];
        [SessionManager sharedSessionManager].updateCloseYN    = [rsltDic objectForKey:@"c_update_close"];
        
        //최소 버전보다 낮은 경우
        if (minBuildVer > nBuildVer) {
            dispatch_async(dispatch_get_main_queue(), ^{
                NSDictionary   *userInfo = [[NSDictionary alloc] initWithObjectsAndKeys:strUpdMsg, @"alert_msg", [NSString stringWithFormat:@"%d", kUpdateAlertTag], @"tag", nil];
                [[NSNotificationCenter defaultCenter] postNotificationName:kShowAlert object:nil userInfo:userInfo];
            });
        } else {
            //공지사항이 있는지 확인한다.
            if ([[rsltDic objectForKey:@"c_act_yn"] isEqualToString:@"Y"]
                && [rsltDic objectForKey:@"c_act"] != nil
                && ![[rsltDic objectForKey:@"c_act"] isEqualToString:@""]) {
                notice_close_yn = [rsltDic objectForKey:@"c_act_close"]; //공지사항 보여주고 강제 종료할 지의 여부
                
                
                NSDictionary   *userInfo = [[NSDictionary alloc] initWithObjectsAndKeys:strNoticeMsg, @"alert_msg", [NSString stringWithFormat:@"%d", kNoticeAlertTag], @"tag", nil];
                [[NSNotificationCenter defaultCenter] postNotificationName:kShowAlert object:nil userInfo:userInfo];
            }
            
            //left 메뉴 세팅
            if ([rsltDic objectForKey:@"left_menu_info"] != nil) {
                NSArray *arrLeftMenu = [rsltDic objectForKey:@"left_menu_info"];
                [SessionManager sharedSessionManager].leftMenuArr = arrLeftMenu;
                
#if _DEBUG_
                NSLog(@"arrLeftMenu %@", arrLeftMenu.description);
#endif
                //Left Menu의 데이터를 refresh 한다
                [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshSideMenuData" object:nil];
                [[NSNotificationCenter defaultCenter]postNotificationName:@"refreshHomeVC" object:nil];
            }
        }
    }];
    
    //facebook analytics users
    [[FBSDKApplicationDelegate sharedInstance]application:application didFinishLaunchingWithOptions:launchOptions];
    
    return YES;
}


- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    return [[FBSDKApplicationDelegate sharedInstance]application:application openURL:url sourceApplication:sourceApplication annotation:annotation];
}

- (void)showAlert: (NSNotification *)note {
    NSString *strTag = [[note userInfo] objectForKey:@"tag"];
    NSString *strMessage = [[note userInfo] objectForKey:@"alert_msg"];
    
    if ([SysUtil IS_BEFORE_OS_8]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[Language localizedStr:@"MSG_01"] //@"안내"
                                                        message:strMessage
                                                       delegate:self
                                              cancelButtonTitle:[Language localizedStr:@"S20_06"] //@"확인"
                                              otherButtonTitles:nil];
        if (strTag != nil) {
            alert.tag = [[[note userInfo] objectForKey:@"tag"] intValue];
        }
        [alert show];
    } else {
        dispatch_async(dispatch_get_main_queue(), ^(void){
            UIAlertController *alertController;
            if (strTag != nil) {
                // 강제종료
                if ([strTag intValue] == kSystemErrAlertTag) {
                    alertController = [SysUtil getAlertControllerWithmessage:strMessage hasCancelButton:false confirmSelectBlock:^(NSString *strMsg){
                        exit(0);
                    } cancelSelectBlock:nil];
                } else {
                    alertController = [SysUtil getAlertControllerWithmessage:strMessage];
                }
            } else {
                alertController = [SysUtil getAlertControllerWithmessage:strMessage];
            }
            NSDictionary *alertDic = [[NSDictionary alloc] initWithObjectsAndKeys:alertController, @"alertController", nil];
            
            [[NSNotificationCenter defaultCenter] postNotificationName:kShowAlertMain object:nil userInfo:alertDic];
        });
    }
}

- (int)versionToInteger: (NSString *)version {
    version = [version stringByReplacingOccurrencesOfString:@"." withString:@""];
    return [version intValue];
}

#pragma mark - UIAlertView delegate implementation

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (alertView.tag) {
        case kUpdateAlertTag: //강제 업데이트일 경우(9999)
            //URL OPEN 한다.
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[[SessionManager sharedSessionManager] appStoreURL]]];
            
            if ([[SessionManager sharedSessionManager].updateCloseYN isEqualToString:@"Y"]) {
                exit(0);
            }
            break;
        case kNoticeAlertTag: // 공지사항일 경우
            if ([notice_close_yn isEqualToString:@"Y"]) {
                exit(0);
            }
            break;
        case kSystemErrAlertTag: // 시스템 에러(인터넷 사용 불가 등)
            exit(0);
        default:
            break;
    }
}

#pragma mark -

//20140617 check
- (BOOL)canConnectNetwork {
    wifiReach = [Reachability reachabilityForInternetConnection];
    
    //Notification을 받기 위해 HostName으로 Reachability를 설정하자.
    hostReach = [Reachability reachabilityWithHostName:_SM_GATEWAY_URL];
    
    if ([wifiReach currentReachabilityStatus] == NotReachable) {
        return NO;
    } else {
        return YES;
    }
}


- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotiAPNS object:self userInfo:userInfo];
}

//- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler{
//    
//    [[NSNotificationCenter defaultCenter] postNotificationName:kNotiAPNS object:self userInfo:userInfo];
//    
//}

//왼쪽 메뉴 닫기
- (void)closeLeftMenu {
    [kyDrawerController closeDrawerWithAnimation:YES];
}

- (void)loginSuccess {

    UIStoryboard *mainStoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    kyDrawerController = [mainStoryBoard instantiateViewControllerWithIdentifier:@"KYDrawerController"];

    self.window.rootViewController = nil;
    self.window.rootViewController = kyDrawerController;
    
    [self.window makeKeyAndVisible];
    [self.window becomeKeyWindow];
    
    [[NSUserDefaults standardUserDefaults] setValue:@"1" forKey:@"isLogged"];
    
}

// 위변조 검증
- (BOOL)authApp {
    //    NSLog(@"AppIron Integrity Verify");
    
    //탈옥체크 후 앱 자동 종료
    if ([[mVaccine sharedInstance] isJailBreak]) {
        NSDictionary   *userInfo = [[NSDictionary alloc] initWithObjectsAndKeys:[Language localizedStr:@"MSG_03"], @"alert_msg", [NSString stringWithFormat:@"%d", kSystemErrAlertTag], @"tag", nil];
        [[NSNotificationCenter defaultCenter] postNotificationName:kShowAlert object:nil userInfo:userInfo];
        
        return NO;
    }
    
    /* 검증 파라미터 초기화
     *
     * appironUrl -> 무결성 검증 URL
     */
    
    NSString *appironUrl = kAppIronAuthServerURL;
    //    NSLog(@"URL : %@",appironUrl);
    double time = CFAbsoluteTimeGetCurrent();
    /* 무결성 검증 호출 */
    BSAppIron *appiron = [[BSAppIron alloc] init];
    
    
    NSString *appironResult = @"";
    
    appironResult = [appiron authApp:appironUrl];
    
    double total = CFAbsoluteTimeGetCurrent() - time;
    
    //    NSLog(@"RES : %@",appironResult);
    //    NSLog(@"%f ms",total);
    //    NSLog(@"Test for Detecting AppIron");
    
    BOOL _isValidationOk = YES;
    
    if ([appironResult isEqualToString:@"0000"] == NO) {
        
        _isValidationOk = NO;
        
        NSDictionary   *userInfo = [[NSDictionary alloc] initWithObjectsAndKeys:[NSString stringWithFormat:@"위변조 검증 중 오류가 발생하였습니다(%@)",appironResult], @"alert_msg", [NSString stringWithFormat:@"%d", kSystemErrAlertTag], @"tag", nil];
        [[NSNotificationCenter defaultCenter] postNotificationName:kShowAlert object:nil userInfo:userInfo];
    }
    
    [SessionManager sharedSessionManager].sessionId = [appiron getSessionId];
    [SessionManager sharedSessionManager].token     = [appiron getToken];
    
    return _isValidationOk;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    
    //facebook analytics users
    [FBSDKAppEvents activateApp];
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

#pragma mark -
#pragma mark WCPushDelegate
- (void)PushStartNotification:(NSNotification *)note {
    NSString *registerKey = [[note userInfo] objectForKey:@"registerKey"];
    [[WCPushService sharedPushService] registerAPNKey:registerKey companyID:@"WEBCASH"];
}

-(void) returnAPN:(WCPushService *)returnAPN statusCode:(WCPushStatus)statusCode errorCode:(WCPushError)errorCode errorMessage:(NSString *)errorMessage{
    switch (statusCode) {
        case WCPUSH_STATUS_MESSAGE: // Push Message 인 RemoteMessage Local Message가 넘어 온다.
            if (errorCode == WCPUSH_ERROR_NONE) {
                // notificationAPN 는 해당 Push 메세지를 담고 있는  NSDictionary 이다.
                NSLog(@"WCPush APN Message[%@]", returnAPN.notificationAPN);

                if ([[UIApplication sharedApplication] applicationState] == UIApplicationStateInactive) { //
                    [[NSUserDefaults standardUserDefaults] setObject:returnAPN.notificationAPN forKey:@"notiDic"];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    [[NSNotificationCenter defaultCenter] postNotificationName:kNotiAPNS object:self userInfo:returnAPN.notificationAPN];
                }
                
                
            }
            break;
            
        case WCPUSH_STATUS_ERROR: // Push 설정 및 관련 오류가 넘어온다.
            //            [SysUtils showMessage:@"PUSH 서버와의 통신이 원할하지 않습니다. 원할한 서비스 이용을 위하여 재 로그인하여 주시기 바랍니다."];
            
            switch (errorCode) {
                case WCPUSH_ERROR_NONE:
                    //					NSLog(@"WCPush Error None");
                    break;
                case WCPUSH_ERROR_TURNOFFPUSH:
                    //					NSLog(@"WCPush Error Message[%@]", errorMessage);
                    break;
                default:
                    
                    //					NSLog(@"WCPush Error Message[%@]", errorMessage);
                    break;
            }
            break;
            
        case WCPUSH_STATUS_INFO: // 정보성 로그가 넘어온다.
            NSLog(@"Log Message[%@]", errorMessage);
            break;
            
        case WCPUSH_STATUS_GET_TOKENDEVICE: // 디바이스 토큰을 획득 했을 경우 넘어온다.
            NSLog(@"토큰 획득 및 처리 [%@]", returnAPN.deviceTokken);
            if ([returnAPN.deviceTokken isEqualToString:[[NSUserDefaults standardUserDefaults] objectForKey:kDeviceToken]]) {
            }else{
                [[NSUserDefaults standardUserDefaults] setObject:returnAPN.deviceTokken forKey:kDeviceToken];
                
            }
            
            //            [self.viewController setDeviceTokenField:returnAPN.deviceTokken];
            break;
            
        case WCPUSH_STATUS_SET_REGISTERSERVER: // 운영서버의 정보를 활당 성공 후 전달 되어진다.
            NSLog(@"서버등록 성공 ");
            break;
            
        case WCPUSH_STATUS_SET_UNREGISTERSERVER:
            NSLog(@"서버해지 성공 ");
            break;
    }
}

@end
