//
//  MyInfoManagerViewController.swift
//  GlobalExpress
//
//  Created by UDAM on 3/15/16.
//  Copyright © 2016 webcash. All rights reserved.
//

import UIKit

class MyInfoManagerViewController: UIViewController {

    //MARK: - IBOutlet
    // -------------------------------------------------------------------------------
    //	IBOutlet UIScrollView
    // -------------------------------------------------------------------------------
    @IBOutlet weak var scroller         : UIScrollView!
    
    // -------------------------------------------------------------------------------
    // IBOutlet UIView
    // -------------------------------------------------------------------------------
    @IBOutlet weak var centerView       : UIView!
    @IBOutlet weak var bottomView       : UIView!
    @IBOutlet weak var line2BottomView  : UIView!
    @IBOutlet weak var line1BottomView  : UIView!
    @IBOutlet weak var line6View        : UIView!
    
    // -------------------------------------------------------------------------------
    // IBOutlet UILabel
    // -------------------------------------------------------------------------------
    @IBOutlet weak var topLabel         : UILabel!
    @IBOutlet weak var nameKOLabel      : UILabel!
    @IBOutlet weak var nameENLabel      : UILabel!
    @IBOutlet weak var nameKOResult     : UILabel!
    @IBOutlet weak var nameENResult     : UILabel!
    @IBOutlet weak var dobLabel         : UILabel!
    @IBOutlet weak var dobResult        : UILabel!
    @IBOutlet weak var countryLabel     : UILabel!
    @IBOutlet weak var countryResult    : UILabel!
    @IBOutlet weak var phoneLabel       : UILabel!
    @IBOutlet weak var phoneResult      : UILabel!
    @IBOutlet weak var securityNoLabel  : UILabel!
    @IBOutlet weak var securityNoResult : UILabel!
    @IBOutlet weak var expireDateaLabel : UILabel!
    @IBOutlet weak var expireDateResult : UILabel!
    @IBOutlet weak var promoCodeLabel   : UILabel!
    @IBOutlet weak var promoCodeResult  : UILabel!
    @IBOutlet weak var debitBankLabel   : UILabel!
    @IBOutlet weak var debitBankResult  : UILabel!
    @IBOutlet weak var debitAccLabel    : UILabel!
    @IBOutlet weak var debitAccResult   : UILabel!
    @IBOutlet weak var freeCouponLabel  : UILabel!
    @IBOutlet weak var freeCouponResult : UILabel!
    @IBOutlet weak var bottomDescriptionLabel: UILabel!
    
    // -------------------------------------------------------------------------------
    // IBOutlet UIButton
    // -------------------------------------------------------------------------------
    @IBOutlet weak var editButton   : UIButton!
    @IBOutlet weak var editButtonTop: UIButton!
    
    // -------------------------------------------------------------------------------
    // IBOutlet NSLayoutConstraint
    // -------------------------------------------------------------------------------
    @IBOutlet weak var heightNameENResultConstraint     : NSLayoutConstraint!
    @IBOutlet weak var heightNameENLabelConstraint      : NSLayoutConstraint!
    @IBOutlet weak var heightCenterViewConstraint       : NSLayoutConstraint!
    @IBOutlet weak var heightBottomViewConstraint       : NSLayoutConstraint!
    @IBOutlet weak var heightFreeCouponLabelConstraint  : NSLayoutConstraint!
    @IBOutlet weak var heightFreeCouponResultConstraint : NSLayoutConstraint!
    @IBOutlet weak var widthPassNoResultConstraint      : NSLayoutConstraint!
    
    // -------------------------------------------------------------------------------
    // IBOutlet UIImageView
    // -------------------------------------------------------------------------------
    @IBOutlet weak var img6: UIImageView!
    @IBOutlet weak var img7: UIImageView!
    
    //MARK: - Properties
    // -------------------------------------------------------------------------------
    // Property: NSDicionary
    // -------------------------------------------------------------------------------
    var responseData    : NSDictionary!
    private var isKorean: Bool = false
    
    //MARK: - IBAction
    // -------------------------------------------------------------------------------
    //	IBOutlet Click to view EditInfoManagerViewController
    // -------------------------------------------------------------------------------
    @IBAction func editButtonClicked(sender: UIButton) {
        
        /////////// USE THIS FORMAT
        let transaction = TransactionClass()
        transaction.sendAPI(APICode.PROFILE_R002, argument: ["" : ""], success: { (success) -> Void in
            
            if let rslt_cd = success["RSLT_CD"] as? String {
                
                if rslt_cd != "0000" { //오류
                    if let rsltMsg = success["RSLT_MSG"] as? String {
                        
                        print("Error Code : \(rslt_cd), Error Message : \(rsltMsg)")
                        self.showEventAlert("Error Message :\(rsltMsg)", hasCancelButton: false, delegate: self, tag: nil, confirmBlock: nil, cancelBlock: nil)
                    }
                } else { //정상
                    if let respData = success["RESP_DATA"] as? NSDictionary {
                        
                        print("Sucess Code : \(rslt_cd),  Message Data : \(respData)")
                        self.performSegueWithIdentifier("ShowEditInfoManager", sender:respData)
                        
                    }
                    
                }
            }

        })
        
//        self.performSegueWithIdentifier("ShowEditInfoManager", sender:nil)
        
    }
    
    // -------------------------------------------------------------------------------
    //	IBOutlet Click to view PaymentPinDebitAccountViewController
    // -------------------------------------------------------------------------------
    @IBAction func editPinButtonClicked(sender: UIButton) {
        performSegueWithIdentifier("ShowPaymentDebitAccount", sender: nil)
    }
    
    // MARK: - Private function
    // -------------------------------------------------------------------------------
    // Localizing language
    // -------------------------------------------------------------------------------
    override func localize() {
        
        self.navigationItem.title   = Language.localizedStr("S12_01")
        topLabel.text               = Language.localizedStr("S12_02")
        editButton.setTitle(Language.localizedStr("S12_03"), forState: .Normal)
        editButtonTop.setTitle(Language.localizedStr("S12_03"), forState: .Normal)
        nameKOLabel.text            = Language.localizedStr("S12_04")
        nameENLabel.text            = Language.localizedStr("S12_04")
        dobLabel.text               = Language.localizedStr("S12_05")
        countryLabel.text           = Language.localizedStr("S12_06")
        phoneLabel.text             = Language.localizedStr("S12_07")
//        expireDateaLabel.text       = Language.localizedStr("")
        promoCodeLabel.text         = Language.localizedStr("S12_11")
        debitBankLabel.text         = Language.localizedStr("S12_12")
        debitAccLabel.text          = Language.localizedStr("S12_13")
        freeCouponLabel.text        = Language.localizedStr("S12_14")
        bottomDescriptionLabel.text = Language.localizedStr("S12_15")
        
        securityNoLabel.text        = Language.localizedStr("S12_09")
        
    }
    
    // -------------------------------------------------------------------------------
    // Customize View Layout
    // -------------------------------------------------------------------------------
    func customizeViewLayout(){
        
        self.navigationController?.navigationBarHidden = false
        
        //Customize navigation item ,barButton
        let backImage = UIImage(imageLiteral: "back_btn")
        let leftNavButton = UIButton(frame: CGRectMake(269.0, 20.0, backImage.size.width/2, backImage.size.height/2))
        leftNavButton.setBackgroundImage(backImage, forState: .Normal)
        leftNavButton.addTarget(self, action: "popUpViewController", forControlEvents:UIControlEvents.TouchUpInside)
        let barButton = UIBarButtonItem(customView: leftNavButton)
        self.navigationItem.leftBarButtonItem = barButton
        navigationItem.setHidesBackButton(true, animated: false)
        
    }
    
    // -------------------------------------------------------------------------------
    // setResultToView: Main function to set view which is communicated with server
    // -------------------------------------------------------------------------------
    func setResultToView() {
       
        if isKorean {
            
            //Set result KOREAN from Server
            securityNoLabel.text        = Language.localizedStr("S12_08")
            
            //Customize view coming from Server
            expireDateaLabel.hidden                 = true
            expireDateResult.hidden                 = true
            
            
            //Format PassportNo and Social SecurityNumber
            let securityNo  = self.responseData["RESIDENT_NO"]! as! NSString
            
            if securityNo.length < 6 {
                
                self.securityNoResult.text = "\(securityNo)-"
                
            }else{
                
                let result      = securityNo.substringWithRange(NSRange(location: 0, length: 6))
                
                img6.hidden = false
                img7.hidden = false
                widthPassNoResultConstraint.constant    = 95
                self.securityNoResult.text  = "\(result)-"
                
            }
            
        }
        else {
            
            //Set result FOREIGNER from Server
            securityNoLabel.text        = Language.localizedStr("S12_09")
            
            //Customize view coming from Server
            heightNameENLabelConstraint.constant    = 18 - 44
            heightNameENResultConstraint.constant   = 18 - 44
//          heightCenterViewConstraint.constant     = 311 - 44
//          line6View.hidden                        = true
            nameKOLabel.hidden                      = true
            nameKOResult.hidden                     = true
            
            //Format PassportNo and Social SecurityNumber
            let passportNo  = self.responseData["PASSPORT_NO"]! as! NSString
            
            if passportNo.length < 5 {
                
                self.securityNoResult.text = "\(passportNo)-"
                
            }
            else{
                
                let result1     = passportNo.substringWithRange(NSRange(location: 0, length: 5))
                img6.hidden = true
                img7.hidden = true
                widthPassNoResultConstraint.constant    = 95 - 21
                self.securityNoResult.text  = "\(result1)-"
                
            }
            
        }
        
        //Fill dateOfBirth Label
        let stringDate = self.responseData["BIRTH_YMD"] as! NSString
        
        let year    = stringDate.substringWithRange(NSRange(location: 0, length: 4))
        let month   = stringDate.substringWithRange(NSRange(location: 4, length: 2))
        let day     = stringDate.substringWithRange(NSRange(location: 6, length: 2))
        
        //Passport ExpireDate
        let stringDate1 = self.responseData["PASSPORT_EXPRDT"] as! NSString
        
        let year1   = stringDate1.substringWithRange(NSRange(location: 0, length: 4))
        let month1  = stringDate1.substringWithRange(NSRange(location: 4, length: 2))
        let day1    = stringDate1.substringWithRange(NSRange(location: 6, length: 2))
        
        self.nameKOResult.text      = "\(self.responseData["KOR_NM"]!)"
        self.nameENResult.text      = "\(self.responseData["CUST_NM"]!)"
        self.dobResult.text         = "\(year).\(month).\(day)"
        self.countryResult.text     = "\(self.responseData["NATION_NM"]!)"
        self.phoneResult.text       = "\(self.responseData["INTL_TELCD"]!)-\(self.responseData["HP_NO"]!)"
        self.expireDateResult.text  = "\(year1).\(month1).\(day1)"
        self.debitBankResult.text   = "\(self.responseData["TRBANK_NM"]!)"
        self.debitAccResult.text    = "\(self.responseData["TRACCT_NO"]!)"
        self.freeCouponResult.text  = "\(self.responseData["FREE_CNT"]!)EA"
        
        //Customize view coming from Server
        if responseData["FREE_CNT"] as! String != "0" &&
            responseData["TRBANK_NM"] as! String == "" &&
            responseData["TRACCT_NO"] as! String == "" {
                
                heightBottomViewConstraint.constant         = 134 - 87
                line2BottomView.hidden                      = true
                line1BottomView.hidden                      = true
                heightFreeCouponLabelConstraint.constant    = 18 - 88
                heightFreeCouponResultConstraint.constant   = 18 - 88
                debitBankLabel.hidden                       = true
                debitBankResult.hidden                      = true
                debitAccLabel.hidden                        = true
                debitAccResult.hidden                       = true
                
        }
        else if responseData["FREE_CNT"] as! String == "0" &&
            responseData["TRBANK_NM"] as! String != "" &&
            responseData["TRACCT_NO"] as! String != "" {
                
                heightBottomViewConstraint.constant = 134 - 43
                line2BottomView.hidden              = true
                
        }
        else if responseData["TRBANK_NM"] as! String == "" &&
            responseData["TRACCT_NO"] as! String == "" &&
            responseData["FREE_CNT"] as! String == "0" {
                
                heightBottomViewConstraint.constant = 0
                bottomView.hidden = true
                
        }
        
        // //Customize view coming from Server: Promotion Code
        if self.responseData["RCMD_ID"] as! String == "" {
            self.promoCodeResult.text   = "N\\A"
            
        }else{
            self.promoCodeResult.text   = "\(self.responseData["RCMD_ID"]!)"
            
        }
        
    }
    
    // -------------------------------------------------------------------------------
    // PopupViewController
    // -------------------------------------------------------------------------------
    func popUpViewController() {
        self.navigationController?.popToRootViewControllerAnimated(true)
    }
    
    //MARK: - View Life Cycle
    // -------------------------------------------------------------------------------
    // ViewDidLoad
    // -------------------------------------------------------------------------------
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Localize
        localize()
        
        //Customize
        self.customizeViewLayout()
        
    }
    
    // -------------------------------------------------------------------------------
    // viewWillAppear
    // -------------------------------------------------------------------------------
    override func viewWillAppear(animated: Bool) {
        
        //Check responseData nil
        if responseData == nil {
            return
        }

        //Set isKorean true/false
        (responseData!["NATION_GB"]! as! String != "0") ? (isKorean = false) : (isKorean = true)
        
        //SetResultToView
        setResultToView()

    }
    
    // MARK: - Navigation
    // -------------------------------------------------------------------------------
    // In a storyboard-based application, you will often want to do a little
    // preparation before navigation
    // -------------------------------------------------------------------------------
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if(segue.identifier == "ShowEditInfoManager"){
            
            if sender == nil {
                return
            }
            
            let editInfoManager = segue.destinationViewController as! EditInfoManagerViewController
            editInfoManager.passData = sender as? NSDictionary
        }
        
    }

    // -------------------------------------------------------------------------------
    // unwindToMainView
    // -------------------------------------------------------------------------------
//    @IBAction func unwindToMyInfoManager(storyboard: UIStoryboardSegue ) {
//        self.navigationController?.navigationBarHidden=false
//        navigationItem.setHidesBackButton(false, animated: false)
//    }

}
