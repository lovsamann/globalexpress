//
//  main.m
//  GlobalExpress
//
//  Created by JangWooil on 2015. 12. 29..
//  Copyright © 2015년 webcash. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
